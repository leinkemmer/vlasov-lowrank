#include <iostream>
#include <fstream>
#include <sstream>
#include <random>
#include <limits>

#include <generic/timer.hpp>
#include <algorithms/lowrank-continuoussplit-1x1v.hpp>
#include <algorithms/lowrank-continuoussplit-h2x2v.hpp>
#include <boost/program_options.hpp>
using namespace boost::program_options;

cons_mode parse_cons(string cons) {
    if(cons == "off")
        return cons_mode::off;
    else if(cons == "rank1")
        return cons_mode::rank1;
    else if(cons == "lc_local")
        return cons_mode::lc_local;
    else if(cons == "lc_global")
        return cons_mode::lc_global;
    else if(cons == "lc_both")
        return cons_mode::lc_both;
    else if(cons == "f_local")
        return cons_mode::f_local;
    else {
        cout << "ERROR: conservative method " << cons << " is not supported" << endl;
        exit(1);
    }
}

enum class adaptive_mode {
    off, standard, random
};

adaptive_mode parse_adaptive_mode(string str) {
    if(str == "off")
        return adaptive_mode::off;
    if(str == "standard")
        return adaptive_mode::standard;
    else if(str == "random")
        return adaptive_mode::random;
    else {
        cout << "ERROR: adaptive mode " << str << " could not be parsed" << endl;
        exit(1);
    }
}

enum class adaptive_goal {
    f, ee, E
};

adaptive_goal parse_adaptive_goal(string str) {
    if(str == "f")
        return adaptive_goal::f;
    else if(str == "ee")
        return adaptive_goal::ee;
    else if(str == "E")
        return adaptive_goal::E;
    else {
        cout << "ERROR: adaptive goal " << str << " could not be parsed" << endl;
        exit(1);
    }
}


domain_lowrank2d enlarge_rank(domain_lowrank2d& in, int new_r) {
    int r = in.lr.S.rows();
    array<int,2> ext = {in.lr.X.rows(), in.lr.V.rows()};

    domain_lowrank2d nd(in.a, in.b, new_r, ext, lowrank_type::normalized_integral);
    nd.lr.S.setZero(); nd.lr.X.setZero(); nd.lr.V.setZero();
    nd.lr.S.block(0,0,r,r) = in.lr.S;
    nd.lr.X.block(0,0,ext[0],r) = in.lr.X;
    nd.lr.V.block(0,0,ext[1],r) = in.lr.V;
    return nd;
}

domain_lowrank2d reduce_rank(domain_lowrank2d& in, int new_r) {
    array<int,2> ext = {in.lr.X.rows(), in.lr.V.rows()};

    domain_lowrank2d nd(in.a, in.b, new_r, ext, lowrank_type::normalized_integral);
    nd.lr.S = in.lr.S.block(0,0,new_r,new_r);
    nd.lr.X = in.lr.X.block(0,0,ext[0],new_r);
    nd.lr.V = in.lr.V.block(0,0,ext[1],new_r);
    return nd;
}

void rank_adaptivity_set_random(int r, domain_lowrank2d& in, int num_random) {

        in.diagonalize_S();

        // set random function
        array<int,2> ext = {in.lr.X.rows(), in.lr.V.rows()};
        std::default_random_engine generator;
        std::uniform_real_distribution<double> distribution(0.0,1.0);
        for(int k=0;k<num_random;k++) {
            for(int i=0;i<ext[0];i++)
                in.lr.X(i,r-1-k) = distribution(generator);
            for(int j=0;j<ext[1];j++)
                in.lr.V(j,r-1-k) = distribution(generator);

            in.lr.S(r-1-k,r-1-k) = 0.0;
        }

        // orthogonalize
        in.lr.orthogonalize(in.h[0], in.h[1]);
}

double chopped_electric_field(vlasov_continuoussplit& vcs, domain_lowrank2d& out, int num_removed, VectorXd& E) {
    int r = vcs.r;

    domain_lowrank2d nd = reduce_rank(out, r-num_removed);
    vcs.reset_rank(r-num_removed);
    double ee = vcs.electric_field_energy(nd.lr, E);
    vcs.reset_rank(r);
    return ee;
}


bool rank_adaptivity(adaptive_mode am, adaptive_goal ag, array<double,2> adaptive_tol, int addranks, 
        int& r, domain_lowrank2d& in, domain_lowrank2d& out, vlasov_continuoussplit& vcs, double tau) {
    double safety = 0.1;
    int min_rank  = 3+addranks;

    double factor = 1.0;
    if(am == adaptive_mode::random || ag == adaptive_goal::ee || ag == adaptive_goal::E)
        factor = tau;
    
    if(r<=addranks+1) {
        cout << "ERROR: rank adaptivity can not be used if rank<=addranks" << endl;
        exit(1);
    }

    // compute electric energy for the the entire set
    array<int,2> ext = {in.lr.X.rows(), in.lr.V.rows()};
    VectorXd E_all(ext[0]), E_inc(ext[0]), E_dec(ext[0]);
    double ee_all = chopped_electric_field(vcs, out, 0, E_all);
    double ee_inc = chopped_electric_field(vcs, out, addranks, E_inc);
    double ee_dec = chopped_electric_field(vcs, out, addranks+1, E_dec);

    double err_enlarge, err_reduce;
    if(ag == adaptive_goal::f) {
        VectorXd sv = out.singular_values();
        err_enlarge = abs(sv[sv.size()-addranks])/adaptive_tol[0];
        err_reduce  = abs(sv[sv.size()-addranks-1])/adaptive_tol[0];
    } else if(ag == adaptive_goal::ee) {
        double err_inc = abs(ee_all-ee_inc);
        err_enlarge = std::min(err_inc/adaptive_tol[0], err_inc/abs(ee_all)/adaptive_tol[1]);

        double err_dec = abs(ee_all-ee_dec);
        err_reduce = std::min(err_dec/adaptive_tol[0], err_dec/abs(ee_all)/adaptive_tol[1]);
        cout << "RANK error ee: " << err_inc << " " << err_enlarge << " " << err_dec << " " << err_reduce << endl;
    } else /*if(ag == adaptive_goal::E)*/ {
        double E_norm = E_all.lpNorm<Eigen::Infinity>();
        err_enlarge=0.0;
        err_reduce=0.0;
        for(int i=0;i<E_all.size();i++) {
            double err_linc = abs(E_all[i]-E_inc[i]);
            err_enlarge = std::max(err_enlarge,
                    std::min(err_linc/adaptive_tol[0], err_linc/E_norm/adaptive_tol[1]));

            double err_ldec = abs(E_all[i]-E_dec[i]);
            err_reduce = std::max(err_reduce,
                    std::min(err_ldec/adaptive_tol[0], err_ldec/E_norm/adaptive_tol[1]));
        }

        cout << "RANK error E: " << err_enlarge << " " << err_reduce << endl;
    }

    // rank adaptivity
    VectorXd sv = out.singular_values();
    if(err_enlarge > factor*1.0) {
        int new_r = r+1;
        domain_lowrank2d nd = enlarge_rank(in, new_r);

        // set random function
        std::default_random_engine generator;
        std::uniform_real_distribution<double> distribution(0.0,1.0);
        for(int i=0;i<ext[0];i++)
            nd.lr.X(i,r) = distribution(generator);
        for(int j=0;j<ext[1];j++)
            nd.lr.V(j,r) = distribution(generator);

        // orthogonalize
        nd.lr.orthogonalize(nd.h[0], nd.h[1]);

        cout << "RANK " << r << " not sufficient. Enlarging..." << endl;
        cout << "RANK defect: " << (nd.full()-in.full()).norm() << endl;

        // set in and out to the enlarged version
        r = new_r;
        vcs.reset_rank(new_r);
        out = nd;
        in  = nd;
        return false; // reject the step
    } else if(r>min_rank && err_reduce < factor*safety*1.0) {
        int new_r = r-1;

        // reduce the size
        domain_lowrank2d nd = reduce_rank(out, new_r);

        cout << "RANK " << r << " too large. Reducing..." << endl;
        cout << "RANK defect: " << (nd.full()-out.full()).norm() << endl;

        // set in and out to the enlarged version
        r = new_r;
        vcs.reset_rank(new_r);
        out = nd;
        in  = nd;
        return true; // accept the step
    } else
        return true; // accept the step
}

void run(int r, array<double,2> a, array<double,2> b, array<int,2> ext,
        double T, double tau, bool enable_output, domain_lowrank2d in, string method, string cons_mass, string cons_mom, double lc_both_weight, string str_am, string str_ag, array<double,2> adaptive_tol, int addranks,
        int snapshots) {
    domain_lowrank2d out(a, b, r, ext, lowrank_type::normalized_integral);
    out = in; // needed for output

    VectorXd E(ext[0]);
    adaptive_mode am = parse_adaptive_mode(str_am);
    adaptive_goal ag = parse_adaptive_goal(str_ag);

    vlasov_continuoussplit vcs(r, ext, a, b);
    vcs.cons_mass = parse_cons(cons_mass);
    vcs.cons_mom = parse_cons(cons_mom);
    vcs.lc_both_weight = lc_both_weight;

    int nsteps = ceil(T/tau);
    double t = 0.0;
    ofstream fs("evolution.data");
    fs << "# t mass momentum energy kinetic_energy electric_energy l2 entropy l1 rank rejected_steps" << endl;
    fs.precision(16);

    int rejected_steps = 0;
    for(int k=0;k<nsteps+1;k++) {
        if(tau > T-t)
            tau = T-t;

        if(enable_output && (k % int(ceil(nsteps/double(snapshots-1))) == 0 || k==nsteps)) {
            std::stringstream ss;
            ss << "f-t" << t << ".nc";
            NCWriter ncw(ss.str());
            // write f
            out.write_nc(ncw, "f");
            // write E
            vcs.electric_field_energy(out.lr, E);
            ncw.write("E", {E.size()}, E.data(), {"x"});
        }

        if(k != nsteps) { // last step only for output

            while(true) {
                if(am == adaptive_mode::random)
                    rank_adaptivity_set_random(r, in, addranks);

                if(method == "lie")
                    vcs.Lie_step(tau, in.lr, out.lr);
                else if(method == "strang")
                    vcs.Strang_step(tau, in.lr, out.lr);
                else {
                    cout << "ERROR: method must be either lie or strang" << endl;
                    exit(1);
                }

                if(am == adaptive_mode::off ||
                        rank_adaptivity(am, ag, adaptive_tol, addranks, r, in, out, vcs, tau))
                    break;
                else
                    rejected_steps++;
            }
            // we do not use the random vectors for the output that follows
            if(am == adaptive_mode::random) {
                out.diagonalize_S();
                for(int i=1;i<=addranks;i++)
                    out.lr.S(r-i,r-i) = 0.0;
            }

            in.lr = out.lr;

            t += tau;
            double ee = vcs.electric_field_energy(out.lr, E);
            double ke = out.kin_energy();
            cout << "t=" << t << "\tmass=" << out.mass() << "\tenergy=" << ee+ke << endl;
            fs << t << "\t" << out.mass() << "\t" << out.momentum() << "\t" << ee+ke << "\t" << ke << "\t" << ee << "\t" << out.l2() << "\t" << out.entropy() << "\t" << out.l1() << "\t" << r << "\t" << rejected_steps << endl;
        }
    }
    fs.close();

/*
    if(enable_output) {
        std::stringstream ss;
        ss << "f-t" << t << ".nc";
        NCWriter ncw(ss.str());
        out.write_nc(ncw, "f");
    }
*/
    cout << "rejected steps: " << rejected_steps << endl;
}

void run_plasma_echo(int r, array<double,2> a, array<double,2> b, array<int,2> ext,
        double T, double tau, bool enable_output, domain_lowrank2d in, string method,
        double t2, double k2, string str_am, string str_ag, array<double,2> adaptive_tol, int addranks,
        int snapshots) {

    domain_lowrank2d out(a, b, r, ext, lowrank_type::normalized_integral);
    out = in; // needed for output

    VectorXd E(ext[0]);
    adaptive_mode am = parse_adaptive_mode(str_am);
    adaptive_goal ag = parse_adaptive_goal(str_ag);

    vlasov_continuoussplit vcs(r, ext, a, b);

    int nsteps = ceil(T/tau);
    double t = 0.0;
    ofstream fs("evolution.data");
    fs << "# t mass momentum energy kinetic_energy electric_energy l2 entropy l1 rank" << endl;
    fs.precision(16);

    int rejected_steps = 0;
    for(int k=0;k<nsteps+1;k++) {
        if(tau > T-t)
            tau = T-t;

        if(enable_output && (k % int(ceil(nsteps/double(snapshots-1))) == 0 || k==nsteps)) {
            std::stringstream ss;
            ss << "f-t" << t << ".nc";
            NCWriter ncw(ss.str());
            // write f
            out.write_nc(ncw, "f");
            // write E
            vcs.electric_field_energy(out.lr, E);
            ncw.write("E", {E.size()}, E.data(), {"x"});
        }

        if(k != nsteps) { // last step only for output
            while(true) {
                
                if(am == adaptive_mode::random)
                    rank_adaptivity_set_random(r, in, addranks);

                if(method == "lie")
                    vcs.Lie_step(tau, in.lr, out.lr);
                else if(method == "strang")
                    vcs.Strang_step(tau, in.lr, out.lr);
                else {
                    cout << "ERROR: method must be either lie or strang" << endl;
                    exit(1);
                }
                if(am == adaptive_mode::off || 
                        rank_adaptivity(am, ag, adaptive_tol, addranks, r, in, out, vcs, tau))
                    break;
                else
                    rejected_steps++;
            }
            // we do not use the random vectors for the output that follows
            if(am == adaptive_mode::random) {
                out.diagonalize_S();
                for(int i=1;i<=addranks;i++)
                    out.lr.S(r-i,r-i) = 0.0;
            }

            in.lr = out.lr;

            t += tau;
            double ee = vcs.electric_field_energy(out.lr, E);
            double ke = out.kin_energy();
            cout << "t=" << t << "\tmass=" << out.mass() << "\tenergy=" << ee+ke << endl;
            fs << t << "\t" << out.mass() << "\t" << out.momentum() << "\t" << ee+ke << "\t" << ke << "\t" << ee << "\t" << out.l2() << "\t" << out.entropy() << "\t" << out.l1() << "\t" << r << endl;


            // add an additional perturbation with wavenumber k2 at time t2
            if(t >= t2 && t<t2+tau) {
                domain_lowrank2d f2(a, b, r, ext, lowrank_type::normalized_integral);
                f2.init([k2](double x) { return 0.001*cos(k2*x); },
                        [](double v)   { return exp(-0.5*v*v)/sqrt(2.0*M_PI); }
                       );

                in.add_to(f2);
            }
        }
    }
    fs.close();

    cout << "rejected steps: " << rejected_steps << endl;
}

void run(double T, double tau, bool enable_output, domain_lowrank4d in, string method) {
    domain_lowrank4d out = in;

    continuoussplit_4dh vcs(in.ddX, in.ddV);
    int nsteps = ceil(T/tau);
    double t = 0.0;
    ofstream fs("evolution.data");
    fs << "# t mass momentum_v momentum_w energy kinetic_energy electric_energy l2 entropy l1" << endl;
    fs.precision(16);

    timer t_stepper, t_output;
    timer t_total;
    t_total.start();
    double l1, entropy, l2;
    for(int k=0;k<nsteps;k++) {
        if(tau > T-t)
            tau = T-t;

        t_stepper.start();
        if(method == "lie")
            vcs.Lie_step(tau, in.lr, out.lr);
        else if(method == "strang")
            vcs.Strang_step(tau, in.lr, out.lr);
        else {
            cout << "ERROR: method must be lie" << endl;
            exit(1);
        }
        in = out;
        t_stepper.stop();

        t += tau;

        t_output.start();
        double ee = vcs.electric_field_energy(out.lr);
        double ke = out.kin_energy();
        /*
        if(k % 20 == 0) {
            Tensor4 df = out.full();
            l2 = out.l2(df);
            entropy = out.entropy(df);
            l1 = out.l1(df);
        }*/

        cout << "t=" << t << "\tmass=" << out.mass() << "\tenergy=" << ee+ke << endl;
        fs << t << "\t" << out.mass() << "\t" << out.momentum(0) << "\t" << out.momentum(1) << "\t" << ee+ke << "\t" << ke << "\t" << ee << "\t" << l2 << "\t" << entropy << "\t" << l1 << endl;
        t_output.stop();
    }
    fs.close();
    t_total.stop();

    cout << "Total time: " << t_total.total() << "\tstepper: " << t_stepper.total() << "\toutput: " << t_output.total() << endl;
}

template<size_t d>
array<int,d> parse_rank(string rank) {
    array<int,d> ret;
    std::istringstream iss(rank);
    int r;
    int i=0;
    while(iss >> r)
        ret[i++] = r;

    if(i != d) {
        cout << "ERROR: could not parse rank" << endl;
        exit(1);
    }
    return ret;
}

int main(int argc, char* argv[]) {
    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string problem, method, r_str, cons_mass, cons_mom, str_am, str_ag;
    int nx, nv, adaptive_addranks, number_snapshots;
    double T, tau, lc_both_weight;
    array<double,2> adaptive_tol;
    bool enable_output;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("enable_output",value<bool>(&enable_output)->default_value(true),
         "enable or disable writing to disk")
        ("nx",value<int>(&nx)->default_value(128),
         "number of cells used in the space directions")
        ("nv",value<int>(&nv)->default_value(128),
         "number of cells used in the velocity directions")
        ("rank,r",value<string>(&r_str)->default_value("10"),
         "rank of the approximation")
        ("final_time,T",value<double>(&T)->default_value(100.0),
         "final time of the simulation")
        ("step_size",value<double>(&tau)->default_value(0.025),
         "time step size used in the simulation")
        ("problem",value<string>(&problem)->default_value("ll"),
         "initial value for the problem that is to be solved")
        ("method,m",value<string>(&method)->default_value("lie"),
         "splitting method used")
        ("cons_mass",value<string>(&cons_mass)->default_value("off"),
         "off (standard) or rank1, lc_local, lc_global, lc_both.")
        ("cons_mom",value<string>(&cons_mom)->default_value("off"),
         "off (standard) or rank1, lc_local, lc_global, lc_both.")
        ("lc_both_weight",value<double>(&lc_both_weight)->default_value(1.0),
         "how strongly local vs global conservation is weight")
        ("adaptive_mode",value<string>(&str_am)->default_value("off"),
         "mode for adaptivity (off, standard, random)")
        ("adaptive_goal",value<string>(&str_ag)->default_value("f"),
         "goal for adaptivity (f, ee, E)")
        ("adaptive_abs_tol",value<double>(&adaptive_tol[0])->default_value(1e-5),
         "absolute tolerance for adaptive rank selection")
        ("adaptive_rel_tol",value<double>(&adaptive_tol[1])->default_value(1e-5),
         "relative tolerance for adaptive rank selection")
        ("adaptive_addranks",value<int>(&adaptive_addranks)->default_value(2),
         "additional ranks that are used for the adaptivity")
        ("snapshots",value<int>(&number_snapshots)->default_value(2),
         "number of snapshots that are written to disk (including initial and final time)");

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);

    array<int,2> ext = {nx, nv};
    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {
        // Linear or weak Landau damping
        if(problem== "ll") {
            array<double,2> a = {0.0,-6.0};
            array<double,2> b = {4*M_PI,6.0};
            int r = parse_rank<1>(r_str)[0];
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral);
            in.init([](double x) { return 1.0+0.01*cos(0.5*x); },
                    [](double v) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); }
                   );
            run(r, a, b, ext, T, tau, enable_output, in, method, cons_mass, cons_mom, lc_both_weight, str_am,
                    str_ag, adaptive_tol, adaptive_addranks, number_snapshots);
        } else if(problem== "nl") {
            array<double,2> a = {0.0,-6.0};
            array<double,2> b = {4*M_PI,6.0};
            int r = parse_rank<1>(r_str)[0];
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral);
            in.init([](double x) { return 1.0+0.5*cos(0.5*x); },
                    [](double v) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); }
                   );
            run(r, a, b, ext, T, tau, enable_output, in, method, cons_mass, cons_mom, lc_both_weight, str_am,
                    str_ag, adaptive_tol, adaptive_addranks, number_snapshots);
        }
        else if(problem == "ts") {
            array<double,2> a = {0.0,-9.0};
            array<double,2> b = {10*M_PI,9.0};
            int r = parse_rank<1>(r_str)[0];
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral);
            in.init(2,
                    [](double x, int k) {
                        return 0.5*(1.0+0.001*cos(0.2*x));
                    },
                    [](double v, int k) {
                        if(k==0)      return exp(-0.5*pow(v-2.4,2))/sqrt(2.0*M_PI);
                        else if(k==1) return exp(-0.5*pow(v+2.4,2))/sqrt(2.0*M_PI);
                        else  exit(1);
                    }
                   );
            run(r, a, b, ext, T, tau, enable_output, in, method, cons_mass, cons_mom, lc_both_weight, str_am, 
                    str_ag, adaptive_tol, adaptive_addranks, number_snapshots);
        } else if(problem == "bot") {
            array<double,2> a = {0.0,-9.0};
            array<double,2> b = {20*M_PI,9.0};
            int r = parse_rank<1>(r_str)[0];
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral);

            in.init(2,
                    [](double x, int k) {
                        if(k==0)      return 1.0;
                        else if(k==1) return 1.0+0.03*cos(0.3*x);
                        else exit(1);
                    },
                    [](double v, int k) {
                        if(k==0)      return 0.9*exp(-0.5*pow(v,2))/sqrt(2.0*M_PI);
                        else if(k==1) return 0.2*exp(-2.0*pow(v-4.5,2))/sqrt(2.0*M_PI);
                        else  exit(1);
                    }
                   );

            run(r, a, b, ext, T, tau, enable_output, in, method, cons_mass, cons_mom, lc_both_weight, str_am,
                    str_ag, adaptive_tol, adaptive_addranks, number_snapshots);
        } else if(problem == "echo") {
            array<double,2> a = {0.0,-8.0};
            array<double,2> b = {100.0,8.0};
            int r = parse_rank<1>(r_str)[0];

            double k1 = 12.0*M_PI/100.0;
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral);
            in.init([k1](double x) { return 1.0+0.001*cos(k1*x); },
                    [](double v)   { return exp(-0.5*v*v)/sqrt(2.0*M_PI); }
                   );
            
            double k2 = 24.0*M_PI/100.0;
            double t2 = 200.0;
            run_plasma_echo(r, a, b, ext, T, tau, enable_output, in, method, t2, k2, str_am,
                    str_ag, adaptive_tol, adaptive_addranks, number_snapshots);

        } else if(problem == "ll4dh") {
            array<int,3> r = parse_rank<3>(r_str);
            domain2d_data ddX({0.0,0.0},{4*M_PI,4*M_PI},{ext[0],ext[0]},r);
            domain2d_data ddV({-6.0,-6.0},{6.0,6.0},{ext[1],ext[1]},r);
            domain_lowrank4d in(ddX,ddV);

            in.init(1, [](double x, int r) { return 1.0+0.01*cos(0.5*x); },
                       [](double y, int r) { return 1.0; },
                    1, [](double v, int r) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); },
                       [](double w, int r) { return exp(-0.5*w*w)/sqrt(2.0*M_PI); });

            run(T, tau, enable_output, in, method);
        } else if(problem == "ll4dh_notaligned") {
            array<int,3> r = parse_rank<3>(r_str);
            domain2d_data ddX({0.0,0.0},{5*M_PI,5*M_PI},{ext[0],ext[0]},r);
            domain2d_data ddV({-6.0,-6.0},{6.0,6.0},{ext[1],ext[1]},r);
            domain_lowrank4d in(ddX,ddV);

            in.init(2, [](double x, int r) {
                           if(r==1)      return 0.01*cos(0.4*x);
                           else          return 1.0;
                       },
                       [](double y, int r) {
                           if(r==1)      return cos(0.4*y);
                           else          return 1.0;
                       },
                    1, [](double v, int r) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); },
                       [](double w, int r) { return exp(-0.5*w*w)/sqrt(2.0*M_PI); });

            run(T, tau, enable_output, in, method);
        } else if(problem == "ll4dh2") {
            array<int,3> r = parse_rank<3>(r_str);
            domain2d_data ddX({0.0,0.0},{4*M_PI,4*M_PI},{ext[0],ext[0]},r);
            domain2d_data ddV({-6.0,-6.0},{6.0,6.0},{ext[1],ext[1]},r);
            domain_lowrank4d in(ddX,ddV);

            in.init(3, [](double x, int r) {
                           if(r==1)  return 0.01*cos(0.5*x);
                           else      return 1.0;
                       },
                       [](double y, int r) {
                           if(r==2)  return 0.01*cos(0.5*y);
                           else      return 1.0;
                       },
                    1, [](double v, int r) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); },
                       [](double w, int r) { return exp(-0.5*w*w)/sqrt(2.0*M_PI); });

            run(T, tau, enable_output, in, method);
        } else if(problem == "ts4dh") {
            array<int,3> r = parse_rank<3>(r_str);
            domain2d_data ddX({0.0,0.0},{10*M_PI,10*M_PI},{ext[0],ext[0]},r);
            domain2d_data ddV({-9.0,-9.0},{9.0,9.0},{ext[1],ext[1]},r);
            domain_lowrank4d in(ddX,ddV);

            in.init(1, [](double x, int r) { return 0.5*(1.0 + 0.001*cos(0.2*x)); },
                       [](double y, int r) { return 1.0; },
                    1, [](double v, int r) { return exp(-0.5*pow(v-2.4,2))/sqrt(2.0*M_PI) + exp(-0.5*pow(v+2.4,2))/sqrt(2.0*M_PI);
                       },
                       [](double w, int r) { return exp(-0.5*pow(w,2))/sqrt(2.0*M_PI); });

            run(T, tau, enable_output, in, method);
        } else if(problem == "ts4dh2") {
            array<int,3> r = parse_rank<3>(r_str);
            domain2d_data ddX({0.0,0.0},{10*M_PI,10*M_PI},{ext[0],ext[0]},r);
            domain2d_data ddV({-9.0,-9.0},{9.0,9.0},{ext[1],ext[1]},r);
            domain_lowrank4d in(ddX,ddV);

            in.init(2, [](double x, int r) {
                           if(r==0)      return 0.25*(1.0 + 0.001*cos(0.2*x));
                           else if(r==1) return 1.0;
                           else          exit(1);
                       },
                       [](double y, int r) {
                           if(r==0)      return 1.0;
                           else if(r==1) return 0.25*0.001*cos(0.2*y);
                           else          exit(1); 
                       },
                    1, [](double v, int r) { return (exp(-0.5*pow(v-2.4,2)) + exp(-0.5*pow(v+2.4,2)))/sqrt(2.0*M_PI);
                       },
                       [](double w, int r) { return (exp(-0.5*pow(w-2.4,2)) + exp(-0.5*pow(w+2.4,2)))/sqrt(2.0*M_PI); });

            run(T, tau, enable_output, in, method);
        } else if(problem == "nl4dh") {
            array<int,3> r = parse_rank<3>(r_str);
            domain2d_data ddX({0.0,0.0},{4*M_PI,4*M_PI},{ext[0],ext[0]},r);
            domain2d_data ddV({-6.0,-6.0},{6.0,6.0},{ext[1],ext[1]},r);
            domain_lowrank4d in(ddX,ddV);

            in.init(2, [](double x, int r) {
                           if(r==0)  return 1.0;
                           else      return 0.5*cos(0.5*x);
                       },
                       [](double y, int r) {
                           if(r==0)  return 1.0;
                           else      return cos(0.5*y);
                       },
                    1, [](double v, int r) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); },
                       [](double w, int r) { return exp(-0.5*w*w)/sqrt(2.0*M_PI); });

            run(T, tau, enable_output, in, method);
        }
        else {
            cout << "ERROR: problem " << problem << " is not supported (either ll, nl, ts, ll4dh, ll4dh2, ts4dh, ts4dh2, or nl4dh has to be specified)" << endl;
            exit(1);
        }
    }

    exit(0);
}


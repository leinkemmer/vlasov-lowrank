#include <iostream>
#include <fstream>
#include <sstream>

#include <generic/timer.hpp>
#include <algorithms/lowrank-continuoussplit-fluid2d.hpp>

#include <boost/program_options.hpp>
#include <boost/format.hpp>
using namespace boost::program_options;

void run(double T, double tau, bool enable_output, domain_lowrank4d_fluid in, string method, double epsilon, int snapshots, diff_mode dm, solve_mode sm) {
    domain_lowrank4d_fluid out(in.dd, lowrank_type::normalized_integral);
    array<double,4> inv0 = in.invariants();

    vlasov_continuoussplit_fluid4d vcs(in.dd, epsilon, dm, sm);
    int nsteps = ceil(T/tau);
    double t = 0.0;
    ofstream fs("evolution.data");
    fs << "# t mass momentum_x momentum_y kinetic_energy" << endl;
    fs.precision(16);

    out.lr = in.lr; // that the first output works
    for(int k=0;k<nsteps+1;k++) {
        // output
        array<double,4> inv = in.invariants();
        cout << "t=" << t << "  mass err: " << inv0[0]-inv[0] << "  mom_x err: " << inv0[1]-inv[1] << "  mom_y err: " << inv0[2]-inv[2] << "  energy err: " << inv0[3]-inv[3] << endl;
        fs << t << "\t" << inv0[0]-inv[0] << "\t" << inv0[1]-inv[1] << "\t" << inv0[2]-inv[2] << "\t" << inv0[3]-inv[3] << endl;
        if(k % int(ceil(nsteps/double(snapshots-1))) == 0 || k==nsteps) {
            if(enable_output) {
                NCWriter ncw(str(boost::format("rhou-t%1%.nc")%t));
                out.write_nc(ncw);
            }
        }

        if(k < nsteps+1) { // last step is only for output
            if(tau > T-t)
                tau = T-t;

            // cvode complains if we take too small time steps
            if(tau > 1e-12) {
                if(method == "lie")
                    vcs.Lie_step(tau, in.lr, out.lr);
                else if(method == "strang")
                    vcs.Strang_step(tau, in.lr, out.lr);
                else {
                    cout << "ERROR: method must be either lie or strang" << endl;
                    exit(1);
                }
                in.lr = out.lr;
            }

            t += tau;
        }

    }
}

int main(int argc, char* argv[]) {
    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string problem, method, r_str, spaced, solver;
    int nx, nv, number_snapshots;
    double T, tau, reynolds, lambda, v0, delta;
    bool enable_output;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("enable_output",value<bool>(&enable_output)->default_value(true),
         "enable or disable writing to disk")
        ("snapshots",value<int>(&number_snapshots)->default_value(2),
         "number of snapshots that are written to disk (including initial and final time)")
        ("nx",value<int>(&nx)->default_value(32),
         "number of cells used in the space directions")
        ("nv",value<int>(&nv)->default_value(32),
         "number of cells used in the velocity directions")
        ("rank,r",value<string>(&r_str)->default_value("10"),
         "rank of the approximation")
        ("final_time,T",value<double>(&T)->default_value(1.0),
         "final time of the simulation")
        ("step_size",value<double>(&tau)->default_value(0.1),
         "time step size used in the simulation")
        ("problem",value<string>(&problem)->default_value("shear"),
         "initial value for the problem that is to be solved")
        ("method,m",value<string>(&method)->default_value("lie"),
         "splitting method used")
        ("reynolds",value<double>(&reynolds)->default_value(100.0),
         "Reynolds number")
        ("lambda",value<double>(&lambda)->default_value(1.0/30.0),
         "length scale for the initial value")
        ("v0",value<double>(&v0)->default_value(0.1),
         "initial velocity")
        ("delta",value<double>(&delta)->default_value(0.005),
         "amplitude")
        ("spaced",value<string>(&spaced)->default_value("fft"),
         "space discretization used (either fft, cd, or upwind)")
        ("solver",value<string>(&solver)->default_value("cvode"),
         "solver used (either cvode or explicit)");

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);
   
    diff_mode dm;
    if(spaced == "fft")
        dm = DM_FFT;
    else if(spaced == "cd")
        dm = DM_CD;
    else if(spaced == "upwind")
        dm = DM_UPWIND;
    else {
        cout << "ERROR: spaced must be either fft, cd, or upwind" << endl;
        exit(1);
    }

    solve_mode sm;
    if(solver == "cvode")
        sm = SM_CVODE;
    else if(solver == "explicit")
        sm = SM_EXPLICIT;
    else {
        cout << "ERROR: solver must be either cvode or explicit" << endl;
        exit(1);
    }

    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {
        // Linear or weak Landau damping
        if(problem== "shear") {
            domain4d_data dd({0.0,0.0,-6.0,-6.0}, {1.0, 1.0, 6.0, 6.0}, {nx,nx,nv,nv}, atoi(r_str.c_str()));
            domain_lowrank4d_fluid in(dd, lowrank_type::normalized_integral);
            double epsilon = v0/reynolds;
            cout << "epsilon: " << epsilon << endl;
            double rho = 1.0;
            in.init(6,
                    [rho,v0,lambda,delta](double x, double y, int r) {
                        double u1 = v0*( (y<=0.5) ? tanh((y-0.25)/lambda) : tanh((0.75-y)/lambda) );
                        double u2 = delta*sin(2*M_PI*x);

                        if(r==0)        return rho*(1-0.5*(u1*u1+u2*u2));
                        else if(r==1)   return rho*u1;
                        else if(r==2)   return rho*u2;
                        else if(r==3)   return rho*pow(u1,2);
                        else if(r==4)   return rho*pow(u2,2);
                        else if(r==5)   return rho*u1*u2;
                        cout << "ERROR: should not happen" << endl;
                        exit(1);
                    },
                    [](double v, double w, int r) {
                        double expo = exp(-0.5*pow(v,2)-0.5*pow(w,2))/(2.0*M_PI);

                        if(r==0)        return expo;
                        else if(r==1)   return expo*v;
                        else if(r==2)   return expo*w;
                        else if(r==3)   return expo*0.5*pow(v,2);
                        else if(r==4)   return expo*0.5*pow(w,2);
                        else if(r==5)   return expo*v*w;
                        cout << "ERROR: should not happen" << endl;
                        exit(1);
                    });

            run(T, tau, enable_output, in, method, epsilon, number_snapshots, dm, sm);
        } else if(problem == "wave") {
            domain4d_data dd({0.0,0.0,-6.0,-6.0}, {1.0, 1.0, 6.0, 6.0}, {nx,nx,nv,nv}, atoi(r_str.c_str()));
            domain_lowrank4d_fluid in(dd, lowrank_type::normalized_integral);
            double epsilon = 1.0/reynolds;
            cout << "epsilon: " << epsilon << endl;

            // this is a plane wave solution
            // Example: ./vlasov-lowrank-fluid --nx 64 --nv 64 -T 1 --step_size 0.2 --problem wave --method lie --reynolds 1000 --v0 1e-2 --snapshots 21 --lambda 0.01 --spaced fft
            // For reynolds>5000 oscillations become a problem.
            //

            in.init(3,
                    [delta](double x, double y, int r) {
                        double u1 = 0.0;
                        double u2 = delta*sin(2*M_PI*y);
                        double rho = 1.0 + delta*sin(2*M_PI*y);

                        if(r==0)        return rho*(1.0-0.5*(u1*u1+u2*u2));
                        else if(r==1)   return rho*u2;
                        else if(r==2)   return rho*pow(u2,2);
                        cout << "ERROR: should not happen" << endl;
                        exit(1);
                    },
                    [](double v, double w, int r) {
                        double expo = exp(-0.5*pow(v,2)-0.5*pow(w,2))/(2.0*M_PI);

                        if(r==0)        return expo;
                        else if(r==1)   return expo*w;
                        else if(r==2)   return expo*0.5*pow(w,2);
                        cout << "ERROR: should not happen" << endl;
                        exit(1);
                    });

            run(T, tau, enable_output, in, method, epsilon, number_snapshots, dm, sm);
        } else {
            cout << "ERROR: problem " << problem << " is not supported" << endl;
            exit(1);
        }
    }
}


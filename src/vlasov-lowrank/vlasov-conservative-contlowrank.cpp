#include <iostream>
#include <fstream>
#include <sstream>
#include <random>
#include <limits>

#include <generic/timer.hpp>
#include <algorithms/lowrank-conservative-1x1v.hpp>
#include <boost/program_options.hpp>
using namespace boost::program_options;


void run(int r, array<double,2> a, array<double,2> b, array<int,2> ext,
        double T, double tau, bool enable_output, domain_lowrank2d in,
        string method, int snapshots,
        function<double(double)> f0v = [](double){ return 1.0; }, int n_fixed_basis=0) {
    domain_lowrank2d out(a, b, r, ext, lowrank_type::normalized_integral);
    out = in; // needed for output

    VectorXd E(ext[0]);

    vlasov_continuous_conservative vcs(r, ext, a, b, f0v, n_fixed_basis);

    int nsteps = ceil(T/tau);
    double t = 0.0;
    ofstream fs("evolution.data");
    fs << "# t mass momentum energy kinetic_energy electric_energy l2 entropy ";
    fs << "l1 rank" << endl;
    fs.precision(16);

    for(int k=0;k<nsteps+1;k++) {
        if(tau > T-t)
            tau = T-t;

        if(enable_output && (k % int(ceil(nsteps/double(snapshots-1))) == 0 || k==nsteps)) {
            std::stringstream ss;
            ss << "f-t" << t << ".nc";
            NCWriter ncw(ss.str());
            // write f
            out.write_nc(ncw, "f");
            // write E
            vcs.electric_field_energy(out.lr, E);
            ncw.write("E", {E.size()}, E.data(), {"x"});
        }

        if(k != nsteps) { // last step only for output

            if(method == "euler")
                vcs.step_euler(tau, in.lr, out.lr);
            else if(method == "cons_euler")
                vcs.step_conservative_euler(tau, in.lr, out.lr);
            else if(method == "dopri")
                vcs.step_dopri(tau, in.lr, out.lr);
            else if(method == "unconv")
                vcs.step_unconventional(tau, in.lr, out.lr);
            else if(method == "unconv2")
                vcs.step_unconventional2(tau, in.lr, out.lr);
            else {
                cout << "ERROR: method " << method << " not available." << endl;
                exit(1);
            }


            in.lr = out.lr;

            t += tau;
            double ee = vcs.electric_field_energy(out.lr, E);
            double ke = out.kin_energy();
            cout << "t=" << t << "\tmass=" << out.mass() << "\tenergy=" << ee+ke 
                 << endl;
            fs << t << "\t" << out.mass() << "\t" << out.momentum() << "\t" 
               << ee+ke << "\t" << ke << "\t" << ee << "\t" << out.l2() << "\t"
               << out.entropy() << "\t" << out.l1() << "\t" << r << endl;
        }
    }
    fs.close();
}


template<size_t d>
array<int,d> parse_rank(string rank) {
    array<int,d> ret;
    std::istringstream iss(rank);
    int r;
    int i=0;
    while(iss >> r)
        ret[i++] = r;

    if(i != d) {
        cout << "ERROR: could not parse rank" << endl;
        exit(1);
    }
    return ret;
}

int main(int argc, char* argv[]) {
    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string problem, method, r_str;
    int nx, nv, number_snapshots, n_fixed_basis;
    double T, tau;
    bool enable_output;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("enable_output",value<bool>(&enable_output)->default_value(true),
         "enable or disable writing to disk")
        ("nx",value<int>(&nx)->default_value(128),
         "number of cells used in the space directions")
        ("nv",value<int>(&nv)->default_value(128),
         "number of cells used in the velocity directions")
        ("rank,r",value<string>(&r_str)->default_value("10"),
         "rank of the approximation")
        ("final_time,T",value<double>(&T)->default_value(100.0),
         "final time of the simulation")
        ("step_size",value<double>(&tau)->default_value(0.025),
         "time step size used in the simulation")
        ("problem",value<string>(&problem)->default_value("ll"),
         "initial value for the problem that is to be solved")
        ("method,m",value<string>(&method)->default_value("euler"),
         "splitting method used")
        ("n_fixed_basis", value<int>(&n_fixed_basis)->default_value(0),
         "number of basis functions that are held fixed")
        ("snapshots",value<int>(&number_snapshots)->default_value(2),
         "number of snapshots that are written to disk (including initial and final time)");

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);

    array<int,2> ext = {nx, nv};
    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {
        cout << "PROBLEM: " << problem << endl;
        // Linear or weak Landau damping
        if(problem == "rank2") {
            array<double,2> a = {0.0,-6.0};
            array<double,2> b = {2*M_PI,6.0};
            int r = parse_rank<1>(r_str)[0];
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral);
            in.init(2,
                    [](double x, int k) {
                        if(k==0)      return sin(x);
                        else if(k==1) return cos(x);
                        else exit(1);
                    },
                    [](double v, int k) {
                        double t0 = 0.2;
                        if(k==0)      return sin(t0*v)*exp(-0.5*v*v);
                        else if(k==1) return cos(t0*v)*exp(-0.5*v*v);
                        else  exit(1);
                    }
                   );
            run(r, a, b, ext, T, tau, enable_output, in, method,
                    number_snapshots);

        } else if(problem == "ll") {
            array<double,2> a = {0.0,-6.0};
            array<double,2> b = {4*M_PI,6.0};
            int r = parse_rank<1>(r_str)[0];
            auto f0v = [](double v) { return exp(-0.5*v*v); };
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral, f0v);
            in.init(1, [](double x, int k) {
                        if(k==0)      return 1.0+0.01*cos(0.5*x);
                        else          exit(1);
                    },
                    [](double v, int k) {
                        if(k==0)      return 1.0/sqrt(2.0*M_PI);
                        else          exit(1);
                    });
            run(r, a, b, ext, T, tau, enable_output, in, method, number_snapshots, f0v, 1);
        } else if(problem == "ts") {
            array<double,2> a = {0.0,-7.0};
            array<double,2> b = {10*M_PI,7.0};
            int r = parse_rank<1>(r_str)[0];
            auto f0v = [](double v) { return exp(-0.5*pow(v-2.4,2))+exp(-0.5*pow(v+2.4,2)); };
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral, f0v);
            in.init(1,
                    [](double x, int k) {
                        if(k==0) return 0.5*(1.0+0.001*cos(0.2*x));
                        else     exit(1);

                    },
                    [](double v, int k) {
                        if(k==0) return 1.0/sqrt(2.0*M_PI);
                        else     exit(1);
                    }
                   );
            run(r, a, b, ext, T, tau, enable_output, in, method, number_snapshots, f0v, 1);
        } else if(problem == "llpert") {
            array<double,2> a = {0.0,-6.0};
            array<double,2> b = {4*M_PI,6.0};
            int r = parse_rank<1>(r_str)[0];
            auto f0v = [](double v) { return exp(-0.5*v*v); };
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral, f0v);
            in.init(std::min(6,r), [](double x, int k) {
                        if(k==0)      return 1.0+0.01*cos(0.5*x);
                        else if(k==1) return 1e-4*cos(2*0.5*x);
                        else if(k==2) return 1e-4*cos(3*0.5*x);
                        else if(k==3) return 1e-4*cos(4*0.5*x);
                        else if(k==4) return 1e-5*cos(5*0.5*x);
                        else if(k==5) return 1e-5*cos(6*0.5*x);
                        else          exit(1);
                    },
                    [](double v, int k) {
                        if(k==0)      return 1.0/sqrt(2.0*M_PI);
                        else if(k==1) return v;
                        else if(k==2) return pow(v,2);
                        else if(k==3) return pow(v,3);
                        else if(k==4) return pow(v,4);
                        else if(k==5) return pow(v,5);
                        else          exit(1);
                    });
            run(r, a, b, ext, T, tau, enable_output, in, method,
                    number_snapshots, f0v, n_fixed_basis);
        } else if(problem == "llpert_energy") {
            array<double,2> a = {0.0,-6.0};
            array<double,2> b = {4*M_PI,6.0};
            int r = parse_rank<1>(r_str)[0];
            auto f0v = [](double v) { return exp(-0.5*v*v); };
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral, f0v);
            in.init(std::min(6,r), [](double x, int k) {
                        if(k==0)      return 1e-4*cos(3*0.5*x);
                        else if(k==1) return 1e-4*cos(2*0.5*x);
                        else if(k==2) return 1.0+0.01*cos(0.5*x);
                        else if(k==3) return 1e-4*cos(4*0.5*x);
                        else if(k==4) return 1e-5*cos(5*0.5*x);
                        else if(k==5) return 1e-5*cos(6*0.5*x);
                        else          exit(1);
                    },
                    [](double v, int k) {
                        if(k==0) return pow(v,2);
                        else if(k==1) return v;
                        else if(k==2) return 1.0/sqrt(2.0*M_PI);
                        else if(k==3) return pow(v,3);
                        else if(k==4) return pow(v,4);
                        else if(k==5) return pow(v,5);
                        else          exit(1);
                    });
            run(r, a, b, ext, T, tau, enable_output, in, method,
                    number_snapshots, f0v, n_fixed_basis);
        } else if(problem == "llnonconstu") {
            array<double,2> a = {0.0,-6.0};
            array<double,2> b = {4*M_PI,6.0};
            int r = parse_rank<1>(r_str)[0];
            auto f0v = [](double v) { return exp(-0.5*v*v); };
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral, f0v);
            in.init(std::min(6,r), [](double x, int k) {
                        double rho = (1.0+0.01*cos(0.5*x))/sqrt(2.0*M_PI);
                        double u = 0.2*cos(x);
                        if(k==0)      return rho;
                        else if(k==1) return rho*u;
                        else if(k==2) return 0.5*rho*u*u;
                        else if(k==3) return 1.0/6.0*pow(u,3);
                        else if(k==4) return 1.0/24.0*pow(u,4);
                        else if(k==5) return 1.0/120.0*pow(u,5);
                        else exit(1);
                    },
                    [](double v, int k) {
                        if(k==0)      return 1.0;
                        else if(k==1) return v;
                        else if(k==2) return pow(v,2)-1.0;
                        else if(k==3) return pow(v,3)-3.0*v;
                        else if(k==4) return 3.0-6.0*pow(v,2)+pow(v,4);
                        else if(k==5) return 15.0*v-10.0*pow(v,3)+pow(v,5);
                        else exit(1);
                    });
            VectorXd int_V = in.integrate_v(in.lr.V, [f0v](double v) { return f0v(v);});
            cout << "HH: " << int_V << endl;
            run(r, a, b, ext, T, tau, enable_output, in, method,
                    number_snapshots, f0v, n_fixed_basis);
        } else {
            cout << "ERROR: problem " << problem << " is not supported." << endl;
            exit(1);
        }
    }

    exit(0);
}


#include <iostream>
#include <fstream>

#include <algorithms/lowrank-vlasovmaxwell-1x2v.hpp>
#include <generic/timer.hpp>

#include <boost/program_options.hpp>
using namespace boost::program_options;

void run(int r, domain1x_data ddx, domain2v_data ddv, double T, double tau,
        bool enable_output, domain_lowrank1x2v in, array<VectorXd,2> E_in, VectorXd B_in, string method, bool order, double damp_fact, int snapshots) {
			
	if (order==false){
		
		domain_lowrank1x2v out(in);
		array<VectorXd,2> E_out(E_in);
		VectorXd B_out(B_in);

		vlasovmaxwell_1x2v vcs(r, ddx, ddv);
/*
		if(enable_output) {
			NCWriter ncw("f0.nc");
			in.write_nc(ncw, "f");
		}
		*/
		int nsteps = ceil(T/tau);
		
		double t = 0.0;
		// ofstream fs("initial_2si_5_wc.data");
		// fs << "# mass energy" << endl;
		// fs.precision(16);
				
		ofstream fs("evolution.data");
		fs << "# t mass momentum energy kinetic_energy electric_energy magnetic_energy E_1(k=1.25) E_2(k=1.25) B_3(k=1.25) entropy norm_l1 norm_l2 div_err energy_err smallest_sing_value int_E1 int_E2 int_B" << endl;
		fs.precision(16);
		
		double ee_0 = electric_field_energy(E_in, ddx.h);
		double me_0 = magnetic_field_energy(B_in, ddx.h);
		double ke_0 = in.kin_energy();  
		//fs << in.mass() << "\t" << ee_0+ke_0+me_0;
		// fs.close();
				
		if(method == "strang"){
			B_in = B_in-0.5*tau*vcs.diff_fft(E_in[1],ddx.b-ddx.a);
		}
		
		for(int k=0;k<nsteps+1;k++) {
			if(tau > T-t)
				tau = T-t;

			if(enable_output && (k % int(ceil(nsteps/double(snapshots-1))) == 0 || k==nsteps)) {
				std::stringstream ss;
				ss << "f-t" << t << ".nc";
				NCWriter ncw(ss.str());
				// write f
				out.write_nc(ncw, "f");
				// write E
				ncw.write("E0", {E_out[0].size()}, E_out[0].data(), {"x"});
				ncw.write("E1", {E_out[1].size()}, E_out[1].data(), {"x"});
				ncw.write("B", {B_out.size()}, B_out.data(), {"x"});
			}

			if(k != nsteps) {
				if(method == "lie")
					vcs.Lie_step(tau, in.lr, out.lr, E_in, B_in, E_out, B_out);
					
				else if(method == "strang")
					vcs.Strang_step(tau, in.lr, out.lr, E_in, B_in, damp_fact, E_out, B_out);
				else {
					cout << "ERROR: method must be lie " << endl;
					exit(1);
				}
				
				in.lr = out.lr;
				E_in = E_out;
				B_in = B_out; 
						
				t += tau;
				double ee = electric_field_energy(E_out, ddx.h);
				double ke = out.kin_energy();
				double me = magnetic_field_energy(B_out,ddx.h);
				double err_energy = ee_0+me_0+ke_0-ee-me-ke; // error in total energy  
				
				vector<complex<double>> hatE1(ddx.n), hatE2(ddx.n), hatB(ddx.n); 
				fft(ddx.n, &E_out[0](0), &hatE1[0]);
				fft(ddx.n, &E_out[1](0), &hatE2[0]);
				fft(ddx.n, &B_out(0), &hatB[0]);
				
				double e1 = abs(hatE1[1]);
				double e2 = abs(hatE2[1]);
				double b = abs(hatB[1]);
				
				double int_E1 = 0.0;
				double int_E2 = 0.0; 
				double int_B = 0.0; 
				for (int i=0;i<ddx.n;i++){
					int_E1 += ddx.h*E_out[0](i);
					int_E2 += ddx.h*E_out[1](i);
					int_B += ddx.h*B_out(i);
				}
				cout << "int_E1" << "\t" << int_E1 << "\t" << "int_E2" << "\t" << int_E2 << "\t" << "int_B" << "\t" << int_B <<endl;
				
				double l1, l2, entropy,div_err,min_sv; 
				if(k % 20 == 0) { // modulus operation - integer representation of the remainder
					l2 = out.l2(); // not yet normalized by the number of grid points!
					l1 = out.l1(); // not yet normalized by the number of grid points!
					entropy = out.entropy(); // not yet normalized by the number of grid points!
					// to compute the smallest singular value
					Eigen::BDCSVD<MatrixXd> svd(out.full(), Eigen::ComputeFullU | Eigen::ComputeFullV);
					MatrixXd XX = svd.matrixU().block(0,0,ddx.n,r);
					MatrixXd VV = svd.matrixV().block(0,0,ddv.n[0]*ddv.n[1],r);
					MatrixXd SS = svd.singularValues().head(r).asDiagonal();
					VectorXd dd = sqrt(ddx.h*ddv.h[0]*ddv.h[1])*SS.diagonal(); 
					min_sv = dd.minCoeff();
				}
				VectorXd diff_div = vcs.diff_fft(E_out[0],ddx.b-ddx.a)-vcs.rho(out.lr.X*out.lr.S,out.lr.V);
				div_err = sqrt((diff_div.array()*diff_div.array()).sum()*ddx.h); 
				cout << "div err after correction" << "\t" << div_err << endl;
				cout << "t=" << t << "\tmass=" << out.mass() << "\tenergy=" << ee+ke << endl;
				fs<<t<<"\t"<<out.mass()<<"\t"<<out.momentum()<<"\t"<<ee+ke<<"\t"<<ke<<"\t"<<ee<<"\t"<< me<<"\t"<<e1<<"\t"<<e2<<"\t"<<b<<"\t"<<entropy<<"\t"<<l1<<"\t"
				<<l2<<"\t" << div_err << "\t" << abs(err_energy) << "\t" << min_sv << "\t" << int_E1 << "\t" << int_E2 << "\t" << int_B << endl;
			}
		}
		fs.close();
/*
		if(enable_output) {
			NCWriter ncw("f1.nc");
			out.write_nc(ncw, "f");
		}
		*/
	}
			
	if (order==true){
				
		ofstream fs("order_of_convergence_Strang.data");
		fs << "tau err reference_order1 reference_order2" << endl;
        
        // reference solution with tau=2^(-9)
		// output
		domain_lowrank1x2v out_ref(in);
		array<VectorXd,2> E_out_ref(E_in);
		VectorXd B_out_ref(B_in);
		// input
		domain_lowrank1x2v in_ref = in; 
		array<VectorXd,2> E_in_ref = E_in;
		VectorXd B_in_ref = B_in;

		vlasovmaxwell_1x2v vcs_ref(r, ddx, ddv);
		
		int exp_ref = 9; 
		double tau = pow(2,-exp_ref);
		int nsteps = ceil(T/tau);
		cout << "#steps for the reference solution:" << "\t" << nsteps << endl; 
		
		if(method == "strang"){ 
			B_in = B_in-0.5*tau*vcs_ref.diff_fft(E_in[1],ddx.b-ddx.a); // to get B^(1/2) - for the 2nd order correction
		}
		
		double t = 0.0;
		
		for(int k=0;k<nsteps;k++) {
			if(tau > T-t)
				tau = T-t;
				
			if(method == "lie")
				vcs_ref.Lie_step(tau, in_ref.lr, out_ref.lr, E_in_ref, B_in_ref, E_out_ref, B_out_ref);
				
			else if(method == "strang")
				vcs_ref.Strang_step(tau, in_ref.lr, out_ref.lr, E_in_ref, B_in_ref, damp_fact, E_out_ref, B_out_ref);
			else {
				cout << "ERROR: method must be lie " << endl;
				exit(1);
			}
			in_ref.lr = out_ref.lr;
			E_in_ref = E_out_ref;
			B_in_ref = B_out_ref;

			t += tau;
		}
		double norm_sol_ref = out_ref.l2(); // for the relative error
		
		// compute solutions with different time steps 
		// output
		domain_lowrank1x2v out_sol(in);
		array<VectorXd,2> E_out_sol(E_in);
		VectorXd B_out_sol(B_in);
		// input
		domain_lowrank1x2v in_sol(in); 
		array<VectorXd,2> E_in_sol(E_in);
		VectorXd B_in_sol(B_in);
		
		vlasovmaxwell_1x2v vcs_sol(r, ddx, ddv);
		
		for (int i=3;i<exp_ref-1;i++){
			in_sol = in; 
			E_in_sol = E_in;
			B_in_sol = B_in; 
			
			tau = pow(2,-i);
			nsteps = ceil(T/tau);
			
			if(method == "strang"){ 
				B_in = B_in-0.5*tau*vcs_sol.diff_fft(E_in[1],ddx.b-ddx.a); // to get B^(1/2) - for the 2nd order correction
			}
			t = 0.0;
			for(int k=0;k<nsteps;k++) {
				if(tau > T-t)
					tau = T-t;
				if(method == "lie")
					vcs_sol.Lie_step(tau, in_sol.lr, out_sol.lr, E_in_sol, B_in_sol, E_out_sol, B_out_sol);
					
				else if(method == "strang")
					vcs_sol.Strang_step(tau, in_sol.lr, out_sol.lr, E_in_sol, B_in_sol, damp_fact, E_out_sol, B_out_sol);
				else {
					cout << "ERROR: method must be lie " << endl;
					exit(1);
				}
				in_sol.lr = out_sol.lr;
				E_in_sol = E_out_sol;
				B_in_sol = B_out_sol;

				t += tau;
			}
				
			MatrixXd diff = out_ref.full()-out_sol.full();
			double err = sqrt((diff.array()*diff.array()).sum()*ddx.n*ddv.n[0]*ddv.n[1]);
			err = err/sqrt(norm_sol_ref); // relative error
			cout << "#steps solution:" << "\t" << nsteps << ",\t" << "relative error:" << "\t" << err << endl; 
			fs << tau << "\t" << err << "\t" << tau <<"\t" << pow(tau,2) << endl;  
		}
		fs.close();
	}
}


int main(int argc, char* argv[]) {
    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string problem, method, cons_mass, cons_mom;
    int r, nx, nv, nw, number_snapshots;
    double T, tau, damp_fact;
    bool enable_output;
    bool order;
   
    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("enable_output",value<bool>(&enable_output)->default_value(true),
         "enable or disable writing to disk")
        ("nx",value<int>(&nx)->default_value(128),
         "number of cells used in the space directions")
        ("nv",value<int>(&nv)->default_value(128),
         "number of cells used in the velocity direction v")
        ("nw",value<int>(&nw)->default_value(128),
         "number of cells used in the velocity direction w")
        ("rank,r",value<int>(&r)->default_value(10),
         "rank of the approximation")
        ("final_time,T",value<double>(&T)->default_value(100.0),
         "final time of the simulation")
        ("step_size",value<double>(&tau)->default_value(0.025),
         "time step size used in the simulation")
        ("problem",value<string>(&problem)->default_value("ll"),
         "initial value for the problem that is to be solved")
        ("method,m",value<string>(&method)->default_value("lie"),
         "splitting method used")
         ("order",value<bool>(&order)->default_value(false),
         "order of convergence")
         ("damp_fact", value<double>(&damp_fact)->default_value(0.0),
         "damping factor for E and B: h_x^2*damp_fact, default no damping")
         ("snapshots",value<int>(&number_snapshots)->default_value(2),
         "number of snapshots that are written to disk (including initial and final time)");

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);

    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {
        if(problem== "ll") { // Linear Landau damping
            domain1x_data ddx(0.0, 4*M_PI, nx);
            domain2v_data ddv({-6.0,-6.0}, {6.0, 6.0}, {nv,nw});

            // set f
            domain_lowrank1x2v in(r, ddx, ddv);
            in.init(1,[](double x,int) { return 1.0+0.01*cos(0.5*x); },
                      [](double v,double w,int) { return exp(-0.5*v*v-0.5*w*w)/(2.0*M_PI); }
                   );

            // set E and B
            array<VectorXd,2> E0 = {VectorXd::Zero(ddx.n), VectorXd::Zero(ddx.n)};
            set_vec_from_func(E0[0], [](double x) { return 0.01*sin(0.5*x); }, ddx.a, ddx.h);
            VectorXd B0 = VectorXd::Zero(ddv.n[0]);

            run(r, ddx, ddv, T, tau, enable_output, in, E0, B0, method, order, damp_fact, number_snapshots);
        } 
        else if(problem== "wi") { // Weibel instability
			domain1x_data ddx(0.0, 2*M_PI/1.25, nx);
			domain2v_data ddv({-0.3,-0.3}, {0.3,0.3}, {nv,nw});
							
            // set f
            domain_lowrank1x2v in(r, ddx, ddv);
            in.init(1,[](double x,int) { return 1.0+0.0001*cos(1.25*x); },
                      [](double v,double w,int) {return exp(-(v*v+w*w/12)/0.0004)/(M_PI*0.0004*sqrt(12)); }
                   );
                               
            // set E and B
            array<VectorXd,2> E0 = {VectorXd::Zero(ddx.n), VectorXd::Zero(ddx.n)};
            set_vec_from_func(E0[0], [](double x) { return 0.0001/1.25*sin(1.25*x); }, ddx.a, ddx.h);
            VectorXd B0 = VectorXd::Zero(ddx.n); 
            set_vec_from_func(B0, [](double x) { return 0.0001*cos(1.25*x); }, ddx.a, ddx.h);

            run(r, ddx, ddv, T, tau, enable_output, in, E0, B0, method, order, damp_fact, number_snapshots);
            
        }
        else if(problem== "2si") { // two-stream instability
			domain1x_data ddx(0.0, 2*M_PI, nx);
			domain2v_data ddv({-0.4,-0.4}, {0.4,0.4}, {nv,nw});
							
            // set f
            domain_lowrank1x2v in(r, ddx, ddv);
            in.init(1,[](double x,int) { return 1.0/(2*M_PI*0.002); },
                      [](double v,double w,int) {return exp(-w*w/0.002)*(exp(-(v-0.2)*(v-0.2)/0.002)+exp(-(v+0.2)*(v+0.2)/0.002)); }
                   );
                               
            // set E and B
            array<VectorXd,2> E0 = {VectorXd::Zero(ddx.n), VectorXd::Zero(ddx.n)};
            VectorXd B0 = VectorXd::Zero(ddx.n); 
            set_vec_from_func(B0, [](double x) { return 0.001*sin(x); }, ddx.a, ddx.h);

            run(r, ddx, ddv, T, tau, enable_output, in, E0, B0, method, order, damp_fact, number_snapshots);
            
        }
        else if(problem== "lt") { // landau type
            domain1x_data ddx(0.0, 2*M_PI/0.4, nx);
            domain2v_data ddv({-5.0,-5.0}, {5.0, 5.0}, {nv,nw});

            // set f
            domain_lowrank1x2v in(r, ddx, ddv);
            in.init(1,[](double x,int) { return 1.0+0.01*cos(0.4*x); },
                      [](double v,double w,int) { return exp(-0.5*v*v-0.5*w*w)/(2.0*M_PI); }
                   );

            // set E and B
            array<VectorXd,2> E0 = {VectorXd::Zero(ddx.n), VectorXd::Zero(ddx.n)};
            set_vec_from_func(E0[0], [](double x) { return 0.01/0.4*sin(0.4*x); }, ddx.a, ddx.h);
            VectorXd B0 = VectorXd::Zero(ddx.n);
            set_vec_from_func(B0, [](double x) { return 0.01/0.4*sin(0.4*x); }, ddx.a, ddx.h);

            run(r, ddx, ddv, T, tau, enable_output, in, E0, B0, method, order, damp_fact, number_snapshots);
        } 
        else if(problem== "bot") { // bump on tail
            domain1x_data ddx(0.0, 20*M_PI, nx);
            domain2v_data ddv({-9.0,-9.0}, {9.0, 9.0}, {nv,nw});

            // set f
            domain_lowrank1x2v in(r, ddx, ddv);
            in.init(2,[](double x,int k) { if (k==0) return 1.0; else return 1.0+0.03*cos(0.3*x); },
                      [](double v,double w,int k) { if (k==0) return 9/(10*sqrt(2*M_PI))*exp(-0.5*v*v)*exp(-w*w)/sqrt(M_PI);
						  else return 2/(10*sqrt(2*M_PI))*exp(-2.0*(v-4.5)*(v-4.5))*exp(-w*w)/sqrt(M_PI);});

            // set E and B
            array<VectorXd,2> E0 = {VectorXd::Zero(ddx.n), VectorXd::Zero(ddx.n)};
            set_vec_from_func(E0[0], [](double x) { return 0.01*sin(0.3*x); }, ddx.a, ddx.h);
            VectorXd B0 = VectorXd::Zero(ddx.n);
            
            cout << E0[0].size() << " " << E0[1].size() << " " << B0.size() << endl;
            
            run(r, ddx, ddv, T, tau, enable_output, in, E0, B0, method, order, damp_fact, number_snapshots);
        } 
        else {
            cout << "ERROR: problem " << problem << " is not supported." << endl;
            exit(1);
        }
    }

    return 0;
}

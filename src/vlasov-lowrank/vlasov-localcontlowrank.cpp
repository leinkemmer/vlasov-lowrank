#include <generic/common.hpp>
#include <generic/timer.hpp>
#include <algorithms/lowrank-localcontinuoussplit-1x1v.hpp>
#include <boost/program_options.hpp>
using namespace boost::program_options;


cons_mode parse_cons(string cons) {
    if(cons == "off")
        return cons_mode::off;
    else if(cons == "rank1")
        return cons_mode::rank1;
    else if(cons == "lc_local")
        return cons_mode::lc_local;
    else if(cons == "lc_global")
        return cons_mode::lc_global;
    else if(cons == "lc_both")
        return cons_mode::lc_both;
    else if(cons == "f_local")
        return cons_mode::f_local;
    else {
        cout << "ERROR: conservative method " << cons << " is not supported" << endl;
        exit(1);
    }

}


void run(int r, array<double,2> a, array<double,2> b, array<int,2> ext,
        double T, double tau, bool enable_output, int r0, 
        function<double(double,int)> X0, function<double(double,int)> V0,
        string method, string cons_mass, string cons_mom,
        array<int,2> patches={1,1}, double lc_both_weight=1.0) {

    //if(enable_output) {
        //NCWriter ncw("f0.nc");
        //in.write_nc(ncw, "f");
    //}

    domain_lowrank2d_patches in(patches[0],patches[1]);
    domain_lowrank2d_patches out(patches[0],patches[1]);
    vlasov_cs_patches vcs(patches[0],patches[1]);
    for(auto pid : up_to<2>(patches)) {

        array<double,2> la, lb;
        array<int,2> lext;
        patched_global_to_local(pid, patches, a, b, ext, la, lb, lext);
        cout << pid << " " << la << " " << lb << " " << lext << endl;

        in(pid[0],pid[1]).reset(new domain_lowrank2d(la, lb, r, lext, lowrank_type::normalized_integral));
        out(pid[0],pid[1]).reset(new domain_lowrank2d(la, lb, r, lext, lowrank_type::normalized_integral));

        in(pid[0],pid[1])->init(r0, X0, V0);

        auto& lvcs = vcs(pid[0],pid[1]);
        lvcs.reset(new vlasov_continuoussplit(r, lext, la, lb));
        lvcs->cons_mass = parse_cons(cons_mass);
        lvcs->cons_mom  = parse_cons(cons_mom);
        lvcs->lc_both_weight = lc_both_weight;
    }


    int nsteps = ceil(T/tau);
    double t = 0.0;
    ofstream fs("evolution.data");
    fs << "# t mass momentum energy kinetic_energy electric_energy l2 entropy l1 largest_sv sv1 sv3 sv5 sv10" << endl;
    fs.precision(16);

    MatrixXd_patches gbdryx(patches[0],patches[1]), gbdryv(patches[0],patches[1]);
    MatrixXd_patches gbdryX(patches[0],patches[1]), gbdryV(patches[0],patches[1]);
        
    for(int k=0;k<nsteps;k++) {
        if(tau > T-t)
            tau = T-t;
        
        for(auto pid : up_to<2>(patches)) {
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);
            lout->lr = lin->lr;
        }

        // Compute electric field
        VectorXd E(ext[0]);
        electric_field(vcs, in, E, b[0]-a[0]);
        
        // step K
        for(auto pid : up_to<2>(patches)) {
            auto& lout  = out(pid[0],pid[1]);

            auto& in_left  = out(neg_modulo(pid[0]-1,patches[0]),pid[1]);
            auto& in_right = out((pid[0]+1)%patches[0],pid[1]);
            gbdryx(pid[0],pid[1]) = patched_xBC(*lout, *in_left, *in_right);
            gbdryX(pid[0],pid[1]) = patched_XBC(*lout, *in_left, *in_right);

            auto& in_down  = out(pid[0],neg_modulo(pid[1]-1,patches[1]));
            auto& in_up    = out(pid[0],(pid[1]+1)%patches[1]);
            gbdryv(pid[0],pid[1]) = patched_vBC(*lout, *in_down, *in_up);
            gbdryV(pid[0],pid[1]) = patched_VBC(*lout, *in_down, *in_up);
        }
        for(auto pid : up_to<2>(patches)) {
            auto& lvcs = vcs(pid[0],pid[1]);
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);
            auto& bdryx = gbdryx(pid[0],pid[1]);
            auto& bdryv = gbdryv(pid[0],pid[1]);
            
            VectorXd lE = get_local_E(E, pid[0], lin->lr.X.rows());
            lin->lr = lout->lr;
            lvcs->Lie_step_K(tau, lin->lr, lout->lr, &bdryx, &bdryv, &lE);
        }

        // step L
        for(auto pid : up_to<2>(patches)) {
            auto& lvcs = vcs(pid[0],pid[1]);
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);
            auto& bdryX = gbdryX(pid[0],pid[1]);
            auto& bdryV = gbdryV(pid[0],pid[1]);

            VectorXd lE = get_local_E(E, pid[0], lin->lr.X.rows());
            lin->lr = lout->lr;
            lvcs->Lie_step_S(tau, lin->lr, lout->lr, &bdryX, &bdryV, &lE);
        }

        // step L
        for(auto pid : up_to<2>(patches)) {
            auto& lout  = out(pid[0],pid[1]);

            auto& in_left  = out(neg_modulo(pid[0]-1,patches[0]),pid[1]);
            auto& in_right = out((pid[0]+1)%patches[0],pid[1]);
            gbdryx(pid[0],pid[1]) = patched_xBC(*lout, *in_left, *in_right);
            gbdryX(pid[0],pid[1]) = patched_XBC(*lout, *in_left, *in_right);

            auto& in_down  = out(pid[0],neg_modulo(pid[1]-1,patches[1]));
            auto& in_up    = out(pid[0],(pid[1]+1)%patches[1]);
            gbdryv(pid[0],pid[1]) = patched_vBC(*lout, *in_down, *in_up);
            gbdryV(pid[0],pid[1]) = patched_VBC(*lout, *in_down, *in_up);
        }
        for(auto pid : up_to<2>(patches)) {
            auto& lvcs = vcs(pid[0],pid[1]);
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);
            auto& bdryx = gbdryx(pid[0],pid[1]);
            auto& bdryv = gbdryv(pid[0],pid[1]);
            
            // do the remainder of the time step
            VectorXd lE = get_local_E(E, pid[0], lin->lr.X.rows());
            lin->lr = lout->lr;
            lvcs->Lie_step_L(tau, lin->lr, lout->lr, &bdryx, &bdryv, &lE);
        }
        
        // Copy out back to in and compute invariants/diagnostics
        double mass(0.0), momentum(0.0), ke(0.0), l2(0.0), entropy(0.0), l1(0.0);
        double largest_sv(0.0), sv1(0.0), sv3(0.0), sv5(0.0), sv10(0.0);
        for(auto pid : up_to<2>(patches)) {
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);

            lin->lr = lout->lr;

            mass     += lout->mass();
            momentum += lout->momentum();
            ke       += lout->kin_energy();
            //l2       += lout->l2();    // TODO: sqrt over the result not over every patch 
            entropy  += lout->entropy();
            l1       += lout->l1();
            VectorXd sv = lout->singular_values();
            largest_sv = max(largest_sv, sv[sv.size()-1]);
            sv1 = max(sv1, sv[0]);
            if(sv.size() >= 3)
                sv3 = max(sv3, sv[2]);
            if(sv.size() >= 5)
                sv5 = max(sv5, sv[4]);
            if(sv.size() >= 10)
                sv10 = max(sv10, sv[9]);
        }
        double ee = electric_field(vcs, out, E, b[0]-a[0]);

        t += tau;
        cout << "t=" << t << "\tmass=" << mass << "\tenergy=" << ee+ke << endl;
        fs << t << "\t" << mass << "\t" << momentum << "\t" << ee+ke << "\t" << ke << "\t" << ee << "\t" << l2 << "\t" << entropy << "\t" << l1 << "\t" << largest_sv << "\t" << sv1 << "\t" << sv3 << "\t" << sv5 << "\t" << sv10 << endl;
    }
    fs.close();


    //if(enable_output) {
        //NCWriter ncw("f1.nc");
        //out.write_nc(ncw, "f");
    //}

}


template<size_t d>
array<int,d> parse_rank(string rank) {
    array<int,d> ret;
    std::istringstream iss(rank);
    int r;
    int i=0;
    while(iss >> r)
        ret[i++] = r;

    if(i != d) {
        cout << "ERROR: could not parse rank" << endl;
        exit(1);
    }
    return ret;
}

array<int,2> parse_patches(string str_patches) {
    array<int,2> patches;
    std::istringstream iss(str_patches);
    if(!(iss >> patches[0])) {
        cout << "ERROR: parsing first entry of patches" << endl;
        exit(1);
    }
    if(!(iss >> patches[1])) {
        cout << "ERROR: parsing second entry of patches" << endl;
        exit(1);
    }
    return patches;
}

int main(int argc, char* argv[]) {
    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string problem, method, r_str, cons_mass, cons_mom, str_patches;
    int nx, nv;
    double T, tau, lc_both_weight;
    bool enable_output;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("enable_output",value<bool>(&enable_output)->default_value(true),
         "enable or disable writing to disk")
        ("nx",value<int>(&nx)->default_value(128),
         "number of cells used in the space directions")
        ("nv",value<int>(&nv)->default_value(128),
         "number of cells used in the velocity directions")
        ("rank,r",value<string>(&r_str)->default_value("10"),
         "rank of the approximation")
        ("final_time,T",value<double>(&T)->default_value(100.0),
         "final time of the simulation")
        ("step_size",value<double>(&tau)->default_value(0.025),
         "time step size used in the simulation")
        ("problem",value<string>(&problem)->default_value("ll"),
         "initial value for the problem that is to be solved")
        ("method,m",value<string>(&method)->default_value("lie"),
         "splitting method used")
        ("cons_mass",value<string>(&cons_mass)->default_value("off"),
         "off (standard) or rank1, lc_local, lc_global, lc_both.")
        ("cons_mom",value<string>(&cons_mom)->default_value("off"),
         "off (standard) or rank1, lc_local, lc_global, lc_both.")
        ("lc_both_weight",value<double>(&lc_both_weight)->default_value(1.0),
         "how strongly local vs global conservation is weight")
        ("patches",value<string>(&str_patches)->default_value("1 1"),
         "subdivisions in x and v before the low-rank algorithm is applied.");

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);

    array<int,2> ext = {nx, nv};
    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {
        // Linear or weak Landau damping
        if(problem== "ll") {
            array<double,2> a = {0.0,-6.0};
            array<double,2> b = {4*M_PI,6.0};
            int r = parse_rank<1>(r_str)[0];
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral);
            run(r, a, b, ext, T, tau, enable_output,
                    1,
                    [](double x, int) { return 1.0+0.01*cos(0.5*x); },
                    [](double v, int) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); },
                    method, cons_mass, cons_mom, parse_patches(str_patches));
        } else if(problem== "nl") {
            array<double,2> a = {0.0,-6.0};
            array<double,2> b = {4*M_PI,6.0};
            int r = parse_rank<1>(r_str)[0];
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral);
            run(r, a, b, ext, T, tau, enable_output,
                    1,
                    [](double x, int) { return 1.0+0.5*cos(0.5*x); },
                    [](double v, int) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); },
                    method, cons_mass, cons_mom, parse_patches(str_patches));
        } else if(problem== "nl2") {
            array<double,2> a = {0.0,-6.0};
            array<double,2> b = {4*M_PI,6.0};
            int r = parse_rank<1>(r_str)[0];
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral);
            run(r, a, b, ext, T, tau, enable_output,
                    1,
                    [](double x, int) { return 1.0+0.05*cos(0.5*x); },
                    [](double v, int) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); },
                    method, cons_mass, cons_mom, parse_patches(str_patches));
        } else if(problem == "ts") {
            array<double,2> a = {0.0,-9.0};
            array<double,2> b = {10*M_PI,9.0};
            int r = parse_rank<1>(r_str)[0];
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral);
            auto X0 = [](double x, int k) {
                return 0.5*(1.0+0.001*cos(0.2*x));
            };
            auto V0 = [](double v, int k) {
                if(k==0)      return exp(-0.5*pow(v-2.4,2))/sqrt(2.0*M_PI);
                else if(k==1) return exp(-0.5*pow(v+2.4,2))/sqrt(2.0*M_PI);
                else  exit(1);
            };
            run(r, a, b, ext, T, tau, enable_output, 2, X0, V0, method, cons_mass, cons_mom, parse_patches(str_patches));
        } else if(problem == "bot") {
            array<double,2> a = {0.0,-9.0};
            array<double,2> b = {20*M_PI,9.0};
            int r = parse_rank<1>(r_str)[0];
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral);

            auto X0 = [](double x, int k) {
                return 1.0+0.03*cos(0.3*x);
            };
            auto V0 = [](double v, int k) {
                if(k==0)      return 0.9*exp(-0.5*pow(v,2))/sqrt(2.0*M_PI);
                else if(k==1) return 0.2*exp(-2.0*pow(v-4.5,2))/sqrt(2.0*M_PI);
                else  exit(1);
            };
            run(r, a, b, ext, T, tau, enable_output, 2, X0, V0, method, cons_mass, cons_mom, parse_patches(str_patches));
        } else {
            cout << "ERROR: problem " << problem << " is not supported (either ll, nl, ts, ll4dh, ll4dh2, ts4dh, ts4dh2, or nl4dh has to be specified)" << endl;
            exit(1);
        }
    }

    exit(0);
}


#include <iostream>
#include <fstream>
#include <sstream>

#include <generic/timer.hpp>
#include <algorithms/lowrank-bgk-moments.hpp>

#include <boost/program_options.hpp>
#include <boost/format.hpp>
using namespace boost::program_options;

void run(double T, double tau, bool enable_output, domain_lowrank4d_fluid in, moments mom_in, string method, double epsilon, int snapshots, diff_mode dm, std::function<double(double,double)>* C_collision=nullptr) {
    domain_lowrank4d_fluid out(in.dd, lowrank_type::normalized_integral);

    vlasov_bgk_moments vcs(in.dd, epsilon, dm);
    int nsteps = ceil(T/tau);
    double t = 0.0;
    ofstream fs("evolution.data");
    fs << "# t umin umax vmin vmax" << endl;
    fs.precision(16);


    // spatially varying epsilon
    if(C_collision != nullptr) {
        for(auto i : range<2>(in.dd.n[0],in.dd.n[1]))
            vcs.C_collision[in.dd.lin_idx_x(i)] = (*C_collision)(in.dd.x(i), in.dd.y(i));
    }

    out.lr = in.lr; // that the first output works
    moments mom_out = mom_in;

    for(int k=0;k<nsteps+1;k++) {
        timer t1;
        t1.start();

        double umin = *std::min_element(mom_in.u[0].data(), mom_in.u[0].data()+mom_in.u[0].size());
        double umax = *std::max_element(mom_in.u[0].data(), mom_in.u[0].data()+mom_in.u[0].size());
        double vmin = *std::min_element(mom_in.u[1].data(), mom_in.u[1].data()+mom_in.u[1].size());
        double vmax = *std::max_element(mom_in.u[1].data(), mom_in.u[1].data()+mom_in.u[1].size());
        cout << "t=" << t << "\t\tumin=" << umin << ", umax=" << umax
             << "\t\tvmin=" << vmin << " , vmax=" << vmax << endl;
        fs << t << "\t" << umin << "\t" << umax << "\t" << vmin << "\t" << vmax << endl;
        // output
        if(k % int(ceil(nsteps/double(snapshots-1))) == 0 || k==nsteps) {
            if(enable_output) {
                // write moments
                NCWriter ncw(str(boost::format("rhou-t%1%.nc")%t));
                mom_out.write_nc(ncw, in.dd);

                array<VectorXd,4> stress = stress_tensor(in.lr, mom_in, in.dd, vcs.epsilon);
                ncw.write("st_00", {in.dd.n[1],in.dd.n[0]}, stress[0].data(), {"y", "x"});
                ncw.write("st_10", {in.dd.n[1],in.dd.n[0]}, stress[1].data(), {"y", "x"});
                ncw.write("st_01", {in.dd.n[1],in.dd.n[0]}, stress[2].data(), {"y", "x"});
                ncw.write("st_11", {in.dd.n[1],in.dd.n[0]}, stress[3].data(), {"y", "x"});

                array<VectorXd,4> sigma = vcs.compute_sigma(mom_in);
                ncw.write("sigma_00", {in.dd.n[1],in.dd.n[0]}, sigma[0].data(), {"y", "x"});
                ncw.write("sigma_10", {in.dd.n[1],in.dd.n[0]}, sigma[1].data(), {"y", "x"});
                ncw.write("sigma_01", {in.dd.n[1],in.dd.n[0]}, sigma[2].data(), {"y", "x"});
                ncw.write("sigma_11", {in.dd.n[1],in.dd.n[0]}, sigma[3].data(), {"y", "x"});

                // write low-rank info
                NCWriter ncw_lr(str(boost::format("lr-t%1%.nc")%t));
                in.write_lr_factors_nc(ncw_lr);
            }
        }

        if(k < nsteps+1) { // last step is only for output
            if(tau > T-t)
                tau = T-t;

            // cvode complains if we take too small time steps
            if(tau > 1e-12) {
                if(method == "lie")
                    vcs.Lie_step(tau, in.lr, out.lr, mom_in, mom_out);
                else {
                    cout << "ERROR: method must be either lie or strang" << endl;
                    exit(1);
                }
                in.lr = out.lr;
                mom_in = mom_out;
            }

            t += tau;
        }

        t1.stop();
        cout << "time step took: " << t1.total() << endl;

    }
}

int main(int argc, char* argv[]) {
    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string problem, method, r_str, spaced, solver;
    int nx, nv, number_snapshots;
    double T, tau, reynolds, lambda, v0, delta, _epsilon;
    bool enable_output;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("enable_output",value<bool>(&enable_output)->default_value(true),
         "enable or disable writing to disk")
        ("snapshots",value<int>(&number_snapshots)->default_value(2),
         "number of snapshots that are written to disk (including initial and final time)")
        ("nx",value<int>(&nx)->default_value(32),
         "number of cells used in the space directions")
        ("nv",value<int>(&nv)->default_value(32),
         "number of cells used in the velocity directions")
        ("rank,r",value<string>(&r_str)->default_value("10"),
         "rank of the approximation")
        ("final_time,T",value<double>(&T)->default_value(1.0),
         "final time of the simulation")
        ("step_size",value<double>(&tau)->default_value(0.1),
         "time step size used in the simulation")
        ("problem",value<string>(&problem)->default_value("shear"),
         "initial value for the problem that is to be solved")
        ("method,m",value<string>(&method)->default_value("lie"),
         "splitting method used")
        ("reynolds",value<double>(&reynolds)->default_value(-1.0),
         "Reynolds number")
        ("epsilon",value<double>(&_epsilon)->default_value(0.01),
         "epsilon (used if Reynolds number < 0)")
        ("lambda",value<double>(&lambda)->default_value(1.0/30.0),
         "length scale for the initial value")
        ("v0",value<double>(&v0)->default_value(0.1),
         "initial velocity")
        ("delta",value<double>(&delta)->default_value(0.005),
         "amplitude")
        ("spaced",value<string>(&spaced)->default_value("fft"),
         "space discretization used (either fft, upwind_lxf, upwind_staglxf, upwind_nt, or lwvanleer_nt)");

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);
   
    diff_mode dm;
    if(spaced == "fft")
        dm = DM_FFT;
    else if(spaced == "upwind_lxf")
        dm = DM_UPWIND_LXF;
    else if(spaced == "upwind_staglxf")
        dm = DM_UPWIND_STAGLXF;
    else if(spaced == "upwind_nt")
        dm = DM_UPWIND_NT;
    else if(spaced == "lwvanleer_nt")
        dm = DM_LWVANLEER_NT;
    else {
        cout << "ERROR: spaced " << spaced << " not supported" << endl;
        exit(1);
    }


    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {
        if(problem== "shear") {
            domain4d_data dd({0.0,0.0,-6.0,-6.0}, {1.0, 1.0, 6.0, 6.0}, {nx,nx,nv,nv}, atoi(r_str.c_str()));
            domain_lowrank4d_fluid in(dd, lowrank_type::normalized_integral);
            double epsilon;
            if(reynolds > 0)
                epsilon = v0/reynolds;
            else
                epsilon = _epsilon;
            cout << "epsilon: " << epsilon << endl;
            double rho = 1.0;

            moments mom_in;
            mom_in.init(
                    [rho](double x, double y) {
                        return rho;
                    },
                    [v0, lambda](double x, double y) {
                        return v0*( (y<=0.5) ? tanh((y-0.25)/lambda) : tanh((0.75-y)/lambda) );
                    },
                    [delta](double x, double y) {
                        return delta*sin(2*M_PI*x);
                    },
            dd);

            in.init(1, [](double x, double y, int r) {
                        return 1.0;
                    },
                    [](double v, double w, int r) {
                        return 1.0;
                    });

            run(T, tau, enable_output, in, mom_in, method, epsilon, number_snapshots, dm);
        } else if(problem == "shock") {
            domain4d_data dd({-1.0,0.0,-6.0,-6.0}, {1.0, 1.0, 6.0, 6.0}, {nx,nx,nv,nv}, atoi(r_str.c_str()));
            domain_lowrank4d_fluid in(dd, lowrank_type::normalized_integral);

            double epsilon = _epsilon;
            cout << "epsilon: " << epsilon << endl;

            moments mom_in;
            // A Riemann problem that results in a 2-shock
            double rho_l  = 1.0;
            double rho_r  = 0.25;
            double u_l = 0.0/rho_l;
            double u_r = -3.0/8.0/rho_r;
            /*mom_in.init(
                    [rho_l,rho_r](double x, double y) {
                        return (x<=0.0) ? rho_l : rho_r; 
                    },
                    [u_l,u_r](double x, double y) {
                        return (x<=0.0) ? u_l : u_r;
                    },
                    [](double x, double y) {
                        return 0.0;
                    },
            dd);*/
            mom_in.init(
                    [rho_l,rho_r](double x, double y) {
                        return (x<=y) ? rho_l : rho_r; 
                    },
                    [u_l,u_r](double x, double y) {
                        return (x<=y) ? 0.5*u_l : 0.5*u_r;
                    },
                    [u_l,u_r](double x, double y) {
                        return (x<=y) ? 0.5*u_l : 0.5*u_r;
                    },
            dd);

            in.init(1, [](double x, double y, int r) {
                        return 1.0;
                    },
                    [](double v, double w, int r) {
                        return 1.0;
                    });

            run(T, tau, enable_output, in, mom_in, method, epsilon, number_snapshots, dm);
        } else if(problem == "explosion") {
            domain4d_data dd({-1.5,-1.5,-6.0,-6.0}, {1.5, 1.5, 6.0, 6.0}, {nx,nx,nv,nv}, atoi(r_str.c_str()));
            domain_lowrank4d_fluid in(dd, lowrank_type::normalized_integral);

            double epsilon = _epsilon;
            cout << "epsilon: " << epsilon << endl;

            moments mom_in;
            // an explosion that results in a shock front
            double rho_i  = 1.0;
            double rho_o  = 0.125;
            mom_in.init(
                    [rho_i,rho_o](double x, double y) {
                        return (x*x+y*y<0.16) ? rho_i : rho_o;
                    },
                    [](double x, double y) {
                        return 0.0;
                    },
                    [](double x, double y) {
                        return 0.0;
                    },
            dd);

            in.init(1, [](double x, double y, int r) {
                        return 1.0;
                    },
                    [](double v, double w, int r) {
                        return 1.0;
                    });

            run(T, tau, enable_output, in, mom_in, method, epsilon, number_snapshots, dm);
        } else if(problem == "explosion2") {
            domain4d_data dd({-1.5,-1.5,-6.0,-6.0}, {1.5, 1.5, 6.0, 6.0}, {nx,nx,nv,nv}, atoi(r_str.c_str()));
            domain_lowrank4d_fluid in(dd, lowrank_type::normalized_integral);

            double epsilon = _epsilon;
            cout << "epsilon: " << epsilon << endl;

            moments mom_in;
            // an explosion that results in a shock front
            double rho_i  = 1.0;
            double rho_o  = 0.1;
            mom_in.init(
                    [rho_i,rho_o](double x, double y) {
                        return (x*x+y*y<0.01) ? rho_i : rho_o;
                    },
                    [](double x, double y) {
                        return 0.0;
                    },
                    [](double x, double y) {
                        return 0.0;
                    },
            dd);

            in.init(1, [](double x, double y, int r) {
                        return 1.0;
                    },
                    [](double v, double w, int r) {
                        return 1.0;
                    });

            run(T, tau, enable_output, in, mom_in, method, epsilon, number_snapshots, dm);
        } else if(problem == "beam") {
            // Example taken from https://doi.org/10.1016/j.jcp.2004.09.006
            domain4d_data dd({0.0,0.0,-8.0,-8.0}, {1.0, 1.0, 8.0, 8.0}, {nx,nx,nv,nv}, atoi(r_str.c_str()));
            domain_lowrank4d_fluid in(dd, lowrank_type::normalized_integral);

            double epsilon = _epsilon;
            cout << "epsilon: " << epsilon << endl;

            moments mom_in;
            // background plasma is a Maxwellian
            mom_in.init(
                    [](double x, double y) {
                        return 1.0;
                    },
                    [](double x, double y) {
                        return 0.0;
                    },
                    [](double x, double y) {
                        return 0.0;
                    },
            dd);

            // beam is also a maxwellian
            double n_b = 1e-3;
            double v_b = 4.0;
            double w_b = 2.0;
            double T_b = 1e-1;
            in.init(2, [n_b](double x, double y, int r) {
                        if(r==0)  return 1.0;
                        else      return n_b;
                    },
                    [v_b, w_b, T_b](double v, double w, int r) {
                        if(r==0)  return 1.0;
                        else      return exp(-(pow(v-v_b,2)+pow(w-w_b,2))/(2.0*T_b) + 0.5*v*v + 0.5*w*w);
                    });

            run(T, tau, enable_output, in, mom_in, method, epsilon, number_snapshots, dm);
        } else if(problem == "beam_ap") {
            domain4d_data dd({-1.0,0.0,-8.0,-8.0}, {1.0, 1.0, 8.0, 8.0}, {nx,nx,nv,nv}, atoi(r_str.c_str()));
            domain_lowrank4d_fluid in(dd, lowrank_type::normalized_integral);

            double epsilon = _epsilon;
            cout << "epsilon: " << epsilon << endl;

            std::function<double(double,double)> C_collision = [epsilon](double x, double y) {
                return 1.0/(epsilon + tanh(1.0-11.0*x) + tanh(1.0+11.0*x));
            };

            moments mom_in;
            // background plasma is a Maxwellian
            mom_in.init(
                    [](double x, double y) {
                        return 1.0;
                    },
                    [](double x, double y) {
                        return 0.0;
                    },
                    [](double x, double y) {
                        return 0.0;
                    },
            dd);

            // beam is also a maxwellian
            double n_b = 1e-3;
            double v_b = 4.0;
            double w_b = 2.0;
            double T_b = 1e-1;
            in.init(1, [](double x, double y, int r) {
                        if(r==0)  return 1.0;
                        else      exit(1);
                    },
                    [v_b, w_b, T_b, n_b](double v, double w, int r) {
                        if(r==0)  return 1.0 + n_b*exp(-(pow(v-v_b,2)+pow(w-w_b,2))/(2.0*T_b) + 0.5*v*v + 0.5*w*w);
                        else      exit(1);
                    });

            run(T, tau, enable_output, in, mom_in, method, 1.0, number_snapshots, dm, &C_collision);
        } else if(problem == "twobump_ap") {
            domain4d_data dd({-1.0,0.0,-8.0,-8.0}, {1.0, 1.0, 8.0, 8.0}, {nx,nx,nv,nv}, atoi(r_str.c_str()));
            domain_lowrank4d_fluid in(dd, lowrank_type::normalized_integral);
            
            double epsilon = _epsilon;
            cout << "epsilon: " << epsilon << endl;

            std::function<double(double,double)> C_collision = [epsilon](double x, double y) {
                return 1.0/(epsilon + tanh(1.0-11.0*x) + tanh(1.0+11.0*x));
            };

            moments mom_in;
            mom_in.init(
                    [](double x, double y) {
                        return 0.5 + 0.1*sin(M_PI*x);
                    },
                    [](double x, double y) {
                        return 1.0;
                    },
                    [](double x, double y) {
                        return 0.0;
                    },
            dd);

            in.init(2, [](double x, double y, int r) {
                        if(r==0)  return 1.0;
                        else      return 0.1;
                    },
                    [](double v, double w, int r) {
                        if(r==0)  return 1.0;
                        else      return exp(-0.5*(pow(v+0.5,2)+pow(w,2)) + 0.5*pow(v-1.0,2) + 0.5*w*w);
                    });

            run(T, tau, enable_output, in, mom_in, method, 1.0, number_snapshots, dm, &C_collision);
        } else {
            cout << "ERROR: problem " << problem << " is not supported" << endl;
            exit(1);
        }
    }
}


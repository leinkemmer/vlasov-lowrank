#include <generic/common.hpp>
#include <generic/timer.hpp>
#include <algorithms/lowrank-localcontinuoussplit-1x1v.hpp>
#include <boost/program_options.hpp>
using namespace boost::program_options;


void run(int r, array<double,2> a, array<double,2> b, array<int,2> ext,
        double T, double tau, bool enable_output, int r0, 
        function<double(double,int)> X0, function<double(double,int)> V0,
        string method) {

    array<int,2> patches = {3,3};
    array<double,2> _a = {a[0]-(b[0]-a[0]), a[1]-(b[1]-a[1])};
    array<double,2> _b = {b[0]+(b[0]-a[0]), b[1]+(b[1]-a[1])};
    array<int,2>   _ext = {ext[0]*3, ext[1]*3};

    domain_lowrank2d_patches in(patches[0],patches[1]);
    domain_lowrank2d_patches out(patches[0],patches[1]);
    vlasov_cs_patches vcs(patches[0],patches[1]);
    for(auto pid : up_to<2>(patches)) {

        array<double,2> la, lb;
        array<int,2> lext;
        patched_global_to_local(pid, patches, _a, _b, _ext, la, lb, lext);
        cout << pid << " " << la << " " << lb << " " << lext << endl;

        in(pid[0],pid[1]).reset(new domain_lowrank2d(la, lb, r, lext, lowrank_type::normalized_integral));
        out(pid[0],pid[1]).reset(new domain_lowrank2d(la, lb, r, lext, lowrank_type::normalized_integral));


        auto& lvcs = vcs(pid[0],pid[1]);
        lvcs.reset(new vlasov_continuoussplit(r, lext, la, lb));
    }

    // initial value
    //in(1,1)->init(r0, X0, V0);

    in(1,1)->init(1,
            [](double x, int) { return 1.0+0.5*cos(0.5*x); },
            [](double v, int) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); }
            );


    // setup boundary conditions consistent with periodic initial value
    auto& lin  = in(1,1);
    in(0,1)->lr = lin->lr;
    in(2,1)->lr = lin->lr;
    in(1,0)->lr = lin->lr;
    in(1,2)->lr = lin->lr;

    //in(0,1)->init(2,
            //[](double x, int k) { return 1.0 + 0.5*cos(0.5*x); },
            //[](double v, int k) { return (k==0) ? (1.0+v)*exp(-0.5*v*v)/sqrt(2.0*M_PI) : -v*exp(-0.5*v*v)/sqrt(2.0*M_PI); }
            //);
    //in(2,1)->init(2,
            //[](double x, int k) { return 1.0 + 0.5*cos(0.5*x); },
            //[](double v, int k) { return (k==0) ? (1.0+v)*exp(-0.5*v*v)/sqrt(2.0*M_PI) : -v*exp(-0.5*v*v)/sqrt(2.0*M_PI); }
            //);

    //in(0,1)->init(1,
            //[](double x, int k) { return 1.0 + 0.5*cos(0.5*x); },
            //[](double v, int k) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); }
            //);
    //in(2,1)->init(1,
            //[](double x, int k) { return 1.0 + 0.5*cos(0.5*x); },
            //[](double v, int k) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); }
            //);

    cout << "this should be zero: " << (in(0,1)->lr.full()-lin->lr.full()).norm() << endl;

    auto& in_left  = in(0,1);
    auto& in_right = in(2,1);
    MatrixXd bdryx = patched_xBC(*lin, *in_left, *in_right);
    cout << bdryx << endl;
    cout << bdryx.rows() << " " << bdryx.cols() << endl;

    //exit(1);

    
    int nsteps = ceil(T/tau);
    double t = 0.0;
    ofstream fs("evolution.data");
    fs << "# t mass momentum energy kinetic_energy electric_energy l2 entropy l1" << endl;
    fs.precision(16);

    for(int k=0;k<nsteps;k++) {
        if(tau > T-t)
            tau = T-t;

        auto& lin  = in(1,1);
        auto& lout = out(1,1);
        auto& lvcs = vcs(1,1);
        lout->lr = lin->lr;

        VectorXd E(ext[0]);
        E.setZero();

        // copy (1,1) to the left and right
        //in(0,1)->lr = lin->lr;
        //in(2,1)->lr = lin->lr;
        //in(1,0)->lr = lin->lr;
        //in(1,2)->lr = lin->lr;

        auto& in_left  = in(0,1);
        auto& in_right = in(2,1);
        MatrixXd bdryx = patched_xBC(*lin, *in_left, *in_right);



        auto& in_down  = in(1,0);
        auto& in_up    = in(1,2);
        MatrixXd bdryv = patched_vBC(*lin, *in_down, *in_up);

        // step K
        lin->lr = lout->lr;
        lvcs->Lie_step_K(tau, lin->lr, lout->lr, &bdryx, &bdryv, &E);

        // step S
        lin->lr = lout->lr;
        lvcs->Lie_step_S(tau, lin->lr, lout->lr, &bdryx, &bdryv, &E);

        // L step
        lin->lr = lout->lr;
        lvcs->Lie_step_L(tau, lin->lr, lout->lr, &bdryx, &bdryv, &E);

        // prepare for next step
        lin->lr = lout->lr;

        // Copy out back to in and compute invariants/diagnostics
        double mass = lout->mass();
        double momentum = lout->momentum();
        double ke = lout->kin_energy();
        double entropy = lout->entropy();
        double l1 = lout->l1();
        double ee = lvcs->electric_field_energy(lout->lr, E);
        double l2 = 0.0;
        
        t += tau;
        cout << "t=" << t << "\tmass=" << mass << "\tenergy=" << ee+ke << endl;
        fs << t << "\t" << mass << "\t" << momentum << "\t" << ee+ke << "\t" << ke << "\t" << ee << "\t" << l2 << "\t" << entropy << "\t" << l1 << endl;
    }

    fs.close();
}

template<size_t d>
array<int,d> parse_rank(string rank) {
    array<int,d> ret;
    std::istringstream iss(rank);
    int r;
    int i=0;
    while(iss >> r)
        ret[i++] = r;

    if(i != d) {
        cout << "ERROR: could not parse rank" << endl;
        exit(1);
    }
    return ret;
}

int main(int argc, char* argv[]) {
    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string problem, method, r_str;
    int nx, nv;
    double T, tau;
    bool enable_output;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("enable_output",value<bool>(&enable_output)->default_value(true),
         "enable or disable writing to disk")
        ("nx",value<int>(&nx)->default_value(128),
         "number of cells used in the space directions")
        ("nv",value<int>(&nv)->default_value(128),
         "number of cells used in the velocity directions")
        ("rank,r",value<string>(&r_str)->default_value("10"),
         "rank of the approximation")
        ("final_time,T",value<double>(&T)->default_value(100.0),
         "final time of the simulation")
        ("step_size",value<double>(&tau)->default_value(0.025),
         "time step size used in the simulation")
        ("problem",value<string>(&problem)->default_value("ll"),
         "initial value for the problem that is to be solved")
        ("method,m",value<string>(&method)->default_value("lie"),
         "splitting method used");

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);

    array<int,2> ext = {nx, nv};
    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {
        // Linear or weak Landau damping
        if(problem== "ll") {
            array<double,2> a = {0.0,-6.0};
            array<double,2> b = {4*M_PI,6.0};
            int r = parse_rank<1>(r_str)[0];
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral);
            run(r, a, b, ext, T, tau, enable_output,
                    1,
                    [](double x, int) { return 1.0+0.01*cos(0.5*x); },
                    [](double v, int) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); },
                    method);
        } else if(problem== "nl") {
            array<double,2> a = {0.0,-6.0};
            array<double,2> b = {4*M_PI,6.0};
            int r = parse_rank<1>(r_str)[0];
            domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral);
            run(r, a, b, ext, T, tau, enable_output,
                    1,
                    [](double x, int) { return 1.0+0.5*cos(0.5*x); },
                    [](double v, int) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); },
                    method);
        } else {
            cout << "ERROR: problem " << problem << " is not supported (either ll, nl, ts, ll4dh, ll4dh2, ts4dh, ts4dh2, or nl4dh has to be specified)" << endl;
            exit(1);
        }
    }

    exit(0);
}


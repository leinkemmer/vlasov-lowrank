
#define BOOST_TEST_MODULE VLASOVMAXWELL
#include <boost/test/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>
#include <unsupported/Eigen/MatrixFunctions>
#include <algorithms/lowrank-vlasovmaxwell-1x2v.hpp>


BOOST_AUTO_TEST_CASE( INIT ) {
    int r=5;
    int nx=16, nv=17, nw=18;

    domain1x_data ddx(0.0, 4*M_PI, nx);
    domain2v_data ddv({-6.0,-6.0}, {6.0, 6.0}, {nv,nw});

    domain_lowrank1x2v f(r, ddx, ddv);
    f.init(2,[](double x,int) { return 1.0+0.1*cos(0.5*x); },
             [](double v,double w,int k) { 
                if(k==0) return exp(-0.5*v*v);
                else     return exp(-0.5*w*w);
             });
    
    MatrixXd f_full = f.full();

    double error = 0.0;
    for(int i=0;i<ddx.n;i++) {
        for(auto j : up_to<2>(ddv.n)) {
            double x = ddx.x(i);
            double v = ddv.v(j);
            double w = ddv.w(j);
            double f_exact = (1.0+0.1*cos(0.5*x))*(exp(-0.5*v*v)+exp(-0.5*w*w));

            error = max(error, abs(f_exact-f_full(i,ddv.lin_idx(j))));
        }
    }
    
    BOOST_CHECK_SMALL( error, 1e-10 );
}

BOOST_AUTO_TEST_CASE( ENERGY ) {
    int nx=20;
    domain1x_data ddx(0.0, 4*M_PI, nx);
    
	double electric_exact = 5*M_PI;
    
    array<VectorXd,2> E; 
    VectorXd e0(ddx.n);
	VectorXd e1(ddx.n);
    for(int i=0;i<ddx.n;i++){
		double x = ddx.x(i);
		e0(i) = cos(x); e1(i) = 2*sin(x);
		E[0] = e0; E[1] = e1;  
	}
	
	double energy = electric_field_energy(E,ddx.h); 
		
	double error = abs(electric_exact-energy);
	
	BOOST_CHECK_SMALL( error, 1e-10 );
	// cout << "error_electric_energy";
	// cout << " " << error << "\n";
}

BOOST_AUTO_TEST_CASE( RHO ) {
    int r=1;
    int nx=15, nv=16, nw=17;
    domain1x_data ddx(0.0, 4*M_PI, nx);
    domain2v_data ddv({-6.0,-6.0}, {6.0, 6.0}, {nv,nw});
    
	vlasovmaxwell_1x2v vm(r, ddx, ddv);
    
    VectorXd K(ddx.n);
    for(int i=0;i<ddx.n;i++){
		K(i) = cos(ddx.x(i));
	}    
	
	VectorXd rho_exact = 6.28319*K-VectorXd::Constant(nx,1.0);
	
	VectorXd rho_approx(ddx.n);
    MatrixXd V(nv*nw,r);
    int idx = 0;
    for(int j=0;j<ddv.n[1];j++) { //index on w
		for(int i=0;i<ddv.n[0];i++) { //index on v
			double v = ddv.v({i,j});
			double w = ddv.w({i,j});
			V(ddv.lin_idx({i,j}),idx) = exp(-0.5*v*v)*exp(-0.5*w*w);
		}
	}
	
	rho_approx = vm.rho(K, V); 
    VectorXd diff = rho_exact-rho_approx;
    double l2 = 0.0; 
    for(int i=0;i<ddx.n;i++){
		l2 += pow(diff(i),2);
	} 
    l2 = sqrt(l2)/ddx.n;
	
	cout << "error_rho" << " " << l2 << "\n";
}

BOOST_AUTO_TEST_CASE( GRAD_V ) {
	int r=1;
    int nx=15, nv=160, nw=17;
	
	domain1x_data ddx(0.0, 4*M_PI, nx);
    domain2v_data ddv({-6.0,-6.0}, {6.0, 6.0}, {nv,nw});
    
	vlasovmaxwell_1x2v vm(r, ddx, ddv);
    
    MatrixXd V(nv*nw,r);
    int idx = 0;
    for(int j=0;j<ddv.n[1];j++) { //index on w
		for(int i=0;i<ddv.n[0];i++) { //index on v
			V(ddv.lin_idx({i,j}),idx) = exp(-pow(ddv.v({i,j}),2)/2);
		}
	}
	MatrixXd dV(nv*nw,r); 
    dV = vm.grad_V(V,ddv.b[0]-ddv.a[0]); 
        
    MatrixXd dV_exact(nv*nw,r);
    double error; 
    for(int j=0;j<ddv.n[1];j++) { //index on w
		for(int i=0;i<ddv.n[0];i++) { //index on v
			dV_exact(ddv.lin_idx({i,j}),idx) = -ddv.v({i,j})*exp(-pow(ddv.v({i,j}),2)/2);
		}
	}
	
	MatrixXd diff = (dV_exact-dV).cwiseAbs();
	error = diff.maxCoeff();
    cout << "error grad_V" << " " << error << "\n"; 
}

BOOST_AUTO_TEST_CASE( GRAD_W ) {
	int r=1;
    int nx=15, nv=16, nw=17;
    	
	domain1x_data ddx(0.0, 4*M_PI, nx);
    domain2v_data ddv({-6.0,-6.0}, {6.0, 6.0}, {nv,nw});
    
	vlasovmaxwell_1x2v vm(r, ddx, ddv);
    
    MatrixXd W(nv*nw,r);
    int idx = 0;
    for(int j=0;j<ddv.n[1];j++) { //index on w
		for(int i=0;i<ddv.n[0];i++) { //index on v
			W(ddv.lin_idx({i,j}),idx) = ddv.v({i,j})*exp(-pow(ddv.w({i,j}),2)/2);
		}
	}
	MatrixXd dW(nv*nw,r); 
    dW = vm.grad_W(W,ddv.b[1]-ddv.a[1]); 
    
    
    MatrixXd dW_exact(nv*nw,r);
    double error; 
    for(int j=0;j<ddv.n[1];j++) { //index on w
		for(int i=0;i<ddv.n[0];i++) { //index on v
			dW_exact(ddv.lin_idx({i,j}),idx) = -ddv.v({i,j})*ddv.w({i,j})*exp(-pow(ddv.w({i,j}),2)/2);
		}
	}
	
	MatrixXd diff = (dW_exact-dW).cwiseAbs();
    error = diff.maxCoeff();
    cout << "error grad_W" << " " << error << "\n"; 
}

BOOST_AUTO_TEST_CASE( CURRENT_V ) {
	int r=1; 
	int nx=15, nv=16, nw=17;
	
	domain1x_data ddx(0.0, 4*M_PI, nx);
    domain2v_data ddv({-6.0,-6.0}, {6.0, 6.0}, {nv,nw});
    vlasovmaxwell_1x2v vm(r, ddx, ddv);
    
    MatrixXd V(nv*nw,r);
    for(int j=0;j<ddv.n[1];j++) { //index on w
		for(int i=0;i<ddv.n[0];i++) { //index on v
			double v = ddv.v({i,j});
			double w = ddv.w({i,j});
			V(ddv.lin_idx({i,j}),0) = exp(-0.5*v*v)*exp(-0.5*w*w);
		}
	}
	MatrixXd K(nx,r); 
	for(int j=0;j<ddx.n;j++) { 
		double xx = ddx.x(j);
		K(j,0) = 1.0+0.1*cos(0.5*xx);
		}
	VectorXd j_v(nx);
	j_v=vm.current_v(K,V);
	
	VectorXd exact_jv(nx);
	exact_jv.setZero();
	
	VectorXd diff = (j_v-exact_jv).cwiseAbs();
    double error = diff.maxCoeff();
    cout << "max error" << " " << error << "\n"; 
}


BOOST_AUTO_TEST_CASE( DER_PHI ) {
	int r=1; 
	int nx=15, nv=16, nw=17;
	
	domain1x_data ddx(0.0, 2*M_PI, nx);
	domain2v_data ddv({-6.0,-6.0}, {6.0, 6.0}, {nv,nw});
	vlasovmaxwell_1x2v vm(r, ddx, ddv);
        
    VectorXd Phi(nx);
    VectorXd gradPhi(nx);
    VectorXd exact_der(nx); 
    
    vector<complex<double>> hat(nx);
    
    for(int i=0;i<nx;i++){
		double x = ddx.x(i);
		Phi(i) = cos(x); 
		exact_der(i) = -sin(x);
		}
	
	gradPhi = vm.diff_fft(Phi,ddx.b-ddx.a);
    
    VectorXd diff = (exact_der-gradPhi).cwiseAbs();
    double error = diff.maxCoeff();
    cout << "max error derivative" << " " << error << "\n"; 
}

BOOST_AUTO_TEST_CASE( CORRECTION_PHI ) {
	int r=1; 
	int nx=15, nv=16, nw=17;
	
	domain1x_data ddx(0.0, 2*M_PI, nx);
	domain2v_data ddv({-6.0,-6.0}, {6.0, 6.0}, {nv,nw});
	vlasovmaxwell_1x2v vm(r, ddx, ddv);
        
    VectorXd f(nx);
    VectorXd gradPhi(nx);
    VectorXd exact_der(nx); 
    
    vector<complex<double>> hat(nx);
    
    for(int i=0;i<nx;i++){
		double x = ddx.x(i);
		f(i) = -cos(x); 
		exact_der(i) = -sin(x);
		}
	
	gradPhi = vm.poisson_correction(f);
    
    VectorXd diff = (exact_der-gradPhi).cwiseAbs();
    double error = diff.maxCoeff();
    cout << "max error grad correction" << " " << error << "\n"; 
}

//BOOST_AUTO_TEST_CASE( DER_PHI_FD ) {
	//int nx=100;
	
	//domain1x_data ddx(0.0, 1, nx);
        
    //VectorXd gradPhi(nx);
    //VectorXd Phi(nx);
    
    //for(int i=0;i<nx;i++){
		//double x = ddx.x(i);
		//Phi(i) = sin(2*M_PI*x); 
		//}
	
	//MatrixXd B(nx,nx);
	//B.setZero(); 
	//for (int i=1;i<nx-1;i++){
		//B(i,i+1) = 1/(2*ddx.h);
		//B(i,i-1) = -1/(2*ddx.h); 
		//}
	//B(0,0) = -1/ddx.h; B(0,1) = 1/ddx.h;
	//B(ddx.n-1,ddx.n-1) = 1/ddx.h; B(ddx.n-1,ddx.n-2) = -1/ddx.h;
	
	//gradPhi = B*Phi; 
    
    //VectorXd exact_der(nx); 
    //for(int i=0;i<nx;i++){
		//double x = ddx.x(i);
		//exact_der(i) = 2*M_PI*cos(2*M_PI*x); 
		//}
	        
	//VectorXd diff = (exact_der-gradPhi).cwiseAbs();
	//double error = diff.maxCoeff();
    //cout << "max error derivative fd" << " " << error << "\n"; 
//}


BOOST_AUTO_TEST_CASE( OTHER_TEST ) {
    // TODO: additional tests 
}


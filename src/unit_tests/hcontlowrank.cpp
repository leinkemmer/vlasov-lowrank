
#define BOOST_TEST_MODULE LOWRANK
#include <boost/test/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>
#include <algorithms/lowrank-continuoussplit-h2x2v.hpp>


template<size_t d>
bool operator==(const array<int,d>& a, const array<int,d>& b) {
    bool ret = true;
    for(size_t i=0;i<d;i++)
        ret = ret && (a[i]==b[i]);
    return ret;
}

BOOST_AUTO_TEST_CASE( TENSOR3 ) {
    array<int,3> r = {3,4,2};

    // check that the range based iterator works
    int c = 0;
    for(auto i : up_to<3>(r))
        c++;
    BOOST_CHECK( c == 3*4*2 );

    Tensor3 t1(r);
    for(auto i : up_to<3>(r))
        t1(i) = 2*i[0]+3*i[1]+5*i[2];

    // check that everything is assigned ok
    for(auto i : up_to<3>(r)) {
        BOOST_CHECK_SMALL( abs(t1(i[0],i[1],i[2]) - (2*i[0]+3*i[1]+5*i[2])) , 1e-14 );
    }
    
    // check that we get the correct matrix
    for(auto i : up_to<3>(r)) {
        MatrixXd m = t1(i[0]);
        BOOST_CHECK_SMALL( m(i[1],i[2]) - (2*i[0]+3*i[1]+5*i[2]), 1e-14 );
    }

    // check permute
    array<int,3> p = permute(r, {0,1,2});
    BOOST_CHECK( (p == array<int,3>({3,4,2})) );

    p = permute(r, {2,1,0});
    BOOST_CHECK( (p == array<int,3>({2,4,3})) );

    MatrixXd m = t1.reshape21({2,1,0});
    for(auto i : up_to<3>(r))
        BOOST_CHECK_SMALL( m(i[2] + i[1]*r[2],i[0]) - (2*i[0]+3*i[1]+5*i[2]), 1e-14 );

    Tensor3 t2(r); t2.from_reshape21(m, {2,1,0});
    for(auto i : up_to<3>(r))
        BOOST_CHECK_SMALL( t1(i)-t2(i) , 1e-14 );
}

BOOST_AUTO_TEST_CASE( EMPTY_STEP ) {
    array<int,3> r = {5,12,13};
    array<int,2> n = {100,120};
    domain2d_data dd({-2.1,0.0},{5.0,6.0},n,r);
    continuoussplit_2dh cs(dd);

    lowrank2dh in(r, n), out(r, n);
    in.X.setRandom();
    rectangular_QR qr(in.X);
    in.X = qr.Q()/sqrt(dd.h[0]);

    in.Y.setRandom();
    rectangular_QR qr2(in.Y);
    in.Y = qr2.Q()/sqrt(dd.h[1]);

    MatrixXd m = MatrixXd::Random(r[0]*r[1],r[2]);
    in.S.from_reshape21(m, {0,1,2});

    // We set C1 to zero which implies that the equation
    // K' = 0 is solved. This is to check that the QR
    // decomposition and reshapes are set up correctly.
    array<MatrixXd, 2> zeroE = {MatrixXd::Zero(n[0],n[1]), MatrixXd::Zero(n[0],n[1])};
    MatrixXd zero = MatrixXd::Zero(r[0],r[0]);
    cs.Lie_step(0.1, in, out, {zero,zero}, {zero,zero}, zeroE);

    for(int i=0;i<r[0];i++)
        BOOST_CHECK_SMALL( (in.full(i)-out.full(i)).norm(), 1e-13 );

}

BOOST_AUTO_TEST_CASE( MATRIX_GENERATION_1 ) {
    domain2d_data dd({-M_PI,-1.0},{M_PI,1.0},{100,120},{2,3,4});
    continuoussplit_2dh cs(dd);

    MatrixXd c1(2,2);
    c1 <<  0.5, 1.0,
           1.5, 2.0;
    Tensor3 Q({dd.r[0],dd.r[2],dd.r[2]});
    for(auto i : up_to<3>(Q.r))
        Q(i) = 1.0+i[0]+i[1]-i[2];

    // check a1
    MatrixXd a1 = cs.compute_a1(c1, Q);

    MatrixXd a1_ex(dd.r[1],dd.r[1]);
    a1_ex << 223.0, 159.0, 95.0,
             161.0, 117.0, 73.0,
              99.0,  75.0, 51.0;

    BOOST_CHECK_SMALL( (a1_ex-a1).norm(), 1e-14 );

    // check d1
    MatrixXd Y(dd.n[1],dd.r[2]);
    set_from_func(Y, [](double y, int r) { return pow(y-0.1,r)*exp(-30*pow(y,2)); }, dd.a[1], dd.h[1]);
    MatrixXd d1 = cs.compute_d1(Y);

    MatrixXd d1_ex(dd.r[2],dd.r[2]);
    d1_ex << 0.0, 0.1144114041079711, -0.02288228082159422, 0.006292627225938412,
            -0.1144114041079711, 0.0, 0.002097542408646137, -0.0008008798287557979,
             0.02288228082159422, -0.002097542408646137, 0.0, 0.00009248255165394333,
            -0.006292627225938412, 0.0008008798287557979, -0.00009248255165394333, 0.0;

    BOOST_CHECK_SMALL( (d1_ex-d1).norm(), 1e-14 );

    // check a2
    MatrixXd a2 = cs.compute_a2(c1, Q, Y);

    MatrixXd a2_ex(dd.r[1],dd.r[1]);
    a2_ex << 0.04405649472269320, 0.4846214419496252, 0.9251863891765571,
            -0.3965084525042388, 0.04405649472269320, 0.4846214419496252,
            -0.8370733997311708, -0.3965084525042388, 0.04405649472269320;
    BOOST_CHECK_SMALL( (a2_ex-a2).norm(), 1e-14 );

    // check b2
    MatrixXd X(dd.n[0],dd.r[1]);
    set_from_func(X, [](double x, int r) { return pow(x-0.1,r)*exp(-30*pow(x,2)); }, dd.a[0], dd.h[0]);
    MatrixXd b1 = cs.compute_b1(X);

    MatrixXd b1_ex(dd.r[1],dd.r[1]);
    b1_ex << 0.0, 0.1144114041079711, -0.02288228082159422,
            -0.1144114041079711, 0.0, 0.002097542408646137,
             0.02288228082159422, -0.002097542408646137, 0.0;
    
    BOOST_CHECK_SMALL( (b1_ex-b1).norm(), 1e-14 );

    // check h1
    Tensor3 Q2({dd.r[1],dd.r[0],dd.r[2]});
    for(auto i : up_to<3>(Q2.r))
        Q2(i) = 1.0+i[0]+i[1]-i[2];

    MatrixXd h1 = cs.compute_h1(Q2, c1);

    MatrixXd h1_ex(dd.r[2], dd.r[2]);
    h1_ex << 115, 0.5*149, 34, -0.5*13,
              76, 0.5*101, 25, -0.5,
              37, 0.5*53, 16, 0.5*11,
              -2, 0.5*5, 7, 0.5*23;
    BOOST_CHECK_SMALL( (h1-h1_ex).norm(), 1e-14 );

    // check h2
    MatrixXd b(dd.r[1],dd.r[1]);
    b  << 1, -1, 2,
          -2, 3, -3,
          4, -4, 5;

    MatrixXd h2 = cs.compute_h2(Q2, c1, b);

    MatrixXd h2_ex(dd.r[2],dd.r[2]);
    h2_ex << 0.5*455, 145, 0.5*125, -20,
          0.5*315, 100, 0.5*85,  -15,
          0.5*175, 55,  0.5*45,  -10,
          0.5*35,  10,  0.5*5,   -5;
    BOOST_CHECK_SMALL( (h2-h2_ex).norm(), 1e-14 );
}

BOOST_AUTO_TEST_CASE( MATRIX_GENERATION_2 ) {
    domain2d_data dd({-M_PI,-1.0},{M_PI,1.0},{400,420},{2,3,4});
    continuoussplit_2dh cs(dd);

    // check d2
    MatrixXd Y(dd.n[1],dd.r[2]);
    set_from_func(Y, [](double y, int r) { return pow(y,r); }, dd.a[1], dd.h[1]);
    array<MatrixXd, 2> E;
    E[0].resize(dd.n[0],dd.n[1]); E[1].resize(dd.n[0],dd.n[1]);
    for(auto i : up_to<2>({dd.n[0],dd.n[1]})) {
        double x = dd.a[0] + i[0]*dd.h[0];
        double y = dd.a[1] + i[1]*dd.h[1];
        E[0](i[0],i[1]) = x*y;
        E[1](i[0],i[1]) = 1.0;
    }
    
    array<Tensor3, 2> d2 = cs.compute_d2(Y, E);

    double err = 0.0, mm = 0.0;
    for(auto i : up_to<3>({dd.r[2],dd.r[2],dd.n[0]})) {
        double x = dd.a[0] + i[2]*dd.h[0];
        int r = i[0];
        int m = i[1];

        double exact0 = (1.0+pow(-1.0,1.0+m+r))*x/(2.0+m+r);
        err = max(err, abs(d2[0](i[0],i[1],i[2])-exact0));

        double exact1 = (1+pow(-1.0,m+r))/(1.0+m+r);
        err = max(err, abs(d2[1](i[0],i[1],i[2])-exact1));

        mm = max(mm, abs(exact0));
        mm = max(mm, abs(exact1));
    }
    cout << "ERROR(MAT2): " << err/mm << " " << mm << endl;
    BOOST_CHECK_SMALL( err/mm, 0.02 );

    // check a3
    Tensor3 Q({dd.r[0],dd.r[2],dd.r[1]});
    for(auto i : up_to<3>(Q.r))
        Q(i) = i[0]+i[1]-0.5*i[2];
    MatrixXd c2x(dd.r[0],dd.r[0]);
    c2x << 1, 2,
           3, 4;
    MatrixXd c2y(dd.r[0],dd.r[0]);
    c2y << 1, -1,
           -1, 2;
    Tensor3 a3 = cs.compute_a3({c2x, c2y}, Q, Y, E);

    MatrixXd A(dd.r[1],dd.r[1]);
    A << 3016.0/105.0, 2452.0/105.0, 1888.0/105.0,
         2452.0/105.0, 2032.0/105.0, 1612.0/105.0,
         1888.0/105.0, 1612.0/105.0, 1336.0/105.0;
    MatrixXd B(dd.r[1],dd.r[1]);
    B << (4296.0)/35.0,  88.0, (1864.0)/35.0,
         (9424.0)/105.0, (2232.0)/35.0, (3968.0)/105.0,
         (1192.0)/21.0,  (1384.0)/35.0, (2344.0)/105.0;

    err=0.0, mm = 0.0;
    for(auto i : up_to<3>({dd.r[1],dd.r[1],dd.n[0]})) {
        double x = dd.a[0] + i[2]*dd.h[0];

        double exact = A(i[0],i[1]) + B(i[0],i[1])*x;

        err = max(err, abs(exact-a3(i[0],i[1],i[2])));
        mm = max(mm, abs(exact));
    }
    cout << "ERROR(MAT2.1): " << err/mm << endl;
    BOOST_CHECK_SMALL( err/mm, 0.03 );

    // check e
    MatrixXd X(dd.n[0],dd.r[1]);
    set_from_func(X, [](double x, int r) { return pow(x,r); }, dd.a[0], dd.h[0]);

    array<Tensor4, 2> e = cs.compute_e(X, d2);

    const double Pi = M_PI;
    Tensor4 e0_exact({dd.r[2],dd.r[2],dd.r[1],dd.r[1]});
    e0_exact.data = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,(4*pow(Pi,3))/9.,0,(4*pow(Pi,3))/15.,
   (4*pow(Pi,3))/9.,0,(4*pow(Pi,3))/15.,0,0,(4*pow(Pi,3))/15.,0,(4*pow(Pi,3))/21.,
   (4*pow(Pi,3))/15.,0,(4*pow(Pi,3))/21.,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   (4*pow(Pi,3))/9.,0,(4*pow(Pi,3))/15.,(4*pow(Pi,3))/9.,0,(4*pow(Pi,3))/15.,0,0,
   (4*pow(Pi,3))/15.,0,(4*pow(Pi,3))/21.,(4*pow(Pi,3))/15.,0,(4*pow(Pi,3))/21.,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,(4*pow(Pi,5))/15.,0,(4*pow(Pi,5))/25.,
   (4*pow(Pi,5))/15.,0,(4*pow(Pi,5))/25.,0,0,(4*pow(Pi,5))/25.,0,
   (4*pow(Pi,5))/35.,(4*pow(Pi,5))/25.,0,(4*pow(Pi,5))/35.,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,(4*pow(Pi,5))/15.,0,(4*pow(Pi,5))/25.,(4*pow(Pi,5))/15.,0,
   (4*pow(Pi,5))/25.,0,0,(4*pow(Pi,5))/25.,0,(4*pow(Pi,5))/35.,(4*pow(Pi,5))/25.,
   0,(4*pow(Pi,5))/35.,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

    Tensor4 e1_exact({dd.r[2],dd.r[2],dd.r[1],dd.r[1]});
    e1_exact.data = {4*Pi,0,(4*Pi)/3.,0,0,(4*Pi)/3.,0,(4*Pi)/5.,(4*Pi)/3.,0,(4*Pi)/5.,0,0,
   (4*Pi)/5.,0,(4*Pi)/7.,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,(4*pow(Pi,3))/3.,0,
   (4*pow(Pi,3))/9.,0,0,(4*pow(Pi,3))/9.,0,(4*pow(Pi,3))/15.,(4*pow(Pi,3))/9.,0,
   (4*pow(Pi,3))/15.,0,0,(4*pow(Pi,3))/15.,0,(4*pow(Pi,3))/21.,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,(4*pow(Pi,3))/3.,0,(4*pow(Pi,3))/9.,0,0,(4*pow(Pi,3))/9.,0,
   (4*pow(Pi,3))/15.,(4*pow(Pi,3))/9.,0,(4*pow(Pi,3))/15.,0,0,(4*pow(Pi,3))/15.,0,
   (4*pow(Pi,3))/21.,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,(4*pow(Pi,3))/3.,0,
   (4*pow(Pi,3))/9.,0,0,(4*pow(Pi,3))/9.,0,(4*pow(Pi,3))/15.,(4*pow(Pi,3))/9.,0,
   (4*pow(Pi,3))/15.,0,0,(4*pow(Pi,3))/15.,0,(4*pow(Pi,3))/21.,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,(4*pow(Pi,5))/5.,0,(4*pow(Pi,5))/15.,0,0,(4*pow(Pi,5))/15.,0,
   (4*pow(Pi,5))/25.,(4*pow(Pi,5))/15.,0,(4*pow(Pi,5))/25.,0,0,(4*pow(Pi,5))/25.,
   0,(4*pow(Pi,5))/35.};

    // Mathematica flattens in row-major order so we have to reverse it here
    err = 0.0;
    for(auto i : up_to<4>(e[0].r)) {
        err = max(err, abs(e[0](i)-e0_exact(i[3],i[2],i[1],i[0])));
        err = max(err, abs(e[1](i)-e1_exact(i[3],i[2],i[1],i[0])));
    }

    cout << "ERROR(MAT2.2): " << err/norm_inf(e0_exact) << endl;
    BOOST_CHECK_SMALL( err/norm_inf(e0_exact), 0.04 );

    // check E
    Tensor4 EE  = cs.compute_E({c2x, c2y}, X, Q, d2);

    Tensor4 E_exact({dd.r[1],dd.r[1],dd.r[1],dd.r[1]});
    E_exact.data = {(6032*Pi)/105.,(4904*Pi)/105.,(3776*Pi)/105.,(2864*pow(Pi,3))/35.,
   (176*pow(Pi,3))/3.,(3728*pow(Pi,3))/105.,(6032*pow(Pi,3))/315.,
   (4904*pow(Pi,3))/315.,(3776*pow(Pi,3))/315.,(4904*Pi)/105.,(4064*Pi)/105.,
   (3224*Pi)/105.,(18848*pow(Pi,3))/315.,(1488*pow(Pi,3))/35.,
   (7936*pow(Pi,3))/315.,(4904*pow(Pi,3))/315.,(4064*pow(Pi,3))/315.,
   (3224*pow(Pi,3))/315.,(3776*Pi)/105.,(3224*Pi)/105.,(2672*Pi)/105.,
   (2384*pow(Pi,3))/63.,(2768*pow(Pi,3))/105.,(4688*pow(Pi,3))/315.,
   (3776*pow(Pi,3))/315.,(3224*pow(Pi,3))/315.,(2672*pow(Pi,3))/315.,
   (2864*pow(Pi,3))/35.,(176*pow(Pi,3))/3.,(3728*pow(Pi,3))/105.,
   (6032*pow(Pi,3))/315.,(4904*pow(Pi,3))/315.,(3776*pow(Pi,3))/315.,
   (8592*pow(Pi,5))/175.,(176*pow(Pi,5))/5.,(3728*pow(Pi,5))/175.,
   (18848*pow(Pi,3))/315.,(1488*pow(Pi,3))/35.,(7936*pow(Pi,3))/315.,
   (4904*pow(Pi,3))/315.,(4064*pow(Pi,3))/315.,(3224*pow(Pi,3))/315.,
   (18848*pow(Pi,5))/525.,(4464*pow(Pi,5))/175.,(7936*pow(Pi,5))/525.,
   (2384*pow(Pi,3))/63.,(2768*pow(Pi,3))/105.,(4688*pow(Pi,3))/315.,
   (3776*pow(Pi,3))/315.,(3224*pow(Pi,3))/315.,(2672*pow(Pi,3))/315.,
   (2384*pow(Pi,5))/105.,(2768*pow(Pi,5))/175.,(4688*pow(Pi,5))/525.,
   (6032*pow(Pi,3))/315.,(4904*pow(Pi,3))/315.,(3776*pow(Pi,3))/315.,
   (8592*pow(Pi,5))/175.,(176*pow(Pi,5))/5.,(3728*pow(Pi,5))/175.,
   (6032*pow(Pi,5))/525.,(4904*pow(Pi,5))/525.,(3776*pow(Pi,5))/525.,
   (4904*pow(Pi,3))/315.,(4064*pow(Pi,3))/315.,(3224*pow(Pi,3))/315.,
   (18848*pow(Pi,5))/525.,(4464*pow(Pi,5))/175.,(7936*pow(Pi,5))/525.,
   (4904*pow(Pi,5))/525.,(4064*pow(Pi,5))/525.,(3224*pow(Pi,5))/525.,
   (3776*pow(Pi,3))/315.,(3224*pow(Pi,3))/315.,(2672*pow(Pi,3))/315.,
   (2384*pow(Pi,5))/105.,(2768*pow(Pi,5))/175.,(4688*pow(Pi,5))/525.,
   (3776*pow(Pi,5))/525.,(3224*pow(Pi,5))/525.,(2672*pow(Pi,5))/525.};
    
    // Mathematica flattens in row-major order so we have to reverse it here
    err = 0.0;
    for(auto i : up_to<4>(EE.r))
        err = max(err, abs(EE(i)-E_exact(i[3],i[2],i[1],i[0])));

    cout << "ERROR(MAT2.3): " << err/norm_inf(E_exact) << endl;
    BOOST_CHECK_SMALL( err/norm_inf(E_exact), 0.041 );

    // check b2
    array<Tensor3, 2> b2 = cs.compute_b2(X, E);
    
    err = 0.0; mm = 0.0;
    for(auto i : up_to<3>({dd.r[1],dd.r[1],dd.n[1]})) {
        double y = dd.a[1] + i[2]*dd.h[1];
        int pi = i[0];
        int delta = i[1];

        double exact0 = (1.0+pow(-1.0,1.0+delta+pi))*pow(M_PI,2+delta+pi)*y/(2+delta+pi);
        err = max(err, abs(b2[0](i[0],i[1],i[2])-exact0));

        double exact1 = (1.0+pow(-1.0,delta+pi))*pow(M_PI,1+delta+pi)/(1.0+delta+pi);
        err = max(err, abs(b2[1](i[0],i[1],i[2])-exact1));

        mm = max(mm, abs(exact0));
        mm = max(mm, abs(exact1));
    }
    cout << "ERROR(MAT2.4): " << err/mm << endl;
    BOOST_CHECK_SMALL( err/mm, 0.04 );

    // check h3
    Q.alloc({dd.r[1],dd.r[0],dd.r[2]}); // the order for Q is different here from the mathematical notation
    for(auto i : up_to<3>(Q.r))
        Q(i) = i[1]+i[0]-0.5*i[2];

    Tensor3 h3 = cs.compute_h3(Q, {c2x, c2y}, b2);

    A.resize(dd.r[2], dd.r[2]);
    A << 1505.366174318083, 1256.600217432725, 1007.834260547367, 759.0683036620085,
         1256.600217432725, 1055.510163692840, 854.4201099529544, 653.3300562130691,
         1007.834260547367, 854.4201099529544, 701.0059593585420, 547.5918087641296,
         759.0683036620085, 653.3300562130691, 547.5918087641296, 441.8535613151902;
    B.resize(dd.r[2], dd.r[2]);
    B << 11083.57290405199,  8142.549464499117, 5201.526024946241, 2260.502585393366,
         8285.628189533429, 6059.998375152116, 3834.368560770803, 1608.738746389490,
         5487.683475014866, 3977.447285805116, 2467.211096595365, 956.9749073856141,
         2689.738760496303, 1894.896196458115, 1100.053632419927, 305.2110683817381;
    
    err=0.0; mm = 0.0;
    for(auto i : up_to<3>({dd.r[2],dd.r[2],dd.n[1]})) {
        double y = dd.a[1] + i[2]*dd.h[1];

        double exact = A(i[0],i[1]) + B(i[0],i[1])*y;

        err = max(err, abs(exact-h3(i[0],i[1],i[2])));
        mm = max(mm, abs(exact));
    }
    cout << "ERROR(MAT2.5): " << err/mm << endl;
    BOOST_CHECK_SMALL( err/mm, 0.03 );

    // check G
    Tensor4 G = cs.compute_G(Q, {c2x, c2y}, e);
    Tensor4 G_exact({dd.r[2],dd.r[2],dd.r[2],dd.r[2]});
    G_exact.data = {3010.732348636167, 7389.048602701328, 1003.577449545389, 4433.429161620797, 2513.200434865450, 5428.366309666078,  837.7334782884834, 3257.019785799647, 2015.668521094734,  3467.684016630828, 671.8895070315779, 2080.610409978497,  1518.136607324017, 1507.001723595577, 506.0455357746723,  904.2010341573464, 2513.200434865450, 5523.752126355619,  837.7334782884834, 3314.251275813372, 2111.020327385680, 4039.998916768077, 703.6734424618932, 2423.999350060846, 1708.840219905909, 2556.245707180535, 569.6134066353029, 1533.747424308321, 1306.660112426138, 1072.492497592993, 435.5533708087127, 643.4954985557960, 2015.668521094734, 3658.455650009911, 671.8895070315779, 2195.073390005947, 1708.840219905909, 2651.631523870077, 569.6134066353029, 1590.978914322046, 1402.011918717084, 1644.807397730243, 467.3373062390280, 986.8844386381459, 1095.183617528259, 637.9832715904094, 365.0612058427531, 382.7899629542456, 1518.136607324017, 1793.159173664202, 506.0455357746723, 1075.895504198521, 1306.660112426138, 1263.264130972077, 435.5533708087127, 757.9584785832460, 1095.183617528259, 733.3690882799510, 365.0612058427531, 440.0214529679706, 883.7071226303803, 203.4740455878254, 294.5690408767934, 122.0844273526952, 7389.048602701328, 1003.577449545389, 4433.429161620797, 602.1464697272334, 5428.366309666078, 837.7334782884834, 3257.019785799647, 502.6400869730900, 3467.684016630828, 671.8895070315779, 2080.610409978497, 403.1337042189467, 1507.001723595577, 506.0455357746723, 904.2010341573464, 303.6273214648034, 5523.752126355619, 837.7334782884834, 3314.251275813372, 502.6400869730900, 4039.998916768077, 703.6734424618932, 2423.999350060846, 422.2040654771359, 2556.245707180535, 569.6134066353029, 1533.747424308321, 341.7680439811818, 1072.492497592993, 435.5533708087127, 643.4954985557960, 261.3320224852276, 3658.455650009911, 671.8895070315779, 2195.073390005947, 403.1337042189467, 2651.631523870077, 569.6134066353029, 1590.978914322046, 341.7680439811818, 1644.807397730243, 467.3373062390280, 986.8844386381459, 280.4023837434168, 637.9832715904094, 365.0612058427531, 382.7899629542456, 219.0367235056518, 1793.159173664202, 506.0455357746723, 1075.895504198521, 303.6273214648034, 1263.264130972077, 435.5533708087127, 757.9584785832460, 261.3320224852276, 733.3690882799510, 365.0612058427531, 440.0214529679706, 219.0367235056518, 203.4740455878254, 294.5690408767934, 122.0844273526952, 176.7414245260761, 1003.577449545389, 4433.429161620797, 602.1464697272334, 3166.735115443426, 837.7334782884834, 3257.019785799647, 502.6400869730900, 2326.442704142605, 671.8895070315779, 2080.610409978497, 403.1337042189467, 1486.150292841783, 506.0455357746723, 904.2010341573464, 303.6273214648034, 645.8578815409617, 837.7334782884834, 3314.251275813372, 502.6400869730900, 2367.322339866694, 703.6734424618932, 2423.999350060846, 422.2040654771359, 1731.428107186319, 569.6134066353029, 1533.747424308321, 341.7680439811818, 1095.533874505944, 435.5533708087127, 643.4954985557960, 261.3320224852276, 459.6396418255686, 671.8895070315779, 2195.073390005947, 403.1337042189467, 1567.909564289962, 569.6134066353029, 1590.978914322046, 341.7680439811818, 1136.413510230033, 467.3373062390280, 986.8844386381459, 280.4023837434168, 704.9174561701042, 365.0612058427531, 382.7899629542456, 219.0367235056518, 273.4214021101754, 506.0455357746723, 1075.895504198521, 303.6273214648034, 768.4967887132295, 435.5533708087127, 757.9584785832460, 261.3320224852276, 541.3989132737471, 365.0612058427531, 440.0214529679706, 219.0367235056518, 314.3010378342647, 294.5690408767934, 122.0844273526952, 176.7414245260761, 87.20316239478232, 4433.429161620797, 602.1464697272334, 3166.735115443426, 430.1046212337381, 3257.019785799647, 502.6400869730900, 2326.442704142605, 359.0286335522072, 2080.610409978497, 403.1337042189467, 1486.150292841783, 287.9526458706762, 904.2010341573464, 303.6273214648034, 645.8578815409617, 216.8766581891453, 3314.251275813372, 502.6400869730900, 2367.322339866694, 359.0286335522072, 2423.999350060846, 422.2040654771359, 1731.428107186319, 301.5743324836685, 1533.747424308321, 341.7680439811818, 1095.533874505944, 244.1200314151298, 643.4954985557960, 261.3320224852276, 459.6396418255686, 186.6657303465912, 2195.073390005947, 403.1337042189467, 1567.909564289962, 287.9526458706762, 1590.978914322046, 341.7680439811818, 1136.413510230033, 244.1200314151298, 986.8844386381459, 280.4023837434168, 704.9174561701042, 200.2874169595834, 382.7899629542456, 219.0367235056518, 273.4214021101754, 156.4548025040370, 1075.895504198521, 303.6273214648034, 768.4967887132295, 216.8766581891453, 757.9584785832460, 261.3320224852276, 541.3989132737471, 186.6657303465912, 440.0214529679706, 219.0367235056518, 314.3010378342647, 156.4548025040370, 122.0844273526952, 176.7414245260761, 87.20316239478232, 126.2438746614829};
    
    // Mathematica flattens in row-major order so we have to reverse it here
    err = 0.0;
    for(auto i : up_to<4>(G.r))
        err = max(err, abs(G(i)-G_exact(i[3],i[2],i[1],i[0])));
    cout << "ERROR(MAT2.6): " << err/norm_inf(G_exact) << endl;
}

// TODO: all tests in RHS are with a3=h3=...=0
BOOST_AUTO_TEST_CASE( RHSs ) {
    domain2d_data dd({-M_PI,0.0},{M_PI,4*M_PI},{100,120},{2,3,4});
    continuoussplit_2dh cs(dd);

    // check K
    MatrixXd K(dd.n[0], dd.r[1]);
    set_from_func(K, [](double x, int r) { return cos((r+1)*x); }, dd.a[0], dd.h[0]);

    MatrixXd a1(dd.r[1], dd.r[1]);
    a1 << 1, 2, 3,
          4, 5, 6,
          7, 8, 9;
    MatrixXd a2(dd.r[1], dd.r[1]);
    a2 = -2*a1;

    MatrixXd out(K);
    Tensor3 a3({dd.r[1],dd.r[1],dd.n[0]});
    a3.setZero();
    cs.rhs_Ktild(K, {a1, a2}, a3, out);

    MatrixXd out_ex(out);
    set_from_func(out_ex, [](double x, int r) {
            if(r==0)      return 2*(cos(x) + 2*cos(2*x) + 3*cos(3*x)) + sin(x) + 4*sin(2*x) + 9*sin(3*x);
            else if(r==1) return 2*(4*cos(x) + 5*cos(2*x) + 6*cos(3*x) + 2*sin(x) + 5*sin(2*x) + 9*sin(3*x));
            else if(r==2) return 2*(7*cos(x) + 8*cos(2*x) + 9*cos(3*x)) + 7*sin(x) + 16*sin(2*x) + 27*sin(3*x);
            else exit(1);
        }, dd.a[0], dd.h[0]);

    BOOST_CHECK_SMALL( (out_ex-out).norm(), 1e-10 );

    // check N
    MatrixXd N(dd.n[1], dd.r[2]);
    set_from_func(N, [](double y, int r) { return cos((r+1)*y); }, dd.a[1], dd.h[1]);
    
    MatrixXd h1(dd.r[2], dd.r[2]);
    h1 << 1, 2, 3, 4,
          5, 6, 7, 8,
          9, 10, 11, 12,
          13, 14, 15, 16;
    MatrixXd h2(dd.r[2], dd.r[2]);
    h2 = -2*h1;

    MatrixXd out_N(N);
    Tensor3 h3({dd.r[2],dd.r[2],dd.n[1]});
    h3.setZero();
    cs.rhs_N(N, {h1, h2}, h3, out_N);

    MatrixXd out_N_ex(out_N);
    set_from_func(out_N_ex, [](double x, int r) {
            if(r==0)   return 2*cos(x) + 4*cos(2*x) + 6*cos(3*x) + 8*cos(4*x) + sin(x) + 4*sin(2*x) + 9*sin(3*x) + 16*sin(4*x);
            else if(r==1) return 2*(5*cos(x) + 6*cos(2*x) + 7*cos(3*x) + 8*cos(4*x)) + 5*sin(x) + 12*sin(2*x) + 21*sin(3*x) + 32*sin(4*x);
            else if(r==2) return 18*cos(x) + 20*cos(2*x) + 22*cos(3*x) + 24*cos(4*x) + 9*sin(x) + 20*sin(2*x) + 33*sin(3*x) + 48*sin(4*x);
            else if(r==3) return 26*cos(x) + 28*cos(2*x) + 30*cos(3*x) + 32*cos(4*x) + 13*sin(x) + 28*sin(2*x) + 45*sin(3*x) + 64*sin(4*x);
            else exit(1);
            }, dd.a[1], dd.h[1]);
    
    BOOST_CHECK_SMALL( (out_N_ex-out_N).norm(), 1e-10 );

    // check R1
    MatrixXd b1(dd.r[1], dd.r[1]);
    b1 << 1, -1, 0.5,
          2, -2, 0.3,
          0.5, 2, 1;
         
    MatrixXd R1(dd.r[1],dd.r[1]);
    for(auto i : up_to<2>({(int)R1.rows(),(int)R1.cols()}))
        R1(i[0],i[1]) = 0.5*i[0] - i[1];

    MatrixXd out_R1(R1);
    Tensor4 EE({dd.r[1],dd.r[1],dd.r[1],dd.r[1]});
    EE.setZero();
    cs.rhs_R1(R1, {a1, a2}, b1, EE, out_R1);

    MatrixXd out_R1_ex(R1);
    out_R1_ex << 12, 25.5, 39,
                 3.4, 3.4, 3.4,
                 -12, -25.5, -39;

    BOOST_CHECK_SMALL( (out_R1_ex-out_R1).norm(), 1e-14 );
   
    // check R2
    MatrixXd d1(dd.r[2], dd.r[2]);
    d1 << 0, 1, 2, 1,
          0, 2, 1, 1,
          3, 2, 1, 0,
          1, 2, 3, 4;
          
    MatrixXd R2(dd.r[2],dd.r[2]);
    for(auto i : up_to<2>({(int)R2.rows(),(int)R2.cols()}))
        R2(i[0],i[1]) = 0.5*i[0] - i[1];

    MatrixXd out_R2(R2);
    Tensor4 G({dd.r[2],dd.r[2],dd.r[2],dd.r[2]});
    G.setZero();
    cs.rhs_R2(R2, {h1, h2}, d1, G, out_R2);

    MatrixXd out_R2_ex(R2);
    out_R2_ex << 0, 16, 32, 48,
                -15, -23, -31, -39,
                -80, -176, -272, -368,
                -90, -170, -250, -330;
    BOOST_CHECK_SMALL( (out_R2_ex-out_R2).norm(), 1e-14 );

    // check M
    MatrixXd c1x(dd.r[0],dd.r[0]);
    c1x << 0.5, 0.3,
           0.2, 0.6;
    MatrixXd c1y(dd.r[0],dd.r[0]);
    c1y = -2*c1x;

    Tensor3 M({dd.r[0], dd.r[1], dd.r[2]});
    for(auto i : up_to<3>(M.r))
        M(i) = 0.5*i[0] + 0.3*i[1] - 0.2*i[2];
    Tensor3 out_M({dd.r[0], dd.r[1], dd.r[2]});
    array<MatrixXd, 2> c2 = {MatrixXd::Zero(dd.r[0],dd.r[0]), MatrixXd::Zero(dd.r[0],dd.r[0])};
    Tensor4 e0({dd.r[1],dd.r[1],dd.r[2],dd.r[2]});
    e0.setZero();
    Tensor4 e1({dd.r[1],dd.r[1],dd.r[2],dd.r[2]});
    e1.setZero();
    cs.rhs_S(M, {c1x, c1y}, c2, b1, d1, {e0, e1}, out_M);

    MatrixXd out_ex_0(dd.r[1],dd.r[2]); out_ex_0 << -1.435, -1.035, 0.605, -3.235, 0.851, 1.219, 3.787, 1.835, 0.995, 1.875, 5.915, 6.395;
    BOOST_CHECK_SMALL( (out_M(0)-out_ex_0).norm(), 1e-14);
    MatrixXd out_ex_1(dd.r[1],dd.r[2]); out_ex_1 << -0.31, 0.09, 2.33, -0.31, 2.006, 2.374, 5.542, 4.79, 1.67, 2.55, 7.19, 8.87;
    BOOST_CHECK_SMALL( (out_M(1)-out_ex_1).norm(), 1e-14);
}

BOOST_AUTO_TEST_CASE( ADVECTION_STEP ) {
    array<int,3> r = {5,11,11};
    array<int,2> n = {100,120};
    array<double, 2> a = {-M_PI,0.0};
    array<double, 2> b = {M_PI,4*M_PI};
    double tau = 0.5;
    domain2d_data dd(a,b,n,r);
    continuoussplit_2dh cs(dd);

    lowrank2dh in(r,n), out(r,n);

    // set up a low-rank initial value
    domain_lowrank2d K0(a, b, r[1], n, lowrank_type::normalized_integral); 
    K0.init(2, [](double x, int r) { if(r==0)      return exp(-10*pow(x,2));
                                     else if(r==1) return cos(x);
                                     else exit(1);
                                    },
            [](double y, int r) { if(r==0)      return sin(y); //sin(y);
                                  else if(r==1) return exp(-12*pow(y-M_PI,2)); 
                                  else exit(1);
                                }
           );
    in.X = K0.lr.X;
    in.Y = K0.lr.V;
    // every component gets the same initial value
    for(auto i : up_to<3>(r)) {
        in.S(i) = K0.lr.S(i[1],i[2]);
    }

    // compute the approximation
    lowrank2dh work = in;
    MatrixXd zero = MatrixXd::Zero(r[0],r[0]);
    MatrixXd c1x = MatrixXd::Identity(r[0],r[0]);
    MatrixXd c1y = 0.5*MatrixXd::Identity(r[0],r[0]);
    array<MatrixXd, 2> zeroE = {MatrixXd::Zero(n[0],n[1]), MatrixXd::Zero(n[0],n[1])};
    int n_steps = 10;
    for(int i=0;i<n_steps;i++) {
        cs.Lie_step(tau/double(n_steps), work, out, {c1x, c1y}, {zero,zero}, zeroE);
        work = out;
    }

    // set up exact result
    domain_lowrank2d K1(a, b, r[1], n, lowrank_type::normalized_integral);
    K1.lr.X = out.X;
    K1.lr.V = out.Y;
    K1.lr.S = out.S(0);

    double err = error_inf(K1, [tau](double x, double y) { 
            return sin(y-0.5*tau)*exp(-10*pow(x-tau,2)) + exp(-12*pow(y-M_PI-0.5*tau,2))*cos(x-tau);
            });
/*
    { 
        NCWriter ncw("K0.nc");
        K0.write_nc(ncw, "f");
    }
    { 
        NCWriter ncw("K1.nc");
        K1.write_nc(ncw, "f");
    }
*/
    cout << "Error: " << err << endl;
    
    BOOST_CHECK_SMALL(err, 1e-4);
}


void diff_x(const Tensor3& in, Tensor3& out, double L) {
    int n = in.r[0];
    vector< complex<double> > hat(n);
    for(auto i : up_to<2>({in.r[1],in.r[2]})) {
        fft(n, &in(0,i[0],i[1]), &hat[0]);
        for(int k=0;k<n/2+1;k++) {
            double k_freq = double(k)*2.0*M_PI/L;
            hat[k] *= complex<double>(0,1)*k_freq/double(n);
        }
        inv_fft(n, &hat[0], &out(0,i[0],i[1]));
    }
}

void diff_y(const Tensor3& in, Tensor3& out, double L) {
    int n = in.r[1];
    vector< double > stage(n);
    vector< complex<double> > hat(n);
    for(auto i : up_to<2>({in.r[0],in.r[2]})) {
        for(int j=0;j<n;j++)
            stage[j] = in(i[0],j,i[1]);

        fft(n, &stage[0], &hat[0]);
        for(int k=0;k<n/2+1;k++) {
            double k_freq = double(k)*2.0*M_PI/L;
            hat[k] *= complex<double>(0,1)*k_freq/double(n);
        }
        inv_fft(n, &hat[0], &stage[0]);

        for(int j=0;j<n;j++)
            out(i[0],j,i[1]) = stage[j];
    }
}


BOOST_AUTO_TEST_CASE( ADVECTION_STEP_MIXED ) {

    array<int,3> r = {2,5,5};
    array<int,2> n = {100,120};
    array<double, 2> a = {-M_PI,0.0};
    array<double, 2> b = {M_PI,4*M_PI};
    double tau = 0.3;
    domain2d_data dd(a,b,n,r);
    continuoussplit_2dh cs(dd);
    lowrank2dh in(r,n), out(r,n);


    MatrixXd c1x = MatrixXd::Identity(r[0],r[0]) + 0.2*MatrixXd::Random(r[0],r[0]);
    MatrixXd c1y = MatrixXd::Identity(r[0],r[0]) + 0.2*MatrixXd::Random(r[0],r[0]);
    MatrixXd c2x = MatrixXd::Identity(r[0],r[0]) + 0.3*MatrixXd::Random(r[0],r[0]);
    MatrixXd c2y = MatrixXd::Identity(r[0],r[0]) + 0.3*MatrixXd::Random(r[0],r[0]);
    array<MatrixXd, 2> E = {MatrixXd::Zero(n[0],n[1]), MatrixXd::Zero(n[0],n[1])};
    for(auto i : up_to<2>({n[0],n[1]})) {
        double x = dd.a[0] + i[0]*dd.h[0];
        double y = dd.a[1] + i[1]*dd.h[1];
        E[0](i[0],i[1]) = cos(x)+sin(y);
        E[1](i[0],i[1]) = sin(x)+cos(y);
    }

    // set up a low-rank initial value
    domain_lowrank2d K0(a, b, r[1], n, lowrank_type::normalized_integral); 
    K0.init(2,
           [](double x, int r) { 
               if(r==0)      return exp(-10*pow(x,2));
               else if(r==1) return cos(x);
               else exit(1);
           },
           [](double y, int r) {
               if(r==0)      return sin(y);
               else if(r==1) return exp(-12*pow(y-M_PI,2)); 
               else exit(1);
           });
    in.X = K0.lr.X;
    in.Y = K0.lr.V;
    // every component gets the same initial value
    for(auto i : up_to<3>(r)) {
        in.S(i) = K0.lr.S(i[1],i[2]);
    }

    // compute the approximation
    lowrank2dh work = in;
    int n_steps = 10;
    for(int i=0;i<n_steps;i++) {
        cs.Lie_step(tau/double(n_steps), work, out, {c1x, c1y}, {c2x, c2y}, E);
        work = out;
    }

    // check that X and Y are orthogonal
    MatrixXd XX = cs.dot(out.X, out.X, dd.h[0]);
    BOOST_CHECK_SMALL( (XX-MatrixXd::Identity(XX.rows(),XX.cols())).norm() , 1e-14 );
    MatrixXd YY = cs.dot(out.Y, out.Y, dd.h[1]);
    BOOST_CHECK_SMALL( (YY-MatrixXd::Identity(YY.rows(),YY.cols())).norm() , 1e-14 );

    // compute a reference solution
    Tensor3 tin({dd.n[0],dd.n[1],dd.r[0]});
    for(auto i : up_to<3>(tin.r)) {
        double x = dd.a[0] + i[0]*dd.h[0];
        double y = dd.a[1] + i[1]*dd.h[1];
        tin(i) = exp(-10*pow(x,2))*sin(y) + cos(x)*exp(-12*pow(y-M_PI,2));
    }
    Tensor3 tout(tin.r);
    integrate_tensor3([c1x,c1y,c2x,c2y,dd,E](const Tensor3& in, Tensor3& out) {
                out.alloc(in.r);
                out.setZero();

                Tensor3 tmp(in.r);
                diff_x(in, tmp, dd.b[0]-dd.a[0]);
                for(auto i : up_to<3>(in.r))
                    for(int beta=0;beta<dd.r[0];beta++)
                        out(i) -= c1x(i[2],beta)*tmp(i[0],i[1],beta);
               
                diff_y(in, tmp, dd.b[1]-dd.a[1]);
                for(auto i : up_to<3>(in.r))
                    for(int beta=0;beta<dd.r[0];beta++)
                        out(i) -= c1y(i[2],beta)*tmp(i[0],i[1],beta);

                for(auto i : up_to<3>(in.r))
                    for(int beta=0;beta<dd.r[0];beta++)
                        out(i) -= (c2x(i[2],beta)*E[0](i[0],i[1]) + c2y(i[2],beta)*E[1](i[0],i[1]))*in(i[0],i[1],beta);
            }, tin, tout, tau);

    MatrixXd cmp(dd.n[0],dd.n[1]);
    for(auto i : up_to<2>({dd.n[0],dd.n[1]}))
        cmp(i[0],i[1]) = tout(i[0],i[1],0);
    double err = error_inf(out.full(0), cmp);

    
    cout << "Error: " << err << endl;
    BOOST_CHECK_SMALL(err, 1e-3);
}

BOOST_AUTO_TEST_CASE( BASICS_4D ) {
    // check init function
    domain2d_data ddX({-M_PI,0.0},{M_PI,4*M_PI},{50,40},{3,4,4});
    domain2d_data ddV({-6.0,-8.0},{6.0,8.0},{50,40},{3,5,5});
    domain_lowrank4d in(ddX,ddV);

    in.init(1, [](double x, int r) { return cos(x); },
               [](double y, int r) { return sin(y); },
            1, [](double v, int r) { return exp(-3*v*v); },
               [](double w, int r) { return exp(-2*w*w); });

    double err = error_inf(in, [](double x, double y, double v, double w) {
                return cos(x)*sin(y)*exp(-3*v*v)*exp(-2*w*w);
            });
    BOOST_CHECK_SMALL( err, 1e-14 );

    // check orthogonal
    MatrixXd DDx(ddX.r[0],ddX.r[0]);
    MatrixXd DDv(ddV.r[0],ddV.r[0]);
    for(auto i : up_to<2>({ddX.r[0],ddX.r[0]}))
        DDx(i[0],i[1]) = dot(in.lr.X, i[0], in.lr.X, i[1]);
    for(auto i : up_to<2>({ddX.r[0],ddX.r[0]}))
        DDv(i[0],i[1]) = dot(in.lr.V, i[0], in.lr.V, i[1]);
    BOOST_CHECK_SMALL( (DDx-MatrixXd::Identity(ddX.r[0],ddX.r[0])).norm(), 1e-14 );
    BOOST_CHECK_SMALL( (DDv-MatrixXd::Identity(ddV.r[0],ddV.r[0])).norm(), 1e-14 );

    // check QR_2dh
    lowrank2dh XX = in.lr.X;
    int r = XX.S.r[0];
    for(auto i : up_to<3>(XX.S.r))
        XX.S(i) = 0.001*(rand()%1000);
    MatrixXd D(r,r);
    for(auto i : up_to<2>({r,r}))
        D(i[0],i[1]) = dot(XX, i[0], XX, i[1]);
    lowrank2dh Q = XX;
    MatrixXd R(r,r);
    QR_2dh(XX, Q, R);
    for(auto i : up_to<2>({r,r}))
        D(i[0],i[1]) = dot(Q, i[0], Q, i[1]);

    // check orthogonal
    BOOST_CHECK_SMALL( (D-MatrixXd::Identity(r,r)).norm(), 1e-14 );

    lowrank2dh out = Q;
    out.S.setZero();
    for(auto i : up_to<3>(out.S.r))
        for(int gamma=0;gamma<r;gamma++)
            out.S(i) += Q.S(gamma,i[1],i[2])*R(gamma,i[0]);

    // check error
    for(int i=0;i<r;i++)
        BOOST_CHECK_SMALL( (out.full(i)-XX.full(i)).norm(), 1e-14 );

    // check electric field
    continuoussplit_4dh cs(ddX, ddV);

    in.init(1, [](double x, int r) { return 1.0+cos(3*x); },
               [](double y, int r) { return 1.0+sin(2*y); },
            1, [](double v, int r) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); },
               [](double w, int r) { return exp(-0.5*w*w)/sqrt(2.0*M_PI); });

    MatrixXd rho = cs.rho(in.lr);
    err = 0.0;
    for(auto i : up_to<2>({ddX.n[0],ddX.n[1]})) {
        double x = ddX.a[0] + i[0]*ddX.h[0];
        double y = ddX.a[1] + i[1]*ddX.h[1];
        err = max(err, abs(rho(i[0],i[1]) - (cos(3*x) + sin(2*y) + cos(3*x)*sin(2*y))) );
    }
    cout << "Err(rho): " << err << endl;
    BOOST_CHECK_SMALL( err, 1e-8 );


    array<MatrixXd, 2> E = cs.electric_field(in.lr);
    err = 0.0;
    for(auto i : up_to<2>({ddX.n[0],ddX.n[1]})) {
        double x = ddX.a[0] + i[0]*ddX.h[0];
        double y = ddX.a[1] + i[1]*ddX.h[1];
        err = max(err, abs(E[0](i[0],i[1]) - (1.0/3.0*sin(3*x) + 3.0/(9.0+4.0)*sin(3*x)*sin(2*y))) );
        err = max(err, abs(E[1](i[0],i[1]) - (-1.0/2.0*cos(2*y) - 2.0/(9.0+4.0)*cos(3*x)*cos(2*y))) );
    }

    cout << "ERR(E): " << err << endl;
    BOOST_CHECK_SMALL( err, 1e-8 );

    // electric energy
    double ee = cs.electric_field_energy(in.lr);
    BOOST_CHECK_SMALL( abs(ee-187.0*pow(M_PI,2)/234.0), 5e-8 );

    // check compute_C1D1
    domain_lowrank2d VV(ddV.a, ddV.b, ddV.r[1], ddV.n, lowrank_type::normalized_integral); 
    VV.init(5, [](double v, int r) { return pow(v,r)*exp(-2*pow(v-0.3,2)); },
               [](double w, int r) { return pow(w,r)*exp(-3*pow(w-0.2,2)); }
           );

    lowrank2dh V(ddV.r, ddV.n);
    V.X = VV.lr.X;
    V.Y = VV.lr.V;
    for(auto i : up_to<3>(ddV.r))
        V.S(i) = double(i[0]+1)*VV.lr.S(i[1],i[2]);
    
    array<MatrixXd, 2> c1d1 = cs.compute_C1D1(V, ddV, cs.f_v);
    for(auto i : up_to<2>({ddV.r[0],ddV.r[0]})) {
        BOOST_CHECK_SMALL( abs(c1d1[0](i[0],i[1]) - (i[0]+1.0)*(i[1]+1.0)*0.330845), 5e-5 );
        BOOST_CHECK_SMALL( abs(c1d1[1](i[0],i[1]) - (i[0]+1.0)*(i[1]+1.0)*0.239828), 2e-4 );
    }
    
    // check compute_C2D2
    VV.init([](double v) { return exp(-2*pow(v+0.3,2)); },
            [](double w) { return exp(-3*pow(w-0.2,2)); }
           );
    V.X = VV.lr.X;
    V.Y = VV.lr.V;
    for(auto i : up_to<3>(ddV.r))
        V.S(i) = double(i[0]+1)*VV.lr.S(i[1],i[2]);
    
    array<MatrixXd, 2> c2d2 = cs.compute_C2D2(V, ddV);
    for(auto i : up_to<2>({ddV.r[0],ddV.r[0]})) {
        BOOST_CHECK_SMALL( abs(c2d2[0](i[0],i[1])), 5e-5 );
        BOOST_CHECK_SMALL( abs(c2d2[1](i[0],i[1])), 2e-4 );
    }
}


#define BOOST_TEST_MODULE LOWRANK
#include <boost/test/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>
#include <generic/common.hpp>
#include <container/domain-lowrank-1x1v.hpp>
#include <algorithms/lowrank-splitting.hpp>
#include <generic/netcdf.hpp>

BOOST_AUTO_TEST_CASE( INIT ) {

    domain_lowrank2d f({0.0,-6.0},{2*M_PI,6.0},10,{12,10}, lowrank_type::normalized_matrix);
    f.init([](double x) { return cos(x); }, 
           [](double v) { return exp(-v*v); }
          );

    // check that representation is orthogonal
    int r = f.lr.X.cols();
    BOOST_CHECK_SMALL( error_inf(f.lr.X.transpose()*f.lr.X, MatrixXd::Identity(r,r)), 1e-14 );

    // check that representation is accurate
    double err_f = error_inf(f, [](double x, double v) { return cos(x)*exp(-v*v); });
    BOOST_CHECK_SMALL(err_f, 1e-14);

    // check that error_inf works
    MatrixXd f_full = f.full();
    double self_err = error_inf(f_full, f_full);
    BOOST_CHECK_SMALL(self_err, 1e-14);

    // Now for a rank 2 initialization
    f.init(2,
           [](double x, int k) { return cos((k+1)*x); },
           [](double v, int k) { return exp(-v*v)*pow(v,k); }
          );

    // check that representation is orthogonal
    BOOST_CHECK_SMALL( error_inf(f.lr.X.transpose()*f.lr.X, MatrixXd::Identity(r,r)), 1e-14 );

    // check that the representation is accurate
    err_f = error_inf(f, [](double x, double v) { return cos(x)*exp(-v*v) + cos(2*x)*v*exp(-v*v); });
    BOOST_CHECK_SMALL(err_f, 1e-14);
}

BOOST_AUTO_TEST_CASE( QR ) {
    MatrixXd A = MatrixXd::Random(10, 3);
    rectangular_QR qr(10, 3);
    qr.compute(A);

    MatrixXd QR = qr.Q()*qr.R();
    BOOST_CHECK_SMALL(error_inf(QR, A), 1e-14);
}

struct lowrank_update_zero : lowrank_update_base {

    void DeltafV(lowrank2d& in, MatrixXd& DfV) {
        DfV.setZero();
    }

    void UDeltaf(lowrank2d& in, MatrixXd& U1, MatrixXd& U1TDf) {
        U1TDf.setZero();
    }
};

struct lowrank_update_translate : lowrank_update_base {
    lowrank2d orig, shift;
    double alpha, h;

    void set(double _alpha, double _h) {
        alpha = _alpha;
        h = _h;
    }

    lowrank_update_translate(int r, array<int,2> extension) : orig(r,extension), shift(r,extension) {}

    void DeltafV(lowrank2d& in, MatrixXd& DfV) {
        int r = in.S.rows();
        orig = in; shift = in;

        // Construct a matrix that is shifted by one grid point to the left
        for(int j=0;j<r;j++) {
            for(int i=0;i<in.X.rows();i++) {
                int ishift = (i-1>=0) ? i-1 : in.X.rows()-1;
                shift.X(i,j) = in.X(ishift,j);
            }
        }

        // add the alpha*v
        //for(int j=0;j<r;j++) {
        //for(int i=0;i<in.V.rows();i++) {
        //double v = i*h; // v always start at 0 here
        //orig.V(i,j)  *= v;
        //shift.V(i,j) *= v;
        //}
        //}

        // Depending on how Eigen does that this might be expensive
        // TODO: enforce the order of the matrix-matrix products
        // Deltaf = (1-alpha)*original + alpha*shifted - original
        DfV = -alpha*orig.X*orig.S*orig.V.transpose()*in.V
            + alpha*shift.X*shift.S*shift.V.transpose()*in.V;
    }

    void UDeltaf(lowrank2d& in, MatrixXd& U1, MatrixXd& U1TDf) {
        U1TDf = -alpha*U1.transpose()*orig.X*orig.S*orig.V.transpose()
            + alpha*U1.transpose()*shift.X*shift.S*shift.V.transpose(); 
    }
};

BOOST_AUTO_TEST_CASE( LOWRANK_UPDATE ) {
    int nx(420), nv(400), r(10);
    domain_lowrank2d in({0.0,0.0}, {2*M_PI, 6.0}, r, {nx,nv}, lowrank_type::normalized_matrix);
    domain_lowrank2d out({0.0,0.0}, {2*M_PI, 6.0}, r, {nx,nv}, lowrank_type::normalized_matrix);
    lowrank_splitting lrs(nx, nv, r);
    lowrank_update_zero lruz;

    in.lr.X = MatrixXd::Random(nx, r);
    in.lr.V = MatrixXd::Random(nv, r);
    in.lr.S = MatrixXd::Random(r, r);
    in.init([](double x) { return cos(x); },
            [](double v) { return exp(-v*v); }
           );

    lrs(in.lr, out.lr, &lruz);

    BOOST_CHECK_SMALL( error_inf(in.full(), out.full()), 1e-14);
    BOOST_CHECK_SMALL( error_inf(out, [](double x, double v) { return cos(x)*exp(-v*v); }), 1e-14);

    double alpha = 0.01;
    in.init([](double x) { return exp(-10*pow(x-M_PI,2)); },
            [](double v) { return 1.0; }
           );
    lowrank_update_translate lrut(r, {nx,nv});

    {
        NCWriter ncw("f0.nc");
        in.write_nc(ncw, "f");
    }

    int n_steps = 1000; // must be even
    double hx    = 2*M_PI/double(nx);
    lrut.set(alpha, hx);
    for(int k=0;k<n_steps/2;k++) {
        lrs(in.lr, out.lr, &lrut);
        lrs(out.lr, in.lr, &lrut);
    }

    {
        NCWriter ncw("f1.nc");
        in.write_nc(ncw, "f");
    }

    double dist  = n_steps*alpha*hx;
    double err   = error_inf(in, [dist](double x, double v) { return exp(-10*pow(x-M_PI-dist,2)); } );
    cout << "ERROR: " << err << endl;
    BOOST_CHECK_SMALL( err, 4e-2 );
}


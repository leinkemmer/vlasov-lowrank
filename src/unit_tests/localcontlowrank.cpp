
#define BOOST_TEST_MODULE LOWRANK
#include <boost/test/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>
#include <algorithms/lowrank-localcontinuoussplit-1x1v.hpp>


//
// check various smaller functions
//
void check_patched_electric_field(array<int,2> patches) {
    int r = 10;
    array<double,2> a = {0.0,-6.0};
    array<double,2> b = {4*M_PI,6.0};
    array<int,2> ext = {128,64};

    // init
    domain_lowrank2d_patches in(patches[0],patches[1]);
    vlasov_cs_patches vcs(patches[0],patches[1]);
    for(auto pid : up_to<2>(patches)) {

        array<double,2> la, lb;
        array<int,2> lext;
        patched_global_to_local(pid, patches, a, b, ext, la, lb, lext);
        
        in(pid[0],pid[1]).reset(new domain_lowrank2d(la, lb, r, lext, lowrank_type::normalized_integral));
        in(pid[0],pid[1])->init(1,
                [](double x, int r) { return 1.0 + cos((r+1)*x); },
                [](double v, int r) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); });
        
        vcs(pid[0],pid[1]).reset(new vlasov_continuoussplit(r, lext, la, lb));
    }

    // check electric field
    VectorXd rho = patched_rho(vcs, in, ext[0]);
    double err = 0.0;
    for(int i=0;i<rho.size();i++) {
        double x = a[0] + i*(b[0]-a[0])/double(ext[0]);
        double rho_exact = cos(x);
        err = max(err, abs(rho[i] - rho_exact));
    }
    BOOST_CHECK_SMALL( err, 1e-8 );

    VectorXd E(ext[0]);
    electric_field(rho, E, b[0]-a[0]);
    err = 0.0;
    for(int i=0;i<E.size();i++) {
        double x = a[0] + i*(b[0]-a[0])/double(ext[0]);
        double E_exact = sin(x);
        err = max(err, abs(E[i] - E_exact));
    }
    BOOST_CHECK_SMALL( err, 1e-8 );

    // check get_local_e
    err = 0.0;
    VectorXd global_E(ext[0]);
    for(int i=0;i<patches[0];i++) {
        int nx = in(0,0)->lr.X.rows();
        global_E.segment(i*nx,nx) = get_local_E(E,i,nx);
    }
    for(int i=0;i<global_E.size();i++) {
        double x = a[0] + i*(b[0]-a[0])/double(ext[0]);
        double E_exact = sin(x);
        err = max(err, abs(global_E[i] - E_exact));
    }
    BOOST_CHECK_SMALL( err, 1e-8 );

}

BOOST_AUTO_TEST_CASE( patched_electric_field ) {
    check_patched_electric_field({1,1});
    check_patched_electric_field({1,2});
    check_patched_electric_field({1,4});
    check_patched_electric_field({2,1});
    check_patched_electric_field({2,2});
    check_patched_electric_field({2,4});
    check_patched_electric_field({4,1});
    check_patched_electric_field({4,2});
    check_patched_electric_field({4,4});
}

void check_patched_BC(bool identical_vspace) {
    int r = 10;
    array<double,2> a = {0.0,-6.0};
    array<double,2> b = {4*M_PI,6.0};
    array<int,2> ext = {64,128};

    domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral);
    in.init(3,
            [](double x, int r) { return cos((r+1)*x); },
            [](double v, int r) { return pow(v,r)*exp(-0.5*v*v)/sqrt(2.0*M_PI); });
    MatrixXd K = in.lr.X*in.lr.S;
    MatrixXd L = in.lr.V*in.lr.S.transpose();
    
    domain_lowrank2d adjl(a, b, r, ext, lowrank_type::normalized_integral);
    adjl.init(3,
            [](double x, int r) { return cos((r+1)*x); },
            [identical_vspace](double v, int r) {
            return (identical_vspace ? 1.0 : pow(v,r))*exp(-0.5*v*v)/sqrt(2.0*M_PI);
            });
    MatrixXd Kadjl = adjl.lr.X*adjl.lr.S;
    MatrixXd Ladjl = adjl.lr.V*adjl.lr.S.transpose();

    domain_lowrank2d adjr(a, b, r, ext, lowrank_type::normalized_integral);
    adjr.init(3,
            [](double x, int r) { return cos((r+1)*x); },
            [identical_vspace](double v, int r) {
            return (identical_vspace ? 1.0 : pow(v,r))*exp(-0.5*v*v)/sqrt(2.0*M_PI);
            });
    MatrixXd Kadjr = adjr.lr.X*adjr.lr.S;
    MatrixXd Ladjr = adjr.lr.V*adjr.lr.S.transpose();


    MatrixXd bdry_v = patched_vBC(in, adjl, adjr);
    MatrixXd bdry_x = patched_xBC(in, adjl, adjr);

    double err1=0.0, err2=0.0, err3=0.0, err4=0.0;
    // This check only works since the basis of adjl and adjr is a superset
    // of the basis of in.
    for(int i=0;i<r;i++) {
        err1 = max(err1, abs(bdry_v(0,i) - Ladjl(ext[1]-1,i)));
        err2 = max(err2, abs(bdry_v(1,i) - Ladjr(0,i)));

        err3 = max(err3, abs(bdry_x(0,i) - Kadjl(ext[0]-1,i)));
        err4 = max(err4, abs(bdry_x(1,i) - Kadjr(0,i)));
    }

    BOOST_CHECK_SMALL( err1, 1e-14 );
    BOOST_CHECK_SMALL( err2, 1e-14 );
    BOOST_CHECK_SMALL( err3, 1e-14 );
    BOOST_CHECK_SMALL( err4, 1e-14 );
}

BOOST_AUTO_TEST_CASE( patched_BC ) {
    cout << "checking with identical_vspace=true" << endl;
    check_patched_BC(true);
    cout << "checking with identical_vspace=false" << endl;
    check_patched_BC(false);
}

BOOST_AUTO_TEST_CASE( patched_BC_different ) {
    int r = 10;
    array<double,2> a = {0.0,-6.0};
    array<double,2> b = {4*M_PI,6.0};
    array<int,2> ext = {1024,1024};

    array<int,2> patches = {3,3};
    array<double,2> _a = {a[0]-(b[0]-a[0]), a[1]-(b[1]-a[1])};
    array<double,2> _b = {b[0]+(b[0]-a[0]), b[1]+(b[1]-a[1])};
    array<int,2>   _ext = {ext[0]*3, ext[1]*3};

    domain_lowrank2d_patches in(patches[0],patches[1]);
    domain_lowrank2d_patches out(patches[0],patches[1]);
    for(auto pid : up_to<2>(patches)) {

        array<double,2> la, lb;
        array<int,2> lext;
        patched_global_to_local(pid, patches, _a, _b, _ext, la, lb, lext);
        cout << pid << " " << la << " " << lb << " " << lext << endl;

        in(pid[0],pid[1]).reset(new domain_lowrank2d(la, lb, r, lext, lowrank_type::normalized_integral));
        out(pid[0],pid[1]).reset(new domain_lowrank2d(la, lb, r, lext, lowrank_type::normalized_integral));
    }

    auto& lin   = in(1,1);
    auto& left  = in(0,1);
    auto& right = in(2,1);
    lin->init(1,
            [](double x, int) { return 1.0+0.5*cos(0.5*x); },
            [](double v, int) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); }
            );

    // left and right are constructed such that they approximate the same function
    // as lin but the basis is different.
    left->init(2,
            [](double x, int k) { return 1.0 + 0.5*cos(0.5*x); },
            [](double v, int k) { return (k==0) ? (1.0+v)*exp(-0.5*v*v)/sqrt(2.0*M_PI) : -v*exp(-0.5*v*v)/sqrt(2.0*M_PI); }
            );
    right->init(2,
            [](double x, int k) { return 1.0 + 0.5*cos(0.5*x); },
            [](double v, int k) { return (k==0) ? (1.0+v)*exp(-0.5*v*v)/sqrt(2.0*M_PI) : -v*exp(-0.5*v*v)/sqrt(2.0*M_PI); }
            );

    auto& down = in(1,0);
    auto& up   = in(1,2);
    // up and down are constructed such that they approximate the same function
    // as lin but the basis is different.
    down->init(2,
            [](double x, int k) { return (k==0) ? 1.0 : 0.5*cos(0.5*x); },
            [](double v, int k) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); }
            );
    up->init(2,
            [](double x, int k) { return (k==0) ? 1.0 : 0.5*cos(0.5*x); },
            [](double v, int k) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); }
            );

    MatrixXd bdryx = patched_xBC(*lin, *left, *right);
    MatrixXd bdryv = patched_vBC(*lin, *down, *up);

    // Here we check if the computed boundary value is close to the value at the
    // first/last grid point. This error converges to zero as O(h).
    // Note that we can not directly compare the boundary values to K_left or
    // K_right as the basis in V is different.
    double err1=0.0, err2=0.0, err3=0.0, err4=0.0;
    for(int i=0;i<r;i++) {
        err1 = max(err1, abs(bdryx(0,i) - (lin->lr.X*lin->lr.S)(0,i)));
        err2 = max(err2, abs(bdryx(1,i) - (lin->lr.X*lin->lr.S)(ext[0]-1,i)));

        err3 = max(err3, abs(bdryv(0,i) - (lin->lr.V*lin->lr.S.transpose())(0,i)));
        err4 = max(err4, abs(bdryv(1,i) - (lin->lr.V*lin->lr.S.transpose())(ext[1]-1,i)));
    }

    BOOST_CHECK_SMALL( err1, 1e-5 );
    BOOST_CHECK_SMALL( err2, 1e-5 );
    BOOST_CHECK_SMALL( err3, 2e-9 );
    BOOST_CHECK_SMALL( err4, 2e-9 );
}

BOOST_AUTO_TEST_CASE( patched_coefficients ) {
    array<double,4> tol0 = {1e-2, 5e-2, 4e-2, 2e-2};

    // check the finite difference way to compute C2
    for(int fac_n=1;fac_n<=8;fac_n*=2) {

        int nx=100, nv=220*fac_n, r=2;
        MatrixXd V(nv,r);
        vlasov_continuoussplit vcs(r, {nx,nv}, {0.0,-6.0}, {2*M_PI, 6.0});

        for(int k=0;k<r;k++) {
            for(int i=0;i<nv;i++) {
                double v = vcs.v(i);
                V(i,k) = pow(1+0.1*v,k+1);
            }
        }

        MatrixXd bdry;
        MatrixXd C2 = vcs.compute_C2(V, &bdry);

        BOOST_CHECK_SMALL(C2(0,0) - 6./5., tol0[0]/double(fac_n));
        BOOST_CHECK_SMALL(C2(1,1) - 408./125., tol0[1]/double(fac_n));
        BOOST_CHECK_SMALL(C2(0,1) - 336./125., tol0[2]/double(fac_n));
        BOOST_CHECK_SMALL(C2(1,0) - 168./125., tol0[3]/double(fac_n));
    }

    tol0 = {3e-3, 2e-1, 3e-2, 2e-2};
    // check the finite difference way to compute D2
    for(int fac_n=1;fac_n<=16;fac_n*=2) {
        int nx=200*fac_n, nv=120, r=2;
        MatrixXd X(nx,r);
        vlasov_continuoussplit vcs(r, {nx,nv}, {0.0,-6.0}, {2*M_PI, 6.0});

        for(int i=0;i<nx;i++) {
            double x = vcs.x(i);
            for(int k=0;k<r;k++)
                X(i,k) = 0.1*(1.0+pow(x,k+1));
        }

        MatrixXd bdry;
        MatrixXd D2 = vcs.compute_D2(X, &bdry);

        BOOST_CHECK_SMALL(D2(0,0) - (M_PI+pow(M_PI,2))/50., tol0[0]/double(fac_n));
        BOOST_CHECK_SMALL(D2(1,1) - (pow(M_PI,2)+2.0*pow(M_PI,4))/25., tol0[1]/double(fac_n));
        BOOST_CHECK_SMALL(D2(0,1) - (pow(M_PI,2)/25.+4.*pow(M_PI,3)/75.), tol0[2]/double(fac_n));
        BOOST_CHECK_SMALL(D2(1,0) - (M_PI/50.+2.*pow(M_PI,3)/75.), tol0[3]/double(fac_n));
    }
}

//
// some helper functions
//
void init_lr(domain_lowrank2d* in, int r) {
    in->init(r,
            [](double x, int k) {
            if(k==0)
            return 1e-10; // constant
            else if((k-1)%2 == 0)
            return ((k==1)+1e-10)*cos(0.5*((k-1)/2+1)*x);
                else
                    return 1e-10*sin(0.5*((k-1)/2+1)*x);
            },
            [](double v, int k) { return pow(v,k)*exp(-0.5*pow(v,2))/sqrt(2.0*M_PI); });

    MatrixXd randm(r,r);
    rectangular_QR qr(randm);
    MatrixXd randorth = qr.Q();

    in->lr.S = in->lr.S*randorth;
    in->lr.V = in->lr.V*randorth;
}

void init_patches(vlasov_cs_patches& vcs, domain_lowrank2d_patches& in, domain_lowrank2d_patches& out,
        int r, array<double,2> a, array<double,2> b, array<int,2> patches, array<int,2> ext) {

    for(auto pid : up_to<2>(patches)) {

        array<double,2> la, lb;
        array<int,2> lext;
        patched_global_to_local(pid, patches, a, b, ext, la, lb, lext);

        in(pid[0],pid[1]).reset(new domain_lowrank2d(la, lb, r, lext, lowrank_type::normalized_integral));
        out(pid[0],pid[1]).reset(new domain_lowrank2d(la, lb, r, lext, lowrank_type::normalized_integral));

        init_lr(in(pid[0],pid[1]).get(), r);

        auto& lvcs = vcs(pid[0],pid[1]);
        lvcs.reset(new vlasov_continuoussplit(r, lext, la, lb));
    }
}
    
void write_matrix(string fn, const MatrixXd& m) {
    ofstream fs(fn.c_str());
    for(int i=0;i<m.rows();i++) {
        for(int j=0;j<m.cols();j++)
            fs << m(i,j) << " ";
        fs << endl;
    }
}

MatrixXd matrix_from_patches(const domain_lowrank2d_patches& in, array<int,2> ext) {
    array<int,2> patches = {in.rows(), in.cols()};

    MatrixXd m(ext[0],ext[1]);
    for(auto pid : up_to<2>(patches)) {
        auto& lin  = in(pid[0],pid[1]);

        int startx  = pid[0]*ext[0]/patches[0];
        int lengthx = ext[0]/patches[0];
        int startv  = pid[1]*ext[1]/patches[1];
        int lengthv = ext[1]/patches[1];

        MatrixXd lm = lin->full();
        m.block(startx,startv,lengthx,lengthv) = lm;
    }
    return m;
}

//
// check the rhs_S function
//
MatrixXd patched_check_rhs_S(array<int,2> patches, array<int,2> ext) {
    int r = 10;
    array<double,2> a = {0.0,-6.0};
    array<double,2> b = {4*M_PI,6.0};

    domain_lowrank2d_patches in(patches[0],patches[1]);
    domain_lowrank2d_patches out(patches[0],patches[1]);
    vlasov_cs_patches vcs(patches[0],patches[1]);
    init_patches(vcs, in, out, r, a, b, patches, ext);

    VectorXd E(ext[0]);
    for(int i=0;i<ext[0];i++) {
        double x = a[0] + i*(b[0]-a[0])/double(ext[0]);
        E(i) = cos(x);
    }

    for(auto pid : up_to<2>(patches)) {
        auto& lvcs = vcs(pid[0],pid[1]);
        auto& lin  = in(pid[0],pid[1]);
        auto& lout = out(pid[0],pid[1]);

        MatrixXd bdryx, bdryv;

        if(patches[0]==1 && patches[1]==1) {
            MatrixXd C1 = lvcs->compute_C1(lin->lr.V);
            MatrixXd C2 = lvcs->compute_C2(lin->lr.V);
            MatrixXd D2 = lvcs->compute_D2(lin->lr.X);

            lvcs->rhs_S(lin->lr.S, lin->lr.V, lin->lr.X, lout->lr.S, C1, C2, D2, &E);
        } else {
            MatrixXd C1 = lvcs->compute_C1(lin->lr.V);
            MatrixXd C2 = lvcs->compute_C2(lin->lr.V, &bdryv);
            MatrixXd D2 = lvcs->compute_D2(lin->lr.X, &bdryx);

            VectorXd lE = get_local_E(E, pid[0], lin->lr.X.rows());
            lvcs->rhs_S(lin->lr.S, lin->lr.V, lin->lr.X, lout->lr.S, C1, C2, D2, &lE);
        }
    }

    for(auto pid : up_to<2>(patches)) {
        auto& lin  = in(pid[0],pid[1]);
        auto& lout = out(pid[0],pid[1]);
        lin->lr.S = lout->lr.S;
    }
    return matrix_from_patches(in, ext);
}

BOOST_AUTO_TEST_CASE( patched_rhs_S ) {
    MatrixXd m_ref = patched_check_rhs_S({1,1},{256,256}); // reference with standard Lie splitting function
    MatrixXd m = patched_check_rhs_S({8,1},{256,256});
    write_matrix("m_ref.data", m_ref);
    write_matrix("m_pat.data", m);
    write_matrix("m_diff.data", m-m_ref);

    array<double,9> tol0 = {1e-10, 3e-2, 3e-2, 1.5e-2, 3e-2, 3e-2, 1.5e-2, 3e-2, 3e-2};

    int idx = 0;
    int n0 = 64;
    for(int px=1;px<=4;px*=2) {
        for(int pv=1;pv<=4;pv*=2) {
            double prev_err = 0.0;
            for(int n=n0;n<=1024;n*=2) {
                MatrixXd m_ref = patched_check_rhs_S({1,1},{n,n}); // reference with standard Lie splitting function

                MatrixXd m = patched_check_rhs_S({px,pv},{n,n});

                double err = (m_ref-m).lpNorm<Eigen::Infinity>();
                double tol = tol0[idx]/double(n/n0);
                cout << px << "," << pv << " defect: " << err << "\t" << log(prev_err/err)/log(2) << "\t" << tol << endl;
                prev_err = err;
                BOOST_CHECK_SMALL( err, tol );
            }
            idx++;
        }
    }
}

//
// check the rhs_K and the rhs_K_diag functions
//
MatrixXd patched_check_rhs_K(array<int,2> patches, array<int,2> ext, bool diagonalized=false) {
    int r = 3;
    array<double,2> a = {0.0,-6.0};
    array<double,2> b = {4*M_PI,6.0};

    domain_lowrank2d_patches in(patches[0],patches[1]);
    domain_lowrank2d_patches out(patches[0],patches[1]);
    vlasov_cs_patches vcs(patches[0],patches[1]);
    init_patches(vcs, in, out, r, a, b, patches, ext);

    VectorXd E(ext[0]);
    for(int i=0;i<ext[0];i++) {
        double x = a[0] + i*(b[0]-a[0])/double(ext[0]);
        E(i) = cos(x);
    }

    for(auto pid : up_to<2>(patches)) {
        auto& lvcs = vcs(pid[0],pid[1]);
        auto& lin  = in(pid[0],pid[1]);
        auto& lout = out(pid[0],pid[1]);

        auto& in_left  = in(neg_modulo(pid[0]-1,patches[0]),pid[1]);
        auto& in_right = in((pid[0]+1)%patches[0],pid[1]);
        MatrixXd bdryx = patched_xBC(*lin, *in_left, *in_right);
        MatrixXd bdryv;

        MatrixXd K = lin->lr.X*lin->lr.S;
        if(patches[0]==1 && patches[1]==1) {
            MatrixXd C1 = lvcs->compute_C1(lin->lr.V);
            MatrixXd C2 = lvcs->compute_C2(lin->lr.V);

            lvcs->rhs_K(K, lin->lr.V, lin->lr.X, lout->lr.X, C1, C2, nullptr, &E);
        } else {
            MatrixXd C1 = lvcs->compute_C1(lin->lr.V);
            MatrixXd C2 = lvcs->compute_C2(lin->lr.V, &bdryv);

            VectorXd lE = get_local_E(E, pid[0], lin->lr.X.rows());
            if(!diagonalized) {
                lvcs->rhs_K(K, lin->lr.V, lin->lr.X, lout->lr.X, C1, C2, &bdryx, &lE);
            } else {

                //lvcs->Lie_step_K(1.0, lin->lr, lout->lr, &bdryx, &bdryv, &lE);
                Eigen::SelfAdjointEigenSolver<MatrixXd> saes(C1);
                MatrixXd lambda = saes.eigenvalues();
                MatrixXd T = saes.eigenvectors();

                MatrixXd K_in_bar = K*T.inverse().transpose();
                MatrixXd K_out_bar(K_in_bar);
                MatrixXd bdry_bar = bdryx*T.inverse().transpose();

                MatrixXd C2_bar = T.transpose()*C2*T.inverse().transpose();

                lvcs->rhs_K_diag(K_in_bar, K_out_bar, C2_bar, lambda, &bdry_bar, &lE);

                lout->lr.X = K_out_bar*T.transpose();
            }
        }
    }

    for(auto pid : up_to<2>(patches)) {
        auto& lin  = in(pid[0],pid[1]);
        auto& lout = out(pid[0],pid[1]);
        lin->lr.X = lout->lr.X;
        lin->lr.S = MatrixXd::Identity(r,r);
    }
    return matrix_from_patches(in, ext);
}

BOOST_AUTO_TEST_CASE( patched_rhs_K ) {
    MatrixXd m_ref = patched_check_rhs_K({1,1},{256,256}); // reference with standard Lie splitting function
    MatrixXd m = patched_check_rhs_K({1,2},{256,256});
    write_matrix("m_ref.data", m_ref);
    write_matrix("m_pat.data", m);
    write_matrix("m_diff.data", m-m_ref);
    
    int n0=64;

    cout << "CHECKING rhs_K" << endl;
    array<double,9> tol0 = {1e-10, 2.5e-2, 2.5e-2, 0.75e-2, 2.5e-2, 2.5e-2, 0.75e-2, 2.5e-2, 2.5e-2};
    int idx=0;
    for(int px=1;px<=4;px*=2) {
        for(int pv=1;pv<=4;pv*=2) {
            double prev_err = 0.0;
            for(int n=n0;n<=1024;n*=2) {
                MatrixXd m_ref = patched_check_rhs_K({1,1},{n,n}); // reference with standard Lie splitting function

                MatrixXd m = patched_check_rhs_K({px,pv},{n,n});

                double err = (m_ref-m).lpNorm<Eigen::Infinity>();
                double tol = tol0[idx]/pow(double(n/n0),2);
                cout << px << "," << pv << " defect: " << err << "\t" << log(prev_err/err)/log(2) << "\t" << tol << endl;
                prev_err = err;
                BOOST_CHECK_SMALL( err, tol );
            }
            idx++;
        }
    }

    cout << "CHECKING rhs_K_diag" << endl;
    tol0 = {1e-10, 2.5e-2, 2.5e-2, 1.2e-2, 2.5e-2, 2.5e-2, 1.25e-2, 2.5e-2, 2.5e-2};
    idx=0;
    for(int px=1;px<=4;px*=2) {
        for(int pv=1;pv<=4;pv*=2) {
            double prev_err = 0.0;
            for(int n=n0;n<=1024;n*=2) {
                MatrixXd m_ref = patched_check_rhs_K({1,1},{n,n}); // reference with standard Lie splitting function

                MatrixXd m = patched_check_rhs_K({px,pv},{n,n},true);

                double err = (m_ref-m).lpNorm<Eigen::Infinity>();
                double tol = tol0[idx]/double(n/n0);
                cout << px << "," << pv << " defect: " << err << "\t" << log(prev_err/err)/log(2) << "\t" << tol << endl;
                prev_err = err;
                BOOST_CHECK_SMALL( err, tol );
            }
            idx++;
        }
    }

}

//
// check the rhs_L_linear and the rhs_L_linear_diag functions
//
MatrixXd patched_check_rhs_L(array<int,2> patches, array<int,2> ext, bool use_fft=false, bool diagonalized=false) {
    int r = 10;
    array<double,2> a = {0.0,-6.0};
    array<double,2> b = {4*M_PI,6.0};

    domain_lowrank2d_patches in(patches[0],patches[1]);
    domain_lowrank2d_patches out(patches[0],patches[1]);
    vlasov_cs_patches vcs(patches[0],patches[1]);
    init_patches(vcs, in, out, r, a, b, patches, ext);

    VectorXd E(ext[0]);
    for(int i=0;i<ext[0];i++) {
        double x = a[0] + i*(b[0]-a[0])/double(ext[0]);
        E(i) = cos(x);
    }

    for(auto pid : up_to<2>(patches)) {
        auto& lvcs = vcs(pid[0],pid[1]);
        auto& lin  = in(pid[0],pid[1]);
        auto& lout = out(pid[0],pid[1]);

        auto& in_down  = in(pid[0],neg_modulo(pid[1]-1,patches[1]));
        auto& in_up    = in(pid[0],(pid[1]+1)%patches[1]);
        MatrixXd bdryv = patched_vBC(*lin, *in_down, *in_up);

        auto& in_left  = in(neg_modulo(pid[0]-1,patches[0]),pid[1]);
        auto& in_right = in((pid[0]+1)%patches[0],pid[1]);
        MatrixXd bdryx = patched_XBC(*lin, *in_left, *in_right);

        MatrixXd L = lin->lr.V*lin->lr.S.transpose();
        if(patches[0]==1 && patches[1]==1) {
            MatrixXd D1 = lvcs->compute_D1(lin->lr.X, E);
            if(use_fft) {
                MatrixXd D2 = lvcs->compute_D2(lin->lr.X, nullptr);
                lvcs->rhs_L_linear(L, lin->lr.X, lin->lr.V, E, lout->lr.V, D1, D2, nullptr);
            } else {
                MatrixXd D2 = lvcs->compute_D2(lin->lr.X, &bdryx);
                lvcs->rhs_L_linear(L, lin->lr.X, lin->lr.V, E, lout->lr.V, D1, D2, &bdryv);
            }
        } else {
            VectorXd lE = get_local_E(E, pid[0], lin->lr.X.rows());
            MatrixXd D1 = lvcs->compute_D1(lin->lr.X, lE);
            MatrixXd D2 = lvcs->compute_D2(lin->lr.X, &bdryx);

            if(!diagonalized) {
                lvcs->rhs_L_linear(L, lin->lr.X, lin->lr.V, lE, lout->lr.V, D1, D2, &bdryv);
            } else {

                //lvcs->step_L_diagonalized(1.0, lin->lr.X, lin->lr.S, lin->lr.V, L, lout->lr.V, &bdryx, &bdryv, &lE);
                //lvcs->Lie_step_L(1.0, lin->lr, lout->lr, &bdryx, &bdryv, &lE);
                Eigen::SelfAdjointEigenSolver<MatrixXd> saes(D1);
                MatrixXd lambda = saes.eigenvalues();
                MatrixXd T = saes.eigenvectors();

                MatrixXd L_in_bar = L*T.inverse().transpose();
                MatrixXd L_out_bar(L_in_bar);

                // We also need to transform the boundary
                MatrixXd bdry_bar = bdryv*T.inverse().transpose();

                // and D2
                MatrixXd D2_bar = T.transpose()*D2*T.inverse().transpose();
                
                lvcs->rhs_L_linear_diag(L_in_bar, L_out_bar, D2_bar, lambda, &bdry_bar);
        
                lout->lr.V = L_out_bar*T.transpose();
            }
        }
    }

    for(auto pid : up_to<2>(patches)) {
        auto& lin  = in(pid[0],pid[1]);
        auto& lout = out(pid[0],pid[1]);
        lin->lr.V = lout->lr.V;
        lin->lr.S = MatrixXd::Identity(r,r);
    }
    return matrix_from_patches(in, ext);
}

BOOST_AUTO_TEST_CASE( patched_rhs_L ) {
    MatrixXd m_ref = patched_check_rhs_L({1,1},{256,256},true); // reference with standard Lie splitting function
    MatrixXd m = patched_check_rhs_L({4,1},{256,256});
    write_matrix("m_ref.data", m_ref);
    write_matrix("m_pat.data", m);
    write_matrix("m_diff.data", m-m_ref);

    int n0=64;

    cout << "checking rhs_L" << endl;
    array<double,9> tol0 = {9e-3, 9e-3, 9e-3, 1.5e-2, 1.5e-2, 1.5e-2, 1.5e-2, 1.5e-2, 1.5e-2};
    int idx=0;
    for(int px=1;px<=4;px*=2) {
        for(int pv=1;pv<=4;pv*=2) {
            double prev_err = 0.0;
            for(int n=64;n<=1024;n*=2) {
                MatrixXd m_ref = patched_check_rhs_L({1,1},{n,n},true); // reference with standard Lie splitting function

                MatrixXd m = patched_check_rhs_L({px,pv},{n,n});


                double err = (m_ref-m).lpNorm<Eigen::Infinity>();
                double tol = tol0[idx]/double(n/n0);
                cout << px << "," << pv << " defect: " << err << "\t" << log(prev_err/err)/log(2) << "\t" << tol << endl;
                prev_err = err;
                BOOST_CHECK_SMALL( err, tol );
            }
            idx++;
        }
    }

    cout << "checking rhs_L_diag" << endl;
    tol0 = {9e-3, 8e-2, 8e-2, 8e-2, 8e-2, 8e-2, 8e-2, 8e-2, 8e-2};
    idx=0;
    for(int px=1;px<=4;px*=2) {
        for(int pv=1;pv<=4;pv*=2) {
            double prev_err = 0.0;
            for(int n=64;n<=1024;n*=2) {
                MatrixXd m_ref = patched_check_rhs_L({1,1},{n,n},true); // reference with standard Lie splitting function

                MatrixXd m = patched_check_rhs_L({px,pv},{n,n},false,true);


                double err = (m_ref-m).lpNorm<Eigen::Infinity>();
                double tol = tol0[idx]/double(n/n0);
                cout << px << "," << pv << " defect: " << err << "\t" << log(prev_err/err)/log(2) << "\t" << tol << endl;
                prev_err = err;
                BOOST_CHECK_SMALL( err, tol );
            }
            idx++;
        }
    }

}


//
// check the Lie_step_S function
//
MatrixXd patched_check_step_S(double tau, array<int,2> patches, array<int,2> ext) {
    int r = 10;
    array<double,2> a = {0.0,-6.0};
    array<double,2> b = {4*M_PI,6.0};

    domain_lowrank2d_patches in(patches[0],patches[1]);
    domain_lowrank2d_patches out(patches[0],patches[1]);
    vlasov_cs_patches vcs(patches[0],patches[1]);
    init_patches(vcs, in, out, r, a, b, patches, ext);

    VectorXd E(ext[0]);
    for(int i=0;i<ext[0];i++) {
        double x = a[0] + i*(b[0]-a[0])/double(ext[0]);
        E(i) = cos(x);
    }

    int nsteps = ceil(0.05/tau);
    cout << "nsteps: " << nsteps << endl;
    //int nsteps = 1;
    for(int k=0;k<nsteps;k++) {
        for(auto pid : up_to<2>(patches)) {
            auto& lvcs = vcs(pid[0],pid[1]);
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);

            MatrixXd bdryx, bdryv;

            VectorXd lE = get_local_E(E, pid[0], lin->lr.X.rows());
            lvcs->Lie_step_S(tau, lin->lr, lout->lr, &bdryx, &bdryv, &lE);
        }

        for(auto pid : up_to<2>(patches)) {
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);

            lin->lr = lout->lr;
        }
    }

    return matrix_from_patches(out, ext);
}

BOOST_AUTO_TEST_CASE( patched_step_S ) {
    int n0 = 64;  // large n required to go down to 1e-7 in the time error or so
    double tau0=0.01;

    for(int px=1;px<=4;px*=2) {
        for(int pv=1;pv<=4;pv*=2) {
            double prev_err=0.0;
            for(int tau_fac=1;tau_fac<=16;tau_fac*=2) {
                int n = n0*tau_fac;
                MatrixXd m_ref = patched_check_step_S(tau0/double(tau_fac),{1,1},{n,n}); // reference with standard Lie splitting function
                MatrixXd m = patched_check_step_S(tau0/double(tau_fac),{px,pv},{n,n});

                double err = (m_ref-m).lpNorm<Eigen::Infinity>();
                cout << px << "," << pv << " defect: " << err << "\t\t" << log(prev_err/err)/log(2) << endl;
                prev_err = err;

                //BOOST_CHECK_SMALL( (m_ref-m).lpNorm<Eigen::Infinity>(), 1e-15 ); //TODO
            }
        }
    }
}

//
// check the Lie_step_K function
//
MatrixXd patched_check_step_K(double tau, array<int,2> patches, array<int,2> ext) {
    int r = 3;
    array<double,2> a = {0.0,-6.0};
    array<double,2> b = {4*M_PI,6.0};

    domain_lowrank2d_patches in(patches[0],patches[1]);
    domain_lowrank2d_patches out(patches[0],patches[1]);
    vlasov_cs_patches vcs(patches[0],patches[1]);
    init_patches(vcs, in, out, r, a, b, patches, ext);

    VectorXd E(ext[0]);
    for(int i=0;i<ext[0];i++) {
        double x = a[0] + i*(b[0]-a[0])/double(ext[0]);
        E(i) = cos(x);
    }

    int nsteps = ceil(0.05/tau);
    cout << "nsteps: " << nsteps << endl;
    //int nsteps = 1;
    for(int k=0;k<nsteps;k++) {

        for(auto pid : up_to<2>(patches)) {
            auto& lvcs = vcs(pid[0],pid[1]);
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);

            auto& in_left  = in(neg_modulo(pid[0]-1,patches[0]),pid[1]);
            auto& in_right = in((pid[0]+1)%patches[0],pid[1]);
            MatrixXd bdryx = patched_xBC(*lin, *in_left, *in_right);
            MatrixXd bdryv;

            VectorXd lE = get_local_E(E, pid[0], lin->lr.X.rows());
            lvcs->Lie_step_K(tau, lin->lr, lout->lr, &bdryx, &bdryv, &lE);
        }

        for(auto pid : up_to<2>(patches)) {
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);

            lin->lr = lout->lr;
        }
    }

    return matrix_from_patches(out, ext);
}

BOOST_AUTO_TEST_CASE( patched_step_K ) {
    int n0 = 64;  // large n required to go down to 1e-7 in the time error or so
    double tau0=1e-2;

    for(int px=1;px<=4;px*=2) {
        for(int pv=1;pv<=4;pv*=2) {
            double prev_err=0.0;
            for(int tau_fac=1;tau_fac<=16;tau_fac*=2) {
                int n = n0*tau_fac;
                MatrixXd m_ref = patched_check_step_K(tau0/double(tau_fac),{1,1},{n,n}); // reference with standard Lie splitting function
                MatrixXd m = patched_check_step_K(tau0/double(tau_fac),{px,pv},{n,n});

                double err = (m_ref-m).lpNorm<Eigen::Infinity>();
                cout << px << "," << pv << " defect: " << err << "\t\t" << log(prev_err/err)/log(2) << endl;
                prev_err = err;

                //BOOST_CHECK_SMALL( (m_ref-m).lpNorm<Eigen::Infinity>(), 1e-15 ); //TODO
            }
        }
    }
}


//
// check the Lie_step_L function
//
MatrixXd patched_check_step_L(double tau, array<int,2> patches, array<int,2> ext) {
    int r = 10;
    array<double,2> a = {0.0,-6.0};
    array<double,2> b = {4*M_PI,6.0};

    domain_lowrank2d_patches in(patches[0],patches[1]);
    domain_lowrank2d_patches out(patches[0],patches[1]);
    vlasov_cs_patches vcs(patches[0],patches[1]);
    init_patches(vcs, in, out, r, a, b, patches, ext);

    VectorXd E(ext[0]);
    for(int i=0;i<ext[0];i++) {
        double x = a[0] + i*(b[0]-a[0])/double(ext[0]);
        E(i) = cos(x);
    }


    int nsteps = ceil(0.05/tau);
    cout << "nsteps: " << nsteps << endl;
    //int nsteps = 1;
    for(int k=0;k<nsteps;k++) {
        for(auto pid : up_to<2>(patches)) {
            auto& lvcs = vcs(pid[0],pid[1]);
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);

            auto& in_down  = in(pid[0],neg_modulo(pid[1]-1,patches[1]));
            auto& in_up    = in(pid[0],(pid[1]+1)%patches[1]);
            MatrixXd bdryx;
            MatrixXd bdryv = patched_vBC(*lin, *in_down, *in_up);

            VectorXd lE = get_local_E(E, pid[0], lin->lr.X.rows());

            /*
            MatrixXd Lout(lin->lr.V);
            lvcs->step_L_diagonalized(tau, lin->lr.X, lin->lr.S, lin->lr.V, lin->lr.V*lin->lr.S.transpose(), Lout, &bdryx, &bdryv, &lE);
            lout->lr.X = lin->lr.X;
            */
            /*
            MatrixXd L = lin->lr.V*lin->lr.S.transpose();
            MatrixXd D1 = lvcs->compute_D1(lin->lr.X, lE);
            MatrixXd D2 = lvcs->compute_D2(lin->lr.X, &bdryx);

            Eigen::SelfAdjointEigenSolver<MatrixXd> saes(D1);
            MatrixXd lambda = saes.eigenvalues();
            MatrixXd T = saes.eigenvectors();

            MatrixXd L_in_bar = L*T.inverse().transpose();
            MatrixXd L_out_bar(L_in_bar);

            // We also need to transform the boundary
            MatrixXd bdry_bar = bdryv*T.inverse().transpose();

            // and D2
            MatrixXd D2_bar = T.transpose()*D2*T.inverse().transpose();

            lvcs->rhs_L_linear_diag(L_in_bar, L_out_bar, D2_bar, lambda, &bdry_bar);

            lout->lr.V = L_out_bar*T.transpose();
            */
            lvcs->Lie_step_L(tau, lin->lr, lout->lr, &bdryx, &bdryv, &lE);
        }


        for(auto pid : up_to<2>(patches)) {
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);

            lin->lr = lout->lr;
        }
    }

    return matrix_from_patches(out, ext);
}

BOOST_AUTO_TEST_CASE( patched_step_L ) {
    int n0 = 64;  // large n required to go down to 1e-7 in the time error or so
    double tau0=1e-2;

    for(int px=1;px<=4;px*=2) {
        for(int pv=1;pv<=4;pv*=2) {
            double prev_err=0.0;
            for(int tau_fac=1;tau_fac<=16;tau_fac*=2) {
                int n = n0*tau_fac;
                MatrixXd m_ref = patched_check_step_L(tau0/double(tau_fac),{1,1},{n,n}); // reference with standard Lie splitting function
                MatrixXd m = patched_check_step_L(tau0/double(tau_fac),{px,pv},{n,n});

                double err = (m_ref-m).lpNorm<Eigen::Infinity>();
                cout << px << "," << pv << " defect: " << err << "\t\t" << log(prev_err/err)/log(2) << endl;
                prev_err = err;

                //BOOST_CHECK_SMALL( (m_ref-m).lpNorm<Eigen::Infinity>(), 1e-15 ); //TODO
            }
        }
    }
}



//
// check the entire integrator
//
MatrixXd patched_check_step(array<int,2> patches, array<int,2> ext, int tau_fac=1, bool classic_Lie=false) {
    int r = 10;
    array<double,2> a = {0.0,-6.0};
    array<double,2> b = {4*M_PI,6.0};
    double tau = 1e-2/double(tau_fac);

    domain_lowrank2d_patches in(patches[0],patches[1]);
    domain_lowrank2d_patches out(patches[0],patches[1]);
    vlasov_cs_patches vcs(patches[0],patches[1]);
    init_patches(vcs, in, out, r, a, b, patches, ext);

    int nsteps = ceil(0.05/tau);
    for(int k=0;k<nsteps;k++) {

        for(auto pid : up_to<2>(patches)) {
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);
            lout->lr = lin->lr;
        }

        VectorXd E(ext[0]);
        electric_field(vcs, in, E, b[0]-a[0]);

        MatrixXd_patches gbdryx(patches[0],patches[1]);
        MatrixXd_patches gbdryv(patches[0],patches[1]);
        MatrixXd_patches gbdryX(patches[0],patches[1]);
        MatrixXd_patches gbdryV(patches[0],patches[1]);

        // K step
        for(auto pid : up_to<2>(patches)) {
            auto& lout  = out(pid[0],pid[1]);

            auto& in_left  = out(neg_modulo(pid[0]-1,patches[0]),pid[1]);
            auto& in_right = out((pid[0]+1)%patches[0],pid[1]);
            gbdryx(pid[0],pid[1]) = patched_xBC(*lout, *in_left, *in_right);
            gbdryX(pid[0],pid[1]) = patched_XBC(*lout, *in_left, *in_right);

            auto& in_down  = out(pid[0],neg_modulo(pid[1]-1,patches[1]));
            auto& in_up    = out(pid[0],(pid[1]+1)%patches[1]);
            gbdryv(pid[0],pid[1]) = patched_vBC(*lout, *in_down, *in_up);
            gbdryV(pid[0],pid[1]) = patched_VBC(*lout, *in_down, *in_up);
        }
        for(auto pid : up_to<2>(patches)) {
            auto& lvcs = vcs(pid[0],pid[1]);
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);
            auto& bdryx = gbdryx(pid[0],pid[1]);
            auto& bdryv = gbdryv(pid[0],pid[1]);
            
            if(classic_Lie)
                lvcs->Lie_step(tau, lin->lr, lout->lr);
            else {
                VectorXd lE = get_local_E(E, pid[0], lin->lr.X.rows());
                lin->lr = lout->lr;
                lvcs->Lie_step_K(tau, lin->lr, lout->lr, &bdryx, &bdryv, &lE);
            }
        }

        // step S
        for(auto pid : up_to<2>(patches)) {
            auto& lvcs = vcs(pid[0],pid[1]);
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);
            auto& bdryX = gbdryX(pid[0],pid[1]);
            auto& bdryV = gbdryV(pid[0],pid[1]);

            if(!classic_Lie) {
                VectorXd lE = get_local_E(E, pid[0], lin->lr.X.rows());
                lin->lr = lout->lr;
                lvcs->Lie_step_S(tau, lin->lr, lout->lr, &bdryX, &bdryV, &lE);
            }
        }

        // L step
        for(auto pid : up_to<2>(patches)) {
            auto& lout  = out(pid[0],pid[1]);

            auto& in_left  = out(neg_modulo(pid[0]-1,patches[0]),pid[1]);
            auto& in_right = out((pid[0]+1)%patches[0],pid[1]);
            gbdryx(pid[0],pid[1]) = patched_xBC(*lout, *in_left, *in_right);
            gbdryX(pid[0],pid[1]) = patched_XBC(*lout, *in_left, *in_right);

            auto& in_down  = out(pid[0],neg_modulo(pid[1]-1,patches[1]));
            auto& in_up    = out(pid[0],(pid[1]+1)%patches[1]);
            gbdryv(pid[0],pid[1]) = patched_vBC(*lout, *in_down, *in_up);
            gbdryV(pid[0],pid[1]) = patched_VBC(*lout, *in_down, *in_up);
        }
        for(auto pid : up_to<2>(patches)) {
            auto& lvcs = vcs(pid[0],pid[1]);
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);
            auto& bdryx = gbdryx(pid[0],pid[1]);
            auto& bdryv = gbdryv(pid[0],pid[1]);
            
            // do the remainder of the time step
            if(!classic_Lie) {
                VectorXd lE = get_local_E(E, pid[0], lin->lr.X.rows());
                lin->lr = lout->lr;
                lvcs->Lie_step_L(tau, lin->lr, lout->lr, &bdryx, &bdryv, &lE);
            }
        }

        for(auto pid : up_to<2>(patches)) {
            auto& lin  = in(pid[0],pid[1]);
            auto& lout = out(pid[0],pid[1]);
            lin->lr = lout->lr;
        }
    }


    return matrix_from_patches(out, ext);
}

BOOST_AUTO_TEST_CASE( patched_step ) {
    int n0=64;
    int n = 256;
    MatrixXd m_ref = patched_check_step({1,1},{n,n}); // reference with standard Lie splitting function
    MatrixXd m = patched_check_step({1,8},{n,n});
    write_matrix("m_ref.data", m_ref);
    write_matrix("m_pat.data", m);
    write_matrix("m_diff.data", m-m_ref);
    
    // makes the convergence more predictable
    _integrate_single_euler_step = true;

    array<double,9> tol0 = {1e-10, 4e-5, 4e-5, 5.5e-5, 5e-5, 4e-5, 5e-5, 4e-5, 5e-5};

    int idx=0;
    for(int px=1;px<=4;px*=2) {
        for(int pv=1;pv<=4;pv*=2) {
            double prev_err=0.0;
            for(int fac=1;fac<=16;fac*=2) {
                int n = n0*fac;
                MatrixXd m_ref = patched_check_step({1,1},{n,n},fac); // reference with standard Lie splitting function
                MatrixXd m = patched_check_step({px,pv},{n,n},fac);

                double err = (m_ref-m).lpNorm<Eigen::Infinity>();
                double tol = tol0[idx]/double(fac);
                cout << px << "," << pv << " defect: " << err << "\t" << log(prev_err/err)/log(2) << "\t" << tol << endl;
                prev_err = err;

                BOOST_CHECK_SMALL( err, tol );
            }
            idx++;
        }
    }
}


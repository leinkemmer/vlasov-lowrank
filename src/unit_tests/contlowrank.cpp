
#define BOOST_TEST_MODULE LOWRANK
#include <boost/test/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>
#include <unsupported/Eigen/MatrixFunctions>
#include <algorithms/lowrank-continuoussplit-1x1v.hpp>


BOOST_AUTO_TEST_CASE( RHS_K_AND_ELECTRIC_K ) {
    int nx=100, nv=120, r=3;
    MatrixXd K(nx,r), V0(nv,r);
    vlasov_continuoussplit vcs(r, {nx,nv}, {0.0,-6.0}, {2*M_PI, 6.0});

    for(int k=0;k<r;k++) {
        // populate V
        for(int i=0;i<nv;i++) {
            double v = vcs.v(i);
            V0(i,k) = pow(v,k)*exp(-pow(v,2));
        }

        // populate K
        for(int i=0;i<nx;i++) {
            double x = vcs.x(i);
            if(k==0)
                K(i,k) = 1/sqrt(M_PI);
            else
                K(i,k) = cos(k*x) + cos(10*k*x);
        }
    }

    // populate the expected result for rho
    VectorXd exact(nx);
    for(int i=0;i<nx;i++) {
        double x = vcs.x(i);
        exact(i) = sqrt(M_PI)*0.5*(cos(2*x)+cos(20*x));
    }

    VectorXd rho = vcs.rho_K(K, V0);
    BOOST_CHECK_SMALL( (rho-exact).norm() , 1e-10);

    // populate the expected result for the electric field
    auto E_func = [](double x) { return sqrt(M_PI)*0.5*(sin(2*x)/2.0+sin(20*x)/20.0); };
    for(int i=0;i<nx;i++) {
        double x = vcs.x(i);
        exact(i) = sqrt(M_PI)*0.5*(sin(2*x)/2.0+sin(20*x)/20.0);
    }
   
    VectorXd E(nx);
    vcs.electric_field(rho, E);
    BOOST_CHECK_SMALL( (E-exact).norm() , 1e-10);

    // Here we start with the right hand side
    
    MatrixXd C1(r,r), C2(r,r);
    for(int i=0;i<r;i++) {
        for(int j=0;j<r;j++) {
            C1(i,j) = double(abs(i-j));
            C2(i,j) = double(i+2*j);
        }
    }

    // First we check the part with C1 (by setting C2=0)
    MatrixXd rhs_num(nx, r), rhs_exact(nx, r);
    vcs.rhs_K(K, V0, MatrixXd(0,0), rhs_num, C1, MatrixXd::Zero(r,r));
    auto rhs_C1_term = [](double x, int k) {
                           if(k==0)       return sin(x) + 2*(2*sin(2*x) + 5*(sin(10*x) + 4*sin(20*x)));
                           else if(k==1)  return 2*(sin(2*x) + 10*sin(20*x));
                           else if(k==2)  return sin(x) + 10*sin(10*x);
                           else exit(1);
                       };
    vcs.init_x(rhs_C1_term, rhs_exact);

    BOOST_CHECK_SMALL( (rhs_num-rhs_exact).norm(), 1e-10);

    // Then C1=0 and C2!=0
    vcs.rhs_K(K, V0, MatrixXd(0,0), rhs_num, MatrixXd::Zero(r,r), C2);
    auto rhs_C2_term = [E_func](double x, int k) {
                           if(k==0)      return -E_func(x)*(2*(cos(x) + cos(10*x)) 
                                   + 4*(cos(2*x) + cos(20*x)));
                           else if(k==1) return -E_func(x)*(1/sqrt(M_PI) 
                                   + 3*(cos(x) + cos(10*x)) + 5*(cos(2*x) + cos(20*x)));
                           else if(k==2) return -E_func(x)*(2/sqrt(M_PI) 
                                   + 4*(cos(x) + cos(10*x)) + 6*(cos(2*x) + cos(20*x)));
                           else exit(1);
                       };
    vcs.init_x(rhs_C2_term, rhs_exact);

    BOOST_CHECK_SMALL( (rhs_num-rhs_exact).norm(), 1e-10);

    // Then we check both together
    vcs.rhs_K(K, V0, MatrixXd(0,0), rhs_num, C1, C2);
    vcs.init_x(
            [rhs_C1_term, rhs_C2_term](double x, int k) { 
                return rhs_C1_term(x,k) + rhs_C2_term(x,k);
            }, rhs_exact
            );
    
    BOOST_CHECK_SMALL( (rhs_num-rhs_exact).norm(), 1e-10);
}

BOOST_AUTO_TEST_CASE( RHS_L ) {
    int nx=100, nv=120, r=3;
    MatrixXd L(nv,r);
    vlasov_continuoussplit vcs(r, {nx,nv}, {0.0,-6.0}, {2*M_PI, 6.0});

    MatrixXd D1(r,r), D2(r,r);
    for(int i=0;i<r;i++) {
        for(int j=0;j<r;j++) {
            D1(i,j) = double(abs(i-j));
            D2(i,j) = double(i+2*j);
        }
    }

    vcs.init_v([](double v, int k) {
                   return pow(v,k)*exp(-v*v);
               }, L
               );

    MatrixXd rhs_num(nv, r), rhs_exact(nv, r);
    vcs.rhs_L_linear(L, MatrixXd(0,0), MatrixXd(0,0), MatrixXd(0,0), rhs_num, D1, D2);
    vcs.init_v(
            [](double v, int k) {
                if(k==0)      return -exp(-v*v)*(1+4*v);
                else if(k==1) return -exp(-v*v)*(1+3*v*(1+v));
                else if(k==2) return  exp(-v*v)*(-1-2*v*(-1+v*3*v*v));
                else exit(1);
            }, rhs_exact
            );

    BOOST_CHECK_SMALL( (rhs_num.col(0)-rhs_exact.col(0)).norm(), 1e-10);
}

BOOST_AUTO_TEST_CASE( RHS_S ) {
    int nx=100, nv=120, r=3;
    MatrixXd X1(nx,r), V0(nv,r), S(r,r);
    vlasov_continuoussplit vcs(r, {nx,nv}, {0.0,-6.0}, {2*M_PI, 6.0});

    vcs.init_v([](double v, int k) {
            return pow(v,k)*exp(-pow(v,2));
            }, V0);
    vcs.init_x([](double x, int k) {
            if(k==0)      return 1.0;
            else if(k==1) return cos(x) + cos(10*x) + cos(2*x) + cos(20*x);
            else if(k==2) return cos(x) + cos(10*x) - 0.5*(cos(2*x) + cos(20*x));
            else exit(1);
            }, X1);

    S << 1/sqrt(M_PI), 0.0,     0.0,
         0.0, 1.0/3.0, 1.0/1.5,
         0.0, 2.0/3.0, -1.0/1.5;

    // This is just a consistency check that we have the correct electric field
    VectorXd rho = vcs.rho_K(X1*S, V0);
    VectorXd E(rho.rows());
    vcs.electric_field(rho, E);

    VectorXd exact(nx);
    for(int i=0;i<nx;i++) {
        double x = vcs.x(i);
        //exact(i) = sqrt(M_PI)*0.5*(cos(2*x)+cos(20*x));
        exact(i) = sqrt(M_PI)*0.5*(sin(2*x)/2.0+sin(20*x)/20.0);
    }
    BOOST_CHECK_SMALL( (E-exact).norm(), 1e-10);

    // Setup the matrices
    // D1 will be computed inside rhs_S (as a function of the electric field)
    MatrixXd D1(r,r), D2(r,r);
    MatrixXd C1(r,r), C2(r,r);
    for(int i=0;i<r;i++) {
        for(int j=0;j<r;j++) {
            C1(i,j) = double(abs(i-j));
            C2(i,j) = double(i+2*j);
            D2(i,j) = double(i+2*j);
        }
    }

    MatrixXd out(r,r), out_exact(r,r);
    vcs.rhs_S(S, V0, X1, out, C1, C2, D2);
    // Note: only the D2*S*C1.transpose() term is really checked here since with the current electric field
    // D1 is zero.
    out_exact << 2.0/3.0, -4.0/3.0, 10.0/3.0,
               5.0/3.0, -4.0/3.0 + 1.0/sqrt(M_PI) , 13.0/3.0 + 2.0/sqrt(M_PI),
               8.0/3.0, -4.0/3.0 + 2.0/sqrt(M_PI), 16.0/3.0 + 4.0/sqrt(M_PI);

    BOOST_CHECK_SMALL( (out_exact-out).norm(), 1e-10);
}

BOOST_AUTO_TEST_CASE( COMPUTE_C ) {
    double tol = 1e-10;

    int nx=100, nv=220, r=2;
    MatrixXd V(nv,r);
    vlasov_continuoussplit vcs(r, {nx,nv}, {0.0,-6.0}, {2*M_PI, 6.0});

    for(int k=0;k<r;k++) {
        for(int i=0;i<nv;i++) {
            double v = vcs.v(i);
            V(i,k) = pow(v,k)*exp(-pow(v,2));
        }
    }

    MatrixXd C1 = vcs.compute_C1(V);
    BOOST_CHECK_SMALL(C1(0,0) - 0.0, tol);
    BOOST_CHECK_SMALL(C1(1,1) - 0.0, tol);
    BOOST_CHECK_SMALL(C1(0,1) - sqrt(M_PI/32.0), tol);
    BOOST_CHECK_SMALL(C1(1,0) - sqrt(M_PI/32.0), tol);

    MatrixXd C2 = vcs.compute_C2(V);
    BOOST_CHECK_SMALL(C2(0,0) - 0.0, tol);
    BOOST_CHECK_SMALL(C2(1,1) - 0.0, tol);
    BOOST_CHECK_SMALL(C2(0,1) - sqrt(M_PI/8.0), tol);
    BOOST_CHECK_SMALL(C2(1,0) - -sqrt(M_PI/8.0), tol);
}

BOOST_AUTO_TEST_CASE( STENCIL ) {
    int nx=400, nv=120, r=3;
    MatrixXd V(nv,r);
    vlasov_continuoussplit vcs(r, {nx,nv}, {0.0,-6.0}, {2*M_PI, 6.0});

    MatrixXd K(nx,r), exact(nx,r);
    for(int k=0;k<r;k++) {
        for(int i=0;i<nx;i++) {
            double x = vcs.x(i);
            K(i,k) = sin(x)/(3+cos(k*x));
            exact(i,k) = (cos(x)*(3 + cos(k*x)) + k*sin(x)*sin(k*x))/pow(3 + cos(k*x),2);
        }
    }

    MatrixXd dK = vcs.diff_fft(K, vcs.b[0]-vcs.a[0]);
    for(int k=0;k<r;k++)
        BOOST_CHECK_SMALL((dK.col(k)-exact.col(k)).norm(), 1e-10);
}

BOOST_AUTO_TEST_CASE( COMPUTE_D ) {
    double tol = 1e-10;

    int nx=200, nv=120, r=2;
    MatrixXd X(nx,r);
    vlasov_continuoussplit vcs(r, {nx,nv}, {0.0,-6.0}, {2*M_PI, 6.0});

    VectorXd E(nx);
    for(int i=0;i<nx;i++) {
        double x = vcs.x(i);
        X(i,0) = cos(x);
        X(i,1) = sin(x);

        E(i) = cos(x)*sin(x) + cos(2*x);
    }

    MatrixXd D1 = vcs.compute_D1(X, E);
    BOOST_CHECK_SMALL(D1(0,0) - 0.5*M_PI, tol);
    BOOST_CHECK_SMALL(D1(1,1) - -0.5*M_PI, tol);
    BOOST_CHECK_SMALL(D1(0,1) - 0.25*M_PI, tol);
    BOOST_CHECK_SMALL(D1(1,0) - 0.25*M_PI, tol);

    MatrixXd D2 = vcs.compute_D2(X);
    BOOST_CHECK_SMALL(D2(0,0) - 0.0, tol);
    BOOST_CHECK_SMALL(D2(1,1) - 0.0, tol);
    BOOST_CHECK_SMALL(D2(0,1) - M_PI, tol);
    BOOST_CHECK_SMALL(D2(1,0) - -M_PI, tol);
}

BOOST_AUTO_TEST_CASE( INTEGRATE ) {

    MatrixXd u0(10,10), u1(10,10), A(10,10);
    u0.fill(1.0);

    for(int i=0;i<10;i++)
        for(int j=0;j<10;j++)
            A(i,j) = 1.0/(1.0+double(i)+double(j));

    integrate([A](const MatrixXd& in, MatrixXd& out) {
            out = A*in;
            }, u0, u1, 1.0);

    BOOST_CHECK_SMALL( (u1-A.exp()*u0).norm(), 1e-5);
}

void run_order(string method, double T, vlasov_continuoussplit& vcs, const domain_lowrank2d& ref,
         domain_lowrank2d& in, domain_lowrank2d& work, domain_lowrank2d& out, double initial_tolerance) {
    double err_prev = 0.0;
    for(int nsteps=1;nsteps<=16;nsteps*=2) {
        double tau = T/double(nsteps);
        work = in;
        for(int k=0;k<nsteps;k++) {
            if(method == "lie")
                vcs.Lie_step(tau, work.lr, out.lr);
            else if(method == "strang")
                vcs.Strang_step(tau, work.lr, out.lr);
            else
                exit(1);
            work.lr = out.lr;
        }
        double err = error_inf(work.full(),ref.full());
        if(nsteps != 1)
            cout << "nsteps=" << nsteps << "\terror=" << err << "\torder=" << log(err_prev/err)/log(2) << endl;
        else
            cout << "nsteps=" << nsteps << "\terror=" <<  err << endl;

        if(method == "lie")
            BOOST_CHECK_SMALL( err, initial_tolerance/double(nsteps) );
        else
            BOOST_CHECK_SMALL( err, initial_tolerance/pow(double(nsteps),2) );

        err_prev = err;
    }
}

BOOST_AUTO_TEST_CASE( ORDER ) {

    // Two-stream instability initial value
    array<double,2> a = {0.0,-6.0};
    array<double,2> b = {10*M_PI,6.0};
    array<int,2> ext = {100,100};
    int r = 10;
    domain_lowrank2d in(a, b, r,ext, lowrank_type::normalized_integral);
    in.init(2,
            [](double x, int k) {
            return 0.5*(1.0+0.001*cos(0.2*x));
            },
            [](double v, int k) {
            if(k==0)      return exp(-0.5*pow(v-2.4,2))/sqrt(2.0*M_PI);
            else if(k==1) return exp(-0.5*pow(v+2.4,2))/sqrt(2.0*M_PI);
            else  exit(1);
            }
           );

    vlasov_continuoussplit vcs(r, ext, a, b);
    domain_lowrank2d out(a, b, r, ext, lowrank_type::normalized_integral);
    domain_lowrank2d work(a, b, r, ext, lowrank_type::normalized_integral);
    domain_lowrank2d ref(a, b, r, ext, lowrank_type::normalized_integral);

    double T   = 2.0;
    int ref_nsteps = 64; // number of steps for reference solution, must be power of 2

    double tau = T/double(ref_nsteps);
    ref = in;
    for(int k=0;k<ref_nsteps;k++) {
        vcs.Strang_step(tau, ref.lr, out.lr);
        ref.lr = out.lr;
    }

    // Check the order for Lie splitting
    cout << "Lie splitting" << endl;
    run_order("lie", T, vcs, ref, in, work, out, 8e-4);

    // Check the order for Strang splitting
    cout << "Strang splitting" << endl;
    run_order("strang", T, vcs, ref, in, work, out, 3e-4);
}

BOOST_AUTO_TEST_CASE( CONSERVATIVE_RHSK ) {
    // TODO: global mass and momentum conservation is satisfied even for cons_mode::off
    int nx=100, nv=120, r=10;
    // NOTE: from -6.0 to 6.0 is not sufficient in order to get to machine precision here.
    // This is due to the fact that we use an analytic solution (which requires that
    // the integrals over exp(...) are exactly 1.
    array<double,2> a = {0.0,-9.0}; array<double, 2> b = {2*M_PI, 9.0};
    vlasov_continuoussplit vcs(r, {nx,nv}, a, b);

    domain_lowrank2d in(a, b, r, {nx,nv}, lowrank_type::normalized_integral);
    in.init(2,
            [](double x, int k) {
                if(k==0)  return 1.0 + cos(x);
                else      return sin(2.0*x); 
                exit(1);
            },
            [](double v, int k) {
                if(k==0)      return exp(-0.5*v*v)/sqrt(2.0*M_PI);
                else if(k==1) return v*exp(-0.5*v*v)/sqrt(2.0*M_PI);
                exit(1);
            }
           );
    MatrixXd K = in.lr.X*in.lr.S;
    MatrixXd V = in.lr.V;

    // compute rhs
    vcs.cons_mass = cons_mode::rank1;
    vcs.cons_mom  = cons_mode::rank1;
    MatrixXd C1 = vcs.compute_C1(V);
    MatrixXd C2 = vcs.compute_C2(V);
    MatrixXd out(nx,r);
    vcs.rhs_K(K, V, MatrixXd(0,0), out, C1, C2);

    VectorXd int_V  = in.integrate_v(V, [](double) { return 1.0;});
    VectorXd int_vV = in.integrate_v(V, [](double v) { return v;});
    VectorXd delta_rho = out*int_V;
    VectorXd delta_j   = out*int_vV;

    // set up the exact solution
    VectorXd j(nx), rhouu(nx), Erho(nx);
    for(int i=0;i<nx;i++) {
        double x = vcs.x(i);
        j(i)     = 2.0*cos(2.0*x);
        rhouu(i) = -sin(x);
        Erho(i)  = (1.0+cos(x))*sin(x);
    }

    // check total mass
    VectorXd int_out = in.integrate_x(out, [](double) { return 1.0;});
    BOOST_CHECK_SMALL( abs(int_out.dot(int_V)), 1e-14 );

    // check mass continuity equation
    BOOST_CHECK_SMALL( abs((delta_rho+j).norm()), 1e-13 );

    // check total momentummomentum
    BOOST_CHECK_SMALL( abs(int_out.dot(int_vV)), 1e-14 );

    // check momentum continuity equation
    BOOST_CHECK_SMALL( abs((delta_j+rhouu-Erho).norm()), 1e-13 );

}

BOOST_AUTO_TEST_CASE( CONSERVATIVE_RHSL ) {
    int nx=100, nv=120, r=10;
    array<double,2> a = {0.0,-9.0}; array<double, 2> b = {2*M_PI, 9.0};
    vlasov_continuoussplit vcs(r, {nx,nv}, a, b);

    domain_lowrank2d in(a, b, r, {nx,nv}, lowrank_type::normalized_integral);
    in.init(2,
            [](double x, int k) {
                if(k==0)  return 1.0 + cos(x);
                else      return sin(2.0*x); 
                exit(1);
            },
            [](double v, int k) {
                if(k==0)      return exp(-0.5*v*v)/sqrt(2.0*M_PI);
                else if(k==1) return v*exp(-0.5*v*v)/sqrt(2.0*M_PI);
                exit(1);
            }
           );
    MatrixXd X = in.lr.X;
    MatrixXd L = in.lr.V*in.lr.S;

    // compute rhs
    vcs.cons_mass = cons_mode::rank1;
    vcs.cons_mom  = cons_mode::rank1;
    VectorXd rho = vcs.rho_K(X, L);
    VectorXd E(rho.rows());
    vcs.electric_field(rho, E);
    MatrixXd D1 = vcs.compute_D1(X, E);
    MatrixXd D2 = vcs.compute_D2(X);
    MatrixXd out(nv,r);
    vcs.rhs_L_linear(L, X, MatrixXd(0,0), E, out, D1, D2);

    VectorXd int_out  = in.integrate_v(out, [](double) { return 1.0;});
    VectorXd int_vout = in.integrate_v(out, [](double v) { return v;});
    VectorXd delta_rho = X*int_out;
    VectorXd delta_j   = X*int_vout;

    // check total mass
    VectorXd int_X = in.integrate_x(X, [](double) { return 1.0;});
    BOOST_CHECK_SMALL( abs(int_X.dot(int_out)), 1e-14 );

    // check total momentummomentum
    BOOST_CHECK_SMALL( abs(int_X.dot(int_vout)), 1e-14 );

    // NOTE: for rhs_L the continuity equation is not exactly satisfied.
}

BOOST_AUTO_TEST_CASE( CONSERVATIVE_RHSS ) {
    int nx=100, nv=120, r=10;
    array<double,2> a = {0.0,-9.0}; array<double, 2> b = {2*M_PI, 9.0};
    vlasov_continuoussplit vcs(r, {nx,nv}, a, b);

    domain_lowrank2d in(a, b, r, {nx,nv}, lowrank_type::normalized_integral);
    in.init(2,
            [](double x, int k) {
                if(k==0)  return 1.0 + cos(x);
                else      return sin(2.0*x); 
                exit(1);
            },
            [](double v, int k) {
                if(k==0)      return exp(-0.5*v*v)/sqrt(2.0*M_PI);
                else if(k==1) return v*exp(-0.5*v*v)/sqrt(2.0*M_PI);
                exit(1);
            }
           );
    MatrixXd X = in.lr.X;
    MatrixXd S = in.lr.S;
    MatrixXd V = in.lr.V;

    // compute rhs
    vcs.cons_mass = cons_mode::rank1;
    vcs.cons_mom  = cons_mode::rank1;
    MatrixXd C1 = vcs.compute_C1(V);
    MatrixXd C2 = vcs.compute_C2(V);
    MatrixXd D2 = vcs.compute_D2(X); // D1(E) is computed in rhs_S
    MatrixXd out(r,r);
    vcs.rhs_S(S, V, X, out, C1, C2, D2);

    VectorXd int_X  = in.integrate_x(X, [](double) { return 1.0;});
    VectorXd int_V  = in.integrate_v(V, [](double) { return 1.0;});
    VectorXd int_vV = in.integrate_v(V, [](double v) { return v;});
    VectorXd delta_rho = X*out*int_V;
    VectorXd delta_j   = X*out*int_vV;

    // check total mass
    BOOST_CHECK_SMALL( abs(int_X.dot(out*int_V)), 1e-14 );

    // check total momentummomentum
    BOOST_CHECK_SMALL( abs(int_X.dot(out*int_vV)), 1e-14 );

    // NOTE: for rhs_S the continuity equation is not exactly satisfied.
}

BOOST_AUTO_TEST_CASE( CONSERVATIVE_TSI ) {
    // This test runs a TSI (with a coarse space discretization) and checks
    // that mass and momentum is conserved at the end.
    double T=10.0, tau=0.1;
    int nx=32, nv=32, r=5;
    array<int,2> ext = {nx,nv};
    array<double,2> a = {0.0,-6.0}; array<double, 2> b = {10*M_PI, 6.0};

    domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral);
    in.init(2,
            [](double x, int k) {
                return 0.5*(1.0+0.001*cos(0.2*x));
            },
            [](double v, int k) {
                if(k==0)      return exp(-0.5*pow(v-2.4,2))/sqrt(2.0*M_PI);
                else if(k==1) return exp(-0.5*pow(v+2.4,2))/sqrt(2.0*M_PI);
                else  exit(1);
            });

    double mass0 = in.mass();
    double p0    = in.momentum();

    domain_lowrank2d out(a, b, r, ext, lowrank_type::normalized_integral);
    vlasov_continuoussplit vcs(r, ext, a, b);
    vcs.cons_mass = cons_mode::rank1;
    vcs.cons_mom  = cons_mode::rank1;
    int nsteps = ceil(T/tau);
    double t=0.0;
    for(int k=0;k<nsteps;k++) {
        if(tau > T-t)
            tau = T-t;

        vcs.Lie_step(tau, in.lr, out.lr);
        in.lr = out.lr;
        t += tau;
    }

    // check mass
    BOOST_CHECK_SMALL( abs(out.mass()-mass0), 5e-11 );

    // check momentum
    BOOST_CHECK_SMALL( abs(out.momentum()-p0), 5e-11 );

}

BOOST_AUTO_TEST_CASE( CONSERVATIVE_RHSK_LC ) {
    int nx=100, nv=120, r=10;
    array<double,2> a = {0.0,-9.0}; array<double, 2> b = {2*M_PI, 9.0};
    vlasov_continuoussplit vcs(r, {nx,nv}, a, b);

    domain_lowrank2d in(a, b, r, {nx,nv}, lowrank_type::normalized_integral);;
    in.init(4,
            [](double x, int k) {
                if(k==0)       return 1.0 + cos(x);
                else if(k==1)  return sin(2.0*x); 
                else if(k==2)  return cos(x);
                else           return 0.4*sin(3.0*x+0.3);
                exit(1);
            },
            [](double v, int k) {
                if(k==0)      return exp(-0.5*v*v)/sqrt(2.0*M_PI);
                else if(k==1) return v*exp(-0.5*v*v)/sqrt(2.0*M_PI);
                else if(k==2) return v*v*exp(-0.5*v*v)/sqrt(2.0*M_PI);
                else          return (0.3+v)*exp(-0.5*v*v)/sqrt(2.0*M_PI);
                exit(1);
            }
           );
    MatrixXd K = in.lr.X*in.lr.S;
    MatrixXd V = in.lr.V;

    // compute rhs
    vcs.cons_mass = cons_mode::lc_both;
    vcs.cons_mom  = cons_mode::lc_both;
    MatrixXd C1 = vcs.compute_C1(V);
    MatrixXd C2 = vcs.compute_C2(V);
    MatrixXd out(nx,r);
    vcs.rhs_K(K, V, in.lr.X, out, C1, C2);

    VectorXd int_V  = in.integrate_v(V, [](double) { return 1.0;});
    VectorXd int_vV = in.integrate_v(V, [](double v) { return v;});
    VectorXd int_vsqV = in.integrate_v(V, [](double v) { return v*v;});

    // check total mass
    VectorXd int_out = in.integrate_x(out, [](double) { return 1.0;});
    BOOST_CHECK_SMALL( abs(int_out.dot(int_V)), 1e-14 );

    // check mass continuity equation
    VectorXd pdelta_rho = vcs.project_x(in.lr.X, out)*int_V;

    MatrixXd nablaK = vcs.diff_fft(K, b[0]-a[0]);
    VectorXd nabla_j = vcs.project_x(in.lr.X, nablaK)*int_vV;
    
    BOOST_CHECK_SMALL( abs((pdelta_rho+nabla_j).norm()), 1e-13 );

    // check total momentummomentum
    BOOST_CHECK_SMALL( abs(int_out.dot(int_vV)), 1e-14 );

    // check momentum continuity equation
    VectorXd pdelta_j = vcs.project_x(in.lr.X, out)*int_vV;

    VectorXd nabla_rhouu = vcs.project_x(in.lr.X, nablaK)*int_vsqV;

    VectorXd rho = vcs.rho_K(K, V);
    VectorXd E(rho.rows());
    vcs.electric_field(rho, E);
    MatrixXd Erho = vcs.project_func(in.lr.X, E, K)*int_V;

    BOOST_CHECK_SMALL( abs((pdelta_j+nabla_rhouu-Erho).norm()), 1e-13 );
}

BOOST_AUTO_TEST_CASE( CONSERVATIVE_RHSL_LC ) {
    int nx=100, nv=120, r=10;
    array<double,2> a = {0.0,-9.0}; array<double, 2> b = {2*M_PI, 9.0};
    vlasov_continuoussplit vcs(r, {nx,nv}, a, b);

    // Note: we need functions for which int_V is not zero. Otherweise the
    // equations might become inconsistent and there is not solution.
    domain_lowrank2d in(a, b, r, {nx,nv}, lowrank_type::normalized_integral);
    in.init(4,
            [](double x, int k) {
                if(k==0)       return 1.0 + cos(x);
                else if(k==1)  return sin(2.0*x); 
                else if(k==2)  return cos(x);
                else           return 0.4*sin(3.0*x+0.3);
                exit(1);
            },
            [](double v, int k) {
                if(k==0)      return exp(-0.5*v*v)/sqrt(2.0*M_PI);
                else if(k==1) return v*exp(-0.5*v*v)/sqrt(2.0*M_PI);
                else if(k==2) return v*v*exp(-0.5*v*v)/sqrt(2.0*M_PI);
                else          return (0.3+v)*exp(-0.5*v*v)/sqrt(2.0*M_PI);
                exit(1);
            }
           );
    MatrixXd X = in.lr.X;
    MatrixXd L = in.lr.V*in.lr.S;
    
    // compute rhs
    vcs.cons_mass = cons_mode::lc_both;
    vcs.cons_mom  = cons_mode::lc_both;
    VectorXd rho = vcs.rho_K(X, L);
    VectorXd E(rho.rows());
    vcs.electric_field(rho, E);
    MatrixXd D1 = vcs.compute_D1(X, E);
    MatrixXd D2 = vcs.compute_D2(X);
    MatrixXd out(nv,r);
    vcs.rhs_L_linear(L, X, in.lr.V, E, out, D1, D2);

    VectorXd int_out  = in.integrate_v(out, [](double) { return 1.0;});
    VectorXd int_vout = in.integrate_v(out, [](double v) { return v;});
    VectorXd int_X = in.integrate_x(X, [](double) { return 1.0;});

    // check total mass
    BOOST_CHECK_SMALL( abs(int_X.dot(int_out)), 1e-14 );
   
    // check the projected continuity equation
    VectorXd int_vL = in.integrate_v(L, [](double v) { return v; });
    MatrixXd nablaX = vcs.diff_fft(X, b[0]-a[0]);
    VectorXd nabla_j = vcs.project_x(X, nablaX)*int_vL;

    BOOST_CHECK_SMALL( (int_out + nabla_j).norm(), 1e-14 );


    // check total momentummomentum
    BOOST_CHECK_SMALL( abs(int_X.dot(int_vout)), 1e-14 );

    // check the projected momentum balance equation
    VectorXd int_vsqL = in.integrate_v(L, [](double v) { return v*v; });
    VectorXd nabla_rhouu = vcs.project_x(X, nablaX)*int_vsqL;

    VectorXd int_L = in.integrate_v(L, [](double) { return 1.0; });
    MatrixXd Erho = vcs.project_func(X, E, X)*int_L;

    BOOST_CHECK_SMALL( (int_vout + nabla_rhouu - Erho).norm(), 1e-14 );

}

BOOST_AUTO_TEST_CASE( CONSERVATIVE_RHSS_LC ) {
    int nx=100, nv=120, r=10;
    array<double,2> a = {0.0,-9.0}; array<double, 2> b = {2*M_PI, 9.0};
    vlasov_continuoussplit vcs(r, {nx,nv}, a, b);

    domain_lowrank2d in(a, b, r, {nx,nv}, lowrank_type::normalized_integral);
    in.init(4,
            [](double x, int k) {
                if(k==0)       return 1.0 + cos(x);
                else if(k==1)  return sin(2.0*x); 
                else if(k==2)  return cos(x);
                else           return 0.4*sin(3.0*x+0.3);
                exit(1);
            },
            [](double v, int k) {
                if(k==0)      return exp(-0.5*v*v)/sqrt(2.0*M_PI);
                else if(k==1) return v*exp(-0.5*v*v)/sqrt(2.0*M_PI);
                else if(k==2) return v*v*exp(-0.5*v*v)/sqrt(2.0*M_PI);
                else          return (0.3+v)*exp(-0.5*v*v)/sqrt(2.0*M_PI);
                exit(1);
            }
           );
    MatrixXd X = in.lr.X;
    MatrixXd S = in.lr.S;
    MatrixXd V = in.lr.V;

    // compute rhs
    vcs.cons_mass = cons_mode::lc_both;
    vcs.cons_mom  = cons_mode::lc_both;
    MatrixXd C1 = vcs.compute_C1(V);
    MatrixXd C2 = vcs.compute_C2(V);
    MatrixXd D2 = vcs.compute_D2(X); // D1(E) is computed in rhs_S
    MatrixXd out(r,r);
    vcs.rhs_S(S, V, X, out, C1, C2, D2);

    VectorXd int_X  = in.integrate_x(X, [](double) { return 1.0;});
    VectorXd int_V  = in.integrate_v(V, [](double) { return 1.0;});
    VectorXd int_vV = in.integrate_v(V, [](double v) { return v;});
    VectorXd int_vsqV = in.integrate_v(V, [](double v) { return v*v; });

    // check total mass
    BOOST_CHECK_SMALL( abs(int_X.dot(out*int_V)), 1e-14 );

    // check the projected continuity equation
    MatrixXd nablaX = vcs.diff_fft(X, b[0]-a[0]);
    VectorXd nabla_j = vcs.project_x(X, nablaX)*S*int_vV;

    // Note: the continuity equation has a different sign for rhs_S
    BOOST_CHECK_SMALL( (out*int_V - nabla_j).norm(), 1e-14 );


    // check total momentummomentum
    BOOST_CHECK_SMALL( abs(int_X.dot(out*int_vV)), 1e-14 );

    // check the projected momentum balance equation
    VectorXd nabla_rhouu = vcs.project_x(X, nablaX)*S*int_vsqV;
    
    VectorXd rho = vcs.rho_K(X*S, V);
    VectorXd E(rho.rows());
    vcs.electric_field(rho, E);
    MatrixXd Erho = vcs.project_func(X, E, X)*S*int_V;
    
    BOOST_CHECK_SMALL( (out*int_vV - nabla_rhouu + Erho).norm(), 1e-14 );
}


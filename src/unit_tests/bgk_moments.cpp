
#define BOOST_TEST_MODULE BGK_MOMENTS
#include <boost/test/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>
#include <unsupported/Eigen/MatrixFunctions>
#include <algorithms/lowrank-bgk-moments.hpp>

double err_conv(MatrixXd out, MatrixXd exact, domain4d_data dd) {
    Index start_v = (3*dd.n[2])/8;
    Index end_v   = (5*dd.n[2])/8;
    Index start_w = (3*dd.n[3])/8;
    Index end_w   = (5*dd.n[3])/8;
    double err = 0.0;
    for(Index k=0;k<dd.r;k++) {
        for(Index j=start_w;j<end_w;j++) {
            for(Index i=start_v;i<end_v;i++) {
                double diff = out(i+j*dd.n[2],k)-exact(i+j*dd.n[2],k);
                err = max(err, abs(diff));
            }
        }
    }
    return err;
}

BOOST_AUTO_TEST_CASE( CONVOLUTION_V ) {
    domain4d_data dd({0.0,-M_PI,-9.0,-10.0}, {M_PI, M_PI, 9.0, 10.0}, {25,26,32,34}, 4);

    auto V0 = [](double v, double w, Index r) {
        if(r==0)       return 1.0;
        else if(r==1)  return v;
        else if(r==2)  return w;
        else if(r==3)  return v*w;
        else           exit(1);
    };

    lowrank4d lr(dd.r, dd.n);
    lowrank4d_init_v(V0, lr.V, dd);

    array<MatrixXd,2> out = convolution_v(lr.V, dd);

    // exact result v-direction
    auto V_exact_v = [](double v, double w, Index r) {
        if(r==0)       return 2.0*M_PI*v;
        else if(r==1)  return 2.0*M_PI*(1.0 + v*v);
        else if(r==2)  return 2.0*M_PI*v*w;
        else if(r==3)  return 2.0*M_PI*(1.0 + v*v)*w;
        else           exit(1);
    };
    lowrank4d lr_exact_v(dd.r, dd.n);
    lowrank4d_init_v(V_exact_v, lr_exact_v.V, dd);
    BOOST_CHECK_SMALL( err_conv(out[0], lr_exact_v.V, dd), 1e-9 );

    // exact result w-direction
    auto V_exact_w = [](double v, double w, Index r) {
        if(r==0)       return 2.0*M_PI*w;
        else if(r==1)  return 2.0*M_PI*v*w;
        else if(r==2)  return 2.0*M_PI*(1.0+w*w);
        else if(r==3)  return 2.0*M_PI*v*(1.0 + w*w);
        else           exit(1);
    };
    lowrank4d lr_exact_w(dd.r, dd.n);
    lowrank4d_init_v(V_exact_w, lr_exact_w.V, dd);
    BOOST_CHECK_SMALL( err_conv(out[1], lr_exact_w.V, dd), 1e-9 );
}


BOOST_AUTO_TEST_CASE( CONVOLUTION_VTENSORV ) {
    domain4d_data dd({0.0,-M_PI,-9.0,-10.0}, {M_PI, M_PI, 9.0, 10.0}, {25,26,32,34}, 3);

    auto V0 = [](double v, double w, Index r) {
        if(r==0)       return 1.0;
        else if(r==1)  return v;
        else if(r==2)  return w;
        else           exit(1);
    };

    lowrank4d lr(dd.r, dd.n);
    lowrank4d_init_v(V0, lr.V, dd);

    array<MatrixXd,4> out = convolution_vtensorv(lr.V, dd);

    // exact result vv
    auto V_exact_vv = [](double v, double w, Index r) {
        if(r==0)       return 2.0*M_PI*(1.0 + v*v);
        else if(r==1)  return 2.0*M_PI*v*(3.0 + v*v);
        else if(r==2)  return 2.0*M_PI*(1.0 + v*v)*w;
        else           exit(1);
    };
    lowrank4d lr_exact_vv(dd.r, dd.n);
    lowrank4d_init_v(V_exact_vv, lr_exact_vv.V, dd);
    BOOST_CHECK_SMALL( err_conv(out[0], lr_exact_vv.V, dd), 1e-8 );

    // exact result ww
    auto V_exact_ww = [](double v, double w, Index r) {
        if(r==0)       return 2.0*M_PI*(1.0 + w*w);
        else if(r==1)  return 2.0*M_PI*v*(1.0 + w*w);
        else if(r==2)  return 2.0*M_PI*w*(3.0 + w*w);
        else           exit(1);
    };
    lowrank4d lr_exact_ww(dd.r, dd.n);
    lowrank4d_init_v(V_exact_ww, lr_exact_ww.V, dd);
    BOOST_CHECK_SMALL( err_conv(out[3], lr_exact_ww.V, dd), 1e-8 );

    // exact result vw
    auto V_exact_vw = [](double v, double w, Index r) {
        if(r==0)       return 2.0*M_PI*v*w;
        else if(r==1)  return 2.0*M_PI*(1.0 + v*v)*w;
        else if(r==2)  return 2.0*M_PI*v*(1.0 + w*w);
        else           exit(1);
    };
    lowrank4d lr_exact_vw(dd.r, dd.n);
    lowrank4d_init_v(V_exact_vw, lr_exact_vw.V, dd);
    BOOST_CHECK_SMALL( err_conv(out[1], lr_exact_vw.V, dd), 1e-8 );
    BOOST_CHECK_SMALL( err_conv(out[2], lr_exact_vw.V, dd), 1e-8 );
}


BOOST_AUTO_TEST_CASE( EVALUATE_G ) {
    domain4d_data dd({0.0,-M_PI,-6.0,-7.0}, {M_PI, M_PI, 6.0, 7.0}, {51,49,61,55}, 3);
    MatrixXd g(dd.n[2]*dd.n[3], dd.r);
    for(auto i : range<2>(dd.n[2],dd.n[3])) {
        double v = dd.v(i);
        double w = dd.w(i);

        g(dd.lin_idx_v(i), 0) = v*v+w*w;//exp(-v*v-0.5*w*w);
        g(dd.lin_idx_v(i), 1) = exp(-0.2*v*v-0.1*w*w); //v*v + w*w;//sin(2.0*M_PI*v*w);
        g(dd.lin_idx_v(i), 2) = sin(0.3*(v+w));
    }

    srand(0); // make the test determinstic
    array<VectorXd,2> u;
    u[0].resize(dd.n[0]*dd.n[1]);
    u[1].resize(dd.n[0]*dd.n[1]);
    for(auto i : range<2>(dd.n[0],dd.n[1])) {
        // We do not care about the boundaries. The convolution result there is
        // incorrect in any case.
        u[0](dd.lin_idx_x(i)) = 0.7*(dd.a[2] + (dd.b[2]-dd.a[2])*double(rand())/double(RAND_MAX));
        u[1](dd.lin_idx_x(i)) = 0.7*(dd.a[3] + (dd.b[3]-dd.a[3])*double(rand())/double(RAND_MAX));
    }
    MatrixXd out = evaluate_g(g, u, dd);

    MatrixXd exact_result(dd.n[0]*dd.n[1],dd.r);
    for(auto i : range<2>(dd.n[0],dd.n[1])) {
        double v = u[0](dd.lin_idx_x(i));
        double w = u[1](dd.lin_idx_x(i));

        exact_result(dd.lin_idx_x(i), 0) = v*v+w*w;//exp(-v*v-0.5*w*w);
        exact_result(dd.lin_idx_x(i), 1) = exp(-0.2*v*v-0.1*w*w);
        exact_result(dd.lin_idx_x(i), 2) = sin(0.3*(v+w));
    }

    BOOST_CHECK_SMALL( (exact_result-out).norm(), 5e-5 );
}


BOOST_AUTO_TEST_CASE( COEFFICIENTS ) {

    domain4d_data dd({0,-M_PI,-6.0,-5.0}, {2.0*M_PI, M_PI, 6.0, 5.0}, {32*25,32*26,27,28}, 5);

    auto X0 = [](double x, double y, Index r) {
        return cos(2.0*r*x)*sin((r+1)*y);
    };
    auto V0 = [](double v, double w, Index r) {
        return exp(-pow(v+0.2,2)/(r+1.0))*exp(-0.5*pow(w-0.1,2));
    };

    lowrank4d lr(dd.r, dd.n);
    lowrank4d_init_x(X0, lr.X, dd);
    lowrank4d_init_v(V0, lr.V, dd);

    vlasov_bgk_moments vcs(dd, 1e-1);
    vlasov_bgk_moments vcs_upwind(dd, 1e-1, DM_UPWIND_LXF);

    // compute_C1
    array<MatrixXd,2> c1 = vcs.compute_C1(lr.V);

    MatrixXd c10_exact(dd.r,dd.r);
    c10_exact << -0.4442882938147774, -0.5130199320635223  , -0.5441398092689678  , -0.5619851784819181  , -0.5735737209531799,
                 -0.5130199320635223,   -0.6283185307164583,   -0.6882884651430931,   -0.7255197456788222,   -0.7509842836121457,
                 -0.5441398092689678,   -0.6882884651430931,   -0.7695298978546416,   -0.822662060566111 ,   -0.860360555701054,
                 -0.5619851784819181,   -0.7255197456788222,   -0.822662060566111 ,   -0.888576504551976 ,   -0.936641480421451,
                 -0.5735737209531799,   -0.7509842836121457,   -0.860360555701054 ,   -0.936641480421451 ,   -0.993455997502033;
    BOOST_CHECK_SMALL( (c10_exact-c1[0]).norm(), 1e-5);
    MatrixXd c11_exact(dd.r,dd.r);
    c11_exact << 0.2221441468871369, 0.2565099660083765, 0.2720699046096807, 0.2809925892153425,  0.2867868604504451,
                 0.2565099660083765, 0.3141592653295898, 0.3441442325405219, 0.3627598728126813,  0.3754921418080904,
                 0.2720699046096807, 0.3441442325405219, 0.3847649490083232, 0.4113310323886752,  0.430180290192216,
                 0.2809925892153425, 0.3627598728126813, 0.4113310323886752, 0.444288292175697 ,  0.4683209723006971,
                 0.2867868604504451, 0.3754921418080904, 0.430180290192216 , 0.4683209723006971,  0.4967293530762791;
    BOOST_CHECK_SMALL( (c11_exact-c1[1]).norm(), 1e-5);


    // compute_Cs
    array<MatrixXd,4> cs = vcs.compute_Cstar(lr.V);

    MatrixXd exact(dd.r,dd.r);
    exact << 0.644218, 0.957637, 1.12909, 1.23637, 1.30966,
             0.957637, 1.69646, 2.20252, 2.5635, 2.83228,
             1.12909, 2.20252, 3.03964, 3.69023, 4.20501,
             1.23637, 2.5635, 3.69023, 4.6206, 5.39089,
             1.30966, 2.83228, 4.20501, 5.39089, 6.40781;
    BOOST_CHECK_SMALL( (exact - cs[0]).norm(), 2e-5 );

    exact << -0.0444288, -0.051302, -0.054414, -0.0561985, -0.0573574,
             -0.051302, -0.0628319, -0.0688288, -0.072552, -0.0750984,
             -0.054414, -0.0688288, -0.076953, -0.0822662, -0.0860361,
             -0.0561985, -0.072552, -0.0822662, -0.0888577, -0.0936642,
             -0.0573574, -0.0750984, -0.0860361, -0.0936642, -0.0993459;
    BOOST_CHECK_SMALL( (exact - cs[1]).norm(), 2e-7 );
    BOOST_CHECK_SMALL( (exact - cs[2]).norm(), 2e-7 );

    exact << 1.13294, 1.3082, 1.38756, 1.43306, 1.46261,
             1.3082, 1.60221, 1.75514, 1.85008, 1.91501,
             1.38756, 1.75514, 1.9623, 2.09779, 2.19392,
             1.43306, 1.85008, 2.09779, 2.26587, 2.38844,
             1.46261, 1.91501, 2.19392, 2.38844, 2.53332;
    BOOST_CHECK_SMALL( (exact - cs[3]).norm(), 2e-5 );


    // average_v
    MatrixXd average_v = vcs.average_v(lr.V);

    VectorXd exact_avg(dd.r);
    exact_avg << 4.44288, 6.28319, 7.6953, 8.88577, 9.93459;

    for(Index k=0;k<dd.r;k++)
        BOOST_CHECK_SMALL( (average_v-exact_avg).norm() , 2e-3 );


    // average_x
    MatrixXd average_x = vcs.average_x(lr.X, lr.X.col(0));

    exact_avg << 2.0*pow(M_PI,2), 0.0, 0.0, 0.0, 0.0;

    for(Index k=0;k<dd.r;k++)
        BOOST_CHECK_SMALL( (average_x-exact_avg).norm() , 1e-10 );


    // compute_M
    moments mom;
    mom.init(
            [](double x, double y) { return 2.0 + cos(x+y); },
            [](double x, double y) { return cos(2*x-y);     },
            [](double x, double y) { return -sin(x+2*y);    },
            dd);
    VectorXd I1 = init_moment(dd, [](double x, double y) { return sin(x+y); });
    array<VectorXd,2> I2 = {
        init_moment(dd, [](double x, double y) { return sin(2*x+y); }),
        init_moment(dd, [](double x, double y) { return cos(x)*sin(y); })
    };

    M_t M        = vcs.compute_M(mom, I1, I2);
    M_t M_upwind = vcs_upwind.compute_M(mom, I1, I2);

    VectorXd exact_M1 = init_moment(dd, [](double x, double y) {
        return (-cos(2*x-y)*sin(2*x+y)+cos(x)*sin(y)*sin(x+2*y)+sin(x+y)*(1.0+pow(cos(2*x-y),2)+pow(sin(x+2*y),2)))/(2.0 + cos(x+y));
    });

    array<VectorXd,2> exact_M2 = {
        init_moment(dd, [](double x, double y) {
            return sin(4*x-2*y) + (-2*pow(cos(x-0.5*y),2)*sin(x+y)+sin(2*x+y))/(2.0+cos(x+y)) - 0.5*sin(2*(x+2*y));
        }),
        init_moment(dd, [](double x, double y) {
            return -0.5*sin(4*x-2*y) + (cos(x)*sin(y)+sin(x+y)*(-1+sin(x+2*y)))/(2.0+cos(x+y)) - sin(2*(x+2*y));
        }),
    };

    array<VectorXd,4> exact_M3 = {
        init_moment(dd, [](double x, double y) { return -2.0*sin(2*x-y); }),
        init_moment(dd, [](double x, double y) { return -cos(x+2*y); }),
        init_moment(dd, [](double x, double y) { return sin(2*x-y); }),
        init_moment(dd, [](double x, double y) { return -2.0*cos(x+2*y); })
    };

    BOOST_CHECK_SMALL( (exact_M1-M.M1).lpNorm<Eigen::Infinity>(), 1e-11 );
    BOOST_CHECK_SMALL( (exact_M2[0]-M.M2[0]).lpNorm<Eigen::Infinity>(), 1e-11 );
    BOOST_CHECK_SMALL( (exact_M2[1]-M.M2[1]).lpNorm<Eigen::Infinity>(), 1e-11 );
    BOOST_CHECK_SMALL( (exact_M3[0]-M.M3[0]).lpNorm<Eigen::Infinity>(), 1e-11 );
    BOOST_CHECK_SMALL( (exact_M3[1]-M.M3[1]).lpNorm<Eigen::Infinity>(), 1e-11 );
    BOOST_CHECK_SMALL( (exact_M3[2]-M.M3[2]).lpNorm<Eigen::Infinity>(), 1e-11 );
    BOOST_CHECK_SMALL( (exact_M3[3]-M.M3[3]).lpNorm<Eigen::Infinity>(), 1e-11 );

    BOOST_CHECK_SMALL( (exact_M1-M_upwind.M1).lpNorm<Eigen::Infinity>(), 0.03 );
    BOOST_CHECK_SMALL( (exact_M2[0]-M_upwind.M2[0]).lpNorm<Eigen::Infinity>(), 0.03 );
    BOOST_CHECK_SMALL( (exact_M2[1]-M_upwind.M2[1]).lpNorm<Eigen::Infinity>(), 0.03 );
    BOOST_CHECK_SMALL( (exact_M3[0]-M_upwind.M3[0]).lpNorm<Eigen::Infinity>(), 0.03 );
    BOOST_CHECK_SMALL( (exact_M3[1]-M_upwind.M3[1]).lpNorm<Eigen::Infinity>(), 0.03 );
    BOOST_CHECK_SMALL( (exact_M3[2]-M_upwind.M3[2]).lpNorm<Eigen::Infinity>(), 0.03 );
    BOOST_CHECK_SMALL( (exact_M3[3]-M_upwind.M3[3]).lpNorm<Eigen::Infinity>(), 0.03 );

    // compute_D1
    auto X0_2 = [](double x, double y, Index r) {
        return 1.0/(2.0 + sin(x + y + r));
    };
    lowrank4d_init_x(X0_2, lr.X, dd);

    array<MatrixXd,2> d1        = vcs.compute_D1(lr.X);
    array<MatrixXd,2> d1_upwind = vcs_upwind.compute_D1(lr.X);

    MatrixXd d1_exact(dd.r,dd.r);
    d1_exact << 0.0, -1.83854, -1.50734, -0.201537, 1.17789,
                 1.83854, 0.0, -1.83854, -1.50733, -0.201537,
                 1.50733, 1.83854, 0.0, -1.83854, -1.50733,
                 0.201537, 1.50733, 1.83854, 0.0, -1.83854,
                 -1.17789, 0.201537, 1.50733, 1.83854, 0.0;

    // due to the symmetry in X d1[0] and d1[1] give the same result.
    BOOST_CHECK_SMALL( (d1_exact-d1[0]).norm(), 2e-5);
    BOOST_CHECK_SMALL( (d1_exact-d1[1]).norm(), 2e-5);

    BOOST_CHECK_SMALL( (d1_exact-d1_upwind[0]).norm(), 0.03);
    BOOST_CHECK_SMALL( (d1_exact-d1_upwind[1]).norm(), 0.03);

    // compute_Ds
    auto X0_3 = [](double x, double y, Index r) {
        return sin(x+y+r);
    };
    lowrank4d_init_x(X0_3, lr.X, dd);
    VectorXd M1 = init_moment(dd, [](double x, double y) { return sin(x*y); });

    MatrixXd ds = vcs.compute_Ds(lr.X, M1);

    MatrixXd ds_exact(dd.r,dd.r);
    ds_exact << -1.17951, -1.71255, -0.671078, 0.987378, 1.73804,
                -1.71255, -0.671078, 0.987378, 1.73804, 0.89076,
                -0.671078, 0.987378, 1.73804, 0.89076, -0.775485,
                 0.987378, 1.73804, 0.89076, -0.775485, -1.72875,
                 1.73804, 0.89076, -0.775485, -1.72875, -1.09261;

    BOOST_CHECK_SMALL( (ds_exact-ds).norm(), 6e-3 );

    // compute_R
    MatrixXd R = vcs.compute_R(lr.X, M1);

    MatrixXd R_exact(dd.r,dd.r);
    R_exact << -1.17951, -1.71255, -0.671078, 0.987378, 1.73804,
                -1.71255, -0.671078, 0.987378, 1.73804, 0.89076,
                -0.671078, 0.987378, 1.73804, 0.89076, -0.775485,
                 0.987378, 1.73804, 0.89076, -0.775485, -1.72875,
                 1.73804, 0.89076, -0.775485, -1.72875, -1.09261;

    BOOST_CHECK_SMALL( (R_exact-R).norm(), 6e-3 );

    // compute_Dss
    array<VectorXd, 2> M2 = {
        init_moment(dd, [](double x, double y) { return sin(x*y); }),
        init_moment(dd, [](double x, double y) { return cos(x*y); })
    };

    array<MatrixXd,2> dss = vcs.compute_Dss(lr.X, M2);

    MatrixXd dss_exact(dd.r,dd.r);
    dss_exact <<  2.63057, 0.498905, -2.09145, -2.75894, -0.889869,
                  0.498905, 0.0852933, -0.406737, -0.524815, -0.160381,
                 -2.09145, -0.406737, 1.65193, 2.19182, 0.716561,
                 -2.75894, -0.524815, 2.19182, 2.89331, 0.9347,
                 -0.889869, -0.160381, 0.716561, 0.9347, 0.29348;

    BOOST_CHECK_SMALL( (ds_exact-dss[0]).norm(),  6e-3 );
    BOOST_CHECK_SMALL( (dss_exact-dss[1]).norm(), 5e-2 );


    // compute_Dsss
    array<VectorXd, 4> M3 = {
        init_moment(dd, [](double x, double y) { return sin(x*y); }),
        init_moment(dd, [](double x, double y) { return cos(x*y); }),
        init_moment(dd, [](double x, double y) { return 1.0/(2.0 + sin(x)); }),
        init_moment(dd, [](double x, double y) { return 1.0/(2.0 + cos(x*y)); })
    };
    array<MatrixXd,4> dsss = vcs.compute_Dsss(lr.X, M3);

    MatrixXd dsss_exact2(dd.r, dd.r);
    dsss_exact2 << 11.3964, 6.15752, -4.74259, -11.2824, -7.44921,
                   6.15752, 11.3964, 6.15752, -4.74259, -11.2824,
                  -4.74259, 6.15752, 11.3964, 6.15752, -4.74259,
                  -11.2824, -4.74259, 6.15752, 11.3964, 6.15752,
                  -7.44921, -11.2824, -4.74259, 6.15752, 11.3964;

    MatrixXd dsss_exact3(dd.r, dd.r);
    dsss_exact3 << 10.6668, 6.09117, -4.08463, -10.505, -7.26717,
                   6.09117, 11.4595, 6.29203, -4.66032, -11.328,
                   -4.08463, 6.29203, 10.8838, 5.46908, -4.97391,
                   -10.505, -4.66032, 5.46908, 10.5702, 5.95316,
                   -7.26717, -11.328, -4.97391, 5.95316, 11.4069;

    BOOST_CHECK_SMALL( (ds_exact-dsss[0]).norm(),  6e-3 );
    BOOST_CHECK_SMALL( (dss_exact-dsss[1]).norm(), 5e-2 );
    BOOST_CHECK_SMALL( (dsss_exact2-dsss[2]).norm(), 1e-4 );
    BOOST_CHECK_SMALL( (dsss_exact3-dsss[3]).norm(), 2e-2 );
}


BOOST_AUTO_TEST_CASE( STEPS ) {
    domain4d_data dd({0,-M_PI,-6.0,-5.0}, {2.0*M_PI, M_PI, 6.0, 5.0}, {32*32,30*32,27,28}, 3);
    double epsilon=1e-1;
    double tau=0.3;

    vlasov_bgk_moments vcs(dd, epsilon);
    vlasov_bgk_moments vcs_upwind(dd, epsilon, DM_UPWIND_LXF);

    // Step K
    MatrixXd C1_0(dd.r,dd.r), C1_1(dd.r,dd.r);
    C1_0 << 1, 2, 3,
            4, 5, 6,
            7, 8, 9;
    C1_1 << 1, 0, 5,
            0, 2, 0,
            0, 0, 3;

    MatrixXd Cs_0(dd.r,dd.r), Cs_1(dd.r,dd.r), Cs_2(dd.r,dd.r), Cs_3(dd.r,dd.r);
    Cs_0 << 1, 0, 0,
            0, 0, 3,
            0, 0, 2;
    Cs_1 << 1, 0, 0,
            1, 0, 0,
            0, 0, 2;
    Cs_2 << 1, 0, 0,
            0, 1, 0,
            0, 0, 2;
    Cs_3 << 1, 0, 0,
            0, 0, 0,
            2, 0, 2;

    VectorXd avg_V(dd.r);
    avg_V << 0.0, 1.0, 2.0;

    auto f_K0 = [](double x, double y, Index r) {
        return cos(2.0*r*x)*sin((r+1)*y);
    };
    MatrixXd K0(dd.n[0]*dd.n[1], dd.r);
    lowrank4d_init_x(f_K0, K0, dd);

    VectorXd M1 = init_moment(dd, [](double x, double y) { return cos(x+y); });
    array<VectorXd, 2> M2 = {
        init_moment(dd, [](double x, double y) { return cos(y); }),
        init_moment(dd, [](double x, double y) { return sin(x); })
    };
    array<VectorXd, 4> M3 = {
        init_moment(dd, [](double x, double y) { return sin(x); }),
        init_moment(dd, [](double x, double y) { return cos(y); }),
        init_moment(dd, [](double x, double y) { return sin(y); }),
        init_moment(dd, [](double x, double y) { return cos(x); })
    };

    // TODO: currently rho is constant
    MatrixXd K1 = vcs.step_K(tau, K0, M_t(M1, M2, M3), {C1_0, C1_1},
                             {Cs_0, Cs_1, Cs_2, Cs_3}, avg_V, VectorXd::Ones(dd.n[0]*dd.n[1]));
    MatrixXd K1_upwind = vcs_upwind.step_K(tau, K0, M_t(M1, M2, M3), {C1_0, C1_1},
                                           {Cs_0, Cs_1, Cs_2, Cs_3}, avg_V, VectorXd::Ones(dd.n[0]*dd.n[1]));

    auto f_K_exact = [tau,epsilon,f_K0](double x, double y, Index r) {
        double c1 = 0.0;
        if(r==0)
            c1 = cos(y) + 15.0*cos(4*x)*cos(3*y) - 4.0*(sin(2*x)*sin(2*y) + 3.0*sin(4*x)*sin(3*y));
        else if(r==1)
            c1 = 4.0*cos(2*x)*cos(2*y) - 10.0*sin(2*x)*sin(2*y) - 24.0*sin(4*x)*sin(3*y);
        else
            c1 = 9.0*cos(4*x)*cos(3*y) - 4.0*(4.0*sin(2*x)*sin(2*y)+9.0*sin(4*x)*sin(3*y));

        // First part of C2
        double c2 = 0.0;
        if(r==0)
            c2 = cos(x+y)*sin(y);
        else if(r==1)
            c2 = cos(2*x)*cos(x+y)*sin(2*y);
        else
            c2 = cos(4*x)*cos(x+y)*sin(3*y);

        // Second part of C2
        if(r==0)
            c2 += (cos(y) + 4*cos(2*x)*pow(cos(y),2) + sin(x))*sin(y) + cos(4*x)*(3*cos(y) + 5*sin(x))*sin(3*y);
        else if(r==1)
            c2 += 4*cos(y)*sin(y) + cos(2*x)*(5*cos(y) + 2*sin(x))*sin(2*y) + 6*cos(4*x)*cos(y)*sin(3*y);
        else
            c2 += 7*cos(y)*sin(y) + 8*cos(2*x)*cos(y)*sin(2*y) + 3*cos(4*x)*(3*cos(y) + sin(x))*sin(3*y);

        // Third part of C3
        if(r==0)
            c2 += sin(y)*(cos(x) + cos(y) + sin(x) + sin(y));
        else if(r==1)
            c2 += sin(y)*(cos(y) + cos(2*x)*sin(2*y)) + 3*cos(4*x)*sin(x)*sin(3*y);
        else
            c2 += 2*cos(x)*sin(y) + 2*cos(4*x)*(cos(x) + cos(y) + sin(x) + sin(y))*sin(3*y);

        return 1.0/(1.0+tau/epsilon)*(f_K0(x,y,r) - tau*c1 - tau*c2) + tau/(tau+epsilon)*double(r);
    };
    lowrank4d lr_exact(dd.r, dd.n);
    MatrixXd K_exact(dd.n[0]*dd.n[1], dd.r);
    lowrank4d_init_x(f_K_exact, K_exact, dd);

    BOOST_CHECK_SMALL( (K1-K_exact).lpNorm<Eigen::Infinity>(), 1e-12 );
    BOOST_CHECK_SMALL( (K1_upwind-K_exact).lpNorm<Eigen::Infinity>(), 0.05 );


    // step S
    MatrixXd S0(dd.r,dd.r);
    S0 << 1, -2, 2,
          5,  4, 3,
          -3, 7, 2;

    MatrixXd D1_0 = C1_0.transpose();
    MatrixXd D1_1 = C1_1.transpose();

    MatrixXd Ds(dd.r,dd.r);
    Ds << 1, 0, 1,
          -2, -2, 0,
          0, 0, 1;

    MatrixXd Dss_0(dd.r,dd.r);
    Dss_0 << 0, 0, 2,
             0, 3, 0,
             -1, 0, 1;
    MatrixXd Dss_1(dd.r,dd.r);
    Dss_1 << 2, 0, 0,
             0, 0, 1,
             1, 0, 1;

    MatrixXd Dsss_0 = Cs_0.transpose();
    MatrixXd Dsss_1 = Cs_1.transpose();
    MatrixXd Dsss_2 = Cs_2.transpose();
    MatrixXd Dsss_3 = Cs_3.transpose();

    VectorXd avg_X(dd.r);
    avg_X << 3.0, 2.0, 1.0;

    // TODO: R is just an identity matrix
    MatrixXd S1 = vcs.step_S(tau, S0, {C1_0, C1_1}, {Cs_0, Cs_1, Cs_2, Cs_3}, {D1_0, D1_1}, 
                             Ds, {Dss_0, Dss_1}, {Dsss_0, Dsss_1, Dsss_2, Dsss_3}, avg_X,
                             avg_V, MatrixXd::Identity(dd.r,dd.r));

    MatrixXd S1_exact(dd.r, dd.r);
    S1_exact << -20.6, -39.95, -62.8,
                -25.6, -56.45, -87.3,
                -30.,  -57.2,  -88.9;

    BOOST_CHECK_SMALL( (S1_exact-S1).norm(), 1e-13 );


    // step L
    auto f_L0 = [](double v, double w, Index r) {
        return exp(-pow(v+0.2,2)/(r+1.0))*exp(-0.5*pow(w-0.1,2));
    };
    MatrixXd L0(dd.n[2]*dd.n[3], dd.r);
    lowrank4d_init_v(f_L0, L0, dd);

    // TODO: R is just the identity matrix
    MatrixXd L1 = vcs.step_L(tau, L0, {D1_0, D1_1}, Ds, {Dss_0, Dss_1},
                             {Dsss_0, Dsss_1, Dsss_2, Dsss_3}, avg_X, MatrixXd::Identity(dd.r,dd.r));

    auto f_L_exact = [](double v, double w, Index r) {
        if(r==0)
            return exp(-pow(0.2 + v,2) - pow(-0.1 + w,2)/2.)*(0.175 + 2.25*exp(pow(0.2 + v,2) + pow(-0.1 + w,2)/2.) - 0.075*pow(v,2) + v*(-0.075 - 0.15*w) + exp(pow(0.2 + v,2)/2.)*v*(-0.3 - 0.075*w) + (-0.22499999999999998 - 0.075*w)*w + exp((2*pow(0.2 + v,2))/3.)*(-0.075 - 0.6749999999999999*v - 0.15*pow(w,2)));
        else if(r==1)
            return exp(-pow(0.2 + v,2) - pow(-0.1 + w,2)/2.)*(0.15 + 1.5*exp(pow(0.2 + v,2) + pow(-0.1 + w,2)/2.) - 0.15*v + exp(pow(0.2 + v,2)/2.)*(0.4 + v*(-0.6 - 0.075*w) - 0.15*w) + exp((2*pow(0.2 + v,2))/3.)*(-0.6*v - 0.075*w));
        else
            return exp(-pow(0.2 + v,2) - pow(-0.1 + w,2)/2.)*(0.75*exp(pow(0.2 + v,2) + pow(-0.1 + w,2)/2.) - 0.15*v + exp(pow(0.2 + v,2)/2.)*(-0.44999999999999996 - 0.22499999999999998*v)*v - 0.44999999999999996*w + exp((2*pow(0.2 + v,2))/3.)*(0.175 - 0.15*pow(v,2) + v*(-0.75 - 0.3*w) + (-0.3 - 0.15*w)*w));
    };

    MatrixXd L_exact(dd.n[2]*dd.n[3], dd.r);
    lowrank4d_init_v(f_L_exact, L_exact, dd);

    BOOST_CHECK_SMALL( (L_exact-L1).lpNorm<Eigen::Infinity>(), 1e-14 );

}

BOOST_AUTO_TEST_CASE( LAX_FRIEDRICHS_BASICS ) {
    domain4d_data dd({0,-M_PI,-6.0,-5.0}, {2.0*M_PI, M_PI, 6.0, 5.0}, {10,8,27,28}, 3);
    vlasov_bgk_moments vcs(dd, 1e-1);

    VectorXd v(dd.n[0]*dd.n[1]);
    for(auto i : range<2>(dd.n[0],dd.n[1]))
        v(dd.lin_idx_x(i)) = double(i[0])*double(i[0]) - double(i[1])*double(i[1]);

    VectorXd v_x = vcs.avgpm_x(v);
    VectorXd v_y = vcs.avgpm_y(v);

    VectorXd v_x_exact(dd.n[0]*dd.n[1]);
    VectorXd v_y_exact(dd.n[0]*dd.n[1]);
    for(auto i : range<2>(dd.n[0],dd.n[1])) {
        if(i[0]==0)
            v_x_exact(dd.lin_idx_x(i)) =  0.5*(pow(double(dd.n[0]-1),2)+1.0) - double(i[1])*double(i[1]);
        else if(i[0]==dd.n[0]-1)
            v_x_exact(dd.lin_idx_x(i)) =  0.5*pow(double(dd.n[0]-2),2) - double(i[1])*double(i[1]);
        else
            v_x_exact(dd.lin_idx_x(i)) =  double(i[0])*double(i[0]) + 1.0 - double(i[1])*double(i[1]);

        if(i[1]==0)
            v_y_exact(dd.lin_idx_x(i)) =  double(i[0])*double(i[0]) - 0.5*(pow(double(dd.n[1])-1,2)+1.0);
        else if(i[1]==dd.n[1]-1)
            v_y_exact(dd.lin_idx_x(i)) =  double(i[0])*double(i[0]) - 0.5*(pow(double(dd.n[1])-2,2));
        else
            v_y_exact(dd.lin_idx_x(i)) =  double(i[0])*double(i[0]) - double(i[1])*double(i[1]) - 1.0;
    }

    BOOST_CHECK_SMALL( (v_x-v_x_exact).lpNorm<Eigen::Infinity>(), 1e-14 );
    BOOST_CHECK_SMALL( (v_y-v_y_exact).lpNorm<Eigen::Infinity>(), 1e-14 );

}



array<VectorXd,2> compute_flux_rho_constant_advection(const lowrank4d& in,
        const moments& mom, const domain4d_data& dd) {
    return {0.0*mom.rho, 0.0*mom.rho};
}

array<VectorXd,2> compute_flux_u0_constant_advection(const lowrank4d& in,
        const moments& mom, const domain4d_data& dd) {
    return {-mom.u[0], 0.0*mom.u[0]};
}

array<VectorXd,2> compute_flux_u1_constant_advection(const lowrank4d& in,
        const moments& mom, const domain4d_data& dd) {
    return {-mom.u[1], 0.0*mom.u[1]};
}

double run_constant_advection(Index n, double t_final,
        std::function<moments(double tau, const moments&, vlasov_bgk_moments& vcs)> method) {
    domain4d_data dd({-1.0,-1.0,-6.0,-5.0}, {1.0, 1.0, 6.0, 5.0}, {n,n,32,32}, 3);
    vlasov_bgk_moments vcs(dd, 1e-1);
    domain_lowrank4d_fluid unused(dd, lowrank_type::normalized_integral);
    
    moments mom, mom_out;
    mom.init(
            [](double x, double y) {
                return 1.0; 
            },
            [](double x, double y) {
                return sin(M_PI*x);
            },
            [](double x, double y) {
                return sin(2.0*M_PI*x);
            },
    dd);

    double tau = 0.8*dd.h[0];
    Index n_steps = ceil(t_final/tau);
    
    double t=0.0;
    for(Index i=0;i<n_steps;i++) {
        if(t_final - t < tau)
            tau = t_final-t;

        mom_out = method(tau, mom, vcs);

        std::swap(mom_out, mom);
        t += tau;
    }

    moments mom_exact;
    mom_exact.init(
            [t_final](double x, double y) {
                return 1.0;
            },
            [t_final](double x, double y) {
                return sin(M_PI*(x-t_final));
            },
            [t_final](double x, double y) {
                return sin(2.0*M_PI*(x-t_final));
            },
    dd);

    ofstream fs("out.data");
    for(Index i=0;i<dd.n[0];i++)
        fs << dd.x(array<Index,2>({i,0})) << " " << sin(M_PI*dd.x(array<Index,2>({i,0}))) << " " << mom_exact.u[0](i) << " " << mom.u[0](i) << endl;
    fs.close();
   
    double err_rho = (mom_exact.rho - mom.rho).lpNorm<Eigen::Infinity>();
    double err_u0  = (mom_exact.u[0] - mom.u[0]).lpNorm<Eigen::Infinity>();
    double err_u1  = (mom_exact.u[1] - mom.u[1]).lpNorm<Eigen::Infinity>();
    return std::max(err_rho, std::max(err_u0, err_u1));
}

BOOST_AUTO_TEST_CASE( CONSTANT_ADVECTION_LXF ) {

    auto lxf = [](double tau, const moments& mom, vlasov_bgk_moments& vcs) {
        domain4d_data dd = vcs.dd;
        domain_lowrank4d_fluid unused(dd, lowrank_type::normalized_integral);

        array<VectorXd,2> flux_rho = compute_flux_rho_constant_advection(unused.lr, mom, dd);
        array<VectorXd,2> flux_u0  = compute_flux_u0_constant_advection(unused.lr,  mom, dd);
        array<VectorXd,2> flux_u1  = compute_flux_u1_constant_advection(unused.lr,  mom, dd);
       
        VectorXd I1 = vcs.diff_x_cd_vec(flux_rho[0]) + vcs.diff_y_cd_vec(flux_rho[1]);
        array<VectorXd,2> I2 = {
            vcs.diff_x_cd_vec(flux_u0[0]) + vcs.diff_y_cd_vec(flux_u0[1]),
            vcs.diff_x_cd_vec(flux_u1[0]) + vcs.diff_y_cd_vec(flux_u1[1])
        };

        moments mom_cons = to_conservative_form(mom);

        moments cons_out;
        cons_out.rho  = vcs.LaxFriedrichs(tau, mom_cons.rho,  I1);
        cons_out.u[0] = vcs.LaxFriedrichs(tau, mom_cons.u[0], I2[0]);
        cons_out.u[1] = vcs.LaxFriedrichs(tau, mom_cons.u[1], I2[1]);

        return from_conservative_form(cons_out);
    };

    BOOST_CHECK_SMALL( run_constant_advection(64,  1e-1, lxf), 0.012     );
    BOOST_CHECK_SMALL( run_constant_advection(128, 1e-1, lxf), 0.012/2.0 );
    BOOST_CHECK_SMALL( run_constant_advection(256, 1e-1, lxf), 0.012/4.0 );
}

BOOST_AUTO_TEST_CASE( CONSTANT_ADVECTION_STAGLXF ) {

    auto stag_lxf = [](double tau, const moments& mom, vlasov_bgk_moments& vcs) {
        domain4d_data dd = vcs.dd;
        domain_lowrank4d_fluid unused(dd, lowrank_type::normalized_integral);

        moments moms = vcs.lax_friedrichs_staggered_1(0.5*tau, unused.lr, mom,
                compute_flux_rho_constant_advection, compute_flux_u0_constant_advection,
                compute_flux_u1_constant_advection); 

        return vcs.lax_friedrichs_staggered_2(0.5*tau, unused.lr, moms,
                compute_flux_rho_constant_advection, compute_flux_u0_constant_advection,
                compute_flux_u1_constant_advection); 
    };

    BOOST_CHECK_SMALL( run_constant_advection(64,  1e-1, stag_lxf), 5e-3     );
    BOOST_CHECK_SMALL( run_constant_advection(128, 1e-1, stag_lxf), 5e-3/2.0 );
    BOOST_CHECK_SMALL( run_constant_advection(256, 1e-1, stag_lxf), 5e-3/4.0 );
}

BOOST_AUTO_TEST_CASE( CONSTANT_ADVECTION_NT ) {

    auto stag_nt = [](double tau, const moments& mom, vlasov_bgk_moments& vcs) {
        domain4d_data dd = vcs.dd;
        domain_lowrank4d_fluid unused(dd, lowrank_type::normalized_integral);
        
        // with active limiter we only obtain order 1 at extrema
        vcs.limiter = no_limiter;

        moments moms = vcs.nessyahu_tadmor_staggered_1(0.5*tau, unused.lr, mom,
                compute_flux_rho_constant_advection, compute_flux_u0_constant_advection,
                compute_flux_u1_constant_advection); 

        return vcs.nessyahu_tadmor_staggered_2(0.5*tau, unused.lr, moms,
                compute_flux_rho_constant_advection, compute_flux_u0_constant_advection,
                compute_flux_u1_constant_advection);
    };

    BOOST_CHECK_SMALL( run_constant_advection(64,  1e-1, stag_nt), 2e-3      );
    BOOST_CHECK_SMALL( run_constant_advection(128, 1e-1, stag_nt), 2e-3/4.0  );
    BOOST_CHECK_SMALL( run_constant_advection(256, 1e-1, stag_nt), 2e-3/16.0 );
    cout << endl;
}



array<VectorXd,2> compute_flux_rho_euler(const lowrank4d& in,
        const moments& mom, const domain4d_data& dd) {
    return {-mom.rho.array()*mom.u[0].array(), 0.0*mom.rho};
}

array<VectorXd,2> compute_flux_u0_euler(const lowrank4d& in,
        const moments& mom, const domain4d_data& dd) {
    return {-mom.rho.array()*mom.u[0].array()*mom.u[0].array() - mom.rho.array(), 0.0*mom.u[0]};
}

array<VectorXd,2> compute_flux_u1_euler(const lowrank4d& in,
        const moments& mom, const domain4d_data& dd) {
    return {-0.0*mom.u[1], 0.0*mom.u[1]};
}

std::array<double,2> shock_width(double* p, Index pos_shock, Index n, Index stride=1) {
    Index shock_left = pos_shock;
    for(;shock_left>=0;shock_left--)
        if(p[stride*shock_left] >= 0.975)
            break;
    
    Index shock_right = pos_shock;
    for(;shock_right<n;shock_right++)
        if(p[stride*shock_right] <= 0.275)
            break;

    return {shock_left, shock_right};
}

double run_euler(Index n, double t_final,
        std::function<moments(double tau, const moments&, vlasov_bgk_moments& vcs)> method) {
    domain4d_data dd({-1.0,-1.0,-6.0,-5.0}, {1.0, 1.0, 6.0, 5.0}, {n,n,32,32}, 3);
    vlasov_bgk_moments vcs(dd, 1e-1);
    domain_lowrank4d_fluid unused(dd, lowrank_type::normalized_integral);
    
    moments mom, mom_out;
    double rho_l  = 1.0;
    double rho_r  = 0.25;
    double u_l = 0.0/rho_l;
    double u_r = -3.0/8.0/rho_r;
    mom.init(
            [rho_l, rho_r](double x, double y) {
                return (x<=0.0) ? rho_l : rho_r; 
            },
            [u_l, u_r](double x, double y) {
                return (x<=0.0) ? u_l : u_r;
            },
            [](double x, double y) {
                return 0.0;
            },
    dd);

    double tau = 0.8*dd.h[0]/3.0;
    Index n_steps = ceil(t_final/tau);
    
    double t=0.0;
    for(Index i=0;i<n_steps;i++) {
        if(t_final - t < tau)
            tau = t_final-t;

        mom_out = method(tau, mom, vcs);

        std::swap(mom_out, mom);
        t += tau;
    }

    ofstream fs("out.data");
    for(Index i=0;i<dd.n[0];i++)
        fs << dd.x(array<Index,2>({i,0})) << " " << mom.rho(i) << " " << mom.u[0](i) << endl;
    fs.close();

    
    // determine width of shock
    Index pos_shock = 1.1/dd.h[0];
    array<double,2> shock_lr = shock_width(mom.rho.data(), pos_shock, dd.n[0]);
    double left = dd.x(array<Index,2>({shock_lr[0],0}));
    double right = dd.x(array<Index,2>({shock_lr[1],0}));
    return right-left;
}


BOOST_AUTO_TEST_CASE( EULER_LXF ) {

    auto lxf = [](double tau, const moments& mom, vlasov_bgk_moments& vcs) {
        domain4d_data dd = vcs.dd;
        domain_lowrank4d_fluid unused(dd, lowrank_type::normalized_integral);

        array<VectorXd,2> flux_rho = compute_flux_rho_euler(unused.lr, mom, dd);
        array<VectorXd,2> flux_u0  = compute_flux_u0_euler(unused.lr,  mom, dd);
        array<VectorXd,2> flux_u1  = compute_flux_u1_euler(unused.lr,  mom, dd);
       
        VectorXd I1 = vcs.diff_x_cd_vec(flux_rho[0]) + vcs.diff_y_cd_vec(flux_rho[1]);
        array<VectorXd,2> I2 = {
            vcs.diff_x_cd_vec(flux_u0[0]) + vcs.diff_y_cd_vec(flux_u0[1]),
            vcs.diff_x_cd_vec(flux_u1[0]) + vcs.diff_y_cd_vec(flux_u1[1])
        };

        moments mom_cons = to_conservative_form(mom);

        moments cons_out;
        cons_out.rho  = vcs.LaxFriedrichs(tau, mom_cons.rho,  I1);
        cons_out.u[0] = vcs.LaxFriedrichs(tau, mom_cons.u[0], I2[0]);
        cons_out.u[1] = vcs.LaxFriedrichs(tau, mom_cons.u[1], I2[1]);

        return from_conservative_form(cons_out);
    };

    BOOST_CHECK_SMALL( run_euler(128, 2e-1, lxf), 0.16);
    BOOST_CHECK_SMALL( run_euler(256, 2e-1, lxf), 0.09);
}

BOOST_AUTO_TEST_CASE( EULER_STAGLXF ) {

    auto stag_lxf = [](double tau, const moments& mom, vlasov_bgk_moments& vcs) {
        domain4d_data dd = vcs.dd;
        domain_lowrank4d_fluid unused(dd, lowrank_type::normalized_integral);

        moments moms = vcs.lax_friedrichs_staggered_1(0.5*tau, unused.lr, mom,
                compute_flux_rho_euler, compute_flux_u0_euler,
                compute_flux_u1_euler); 

        return vcs.lax_friedrichs_staggered_2(0.5*tau, unused.lr, moms,
                compute_flux_rho_euler, compute_flux_u0_euler,
                compute_flux_u1_euler); 
    };

    BOOST_CHECK_SMALL( run_euler(128, 2e-1, stag_lxf), 0.13);
    BOOST_CHECK_SMALL( run_euler(256, 2e-1, stag_lxf), 0.07);
}

BOOST_AUTO_TEST_CASE( EULER_NT ) {

    auto stag_nt = [](double tau, const moments& mom, vlasov_bgk_moments& vcs) {
        domain4d_data dd = vcs.dd;
        domain_lowrank4d_fluid unused(dd, lowrank_type::normalized_integral);

        moments moms = vcs.nessyahu_tadmor_staggered_1(0.5*tau, unused.lr, mom,
                compute_flux_rho_euler, compute_flux_u0_euler,
                compute_flux_u1_euler); 

        return vcs.nessyahu_tadmor_staggered_2(0.5*tau, unused.lr, moms,
                compute_flux_rho_euler, compute_flux_u0_euler,
                compute_flux_u1_euler); 
    };

    BOOST_CHECK_SMALL( run_euler(128, 2e-1, stag_nt), 0.07 );
    BOOST_CHECK_SMALL( run_euler(256, 2e-1, stag_nt), 0.04 );
}


double run_lwvanleer(Index n, std::function<double(double,double,Index)> f_iv,
        Index dir, bool limiter) {
    domain4d_data dd({-M_PI,-M_PI,-6.0,-5.0}, {M_PI, M_PI, 6.0, 5.0}, {n,n+9,32,32}, 2);
    vlasov_bgk_moments vcs(dd, 1e-1);
    vcs.lr_limiter = limiter;
    double tau = 0.8*2.0/double(n+9);

    auto f_K0 = [f_iv](double x, double y, Index r) {
        return f_iv(x,y,r);
    };
    MatrixXd K0(dd.n[0]*dd.n[1], dd.r);
    lowrank4d_init_x(f_K0, K0, dd);

    VectorXd v(2);
    v << 0.8, -0.6;

    MatrixXd K1;
    Index num_steps = ceil(1.0/tau);
    double t=0.0;
    for(Index i=0;i<num_steps;i++) {
        if(1.0 - t < tau)
            tau = 1.0-t;

        if(dir == 0)
            K1 = K0 - tau*vcs.diff_x_lwvanleer(K0, v, tau)*v.asDiagonal();
        else
            K1 = K0 - tau*vcs.diff_y_lwvanleer(K0, v, tau)*v.asDiagonal();
        K1.swap(K0);

        t += tau;
    }
    K1.swap(K0);

    if(!limiter) {
        auto f_K1_exact = [v,dir,f_iv](double x, double y, Index r) {
            if(dir == 0)
                return f_iv(x-v[r],y,r);
            else
                return f_iv(x,y-v[r],r);
        };
        MatrixXd K1_exact(dd.n[0]*dd.n[1], dd.r);
        lowrank4d_init_x(f_K1_exact, K1_exact, dd);
        return (K1-K1_exact).lpNorm<Eigen::Infinity>();
    } else {
        if(dir == 0) {
            double w1;
            {
                Index pos_shock = (M_PI+v[0])/dd.h[0];
                array<double,2> shock_lr = shock_width(&K1(0,0), pos_shock, dd.n[0]);
                double left = dd.x(array<Index,2>({shock_lr[0],0}));
                double right = dd.x(array<Index,2>({shock_lr[1],0}));
                w1 = right-left;
            }
            
            double w2;
            {
                Index pos_shock = (M_PI+v[1])/dd.h[0];
                array<double,2> shock_lr = shock_width(&K1(0,1), pos_shock, dd.n[0]);
                double left = dd.x(array<Index,2>({shock_lr[0],0}));
                double right = dd.x(array<Index,2>({shock_lr[1],0}));
                w2 = right-left;
            }

            return max(w1, w2);
        } else {
            double w1;
            {
                Index pos_shock = (M_PI+v[0])/dd.h[1];
                array<double,2> shock_lr = shock_width(&K1(0,0), pos_shock, dd.n[0], dd.n[0]);
                double left  = dd.y(array<Index,2>({0,shock_lr[0]}));
                double right = dd.y(array<Index,2>({0,shock_lr[1]}));
                w1 = right-left;
            }

            double w2;
            {
                Index pos_shock = (M_PI+v[1])/dd.h[1];
                array<double,2> shock_lr = shock_width(&K1(0,1), pos_shock, dd.n[0], dd.n[0]);
                double left = dd.y(array<Index,2>({0,shock_lr[0]}));
                double right = dd.y(array<Index,2>({0,shock_lr[1]}));
                w2 = right-left;
            }

            return max(w1,w2);
        }
    }
}

BOOST_AUTO_TEST_CASE( LWVANLEER ) {
    auto f_smooth = [](double x, double y, Index r) {
        return cos((r+1)*x)*sin((r+1)*y);
    };

    // advection in x-direction
    BOOST_CHECK_SMALL( run_lwvanleer(32,  f_smooth, 0, false), 0.035      );
    BOOST_CHECK_SMALL( run_lwvanleer(64,  f_smooth, 0, false), 0.035/4.0  );
    BOOST_CHECK_SMALL( run_lwvanleer(128, f_smooth, 0, false), 0.035/16.0 );
    BOOST_CHECK_SMALL( run_lwvanleer(256, f_smooth, 0, false), 0.035/64.0 );

    // advection in y-direction
    BOOST_CHECK_SMALL( run_lwvanleer(32,  f_smooth, 1, false), 0.03      );
    BOOST_CHECK_SMALL( run_lwvanleer(64,  f_smooth, 1, false), 0.03/4.0  );
    BOOST_CHECK_SMALL( run_lwvanleer(128, f_smooth, 1, false), 0.03/16.0 );
    BOOST_CHECK_SMALL( run_lwvanleer(256, f_smooth, 1, false), 0.03/64.0 );

    // advection in x-direction
    auto f_discont_x = [](double x, double y, Index r) {
        return (x<=0.0);
    };

    BOOST_CHECK_SMALL( run_lwvanleer(32,  f_discont_x, 0, true), 0.8);
    BOOST_CHECK_SMALL( run_lwvanleer(64,  f_discont_x, 0, true), 0.4);
    BOOST_CHECK_SMALL( run_lwvanleer(128, f_discont_x, 0, true), 0.25);
    BOOST_CHECK_SMALL( run_lwvanleer(256, f_discont_x, 0, true), 0.15);

    // advection in y-direction
    auto f_discont_y = [](double x, double y, Index r) {
        return (y<=0.0);
    };

    BOOST_CHECK_SMALL( run_lwvanleer(32,  f_discont_y, 1, true), 0.65 ); 
    BOOST_CHECK_SMALL( run_lwvanleer(64,  f_discont_y, 1, true), 0.35 );
    BOOST_CHECK_SMALL( run_lwvanleer(128, f_discont_y, 1, true), 0.25 );
    BOOST_CHECK_SMALL( run_lwvanleer(256, f_discont_y, 1, true), 0.18 );
}



#define BOOST_TEST_MODULE LOWRANK
#include <boost/test/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>
#include <unsupported/Eigen/MatrixFunctions>
#include <algorithms/lowrank-continuoussplit-fluid2d.hpp>


BOOST_AUTO_TEST_CASE( INIT_AND_MOMENTS ) {

    domain4d_data dd({0.0,-M_PI,-1.0,-2.0}, {M_PI, M_PI, 1.0, 2.0}, {16,20,14,22}, 5);

    BOOST_CHECK_SMALL( abs(dd.x(array<Index,2>({7,5}))-7*M_PI/16.0)           , 1e-13);
    BOOST_CHECK_SMALL( abs(dd.y(array<Index,2>({7,5}))-(-M_PI+5*2*M_PI/20.0)) , 1e-13);
    BOOST_CHECK_SMALL( abs(dd.v(array<Index,2>({7,5}))-(-1.0+7*2.0/14.0))     , 1e-13);
    BOOST_CHECK_SMALL( abs(dd.w(array<Index,2>({7,5}))-(-2.0+5*4.0/22.0))     , 1e-13);

    auto X0 = [](double x, double y, Index r) {
        return cos(2*r*x)*sin((r+1)*y);
    };
    auto V0 = [](double v, double w, Index r) {
        return exp(-pow(v-0.2,2)/(0.02*(r+1.0)))*exp(-10.0*pow(w-0.1,2));
    };
    domain_lowrank4d_fluid f(dd, lowrank_type::normalized_integral);
    f.init(4, X0, V0);

    Tensor4 f_lr = f.full();
    Tensor4 f_ex(Index_to_int(dd.n));
    for(auto i : range<4>(dd.n)) {
        f_ex(Index_to_int(i)) = 0.0;
        for(Index k=0;k<4;k++)
            f_ex(Index_to_int(i)) += X0(dd.x(i),dd.y(i),k)*V0(dd.v(i),dd.w(i),k);
    }

    BOOST_CHECK_SMALL( error_inf(f_lr,f_ex), 1e-12 );

    MatrixXd rho;
    array<MatrixXd,2> rhou;
    f.rhou(rho, rhou);
    double err_rho = 0.0;
    array<double,2> err_rhou = {0.0,0.0};
    for(auto i : range<2>(dd.n[0],dd.n[1])) {
        double x = dd.x(i);
        double y = dd.y(i);
        // TODO: insert correct solution
        double ex_rho = (M_PI*(sin(y) + sqrt(2)*cos(2*x)*sin(2*y) + sqrt(3)*cos(4*x)*sin(3*y) + 2*cos(6*x)*sin(4*y)))/(10.*sqrt(5));
        err_rho     = max(err_rho,  abs(rho(i[0],i[1]) - ex_rho) );
        double ex_rhou0 = (M_PI*(sin(y) + sqrt(2)*cos(2*x)*sin(2*y) + sqrt(3)*cos(4*x)*sin(3*y) + 2*cos(6*x)*sin(4*y)))/(50.*sqrt(5));
        err_rhou[0] = max(err_rhou[0], abs(rhou[0](i[0],i[1]) - ex_rhou0) );
        double ex_rhou1 = (M_PI*(sin(y) + sqrt(2)*cos(2*x)*sin(2*y) + sqrt(3)*cos(4*x)*sin(3*y) + 2*cos(6*x)*sin(4*y)))/(100.*sqrt(5));
        err_rhou[1] = max(err_rhou[1], abs(rhou[1](i[0],i[1]) - ex_rhou1) );
    }

    BOOST_CHECK_SMALL( err_rho, 1e-4 );
    BOOST_CHECK_SMALL( err_rhou[0], 1e-4 );
    BOOST_CHECK_SMALL( err_rhou[1], 1e-5 );
}

BOOST_AUTO_TEST_CASE( COEFFICIENTS ) {

    domain4d_data dd({0.0,-M_PI,-6.0,-5.0}, {M_PI, M_PI, 6.0, 5.0}, {32*25,32*26,27,28}, 5);

    auto X0 = [](double x, double y, Index r) {
        return cos(2.0*r*x)*sin((r+1)*y);
    };
    auto V0 = [](double v, double w, Index r) {
        return exp(-pow(v+0.2,2)/(r+1.0))*exp(-0.5*pow(w-0.1,2));
    };
    
    lowrank4d lr(dd.r, dd.n);
    lowrank4d_init_x(X0, lr.X, dd);
    lowrank4d_init_v(V0, lr.V, dd);

    vlasov_continuoussplit_fluid4d vcs(dd, 1e-1);

    // differentiation routines
    auto X0_dx = [](double x, double y, Index r) {
        return -2.0*r*sin(2.0*r*x)*sin((r+1)*y);
    };
    auto X0_dy = [](double x, double y, Index r) {
        return (r+1.0)*cos(2.0*r*x)*cos((r+1)*y);
    };
    // Derivative computed using fft
    MatrixXd dX = lr.X;
    vcs.diff_x_fft(lr.X, dX);
    MatrixXd dY = lr.X;
    vcs.diff_y_fft(lr.X, dY);
    double err_x = 0.0;
    double err_y = 0.0;
    for(Index k=0;k<5;k++) {
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            double x = dd.x(i);
            double y = dd.y(i);
            err_x = max(err_x, abs(dX(dd.lin_idx_x(i),k) - X0_dx(x,y,k)) );
            err_y = max(err_y, abs(dY(dd.lin_idx_x(i),k) - X0_dy(x,y,k)) );
        }
    }
    BOOST_CHECK_SMALL( err_x, 1e-11 );
    BOOST_CHECK_SMALL( err_y, 1e-11 );

    // derivative via centered differences
    vcs.diff_x_cd(lr.X, dX);
    vcs.diff_y_cd(lr.X, dY);
    err_x = 0.0;
    err_y = 0.0;
    // only up to 3 as the high frequency are difficult to resolve for the CD scheme
    for(Index k=0;k<3;k++) {
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            double x = dd.x(i);
            double y = dd.y(i);
            err_x = max(err_x, abs(dX(dd.lin_idx_x(i),k) - X0_dx(x,y,k)) );
            err_y = max(err_y, abs(dY(dd.lin_idx_x(i),k) - X0_dy(x,y,k)) );
        }
    }
    BOOST_CHECK_SMALL( err_x, 0.02 );
    BOOST_CHECK_SMALL( err_y, 0.02 );

    // derivative via upwind
    vcs.diff_x_upwind(lr.X, dX, VectorXd::Random(lr.X.cols()));
    vcs.diff_y_upwind(lr.X, dY, VectorXd::Random(lr.X.cols()));
    err_x = 0.0;
    err_y = 0.0;
    // only up to 3 as the high frequency are difficult to resolve for the CD scheme
    for(Index k=0;k<3;k++) {
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            double x = dd.x(i);
            double y = dd.y(i);
            err_x = max(err_x, abs(dX(dd.lin_idx_x(i),k) - X0_dx(x,y,k)) );
            err_y = max(err_y, abs(dY(dd.lin_idx_x(i),k) - X0_dy(x,y,k)) );
        }
    }
    cout << "err diff: " << err_x << " " << err_y << endl;
    BOOST_CHECK_SMALL( err_x, 0.07 );
    BOOST_CHECK_SMALL( err_y, 0.07 );

    // C1
    array<MatrixXd,2> c1 = vcs.compute_C1(lr.V);
    MatrixXd c10_exact(dd.r,dd.r);
    c10_exact << -0.4442882938147774, -0.5130199320635223  , -0.5441398092689678  , -0.5619851784819181  , -0.5735737209531799,
                 -0.5130199320635223,   -0.6283185307164583,   -0.6882884651430931,   -0.7255197456788222,   -0.7509842836121457,
                 -0.5441398092689678,   -0.6882884651430931,   -0.7695298978546416,   -0.822662060566111 ,   -0.860360555701054,
                 -0.5619851784819181,   -0.7255197456788222,   -0.822662060566111 ,   -0.888576504551976 ,   -0.936641480421451,
                 -0.5735737209531799,   -0.7509842836121457,   -0.860360555701054 ,   -0.936641480421451 ,   -0.993455997502033;
    BOOST_CHECK_SMALL( (c10_exact-c1[0]).norm(), 1e-5);
    MatrixXd c11_exact(dd.r,dd.r);
    c11_exact << 0.2221441468871369, 0.2565099660083765, 0.2720699046096807, 0.2809925892153425,  0.2867868604504451,
                 0.2565099660083765, 0.3141592653295898, 0.3441442325405219, 0.3627598728126813,  0.3754921418080904,
                 0.2720699046096807, 0.3441442325405219, 0.3847649490083232, 0.4113310323886752,  0.430180290192216,
                 0.2809925892153425, 0.3627598728126813, 0.4113310323886752, 0.444288292175697 ,  0.4683209723006971,
                 0.2867868604504451, 0.3754921418080904, 0.430180290192216 , 0.4683209723006971,  0.4967293530762791;
    BOOST_CHECK_SMALL( (c11_exact-c1[1]).norm(), 1e-5);


    // D1
    auto X0_2 = [](double x, double y, Index r) {
        return pow(x,r)*pow(y,2*r)*exp(-5*pow(x - 0.5*M_PI,2))*exp(-5*pow(y,2));
    };
    lowrank4d_init_x(X0_2, lr.X, dd);
    array<MatrixXd,2> d1 = vcs.compute_D1(lr.X);
    MatrixXd d10_exact(dd.r,dd.r);
    d10_exact <<  0.0                 , 0.007853981637085461  ,  0.003701101782870828  ,  0.00222430779433881   ,  0.001695271799040153,
                 -0.007853981619256336,  0.0                  ,  0.0007414359523755545 ,  0.000847635889511092  ,  0.000951287020508618,
                 -0.003701101673856275, -0.0007414357465689853,  0.0                   ,  0.0003170956836879919 ,  0.000589859980366538,
                 -0.002224307438307359, -0.000847635489563954 ,  -0.0003170956725030988,  0.0                   ,  0.0003294691395963543,
                 -0.001695271634203426, -0.000951287064761717 ,  -0.0005898599803677522,  -0.0003294691408775112,  0.0;
    BOOST_CHECK_SMALL( (d1[0]-d10_exact).norm(), 1e-5);

    MatrixXd d11_exact(dd.r,dd.r);
    d11_exact.setZero(); // TODO: a X0_2 which does not give all zeros would be preferable
    BOOST_CHECK_SMALL( (d1[1]-d11_exact).norm(), 1e-5);
    
    // C3
    auto V0_2 = [](double v, double w, Index r) {
        if(r==0)       return v;
        else if(r==1)  return w;
        else if(r==2)  return v*w;
        else           return 0.0;
    };
    lowrank4d_init_v(V0_2, lr.V, dd);
    MatrixXd rho = MatrixXd::Random(dd.n[0],dd.n[1]);
    MatrixXd u0 = MatrixXd::Random(dd.n[0],dd.n[1]);
    MatrixXd u1 = MatrixXd::Random(dd.n[0],dd.n[1]);
    MatrixXd c3 = vcs.compute_C3(rho, {u0,u1}, lr.V);
    
    double err_c3 = 0.0;
    for(auto i : range<2>(dd.n[0],dd.n[1])) {
        err_c3 = max(err_c3, abs(c3(dd.lin_idx_x(i),0) - u0(i[0],i[1])));
        err_c3 = max(err_c3, abs(c3(dd.lin_idx_x(i),1) - u1(i[0],i[1])));
        err_c3 = max(err_c3, abs(c3(dd.lin_idx_x(i),2) - u0(i[0],i[1])*u1(i[0],i[1])));
        err_c3 = max(err_c3, abs(c3(dd.lin_idx_x(i),3)));
        err_c3 = max(err_c3, abs(c3(dd.lin_idx_x(i),4)));
    }
    BOOST_CHECK_SMALL( err_c3, 1e-3 );

    // D3
    lowrank4d_init_x([](double x, double y, int r) { return pow(sin(2*x),2)*pow(cos(y),2); }, lr.X, dd);
    for(auto i : range<2>(dd.n[0],dd.n[1])) {
        rho(i[0],i[1]) = 2.0;
        u0(i[0],i[1])  = 1.0;
        u1(i[0],i[1])  = 1.0;
    }
    MatrixXd d3 = vcs.compute_D3(rho, {u0,u1}, lr.X);
    double err_d3 = 0.0;
    for(Index k=0;k<dd.r;k++)
        for(auto i : range<2>(dd.n[2],dd.n[3])) {
            double v = dd.v(i);
            double w = dd.w(i);
            // this is with exp((v-u)^2/2)
            //double exact = 2.0*(pow(exp(1),-1 + v - pow(v,2)/2. + w - pow(w,2)/2.)*M_PI)/4.;
            
            // this is with exp(-v^2/2)*(....) the expansion in u
            double exact = 2.0*exp(-0.5*v*v-0.5*w*w)*(v+w)*(2+v+w)*M_PI/8.0;
            err_d3 = max(err_d3, abs(d3(dd.lin_idx_v(i),k)-exact) );
        }
    BOOST_CHECK_SMALL( err_d3, 1e-10 );

    // e
    lowrank4d_init_x([](double x, double y, int r) { return pow(sin(2*(r+1)*x),2); }, lr.X, dd);
    lowrank4d_init_v([](double v, double w, int r) { return pow(w,r); }, lr.V, dd);
    for(auto i : range<2>(dd.n[0],dd.n[1])) {
        rho(i[0],i[1]) = pow(sin(2*dd.y(i)),2);
        u0(i[0],i[1])  = 1.0;
        u1(i[0],i[1])  = 1.0;
    }
    MatrixXd e = vcs.compute_e(rho, {u0,u1}, lr.X, lr.V);
    MatrixXd e_exact(dd.r, dd.r);
    // this is with exp((v-u)^2/2)
    //e_exact << 4.934802200544679, 4.934802200544679,  9.869604401089359,  19.73920880217872,  49.34802200544679,
               //4.934802200544679, 4.934802200544679,  9.869604401089359,  19.73920880217872,  49.34802200544679,
               //4.934802200544679, 4.934802200544679,  9.869604401089359,  19.73920880217872,  49.34802200544679,
               //4.934802200544679, 4.934802200544679,  9.869604401089359,  19.73920880217872,  49.34802200544679,
               //4.934802200544679, 4.934802200544679,  9.869604401089359,  19.73920880217872,  49.34802200544679;
    // this is with exp(-v^2/2)*(....) the expansion in u
    e_exact << 4.9348, 4.9348, 9.8696, 14.8044, 44.4132, 
               4.9348, 4.9348, 9.8696, 14.8044, 44.4132, 
               4.9348, 4.9348, 9.8696, 14.8044, 44.4132, 
               4.9348, 4.9348, 9.8696, 14.8044, 44.4132, 
               4.9348, 4.9348, 9.8696, 14.8044, 44.4132;
    BOOST_CHECK_SMALL( ((e-e_exact).array()/e_exact.array()).matrix().norm(), 2e-2);
}

BOOST_AUTO_TEST_CASE( RHS ) {

    domain4d_data dd({0.0,-M_PI,-6.0,-5.0}, {M_PI, M_PI, 6.0, 5.0}, {32*25,32*26,27,28}, 3);

    lowrank4d lr(dd.r, dd.n);
    lowrank4d_init_x([](double x, double y, int r) { return sin(2*(r+1)*x)*cos((r+1)*y); }, lr.X, dd);
    lowrank4d_init_v([](double v, double w, int r) {
                if(r==0)      return v+w;
                else if(r==1) return pow(v,2)+w;
                else if(r==2) return v+pow(w,2);
                cout << "ERROR: should not happen" << endl;
                exit(1);
            }, lr.V, dd);

    

    // rhs_K
    MatrixXd C10(dd.r,dd.r);
    C10 << 1, 2, 3,
        4, 5, 6,
        7, 8, 9;
    MatrixXd C11 = C10 - 0.5*MatrixXd::Ones(C10.rows(), C10.cols());
    MatrixXd rho(dd.n[0], dd.n[1]);
    for(auto i : range<2>(dd.n[0],dd.n[1]))
        rho(i[0],i[1]) = dd.x(i)*dd.y(i);

    array<double,3> tol = {1e-12, 3e-5, 4e-3};
    for(int dm=0;dm<3;dm++) {
        vlasov_continuoussplit_fluid4d vcs(dd, 1e-1, (diff_mode)dm);

        MatrixXd outX(lr.X.rows(), lr.X.cols());
        MatrixXd C3(lr.X.rows(), lr.X.cols());
        lowrank4d_init_x([](double x, double y, int r) {
                if(r==0)      return x;
                else if(r==1) return y;
                else if(r==2) return x*y; 
                cout << "ERROR: should not happen" << endl;
                exit(1);
                }, C3, dd);
        vcs.rhs_K(lr.X, outX, {C10,C11}, C3, rho);

        MatrixXd outX_exact(lr.X.rows(), lr.X.cols());
        lowrank4d_init_x([](double x, double y, int r) {
                if(r==0)   return -2.*cos(2*x)*cos(y) - 8.*cos(4*x)*cos(2*y) - 18.*cos(6*x)*cos(3*y) + 10.*y*pow(x,2) + cos(x)*sin(x)*(-20.*cos(y) + 1.*sin(y)) + 3.*sin(4*x)*sin(2*y) + 
                7.5*sin(6*x)*sin(3*y);
                else if(r==1) return -8.*cos(2*x)*cos(y) - 20.*cos(4*x)*cos(2*y) - 36.*cos(6*x)*cos(3*y) + 10.*x*pow(y,2) + 7.*cos(x)*sin(x)*sin(y) + sin(4*x)*(-10.*cos(2*y) + 18.*cos(y)*sin(y)) + 16.5*sin(6*x)*sin(3*y);
                else if(r==2) return -14.*cos(2*x)*cos(y) - 32.*cos(4*x)*cos(2*y) - 54.*cos(6*x)*cos(3*y) + 10.*pow(x,2)*pow(y,2) + 13.*cos(x)*sin(x)*sin(y) + 15.*sin(4*x)*sin(2*y) + 
                sin(6*x)*(-10.*cos(3*y) + 25.5*sin(3*y));
                cout << "ERROR: should not happen" << endl;
                exit(1);
                }, outX_exact, dd);

        cout << "err: " << (outX-outX_exact).norm()/outX_exact.norm() << endl;
        BOOST_CHECK_SMALL( (outX-outX_exact).norm()/outX_exact.norm(), tol[dm] );
    }

    // rhs_L
    vlasov_continuoussplit_fluid4d vcs(dd, 1e-1);

    MatrixXd outV(lr.V.rows(), lr.V.cols());
    array<MatrixXd,2> D1 = {C10,C11};
    MatrixXd D3(lr.V.rows(), lr.V.cols());
    lowrank4d_init_v([](double v, double w, int r) {
                if(r==0)      return v;
                else if(r==1) return w;
                else if(r==2) return v*w;
                cout << "ERROR: should not happen" << endl;
                exit(1);
            }, D3, dd);
    vcs.rhs_L(lr.V, outV, D1, D3);

    MatrixXd outV_exact(lr.V.rows(), lr.V.cols());
    lowrank4d_init_v([](double v, double w, int r) {
                if(r==0)        return v*(-6. - 3.*w)*w + w*(-10. + (-2. - 2.5*w)*w) + (-4. - 1.5*w)*pow(v,2) - 2.*pow(v,3);
                else if(r==1)   return -5.*(v*w*(3.6 + 1.2*w) + (4. + 0.9*w)*pow(v,2) + pow(v,3) + (1.6 + 1.1*w)*pow(w,2));
                else if(r==2)   return -8.*(v*(1.25 + w*(2.5 + 1.125*w)) + (2. + 0.9375*w)*pow(v,2) + pow(v,3) + (3. + 1.0625*w)*pow(w,2));
                cout << "ERROR: should not happen" << endl;
                exit(1);
            }, outV_exact, dd);

    BOOST_CHECK_SMALL( (outV-outV_exact).norm(), 1e-10 );

    // rhs_S
    MatrixXd D10 = C10-C11;
    MatrixXd D11 = C10+C11;
    MatrixXd e(dd.r,dd.r);
    e << -1.0, 0.5, 3.0,
         0.3,  2.0, 1.0,
         0.0,  1.5, 0.5;

    lr.S << 1.0, 0.3, 1.0,
            2.0, -0.3, 4.0,
            0.8, 0.2, 0.4;
    MatrixXd outS(dd.r,dd.r);
    vcs.rhs_S(lr.S, outS, {C10, C11}, {D10,D11}, e);

    MatrixXd outS_exact(dd.r,dd.r);
    outS_exact << 81.65,  167.05, 256.45,
                  172.85, 409.45, 739.05,
                  258.05, 682.85, 1140.65;
    
    BOOST_CHECK_SMALL( (outS-outS_exact).norm(), 1e-10 );

    // rho_u
    array<MatrixXd,2> u;
    lowrank4d_init_x([](double x, double y, int r) { return pow(cos(r*x)*cos(r*y),2); }, lr.X, dd);
    lowrank4d_init_v([](double v, double w, int r) {
                return exp(-0.5*pow(v-1.0/(r+1.0),2))*exp(-0.5*pow(w-1.0/(2*r+1.0),2));
            }, lr.V, dd);
    vcs.rho_u(lr.X, lr.S, lr.V, rho, u);

    double err_rho = 0.0;
    double err_u0 = 0.0;
    double err_u1 = 0.0;
    for(auto i : range<2>(dd.n[0],dd.n[1])) {
        double x = dd.x(i);
        double y = dd.y(i);
        double rho_ex = 14.451326206513048 + 35.81415625092364*pow(cos(x),2)*pow(cos(y),2) + 8.796459430051419*pow(cos(2*x),2)*pow(cos(2*y),2);
        err_rho = max(err_rho, abs(rho(i[0],i[1]) - rho_ex) );
        double u0_ex = (9.320058205649719 + 20.001473227855016*pow(cos(x),2)*pow(cos(y),2) + 6.492624817418907*pow(cos(2*x),2)*pow(cos(2*y),2))/rho_ex;
        err_u0  = max(err_u0,  abs(u[0](i[0],i[1]) - u0_ex) );
         double u1_ex = (8.168140899333464 + 16.964600329384886*pow(cos(x),2)*pow(cos(y),2) + 5.948082090796675*pow(cos(2*x),2)*pow(cos(2*y),2))/rho_ex;
        err_u1  = max(err_u1,  abs(u[1](i[0],i[1]) - u1_ex) );
    }
    BOOST_CHECK_SMALL( err_rho, 3e-3);
    BOOST_CHECK_SMALL(err_u0, 3e-5);
    BOOST_CHECK_SMALL(err_u1, 3e-4);
}

BOOST_AUTO_TEST_CASE( CVODE ) {

    MatrixXd in = MatrixXd::Random(5,1);
    MatrixXd out(in);
    MatrixXd A = MatrixXd::Random(5,5);

    UserData ud(5,1,[A](const MatrixXd& in, MatrixXd& out) {
                out = A*in;
            });
    cvode_integrate(1.0, &in(0,0), &out(0,0), &ud, 1e-12, 1e-12);

    BOOST_CHECK_SMALL( (out - A.exp()*in).norm(), 1e-7 );
}

BOOST_AUTO_TEST_CASE( EVOLUTION ) {

    domain4d_data dd({0.0,0.0,-6.0,-6.0}, {1.0, 1.0, 6.0, 6.0}, {64,32,32,32}, 7);
    domain_lowrank4d_fluid in(dd, lowrank_type::normalized_integral);
    domain_lowrank4d_fluid out(dd, lowrank_type::normalized_integral);

    // this only checks the advective part
    {
        in.init(1, [](double x, double y, int r) { return cos(2*M_PI*x); }, [](double v, double w, int r) { return exp(-0.5*v*v-0.5*w*w); });
        double tau = 0.01;
        int n_steps = 10;
        double t = n_steps*tau;
        vlasov_continuoussplit_fluid4d vcs(dd, 1e50); // no diffusion
        vcs.cvode_tolrel = 1e-12; // avoid error due to time integrator
        vcs.cvode_tolabs = 1e-12;
        for(int i=0;i<n_steps;i++) {
            vcs.Lie_step(tau, in.lr, out.lr);
            in.lr = out.lr;
        }

        MatrixXd f = out.lr.full();
        double err = 0.0;
        for(auto i : range<4>(dd.n)) {
            double x = dd.x(i);
            double v = dd.v(i);
            double w = dd.w(i);
            double val_ref = cos(2*M_PI*(x-v*t))*exp(-0.5*v*v-0.5*w*w);

            err = max(err, abs(f(dd.lin_idx_x({i[0],i[1]}),dd.lin_idx_v({i[2],i[3]}))-val_ref) );
        }
        BOOST_CHECK_SMALL( err, 1e-8 );
    }

    // this only checks the collision part
    {
        //in.init(1, [](double x, double y, int r) { return cos(2*M_PI*x); }, [](double v, double w, int r) { return exp(-0.5*v*v-0.5*w*w); });

        double rho = 1.0;
        in.init(6,
                [rho](double x, double y, int r) {
                    double u1 = 0.05*( (y<=0.5) ? tanh((y-0.25)*30.0) : tanh((0.75-y)*30.0) );
                    double u2 = 0.05*sin(2*M_PI*x);

                    if(r==0)        return rho*(1-0.5*(u1*u1+u2*u2));
                    else if(r==1)   return rho*u1;
                    else if(r==2)   return rho*u2;
                    else if(r==3)   return rho*pow(u1,2);
                    else if(r==4)   return rho*pow(u2,2);
                    else if(r==5)   return rho*u1*u2;
                    cout << "ERROR: should not happen" << endl;
                    exit(1);
                },
                [](double v, double w, int r) {
                    double expo = exp(-0.5*pow(v,2)-0.5*pow(w,2))/(2.0*M_PI);

                    if(r==0)        return expo;
                    else if(r==1)   return expo*v;
                    else if(r==2)   return expo*w;
                    else if(r==3)   return expo*0.5*pow(v,2);
                    else if(r==4)   return expo*0.5*pow(w,2);
                    else if(r==5)   return expo*v*w;
                    cout << "ERROR: should not happen" << endl;
                    exit(1);
                });


        double tau = 0.005;
        int n_steps = 20;
        vlasov_continuoussplit_fluid4d vcs(dd, 1e-5);
        vcs.alpha = 0.0; // no advection
        vcs.cvode_tolrel = 1e-12; // avoid error due to time integrator
        vcs.cvode_tolabs = 1e-12;
        for(int i=0;i<n_steps;i++) {
            vcs.Lie_step(tau, in.lr, out.lr);
            in.lr = out.lr;
        }

        MatrixXd f = out.lr.full();
        double err = 0.0;
        for(auto i : range<4>(dd.n)) {
            double x = dd.x(i);
            double y = dd.y(i);
            double v = dd.v(i);
            double w = dd.w(i);

            double u1 = 0.05*( (y<=0.5) ? tanh((y-0.25)*30.0) : tanh((0.75-y)*30.0) );
            double u2 = 0.05*sin(2*M_PI*x);
            double val_ref = (exp(-0.5*pow(v-u1,2)-0.5*pow(w-u2,2))/(2.0*M_PI));

            err = max(err, abs(f(dd.lin_idx_x({i[0],i[1]}),dd.lin_idx_v({i[2],i[3]}))-val_ref) );
        }
        BOOST_CHECK_SMALL( err, 2e-3 );
        cout << "err_coll: " << err << endl;
    }
}

// This test checks if the integrator yields the correct equations of motion
// by estimating the right hand side.
// It does not take into account the nonlinear term and the viscosity term. Thus,
// the assumption is that v0 and epsilon are small.
BOOST_AUTO_TEST_CASE( RHS_MOMENTS ) {

    double tol[3][3] = {{1e-5, 6e-4, 1e-3}, {2e-4,6e-4,1e-3}};

    for(int dm=0;dm<2;dm++) {

        domain4d_data dd({0.0,0.0,-6.0,-6.0}, {1.0, 1.0, 6.0, 6.0}, {64,64,32,32}, 10);
        domain_lowrank4d_fluid in(dd, lowrank_type::normalized_integral);
        domain_lowrank4d_fluid out(dd, lowrank_type::normalized_integral);

        double v0 = 1e-2;
        double v1 = 5e-3;
        in.init(6,
                [v0,v1](double x, double y, int r) {
                double u1 = -v1*sin(2*M_PI*x);
                double u2 = -v0*sin(2*M_PI*y);
                double rho = 1.0 + v0*sin(2*M_PI*y) + v1*sin(2*M_PI*x);

                if(r==0)        return rho*(1-0.5*(u1*u1+u2*u2));
                else if(r==1)   return rho*u1;
                else if(r==2)   return rho*u2;
                else if(r==3)   return rho*pow(u1,2);
                else if(r==4)   return rho*pow(u2,2);
                else if(r==5)   return rho*u1*u2;
                cout << "ERROR: should not happen" << endl;
                exit(1);
                },
                [](double v, double w, int r) {
                double expo = exp(-0.5*pow(v,2)-0.5*pow(w,2))/(2.0*M_PI);

                if(r==0)        return expo;
                else if(r==1)   return expo*v;
                else if(r==2)   return expo*w;
                else if(r==3)   return expo*0.5*pow(v,2);
                else if(r==4)   return expo*0.5*pow(w,2);
                else if(r==5)   return expo*v*w;
                cout << "ERROR: should not happen" << endl;
                exit(1);
                });

        vlasov_continuoussplit_fluid4d vcs(dd, 1e-3, (diff_mode)dm);
        vcs.cvode_tolrel = 1e-10; // avoid error due to time integrator
        vcs.cvode_tolabs = 1e-10; // avoid error due to time integrator

        MatrixXd in_rho;
        array<MatrixXd,2> in_rhou;
        in.rhou(in_rho, in_rhou);

        double tau = 0.000001;
        vcs.Lie_step(tau, in.lr, out.lr);

        MatrixXd rho;
        array<MatrixXd,2> rhou;
        out.rhou(rho, rhou);

        double err_rho=0.0, err_rhou0=0.0, err_rhou1=0.0;
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            double x = dd.x(i);
            double y = dd.y(i);
            double rho_ref   = v0*2*M_PI*cos(2*M_PI*y) + pow(v0,2)*4*M_PI*sin(2*M_PI*y)*cos(2*M_PI*y)
                                + v0*v1*2*M_PI*cos(2*M_PI*y)*sin(2*M_PI*x)
                             + v1*2*M_PI*cos(2*M_PI*x) + pow(v1,2)*4*M_PI*sin(2*M_PI*x)*cos(2*M_PI*x)
                                + v0*v1*2*M_PI*cos(2*M_PI*x)*sin(2*M_PI*y);
            double rhou0_ref = -v1*2*M_PI*cos(2*M_PI*x);
            double rhou1_ref = -v0*2*M_PI*cos(2*M_PI*y);

            double rho_rhs   = (rho(i[0],i[1])-in_rho(i[0],i[1]))/tau;
            double rhou0_rhs = (rhou[0](i[0],i[1])-in_rhou[0](i[0],i[1]))/tau;
            double rhou1_rhs = (rhou[1](i[0],i[1])-in_rhou[1](i[0],i[1]))/tau;
            err_rho   = max(err_rho, abs(rho_rhs-rho_ref) );
            err_rhou0 = max(err_rhou0, abs(rhou0_rhs-rhou0_ref) );
            err_rhou1 = max(err_rhou1, abs(rhou1_rhs-rhou1_ref) );
        }

        cout << "ERR: " << err_rho << " " << err_rhou0 << " " << err_rhou1 << endl;
        BOOST_CHECK_SMALL( err_rho,   tol[dm][0] );
        BOOST_CHECK_SMALL( err_rhou0, tol[dm][1] );
        BOOST_CHECK_SMALL( err_rhou1, tol[dm][2] );
    }
}


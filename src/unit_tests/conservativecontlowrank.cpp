#define BOOST_TEST_MODULE CONSERVATIVELOWRANK
#include <boost/test/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>
#include <unsupported/Eigen/MatrixFunctions>
#include <algorithms/lowrank-conservative-1x1v.hpp>



void run_order(string method, double T, vlasov_continuous_conservative& vcs,
        domain_lowrank2d& ref, domain_lowrank2d& in,
        domain_lowrank2d& work, domain_lowrank2d& out,
        double initial_tolerance, double mass_cons_tol=-1.0) {
    double err_prev = 0.0;

    double mass0 = in.mass();
    cout << "Method: " << method << "\t" << "r: " << in.lr.S.rows() << "\t" << "mass0: " << mass0 << endl;

    int start, stop;
    if(method != "dopri") {
        start = 256; 
        stop  = 2048;
    } else {
        start = 512;
        stop  = 4096;
    }

    Index order = (method == "dopri") ? 5 : 1;

    for(int nsteps=start;nsteps<=stop;nsteps*=2) {
        double tau = T/double(nsteps);
        work = in;
        for(int k=0;k<nsteps;k++) {

            if(method == "euler")
                vcs.step_euler(tau, work.lr, out.lr);
            else if(method == "cons_euler")
                vcs.step_conservative_euler(tau, work.lr, out.lr);
            else if(method == "dopri")
                vcs.step_dopri(tau, work.lr, out.lr);
            else if(method == "unconv")
                vcs.step_unconventional(tau, work.lr, out.lr);
            else if(method == "unconv2")
                vcs.step_unconventional2(tau, work.lr, out.lr);
            else {
                cout << "ERROR: method " << method << " not found" << endl;
                exit(1);
            }
            work.lr = out.lr;
        }
        double err = error_inf(work.full(),ref.full());
        double mass_err = abs(work.mass()-mass0);
        if(mass_cons_tol > 0.0)
            BOOST_CHECK_SMALL( mass_err, mass_cons_tol/double(nsteps/start) );
        if(nsteps != start)
            cout << "nsteps=" << nsteps << "\terror=" << err << "\torder=" 
                 << log(err_prev/err)/log(2) << "\tmass error=" << mass_err << endl;
        else
            cout << "nsteps=" << nsteps << "\terror=" <<  err << "\t\tmass error=" << mass_err << endl;

        BOOST_CHECK_SMALL( err, max(1e-13, initial_tolerance/pow(double(nsteps/start),order)) );

        err_prev = err;
    }
}


void run_order_ref(string method, double T, int r, array<int,2> ext, array<double,2> a,
        array<double,2> b, int r0, function<double(double,int)> X0, function<double(double,int)> V0,
        function<double(double,int)> X_ref, function<double(double,int)> V_ref,
        function<double(double)> efield, function<double(double)> f0v, Index n_fixed_basis,
        double initial_tolerance, double mass_cons_tol=-1.0) {

    vlasov_continuous_conservative vcs(r, ext, a, b, f0v, n_fixed_basis);
    vcs.custom_efield = efield;

    domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral, f0v);
    in.init(r0, X0, V0);

    domain_lowrank2d ref(a, b, r, ext, lowrank_type::normalized_integral);
    ref.init(r0, X_ref, V_ref);

    domain_lowrank2d out(a, b, r, ext, lowrank_type::normalized_integral, f0v);
    domain_lowrank2d work(a, b, r, ext, lowrank_type::normalized_integral, f0v);

    run_order(method, T, vcs, ref, in, work, out, initial_tolerance, mass_cons_tol);
}

void run_order_compute_ref(string method, double T, int r, array<int,2> ext, array<double,2> a,
        array<double,2> b, int r0, function<double(double,int)> X0, function<double(double,int)> V0,
        function<double(double)> efield, function<double(double)> f0v, Index n_fixed_basis,
        double initial_tolerance, double mass_cons_tol=-1.0) {

    vlasov_continuous_conservative vcs(r, ext, a, b, f0v, n_fixed_basis);
    vcs.custom_efield = efield;

    domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral, f0v);
    in.init(r0, X0, V0);
    
    domain_lowrank2d out(a, b, r, ext, lowrank_type::normalized_integral, f0v);
    domain_lowrank2d work(a, b, r, ext, lowrank_type::normalized_integral, f0v);

    domain_lowrank2d ref(a, b, r, ext, lowrank_type::normalized_integral, f0v);
    // we do not have an exact solution so we use a reference solution
    int ref_nsteps = 4*4096;
    double tau = T/double(ref_nsteps);
    ref = in;
    for(int k=0;k<ref_nsteps;k++) {
        vcs.step_unconventional(tau, ref.lr, out.lr);
        ref.lr = out.lr;
    }

    run_order(method, T, vcs, ref, in, work, out, initial_tolerance, mass_cons_tol);
}


void example1(function<double(double)> f0v, function<double(double)> efield, Index n_fixed) {
    cout << "Test: " << boost::unit_test::framework::current_test_case().p_name << endl;

    array<double,2> a = {0.0,-6.0};
    array<double,2> b = {2*M_PI,6.0};
    array<int,2> ext = {30,30};

    double t0 = 0.2;
    double T = 1.0;

    auto X0 = [](double x, int k) {
        if(k==0)      return 1.0;
        else if(k==1) return sin(x);
        else if(k==2) return cos(x);
        else exit(1);
    };
    auto V0 = [t0,f0v](double v, int k) {
        if(k==0)      return exp(-0.5*v*v)/f0v(v);
        else if(k==1) return sin(t0*v)*exp(-0.5*v*v)/f0v(v);
        else if(k==2) return cos(t0*v)*exp(-0.5*v*v)/f0v(v);
        else  exit(1);
    };

    auto X_ref = [](double x, int k) {
        if(k==0)      return 1.0;
        else if(k==1) return sin(x);
        else if(k==2) return cos(x);
        else exit(1);
    };
    auto V_ref = [t0,T](double v, int k) {
        if(k==0)      return exp(-0.5*v*v);
        else if(k==1) return sin((t0+T)*v)*exp(-0.5*v*v);
        else if(k==2) return cos((t0+T)*v)*exp(-0.5*v*v);
        else  exit(1);
    };

    run_order_ref("euler",      T, 3,  ext, a, b, 3, X0, V0, X_ref, V_ref, efield, f0v, n_fixed, 2.5e-3);
    run_order_ref("cons_euler", T, 3,  ext, a, b, 3, X0, V0, X_ref, V_ref, efield, f0v, n_fixed, 2.5e-3);
    run_order_ref("dopri",      T, 3,  ext, a, b, 3, X0, V0, X_ref, V_ref, efield, f0v, n_fixed, 2.5e-3);
    run_order_ref("unconv",     T, 3,  ext, a, b, 3, X0, V0, X_ref, V_ref, efield, f0v, n_fixed, 2.5e-3);
    run_order_ref("unconv",     T, 10, ext, a, b, 3, X0, V0, X_ref, V_ref, efield, f0v, n_fixed, 2.5e-3);
}

void example2(function<double(double)> f0v, function<double(double)> efield, Index n_fixed) {
    cout << "Test: " << boost::unit_test::framework::current_test_case().p_name << endl;

    array<double,2> a = {0.0,-6.0};
    array<double,2> b = {2*M_PI,6.0};
    array<int,2> ext = {30,30};

    double t0 = 0.2;
    double T = 1.0;

    auto X0 = [](double x, int k) {
        if(k==0)      return 1.0 + cos(x) + 0.1*exp(-20.0*pow(x-M_PI,2));
        else if(k==1) return sin(x);
        else if(k==2) return cos(x);
        else exit(1);
    };
    auto V0 = [t0](double v, int k) {
        if(k==0)      return 1.0;
        else if(k==1) return sin(t0*v);
        else if(k==2) return cos(t0*v);
        else  exit(1);
    };

    run_order_compute_ref("euler",      T, 3,  ext, a, b, 3, X0, V0, efield, f0v, n_fixed, 1e-2);
    run_order_compute_ref("cons_euler", T, 3,  ext, a, b, 3, X0, V0, efield, f0v, n_fixed, 1e-2);
    run_order_compute_ref("unconv",     T, 3,  ext, a, b, 3, X0, V0, efield, f0v, n_fixed, 1e-2);
    run_order_compute_ref("unconv2",     T, 3,  ext, a, b, 3, X0, V0, efield, f0v, n_fixed, 1e-2);
    //run_order_compute_ref("unconv",     T, 10, ext, a, b, 3, X0, V0, efield, f0v, n_fixed, 1e-2);
    //run_order_compute_ref("unconv2",    T, 10, ext, a, b, 3, X0, V0, efield, f0v, n_fixed, 1e-2);
}




BOOST_AUTO_TEST_CASE( example1_plain ) {
    example1([](double v) { return 1.0; }, [](double x) { return 0.0; }, 0);
}


BOOST_AUTO_TEST_CASE( example1_f0v ) {
    example1([](double v) { return exp(-0.5*v*v); }, [](double x) { return 0.0; }, 0);
}


BOOST_AUTO_TEST_CASE( example1_f0v_masscons ) {
    example1([](double v) { return exp(-0.5*v*v); }, [](double x) { return 0.0; }, 1);
}


BOOST_AUTO_TEST_CASE( example2_f0v ) {
    example2([](double v) { return exp(-0.5*v*v); }, [](double x) { return 0.0; }, 0);
}


BOOST_AUTO_TEST_CASE( mass_conservation_direct_test ) {
    cout << "Test: " << boost::unit_test::framework::current_test_case().p_name << endl;

    double t0 = 0.2;
    array<double,2> a = {0.0,-6.0};
    array<double,2> b = {2*M_PI,6.0};
    array<int,2> ext = {100,100};
    int r = 3;

    auto f0v = [](double v) { return exp(-0.5*v*v); };
    domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral, f0v);

    in.init(3,
            [](double x, int k) {
                if(k==0)      return 1.0 + cos(x) + 0.1*exp(-20.0*pow(x-M_PI,2));
                else if(k==1) return sin(x);
                else if(k==2) return cos(x);
                else exit(1);
            },
            [t0](double v, int k) {
                if(k==0)      return 1.0;
                else if(k==1) return sin(t0*v);
                else if(k==2) return cos(t0*v);
                else  exit(1);
            });


    // check orthogonalization for f0v!=0
    MatrixXd ip(r,r);
    for(int i=0;i<r;i++)
        for(int j=0;j<r;j++)
            ip(i,j) = in.inner_product(in.lr.V.col(i),in.lr.V.col(j));
    BOOST_CHECK_SMALL( (ip-MatrixXd::Identity(r,r)).norm(), 1e-13 );

    vlasov_continuous_conservative vcs(r, ext, a, b, f0v, 1);
    vcs.custom_efield = [](double x) { return 0.1*cos(2.0*x); };

    domain_lowrank2d out(a, b, r, ext, lowrank_type::normalized_integral, f0v);

    VectorXd int_V = in.integrate_v(in.lr.V, [f0v](double v) { return f0v(v);});
    cout << int_V << endl;

    vcs.step_dopri(0.001, in.lr, out.lr);
    
    // check that the output orthogonal and properly normalized
    for(int i=0;i<r;i++)
        for(int j=0;j<r;j++)
            ip(i,j) = in.inner_product(out.lr.V.col(i),out.lr.V.col(j));
    BOOST_CHECK_SMALL( (ip-MatrixXd::Identity(r,r)).norm(), 6e-8 );

    // check that only a single mode contains mass
    int_V = out.integrate_v(out.lr.V, [f0v](double v) { return f0v(v);});
    VectorXd mass_subset = int_V.tail(int_V.size()-1);
    BOOST_CHECK_SMALL( mass_subset.norm(), 3e-10 );
}

BOOST_AUTO_TEST_CASE( example2_f0v_masscons ) {
    example2([](double v) { return exp(-0.5*v*v); }, [](double x) { return 0.1*cos(2.0*x); }, 1);
}


BOOST_AUTO_TEST_CASE( ORDER_RANK3_f0v_mass_conservation_dopri ) {
    cout << "Test: " << boost::unit_test::framework::current_test_case().p_name << endl;

    double t0 = 0.2;

    array<double,2> a = {0.0,-6.0};
    array<double,2> b = {2*M_PI,6.0};
    array<int,2> ext = {100,100};
    int r = 3;
    
    auto f0v = [](double v) { return exp(-0.5*v*v); };
    domain_lowrank2d in(a, b, r, ext, lowrank_type::normalized_integral, f0v);

    in.init(3,
            [](double x, int k) {
                if(k==0)      return 1.0 + cos(x) + 0.1*exp(-20.0*pow(x-M_PI,2));
                else if(k==1) return sin(x);
                else if(k==2) return cos(x);
                else exit(1);
            },
            [t0](double v, int k) {
                if(k==0)      return 1.0;
                else if(k==1) return sin(t0*v);
                else if(k==2) return cos(t0*v);
                else  exit(1);
            });

    vlasov_continuous_conservative vcs(r, ext, a, b, f0v, 1);
    vcs.custom_efield = [](double x) { return 0.1*cos(2.0*x); };

    domain_lowrank2d out(a, b, r, ext, lowrank_type::normalized_integral, f0v);
    domain_lowrank2d work(a, b, r, ext, lowrank_type::normalized_integral, f0v);
    domain_lowrank2d ref(a, b, r, ext, lowrank_type::normalized_integral, f0v);

    // we do not have an exact solution so we use a reference solution
    double T = 1.0;
    int ref_nsteps = 4*2048;
    double tau = T/double(ref_nsteps);
    ref = in;
    for(int k=0;k<ref_nsteps;k++) {
        vcs.step_dopri(tau, ref.lr, out.lr);
        ref.lr = out.lr;
    }

    cout << "DOPRI scheme" << endl;
    run_order("dopri", T, vcs, ref, in, work, out, 1e-4, 1e-4);
}


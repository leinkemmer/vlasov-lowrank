#!/bin/bash

if [ "$#" -ne "2" ]; then
    echo "ERROR: exactly two folders are required as cli argumetns."
fi

# path where the script is stored
path=$(dirname "$0")

echo $1
echo $2

# cd before ls to make sure that we only get the file name
i=0
for mm in {0..45}; do
    for kk in "" ".25" ".5"  ".75"; do
        python3 $path/plot-sidebyside.py "$1/f-t${mm}${kk}.nc" "$2/f-t${mm}${kk}.nc" "frame-$i.png"
        i=$((i+1))
    done
done

ffmpeg -framerate 24 -i frame-%d.png test.mp4


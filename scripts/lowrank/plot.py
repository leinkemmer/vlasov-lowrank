from sys import argv, exit
# import matplotlib
# matplotlib.use('Agg')
from netCDF4 import *
from pylab import *

if len(argv)!=2 and len(argv)!=3:
    print('ERROR: exactly one argument, the file name, or two arguments, file name and output file name, is required.')
    exit(1)

fn = argv[1]
if len(argv)!=2:
    out_fn = argv[2]

with Dataset(fn,'r') as fs:
    data = fs.variables['f'][:]

imshow(data,cmap='jet')
colorbar()

if len(argv)==2:
    show()
else:
    savefig(out_fn,bbox_inches='tight', pad_inches=0)



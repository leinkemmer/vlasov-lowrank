from sys import argv, exit
from netCDF4 import *
from pylab import *
from scipy.ndimage import convolve

filename=''
if len(argv)==4:
    fn = argv[1]
    times = argv[2].split(',')
    times_evol = argv[3].split(',')
elif len(argv)==5:
    fn = argv[1]
    times = argv[2].split(',')
    times_evol = argv[3].split(',')
    filename = argv[4]
else:
    print('ERROR: wrong command line arguments. Call with either')
    print('plot-lrfac.py lr-t{}.nc "0,0.2,1" "0,0.2,0.4,0.6,0.8,1.0"')
    print('or') 
    print('plot-lrfac.py lr-t{}.nc "0,0.2,0.7,1" "0,0.2,0.4,0.6,0.8,1.0" output_file.pdf')
    exit(1)

def get(time):
    with Dataset(fn.format(time),'r') as fs:
        X = fs.variables['X'][:]
        S = fs.variables['S'][:]
        V = fs.variables['V'][:]
        nx = X.shape[1:]
        nv = V.shape[1:]
        r  = X.shape[0]

        S = S.transpose()

        x = fs.variables['x'][:]
        y = fs.variables['y'][:]
        v = fs.variables['v'][:]
        w = fs.variables['w'][:]

    K = (X.reshape((r,nx[0]*nx[1])).transpose().dot(S)).transpose().reshape((r,nx[1],nx[0]))

    # K_int = sum(K, axis=(1,2))*(x[1]-x[0])*(y[1]-y[0])
    K_eval = K[:,int(nx[0]/2),int(nx[1]/2)]
    return K_eval.dot(V.reshape((r,nv[0]*nv[1]))).reshape((nv[0],nv[1])), x, y, v, w


def plot_density(time):
    title('t={}'.format(time))

    VV, _, _, v, w = get(time)
    imshow(VV, extent=[amin(v), amax(v), amin(w), amax(w)], origin='lower')
    colorbar()


figure(figsize=(8,6))
i=1
for time in times:
    subplot(2, 2, i)
    i += 1

    plot_density(time)

subplot(2, 2, i)
ts      = array([float(time) for time in times_evol])
max_dev = array([amax(absolute(get(time)[0])) for time in times_evol])
min_dev = array([amin(absolute(get(time)[0])) for time in times_evol])
semilogy(ts, (max_dev-min_dev)/(max_dev[0]-min_dev[0]))
ylabel('Deviation from Maxwellian')
xlabel('time')

tight_layout()
if filename != '':
    savefig(filename,bbox_inches='tight',pad_inches=0.05)
else:
    show()


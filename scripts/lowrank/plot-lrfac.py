from sys import argv, exit
from netCDF4 import *
from pylab import *
from scipy.ndimage import convolve

filename=''
if len(argv)==2:
    fn = argv[1]
elif len(argv)==3:
    fn = argv[1]
    filename = argv[2]
else:
    print('ERROR: wrong command line arguments. Call with either')
    print('plot-lrfac.py test.nc')
    print('or') 
    print('plot-lrfac.py test.nc output_file.pdf')
    exit(1)

with Dataset(fn,'r') as fs:
    X = fs.variables['X'][:]
    S = fs.variables['S'][:]
    V = fs.variables['V'][:]
    nx = X.shape[1:][::-1]
    nv = V.shape[1:][::-1]
    r  = X.shape[0]

    S = S.transpose()

    x = fs.variables['x'][:]
    y = fs.variables['y'][:]
    v = fs.variables['v'][:]
    w = fs.variables['w'][:]

K = (X.reshape((r,nx[0]*nx[1])).transpose().dot(S)).transpose().reshape((r,nx[1],nx[0]))


figure(figsize=(8,6))

for i in range(3):
    subplot(3,2,2*i+1)
    title('K_{}'.format(i))
    imshow(K[i,:,:],extent=[amin(x), amax(x), amin(y), amax(y)], origin='lower')
    colorbar()

    subplot(3,2,2*i+2)
    title('V_{}'.format(i))
    imshow(V[i,:,:],extent=[amin(v), amax(v), amin(w), amax(w)], origin='lower')
    colorbar()


tight_layout()
if filename != '':
    savefig(filename,bbox_inches='tight',pad_inches=0.0)
else:
    show()


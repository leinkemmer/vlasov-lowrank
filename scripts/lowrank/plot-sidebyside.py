from sys import argv, exit
# import matplotlib
# matplotlib.use('Agg')
from netCDF4 import *
from pylab import *

if len(argv)!=3 and len(argv)!=4:
    print('ERROR: wrong number of input argumetns.')
    exit(1)

fn1 = argv[1]
fn2 = argv[2]
if len(argv)!=3:
    out_fn = argv[3]

with Dataset(fn1,'r') as fs:
    data1 = fs.variables['f'][:]
with Dataset(fn2,'r') as fs:
    data2 = fs.variables['f'][:]

fig, axis = subplots(nrows=1,ncols=2,figsize=(12,5))
im=axis[0].imshow(data1,aspect='auto',vmin=0.0,vmax=0.3,extent=[0,10*pi,-9,9],cmap='jet')
fig.tight_layout()
axis[0].set_xlabel('x')
axis[0].set_ylabel('v')

im=axis[1].imshow(data2,aspect='auto',vmin=0.0,vmax=0.3,extent=[0,10*pi,-9,9],cmap='jet')
axis[1].set_xlabel('x')
axis[1].get_yaxis().set_visible(False)

fig.subplots_adjust(right=0.95)
cbar_ax = fig.add_axes([0.96, 0.0775, 0.02, 0.871])
fig.colorbar(im, cax=cbar_ax)

if len(argv)==3:
    show()
else:
    savefig(out_fn,bbox_inches='tight', pad_inches=0.02)



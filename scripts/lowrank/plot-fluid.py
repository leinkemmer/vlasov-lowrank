from sys import argv, exit
from netCDF4 import *
from pylab import *
from scipy.ndimage import convolve

filename=''
plot_title=''
if len(argv)==2:
    fn = argv[1]
elif len(argv)==3:
    fn = argv[1]
    filename = argv[2]
elif len(argv)==4:
    fn = argv[1]
    filename = argv[2]
    plot_title = argv[3]
else:
    print('ERROR: wrong command line arguments. Call with either')
    print('plot-fluid.py test.nc')
    print('or') 
    print('plot-fluid.py test.nc output_file.pdf')
    print('or') 
    print('plot-fluid.py test.nc output_file.pdf title')
    exit(1)

with Dataset(fn,'r') as fs:
    rho = fs.variables['rho'][:]
    rhou0 = fs.variables['rhou0'][:]
    rhou1 = fs.variables['rhou1'][:]

    x = fs.variables['x'][:]
    y = fs.variables['y'][:]


u0 = rhou0/rho
u1 = rhou1/rho
stencil_dx = -array([[0,0,0],
              [-1,0,1],
              [0,0,0]])
stencil_dy = -array([[0,-1,0],
              [0,0,0],
              [0,1,0]])
h = 1.0/float(shape(u0)[0])
u1_x = convolve(u1, stencil_dx, mode='wrap')/(2.0*h)
u0_y = convolve(u0, stencil_dy, mode='wrap')/(2.0*h)

print('max_rho: {0}  max_u0: {1}  max_u1:  {2}  max_vort: {3}'.format(amax(rho),amax(abs(u0)),amax(abs(u1)),amax(abs(u1_x-u0_y))))

figure(figsize=(8,6))
suptitle(plot_title, fontsize=16, y=1.025)

subplot(2,2,1)
title('$\\rho$ (density)')
imshow(rho,extent=[amin(x), amax(x), amin(y), amax(y)], origin='lower')
colorbar()

subplot(2,2,2)
title('$u_1$ (velocity x-direction)')
imshow(u0,extent=[amin(x), amax(x), amin(y), amax(y)], origin='lower')
colorbar()

subplot(2,2,3)
title('$u_2$ (velocity y-direction)')
imshow(u1,extent=[amin(x), amax(x), amin(y), amax(y)], origin='lower')
colorbar()

subplot(2,2,4)
title('$\\omega$ (vorticity)')
imshow(u1_x-u0_y,extent=[amin(x), amax(x), amin(y), amax(y)], origin='lower')
colorbar()

tight_layout()
if filename != '':
    savefig(filename,bbox_inches='tight',pad_inches=0)
else:
    show()


from sys import argv, exit
from netCDF4 import *
from pylab import *

if len(argv)!=2:
    print('ERROR: exactly one argument, the file name, is required.')
    exit(1)

fn = argv[1]

with Dataset(fn,'r') as fs:
    data = fs.variables['f'][:]

with Dataset(fn,'r') as fs:
    E0 = fs.variables['E0'][:]
    E1 = fs.variables['E1'][:]
    B = fs.variables['B'][:]

# order of variables w, v, x
nx = shape(data)[2]
nv = shape(data)[1]
nw = shape(data)[0]
print(nx,nv,nw)
num_x = 3
num_y = 3
slices = num_x*num_y

for i in range(slices):
    subplot(num_x+1,num_y,i+1)
    idx = i*int(nw/slices)
    imshow(data[idx,:,:],aspect=0.3)
    colorbar()
    xlabel('x')
    ylabel('v')
    title('w={}'.format(idx))

subplot(num_x+1,num_y,slices+1)
title('E1 amd E2')
plot(range(len(E0)),E0)
plot(range(len(E1)),E1)

subplot(num_x+1,num_y,slices+2)
title('B')
plot(range(len(B)),B)

subplot(num_x+1,num_y,slices+3)
title('slice f')
w_half = int(nw/2)
v_half = int(nv/2)
x_half = int(nx/2)
plot(range(nv),data[w_half,:,x_half],label='f(w=half,:,x=half)')
plot(range(nw),data[:,v_half,x_half],label='f(w=half,v=half,:)')


show()


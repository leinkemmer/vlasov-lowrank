from sys import argv, exit
from netCDF4 import *
from pylab import *
from scipy.ndimage import convolve

filename = ''
if len(argv)==4:
    fn = argv[1]
    fn_rhou = argv[2]
    times = argv[3].split(',')
elif len(argv)==5:
    fn = argv[1]
    fn_rhou = argv[2]
    times = argv[3].split(',')
    filename = argv[4]
else:
    print('ERROR: wrong command line arguments. Usage: python3 plot-lrslice.py "lr-t{}.nc" "rhou-t{}.nc" "0,0.02,0.04" [ouptut_file]')
    exit(1)

def get(fn, fn_rhou, x_eval):
    with Dataset(fn,'r') as fs:
        X = fs.variables['X'][:]
        S = fs.variables['S'][:]
        V = fs.variables['V'][:]
        nx = X.shape[1:][::-1]
        nv = V.shape[1:][::-1]
        r  = X.shape[0]

        S = S.transpose()

        x = fs.variables['x'][:]
        y = fs.variables['y'][:]
        v = fs.variables['v'][:]
        w = fs.variables['w'][:]

    with Dataset(fn_rhou,'r') as fs:
        rho = fs.variables['rho'][:]
        rhou0 = fs.variables['rhou0'][:]
        rhou1 = fs.variables['rhou1'][:]
    u0 = rhou0/rho
    u1 = rhou1/rho

    K = (X.reshape((r,nx[0]*nx[1])).transpose().dot(S)).transpose().reshape((r,nx[1],nx[0]))
    Vr = V.reshape(r,nv[1],nv[0])
    
    close_to_x = argmin(abs(x-x_eval))
    K_eval = K[:,int(nx[1]/2),close_to_x]
    
    w_close_to_2 = argmin(abs(w-2.0))
    V_eval = Vr[:,w_close_to_2,:]

    # maxwellian = rho[nx[1]/2,close_to_x]/(2*pi)*exp(-0.5*(v-u0[close_to_x,int(nx[1]/2)])**2)
    # return multiply(K_eval.dot(V_eval), maxwellian), x, y, v, w
    return K_eval.dot(V_eval), x, y, v, w


def plot_slice(fn, fn_rhou, x,label=''):
    slice, _, y, v, w = get(fn, fn_rhou, x)
    title('g(x={},y={},v,w=2)'.format(x,0.5))
    semilogy(v, slice, label=label)
    minorticks_off()
    xlim([1,7])
    xlabel('v')
    legend()


if filename != '':
    figure(figsize=(12,6))

x_list = [0.2,0.3,0.4,0.8]

i=1
for x in x_list:
    subplot(2,2,i); i+=1
    for t in times:
        plot_slice(fn.format(t), fn_rhou.format(t), x, label='t={}'.format(float(t)))

legend()


tight_layout()
if filename != '':
    savefig(filename,bbox_inches='tight',pad_inches=0.0)
else:
    show()


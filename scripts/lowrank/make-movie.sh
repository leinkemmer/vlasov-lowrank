#!/bin/bash

# path where the script is stored
path=$(dirname "$0")
i=0
for mm in {0..45}; do
    for kk in "" ".25" ".5"  ".75"; do
        python3 $path/plot.py "f-t${mm}${kk}.nc" "frame-$i.png"
        i=$((i+1))
    done
done

ffmpeg -framerate 24 -i frame-%d.png test.mp4



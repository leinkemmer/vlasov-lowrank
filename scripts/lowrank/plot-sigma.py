from sys import argv, exit
from netCDF4 import *
from pylab import *
from scipy.ndimage import convolve
import ast

filename=''
climit=[]
if len(argv)==2:
    fn = argv[1]
elif len(argv)==3:
    fn = argv[1]
    filename = argv[2]
elif len(argv)==4:
    fn = argv[1]
    filename = argv[2]
    climit = ast.literal_eval(argv[3])
else:
    print('ERROR: wrong command line arguments. Call with either')
    print('plot-sigma.py test.nc')
    print('or') 
    print('plot-sigma.py test.nc output_file.pdf')
    print('or') 
    print('plot-sigma.py test.nc output_file.pdf [[-0.2,0.2],...]')
    exit(1)

enable_st = False

with Dataset(fn,'r') as fs:
    st = []
    sigma = []
    for i in range(2):
        for j in range(2):
            try:
                st.append(fs.variables['st_{}{}'.format(i,j)][:])
            except KeyError:
                print('Warning: st_{}{} not found in netcdf file'.format(i,j))
                enable_st = False
                pass
            if enable_st or len(st)==0:
                sigma.append(fs.variables['sigma_{}{}'.format(i,j)][:])

    x = fs.variables['x'][:]
    y = fs.variables['y'][:]


figure(figsize=(8,6))

for i in range(2):
    for j in range(2):
        if enable_st:
            subplot(2,4,2*j + 4*i + 1)
            _st = st[j + 2*i]
            min_st = amin(_st)
            max_st = amax(_st)
            title('$\\mathbb{{P}}^{{(1)}}_{{{}{}}}$'.format(i+1,j+1))
            imshow(_st, extent=[amin(x), amax(x), amin(y), amax(y)], origin='lower')
            # xlabel('x')
            # ylabel('y')
            colorbar()
        
            subplot(2,2,2*j + 4*i + 2)
            _sigma = sigma[j + 2*i]
            min_st = amin(_sigma)
            max_st = amax(_sigma)
            title('$\\sigma_{{{}{}}}$'.format(i+1,j+1))
            imshow(_sigma, extent=[amin(x), amax(x), amin(y), amax(y)], origin='lower')
            # xlabel('x')
            # ylabel('y')
            colorbar()
        else:
            subplot(2,2,j + 2*i + 1)
            if len(st)==0:
                _sigma = sigma[j + 2*i]
                min_st = amin(_sigma)
                max_st = amax(_sigma)
                title('$\\sigma_{{{}{}}}$'.format(i+1,j+1))
                imshow(_sigma, extent=[amin(x), amax(x), amin(y), amax(y)], origin='lower')
                # xlabel('x')
                # ylabel('y')
                if len(climit) != 0:
                    clim(climit[j + 2*i])
                colorbar()
            else:
                _st = st[j + 2*i]
                min_st = amin(_st)
                max_st = amax(_st)
                title('$\\mathbb{{P}}^{{(1)}}_{{{}{}}}$'.format(i+1,j+1))
                imshow(_st, extent=[amin(x), amax(x), amin(y), amax(y)], origin='lower')
                # xlabel('x')
                # ylabel('y')
                if len(climit) != 0:
                    clim(climit[j + 2*i])
                colorbar()


tight_layout()
if filename != '':
    savefig(filename,bbox_inches='tight',pad_inches=0.0)
else:
    show()


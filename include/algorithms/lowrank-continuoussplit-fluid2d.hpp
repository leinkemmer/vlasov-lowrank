#pragma once

#include <generic/common.hpp>
#include <generic/timer.hpp>
#include <generic/integrators.hpp>
#include <generic/fft.hpp>

#include <container/domain-lowrank-fluid2d.hpp>


enum diff_mode {
    DM_FFT, DM_CD, DM_UPWIND
};

enum solve_mode {
    SM_EXPLICIT, SM_CVODE
};

struct vlasov_continuoussplit_fluid4d {
    domain4d_data dd;
    double epsilon;
    double alpha; // can be used to turn of advection for testing, alpha=1.0 by default
    double cvode_tolrel, cvode_tolabs; // can be overwritten (for example in the unit tests)
    diff_mode  dm;
    solve_mode sm;

    rectangular_QR qrx, qrv;

    timer t_cvode, t_rhou;

    vlasov_continuoussplit_fluid4d(domain4d_data _dd, double _epsilon, diff_mode _dm = DM_FFT, solve_mode _sm = SM_CVODE) 
        : dd(_dd), epsilon(_epsilon), dm(_dm),  sm(_sm), qrx(_dd.n[0]*_dd.n[1],_dd.r), qrv(_dd.n[2]*_dd.n[3], _dd.r) {
            alpha=1.0;
            cvode_tolrel = 1e-6;
            cvode_tolabs = 1e-6;
    }

    void Lie_step(double tau, lowrank4d& in, lowrank4d& out) {
        timer t_K, t_S, t_L, t_all, t_qr;
        t_cvode.reset(); t_rhou.reset();
        t_all.start();

        // Step I (the K step)
        // saves output in K1
        t_K.start();
        MatrixXd K1(in.X.rows(), in.X.cols());
        step_K(tau, in.X*in.S, in.V, K1);
        t_K.stop();

        // get X1 and S1 by doing a QR
        // NOTE: it is important to normalize X such that the integral
        // is normalized (and not the sum X^TX).
        t_qr.start();
        qrx.compute(K1);
        out.X = qrx.Q()/sqrt(dd.h[0]*dd.h[1]); // X1=out.X
        MatrixXd S1 = qrx.R()*sqrt(dd.h[0]*dd.h[1]);
        t_qr.stop();

        // Step II (the S step)
        t_S.start();
        MatrixXd S2(S1);
        step_S(tau, out.X, S1, in.V, S2);
        t_S.stop();

        // Step III (the L step)
        t_L.start();
        MatrixXd L(in.V.rows(), in.V.cols());
        step_L(tau, in.V*S2.transpose(), out.X, L);
        t_L.stop();

        // compute the output by doing a QR
        // NOTE: it is important to normalize V such that the integral
        // is normalized (and not the sum V^TV).
        t_qr.start();
        qrv.compute(L);
        out.V = qrv.Q()/sqrt(dd.h[2]*dd.h[3]);
        out.S = qrv.R().transpose()*sqrt(dd.h[2]*dd.h[3]);
        t_qr.stop();

        t_all.stop();
        cout << "step: " << t_all.total() << " K: " << t_K.total() << " S: " << t_S.total() << " L: " << t_L.total() << " QR: " << t_qr.total() << " cvode: " << t_cvode.total() << " rho_u: " << t_rhou.total() << endl;
    }

    void Strang_step(double tau, lowrank4d& in, lowrank4d& out) {
        timer t_K, t_S, t_L, t_all, t_qr;
        t_cvode.reset(); t_rhou.reset();
        t_all.start();

        // Half K step
        t_K.start(); 
        MatrixXd K1(in.X.rows(), in.X.cols());
        step_K(0.5*tau, in.X*in.S, in.V, K1);
        t_K.stop();

        // Decompose result
        t_qr.start();
        qrx.compute(K1);
        out.X = qrx.Q()/sqrt(dd.h[0]*dd.h[1]); // X1=out.X
        MatrixXd S1 = qrx.R()*sqrt(dd.h[0]*dd.h[1]);
        t_qr.stop();

        // Half S step
        t_S.start();
        MatrixXd S2(S1);
        step_S(0.5*tau, out.X, S1, in.V, S2);
        t_S.stop();

        // Full L step
        t_L.start();
        MatrixXd L(out.V.rows(), out.V.cols());
        step_L(tau, in.V*S2.transpose(), out.X, L);
        t_L.stop();

        // Now we decompose the result
        t_qr.start();
        qrv.compute(L);
        out.V = qrv.Q()/sqrt(dd.h[2]*dd.h[3]);
        S1 = qrv.R().transpose()*sqrt(dd.h[2]*dd.h[3]);
        t_qr.stop();

        // Half S step
        t_S.start();
        step_S(0.5*tau, out.X, S1, out.V, S2);
        t_S.stop();

        // Half K step
        t_K.start();
        step_K(0.5*tau, out.X*S2, out.V, K1);
        t_K.stop();

        t_qr.start();
        qrx.compute(K1);
        out.X = qrx.Q()/sqrt(dd.h[0]*dd.h[1]);
        out.S = qrx.R()*sqrt(dd.h[0]*dd.h[1]);
        t_qr.stop();
        
        t_all.stop();
        cout << "step: " << t_all.total() << " K: " << t_K.total() << " S: " << t_S.total() << " L: " << t_L.total() << " QR: " << t_qr.total() << " cvode: " << t_cvode.total() << " rho_u: " << t_rhou.total() << endl;
    }

    void step_K(double tau, const MatrixXd& K, const MatrixXd& V, MatrixXd& K_out) {

        array<MatrixXd,2> C1 = compute_C1(V);

        if(sm == SM_CVODE) {
            t_cvode.start();
            UserData ud(K.rows(), K.cols(),
                        [this,C1,V](const MatrixXd& K, MatrixXd& out) {
                            MatrixXd rho;
                            array<MatrixXd,2> u;
                            rho_u(K, MatrixXd::Identity(dd.r,dd.r), V, rho, u);
                            MatrixXd C3 = compute_C3(rho, u, V);

                            rhs_K(K, out, C1, C3, rho);
                        });
            cvode_integrate(tau, &K(0,0), &K_out(0,0), &ud, cvode_tolrel, cvode_tolabs);
            t_cvode.stop();
        } else {
            // reference implementation
            integrate([this,V,C1](const MatrixXd& K, MatrixXd& out) {
                            MatrixXd rho;
                            array<MatrixXd,2> u;
                            rho_u(K, MatrixXd::Identity(dd.r,dd.r), V, rho, u);
                            MatrixXd C3 = compute_C3(rho, u, V);

                            rhs_K(K, out, C1, C3, rho);
                      }, K, K_out, tau, 20);
        }
    }

    void step_L(double tau, const MatrixXd& L_in,  const MatrixXd& X, MatrixXd& L_out) {
            
        array<MatrixXd,2> D1 = compute_D1(X);

        if(sm == SM_CVODE) {
            t_cvode.start();
            UserData ud(L_in.rows(), L_in.cols(),
                    [this,D1,X](const MatrixXd& L, MatrixXd& out) {
                        MatrixXd rho;
                        array<MatrixXd,2> u;
                        rho_u(X, MatrixXd::Identity(dd.r,dd.r), L, rho, u);
                        MatrixXd D3 = compute_D3(rho, u, X);

                        rhs_L(L, out, D1, D3);
                    });
            cvode_integrate(tau, &L_in(0,0), &L_out(0,0), &ud, cvode_tolrel, cvode_tolabs);
            t_cvode.stop();
        } else {
            // reference implementation
            integrate([this,D1,X](const MatrixXd& L, MatrixXd& out) {
                        MatrixXd rho;
                        array<MatrixXd,2> u;
                        rho_u(X, MatrixXd::Identity(dd.r,dd.r), L, rho, u);
                        MatrixXd D3 = compute_D3(rho, u, X);

                        rhs_L(L, out, D1, D3);
                    }, L_in, L_out, tau, 5);
        }
    }

    void step_S(double tau, const MatrixXd& X, const MatrixXd& S, const MatrixXd& V, MatrixXd& S_out) {
        
        array<MatrixXd,2> C1 = compute_C1(V);
        array<MatrixXd,2> D1 = compute_D1(X);

        if(sm == SM_CVODE) {
            t_cvode.start();
            UserData ud(S.rows(), S.cols(),
                        [this,X,V,C1,D1](const MatrixXd& S, MatrixXd& _out) {
                            MatrixXd rho;
                            array<MatrixXd,2> u;
                            rho_u(X, S, V, rho, u);
                            MatrixXd e = compute_e(rho, u, X, V);

                            rhs_S(S, _out, C1, D1, e);
                        });
            cvode_integrate(tau, &S(0,0), &S_out(0,0), &ud, cvode_tolrel, cvode_tolabs);
            t_cvode.stop();
        } else {
            // reference implementation
            integrate([this,X,V,C1,D1](const MatrixXd& S, MatrixXd& _out) {
                        MatrixXd rho;
                        array<MatrixXd,2> u;
                        rho_u(X, S, V, rho, u);
                        MatrixXd e = compute_e(rho, u, X, V);

                        rhs_S(S, _out, C1, D1, e);
                    }, S, S_out, tau, 5);
        }
    }


    void rho_u(const MatrixXd& X, const MatrixXd& S, const MatrixXd& V,
               MatrixXd& rho, array<MatrixXd,2>& u) {
        domain_lowrank4d_fluid dlf(dd, lowrank_type::normalized_integral);
        dlf.lr.X = X;
        dlf.lr.S = S;
        dlf.lr.V = V;
        t_rhou.start();
        dlf.rhou(rho, u);
        t_rhou.stop();
        u[0] = (u[0].array()/rho.array()).matrix();
        u[1] = (u[1].array()/rho.array()).matrix();
    }

    void rhs_K(const MatrixXd& K, MatrixXd& out,
            const array<MatrixXd,2>& C1, const MatrixXd& C3, const MatrixXd& rho) {

        
        out = -K/epsilon;
        for(Index k=0;k<dd.r;k++)
            for(auto i : range<2>(dd.n[0],dd.n[1]))
                out(dd.lin_idx_x(i),k) += C3(dd.lin_idx_x(i),k)*rho(i[0],i[1])/epsilon;

        if(dm == DM_UPWIND) {
            {
                // In principle we should use a symmetric solver here. But
                // numerical errors in computing C1 destroys this property. This
                // could be fixed by redesigning the routines that compute C1.
                Eigen::EigenSolver<MatrixXd> es;

                es.compute(C1[0]);
                VectorXd lambda = es.eigenvalues().real();
                MatrixXd T = es.eigenvectors().real();

                MatrixXd in = K*T.inverse().transpose();
                MatrixXd Kx(K.rows(), K.cols());
                diff_x_upwind(in, Kx, lambda);

                out -= alpha*Kx*(T*lambda.asDiagonal()).transpose();
            }

            {
                Eigen::EigenSolver<MatrixXd> es;

                es.compute(C1[1]);
                VectorXd lambda = es.eigenvalues().real();
                MatrixXd T = es.eigenvectors().real();

                MatrixXd in = K*T.inverse().transpose();
                MatrixXd Ky(K.rows(), K.cols());
                diff_y_upwind(in, Ky, lambda);

                out -= alpha*Ky*(T*lambda.asDiagonal()).transpose();
            }
        } else {
            MatrixXd Kx(K.rows(), K.cols());
            MatrixXd Ky(K.rows(), K.cols());
            diff_x(K, Kx);
            diff_y(K, Ky);
            out -= alpha*(Kx*C1[0].transpose() + Ky*C1[1].transpose());
        }
    }

    void rhs_L(const MatrixXd& L, MatrixXd& out,
            const array<MatrixXd,2>& D1, const MatrixXd& D3) {

        out = -(L - D3)/epsilon;

        MatrixXd L1 = L*D1[0].transpose();
        MatrixXd L2 = L*D1[1].transpose();
        for(Index k=0;k<dd.r;k++)
            for(auto i : range<2>(dd.n[2],dd.n[3])) {
                L1(dd.lin_idx_v(i),k) *= dd.v(i);
                L2(dd.lin_idx_v(i),k) *= dd.w(i);
            }

        out -= alpha*(L1 + L2);
    }
    
    void rhs_S(const MatrixXd& S, MatrixXd& out,
            const array<MatrixXd,2>& C1, const array<MatrixXd,2>& D1, const MatrixXd& e) {
        
        out = (S-e)/epsilon;

        out += alpha*(D1[0]*S*C1[0].transpose() + D1[1]*S*C1[1].transpose());
    }



    array<MatrixXd,2> compute_C1(const MatrixXd& V) {
        array<MatrixXd,2> C1;
        C1[0].resize(dd.r, dd.r);
        C1[1].resize(dd.r, dd.r);
        C1[0].setZero();
        C1[1].setZero();
        for(auto k : range<2>(dd.r,dd.r))
            for(auto i : range<2>(dd.n[2],dd.n[3])) {
                C1[0](k[0],k[1]) += dd.h[2]*dd.h[3]*dd.v(i)*V(dd.lin_idx_v(i),k[0])*V(dd.lin_idx_v(i),k[1]);
                C1[1](k[0],k[1]) += dd.h[2]*dd.h[3]*dd.w(i)*V(dd.lin_idx_v(i),k[0])*V(dd.lin_idx_v(i),k[1]);
            }

        return C1;
    }
    
    array<MatrixXd,2> compute_D1(const MatrixXd& X) {
        array<MatrixXd,2> D1;
        D1[0].resize(dd.r,dd.r);
        D1[1].resize(dd.r,dd.r);
        D1[0].setZero();
        D1[1].setZero();

        MatrixXd dX = X;
        diff_x(X, dX);
        MatrixXd dY = X;
        diff_y(X, dY);

        for(auto k : range<2>(dd.r,dd.r))
            for(auto i : range<2>(dd.n[0],dd.n[1])) {
                D1[0](k[0],k[1]) += dd.h[0]*dd.h[1]*X(dd.lin_idx_x(i),k[0])*dX(dd.lin_idx_x(i),k[1]);
                D1[1](k[0],k[1]) += dd.h[0]*dd.h[1]*X(dd.lin_idx_x(i),k[0])*dY(dd.lin_idx_x(i),k[1]);
            }

        return D1;
    }
   
    double h_eq(const array<MatrixXd,2>& u, array<Index,2> i_x, double v, double w) {
        // theta=1
        double expo = exp(-0.5*(pow(v-u[0](i_x[0],i_x[1]),2)+pow(w-u[1](i_x[0],i_x[1]),2)));
        return expo/(2*M_PI); // normalization for 2d
    }

    double h_eq(double u1, double u2, double v, double w) {
        double expo = exp(-0.5*(pow(v-u1,2)+pow(w-u2,2)));
        return expo/(2*M_PI); // normalization for 2d
    }

    double h_x(double u1, double u2, Index k) {
        if(k==0)       return 1-0.5*(u1*u1+u2*u2);
        else if(k==1)  return u1;
        else if(k==2)  return u2;
        else if(k==3)  return u1*u1;
        else if(k==4)  return u2*u2;
        else if(k==5)  return u1*u2;
        else           return 0.0;
    }

    double h_v(double v, double w, Index k) {
        if(k==0)       return 1.0;
        else if(k==1)  return v;
        else if(k==2)  return w;
        else if(k==3)  return 0.5*v*v;
        else if(k==4)  return 0.5*w*w;
        else if(k==5)  return v*w;
        else           return 0.0;
    }

    MatrixXd compute_C3(const MatrixXd& rho, const array<MatrixXd,2>& u, const MatrixXd& V) {
        MatrixXd C3(dd.n[0]*dd.n[1],dd.r);
        C3.setZero();

        Index n_lr = 6;

        MatrixXd I1(dd.r,n_lr);
        I1.setZero();
        for(auto i : range<2>(dd.n[2],dd.n[3])) {
            double v = dd.v(i);
            double w = dd.w(i);
            double expo = exp(-0.5*v*v-0.5*w*w)/(2.0*M_PI);

            for(Index j=0;j<dd.r;j++)
                for(Index k=0;k<n_lr;k++)
                    I1(j,k) += dd.h[2]*dd.h[3]*expo*V(dd.lin_idx_v(i),j)*h_v(v,w,k);
        }
        
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            double u1 = u[0](i[0],i[1]);
            double u2 = u[1](i[0],i[1]);

            for(Index j=0;j<dd.r;j++)
                for(Index k=0;k<n_lr;k++)
                    C3(dd.lin_idx_x(i),j) += h_x(u1,u2,k)*I1(j,k);
        }

/*
        // reference implementation
        for(auto j : range<2>(dd.n[2],dd.n[3])) {
            for(auto i : range<2>(dd.n[0],dd.n[1])) {
                double v = dd.v(j);
                double w = dd.w(j);
                double u1 = u[0](i[0],i[1]);
                double u2 = u[1](i[0],i[1]);
                //double hq = h_eq(u1, u2, v, w);

                double hq = 0.0;
                for(Index m=0;m<6;m++)
                    hq += h_x(u1,u2,m)*h_v(v,w,m);
                double expo = exp(-0.5*v*v-0.5*w*w)/(2.0*M_PI);
                hq = expo*hq;


                for(Index k=0;k<dd.r;k++)
                    C3(dd.lin_idx_x(i),k) += dd.h[2]*dd.h[3]*V(dd.lin_idx_v(j),k)*hq;
            }
        }
*/
        return C3;
    }

    MatrixXd compute_D3(const MatrixXd& rho, const array<MatrixXd,2>& u, const MatrixXd& X) {
        MatrixXd D3(dd.n[2]*dd.n[3],dd.r);
        D3.setZero();

        const Index n_lr = 6;

        MatrixXd I2(dd.r,n_lr);
        I2.setZero();
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            double u1 = u[0](i[0],i[1]);
            double u2 = u[1](i[0],i[1]);

            for(Index j=0;j<dd.r;j++)
                for(Index k=0;k<n_lr;k++)
                    I2(j,k) += dd.h[0]*dd.h[1]*rho(i[0],i[1])*X(dd.lin_idx_x(i),j)*h_x(u1,u2,k);
        }
        
        for(auto i : range<2>(dd.n[2],dd.n[3])) {
            double v = dd.v(i);
            double w = dd.w(i);
            double expo = exp(-0.5*v*v-0.5*w*w)/(2.0*M_PI);

            for(Index j=0;j<dd.r;j++)
                for(Index k=0;k<n_lr;k++)
                    D3(dd.lin_idx_v(i),j) += expo*h_v(v,w,k)*I2(j,k);
        }

/*
        // Reference implementation
        for(auto j : range<2>(dd.n[2],dd.n[3]))
                for(auto i : range<2>(dd.n[0],dd.n[1])) {
                    
                    //double fq = rho(i[0],i[1])*h_eq(u, i, dd.v(j), dd.w(j));

                    double u1 = u[0](i[0],i[1]);
                    double u2 = u[1](i[0],i[1]);
                    double v = dd.v(j);
                    double w = dd.w(j);
                    double fq = 0.0;
                    for(Index m=0;m<6;m++)
                        fq += h_x(u1,u2,m)*h_v(v,w,m);
                    double expo = rho(i[0],i[1])*exp(-0.5*v*v-0.5*w*w)/(2.0*M_PI);
                    fq = expo*fq;

                    for(Index k=0;k<dd.r;k++)
                        D3(dd.lin_idx_v(j),k) += dd.h[0]*dd.h[1]*X(dd.lin_idx_x(i),k)*fq;
                }
*/

        return D3;
    }

    MatrixXd compute_e(const MatrixXd& rho, const array<MatrixXd,2>& u, const MatrixXd& X, const MatrixXd& V) {
        MatrixXd e(dd.r,dd.r);
        e.setZero();

        MatrixXd C3 = compute_C3(rho, u, V);
        for(auto k : range<2>(dd.r,dd.r)) {
            for(auto i : range<2>(dd.n[0],dd.n[1]))
                e(k[0],k[1]) += dd.h[0]*dd.h[1]*X(dd.lin_idx_x(i),k[0])*rho(i[0],i[1])*C3(dd.lin_idx_x(i),k[1]);
        }

/*
        // Reference implementation but quite slow
        double fac = dd.h[0]*dd.h[1]*dd.h[2]*dd.h[3];
        for(auto j : range<2>(dd.n[2],dd.n[3]))
            for(auto i : range<2>(dd.n[0],dd.n[1])) {
                double fq = rho(i[0],i[1])*h_eq(u, i, dd.v(j), dd.w(j));
                for(auto k : range<2>(dd.r,dd.r))
                    e(k[0],k[1]) += fac*X(dd.lin_idx_x(i),k[0])*V(dd.lin_idx_v(j),k[1])*fq;
            }
*/

        return e;
    }
    
    void diff_x(const MatrixXd& K, MatrixXd& out) {
        if(dm == DM_FFT)
            diff_x_fft(K, out);
        else
            diff_x_cd(K, out);
    }
    
    void diff_y(const MatrixXd& K, MatrixXd& out) {
        if(dm == DM_FFT)
            diff_y_fft(K, out);
        else
            diff_y_cd(K, out);
    }

    void diff_x_upwind(const MatrixXd& K, MatrixXd& out, const VectorXd& v) {
        for(Index j=0;j<dd.r;j++) {
            for(auto i : range<2>(dd.n[0],dd.n[1])) {
                array<Index,2> ip1 = {(i[0]+1)%dd.n[0], i[1]};
                array<Index,2> im1 = {(i[0]==0) ? dd.n[0]-1 : (i[0]-1), i[1]};
                if(v(j) >= 0.0)
                    out(dd.lin_idx_x(i),j) = (K(dd.lin_idx_x(i),j)-K(dd.lin_idx_x(im1),j))/(dd.h[0]);
                else
                    out(dd.lin_idx_x(i),j) = (K(dd.lin_idx_x(ip1),j)-K(dd.lin_idx_x(i),j))/(dd.h[0]);
            }
        }
    }
    
    void diff_y_upwind(const MatrixXd& K, MatrixXd& out, const VectorXd& v) {
        for(Index j=0;j<dd.r;j++) {
            for(auto i : range<2>(dd.n[0],dd.n[1])) {
                array<Index,2> ip1 = {i[0], (i[1]+1) % dd.n[1]};
                array<Index,2> im1 = {i[0], (i[1]==0) ? dd.n[1]-1 : (i[1]-1)};
                if(v(j) >= 0.0)
                    out(dd.lin_idx_x(i),j) = (K(dd.lin_idx_x(i),j)-K(dd.lin_idx_x(im1),j))/(dd.h[1]);
                else
                    out(dd.lin_idx_x(i),j) = (K(dd.lin_idx_x(ip1),j)-K(dd.lin_idx_x(i),j))/(dd.h[1]);
            }
        }
    }

    void diff_x_cd(const MatrixXd& K, MatrixXd& out) {
        for(Index j=0;j<dd.r;j++) {
            for(auto i : range<2>(dd.n[0],dd.n[1])) {
                array<Index,2> ip1 = {(i[0]+1)%dd.n[0], i[1]};
                array<Index,2> im1 = {(i[0]==0) ? dd.n[0]-1 : (i[0]-1), i[1]};
                out(dd.lin_idx_x(i),j) = (K(dd.lin_idx_x(ip1),j)-K(dd.lin_idx_x(im1),j))/(2.0*dd.h[0]);
            }
        }
    }
    
    void diff_y_cd(const MatrixXd& K, MatrixXd& out) {
        for(Index j=0;j<dd.r;j++) {
            for(auto i : range<2>(dd.n[0],dd.n[1])) {
                array<Index,2> ip1 = {i[0], (i[1]+1) % dd.n[1]};
                array<Index,2> im1 = {i[0], (i[1]==0) ? dd.n[1]-1 : (i[1]-1)};
                out(dd.lin_idx_x(i),j) = (K(dd.lin_idx_x(ip1),j)-K(dd.lin_idx_x(im1),j))/(2.0*dd.h[1]);
            }
        }
    }


    void diff_x_fft(const MatrixXd& K, MatrixXd& out) {
        vector< complex<double> > hat(dd.n[0]);

        // We assume that K and out are in Fortran-major order
        for(Index j=0;j<dd.r;j++) {
            for(Index m=0;m<dd.n[1];m++) {
                fft(dd.n[0], &K(m*dd.n[0],j), &hat[0]);
                for(Index k=0;k<dd.n[0];k++) {
                    double k_freq = double(k)*2.0*M_PI/(dd.b[0]-dd.a[0]);
                    hat[k] *= complex<double>(0,1)*k_freq/double(dd.n[0]);
                }
                inv_fft(dd.n[0], &hat[0], &out(m*dd.n[0],j));
            }
        }
    }

    void diff_y_fft(const MatrixXd& K, MatrixXd& out) {
        vector<double> tmp(dd.n[1]);
        vector< complex<double> > hat(dd.n[1]);

        // We assume that K and out are in Fortran-major order
        for(Index j=0;j<dd.r;j++) {
            for(Index m=0;m<dd.n[0];m++) {
                for(Index l=0;l<dd.n[1];l++)
                    tmp[l] = K(m+l*dd.n[0],j);
                fft(dd.n[1], &tmp[0], &hat[0]);
                for(Index k=0;k<dd.n[1];k++) {
                    double k_freq = double(k)*2.0*M_PI/(dd.b[1]-dd.a[1]);
                    hat[k] *= complex<double>(0,1)*k_freq/double(dd.n[1]);
                }
                inv_fft(dd.n[1], &hat[0], &tmp[0]);
                for(Index l=0;l<dd.n[1];l++)
                    out(m+l*dd.n[0],j) = tmp[l];
            }
        }
    }

};


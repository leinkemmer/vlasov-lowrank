#pragma once

#include <generic/common.hpp>
#include <generic/integrators.hpp>
#include <generic/fft.hpp>
#include <container/domain-vlasovmaxwell-1x2v.hpp>
#include <generic/timer.hpp>


struct vlasovmaxwell_1x2v {
    int r;
    domain1x_data ddx;
    domain2v_data ddv;
        
    rectangular_QR qrx, qrv;

    vlasovmaxwell_1x2v(int _r, domain1x_data _ddx, domain2v_data _ddv)
        : r(_r), ddx(_ddx), ddv(_ddv), qrx(_ddx.n,_r), qrv(_ddv.n[0]*_ddv.n[1],_r) {

    }
	
	void Lie_step(double tau, lowrank1x2v& in, lowrank1x2v& out,
            const array<VectorXd,2>& E_in, const VectorXd& B_in,
            array<VectorXd,2>& E_out, VectorXd& B_out) {
        
        MatrixXd dV = grad_V(in.V,ddv.b[0]-ddv.a[0]);
		MatrixXd dW = grad_W(in.V,ddv.b[1]-ddv.a[1]);
		
		MatrixXd C1 		 = compute_C1(in.V);
        array<MatrixXd,2> C2 = compute_C2(in.V,dV,dW);
        MatrixXd C3          = compute_C3(in.V,dV,dW);
        
        // Step I (the K step)
        MatrixXd K(in.X.rows(), in.X.cols());
        step_K(tau, in.X*in.S, K, C1, C2, C3, E_in, B_in);
        // get X1 and S1 by doing a QR
        // NOTE: it is important to normalize X such that the integral
        // is normalized (and not the sum X^TX).
        qrx.compute(K);
        out.X = qrx.Q()/sqrt(ddx.h); // X1=out.X
        MatrixXd S1 = qrx.R()*sqrt(ddx.h);
        
        array<MatrixXd,2> D1 = compute_D1(out.X, E_in);
        MatrixXd D2          = compute_D2(out.X);
        MatrixXd D3          = compute_D3(out.X, B_in);
                        
        // Step II (the S step)
        MatrixXd S2(S1);
        step_S(tau, S1, S2, C1, C2, C3, D1, D2, D3);

        // Step III (the L step)
        MatrixXd L(in.V.rows(), in.V.cols());
        step_L(tau, in.V*S2.transpose(), L, D1, D2, D3);
        // compute the output by doing a QR
        // NOTE: it is important to normalize V such that the integral
        // is normalized (and not the sum V^TV).
        qrv.compute(L);
        out.V = qrv.Q()/sqrt(ddv.h[0]*ddv.h[1]);
        out.S = qrv.R().transpose()*sqrt(ddv.h[0]*ddv.h[1]);

        VectorXd mrho_old = rho(in.X*in.S, in.V); //rho^n
        VectorXd mrho 	  = rho(out.X*out.S, out.V); //rho^{n+1}
        VectorXd jv 	  = current_v(in.X*in.S, in.V); //j_1^{n}
        VectorXd jw 	  = current_w(in.X*in.S, in.V); //j_2^n
               
		// update first component of E
		VectorXd E1 = E_in[0]-tau*jv; 
		VectorXd diff = E1-mrho;
		double err = sqrt((diff.array()*diff.array()).sum()*ddx.h); 
		cout << " div err before correction" << "\t" <<  err<< endl; 
        VectorXd rhs_poisson = -(mrho-mrho_old)/tau-diff_fft(jv, ddx.b-ddx.a);
        VectorXd gradPhi = poisson_correction(rhs_poisson);
        E1 -= tau*gradPhi;
		E_out[0] = E1;
		
		// update second component of E
        // VectorXd E2 = E_in[1]-tau*(diff_fft(B_in,ddx.b-ddx.a)+jw);                     
        //E_out[1] = E2;
        
        // update B
        // VectorXd B = B_in-tau*diff_fft(E_in[1],ddx.b-ddx.a);
        //VectorXd B = B_in-tau*diff_fft(E2,ddx.b-ddx.a); 
        //B_out = B; 
        
        // update B
        VectorXd B = B_in-tau*diff_fft(E_in[1],ddx.b-ddx.a);
        B_out = B;
        
        // update second component of E
        VectorXd E2 = E_in[1]-tau*(diff_fft(B,ddx.b-ddx.a)+jw);
        E_out[1] = E2;
    }
    
    void Strang_step(double tau, lowrank1x2v& in, lowrank1x2v& out,
        const array<VectorXd,2>& E_in, const VectorXd& B_in, double damp_fact,
        array<VectorXd,2>& E_out, VectorXd& B_out) {
				
	    VectorXd mrho_old = rho(in.X*in.S, in.V); //rho^n
		VectorXd jv = current_v(in.X*in.S, in.V); //j_1^{n}
        VectorXd jw = current_w(in.X*in.S, in.V); //j_2^n
        		
		// E^{n+1/2} - forward Euler
		VectorXd E1 = E_in[0]-0.5*tau*jv;
		VectorXd E2 = E_in[1]-0.5*tau*(diff_fft(B_in,ddx.b-ddx.a)+jw);
		array<VectorXd,2> E = {E1,E2};
		VectorXd B = B_in; // this is already B^{n+1/2} - see vlasovmaxwell.cpp
	    
	    array<MatrixXd,2> D1 = compute_D1(in.X, E);
        MatrixXd D2          = compute_D2(in.X);
        MatrixXd D3          = compute_D3(in.X, B);
		
		// Step L 
        MatrixXd L1(in.V.rows(), in.V.cols());
        step_L(0.5*tau, in.V*in.S.transpose(), L1, D1, D2, D3);
        // compute the output by doing a QR
        // NOTE: it is important to normalize V such that the integral
        // is normalized (and not the sum V^TV).
        qrv.compute(L1);
        MatrixXd V1 = qrv.Q()/sqrt(ddv.h[0]*ddv.h[1]);
        MatrixXd S1 = qrv.R().transpose()*sqrt(ddv.h[0]*ddv.h[1]);
	
		MatrixXd dV = grad_V(V1,ddv.b[0]-ddv.a[0]);
		MatrixXd dW = grad_W(V1,ddv.b[1]-ddv.a[1]);
        
        MatrixXd C1			 = compute_C1(V1);
        array<MatrixXd,2> C2 = compute_C2(V1,dV,dW);
        MatrixXd C3          = compute_C3(V1,dV,dW);
        
        // Step S
        MatrixXd S2(S1);
        step_S(0.5*tau, S1, S2, C1, C2, C3, D1, D2, D3);
        
        // Step K half
        MatrixXd K_half(in.X.rows(), in.X.cols());
        step_K(0.5*tau, in.X*S2, K_half, C1, C2, C3, E, B);
        // get X1 and S1 by doing a QRrrection(rhs_p
        // NOTE: it is important to normalize X such that the integral
        // is normalized (and not the sum X^TX).
        qrx.compute(K_half);
        MatrixXd X_half = qrx.Q()/sqrt(ddx.h); 
        MatrixXd S_half = qrx.R()*sqrt(ddx.h);
        
        VectorXd jv_half = current_v(X_half*S_half, V1); //j_1^{n+1/2}
        VectorXd jw_half = current_w(X_half*S_half, V1); //j_2^{n+1/2}
        
        // Step K 
        MatrixXd K(in.X.rows(), in.X.cols());
        step_K(tau, in.X*S2, K, C1, C2, C3, E, B);
        // get X1 and S1 by doing a QR
        // NOTE: it is important to normalize X such that the integral
        // is normalized (and not the sum X^TX).
        qrx.compute(K);
        out.X = qrx.Q()/sqrt(ddx.h); 
        MatrixXd S3 = qrx.R()*sqrt(ddx.h);
        
		D1 = compute_D1(out.X, E);
        D2 = compute_D2(out.X);
        D3 = compute_D3(out.X, B);              
       
		// Step S
        MatrixXd S4(S3);
        step_S(0.5*tau, S3, S4, C1, C2, C3, D1, D2, D3);
               
        // Step L
        MatrixXd L(in.V.rows(), in.V.cols());
        step_L(0.5*tau, V1*S4.transpose(), L, D1, D2, D3);
        // compute the output by doing a QR
        // NOTE: it is important to normalize V such that the integral
        // is normalized (and not the sum V^TV).
        qrv.compute(L);
        out.V = qrv.Q()/sqrt(ddv.h[0]*ddv.h[1]);
        out.S = qrv.R().transpose()*sqrt(ddv.h[0]*ddv.h[1]);
                
        VectorXd mrho = rho(out.X*out.S, out.V); //rho^{n+1}
        
		// update first component of E
		// VectorXd E1_bar = E_in[0]-tau*jv_half;
		// VectorXd E1_bar = E_in[0]-tau*jv_half+tau*pow(ddx.h,2)*damp_fact*diff2(E_in[0]);
		
		vector<complex<double>> E1_bar_fourier(ddx.n);
		VectorXd rhs_E1 = E_in[0]-tau*jv_half;
		fft(ddx.n, &rhs_E1(0), &E1_bar_fourier[0]);
		
		VectorXd E1_bar = impl_Euler_fourier(E1_bar_fourier,tau*pow(ddx.h,2)*damp_fact, ddx.b-ddx.a);
		 
		// VectorXd rhs_poisson = -(mrho-mrho_old)/tau-diff_fft(jv_half, ddx.b-ddx.a);
        // VectorXd rhs_poisson = -(mrho-diff_fft(E_in[0],ddx.b-ddx.a))/tau-diff_fft(jv_half, ddx.b-ddx.a);
        VectorXd rhs_poisson = E_in[0]-tau*jv_half;
        VectorXd gradPhi = poisson_correction_damp(rhs_poisson, mrho, tau*pow(ddx.h,2)*damp_fact, ddx.b-ddx.a, tau);
        
        // E1 = E1_bar; 
        E1 = E1_bar-tau*gradPhi;
        
        VectorXd diff = diff_fft(E1_bar,ddx.b-ddx.a)-mrho;
		double err = sqrt((diff.array()*diff.array()).sum()*ddx.h); 
		cout << "div err before correction" << "\t" << err << endl; 
		
        VectorXd diff1 = diff_fft(E1,ddx.b-ddx.a)-mrho;
		double err1 = sqrt((diff1.array()*diff1.array()).sum()*ddx.h); 
		cout << "div err after correction" << "\t" << err1 << endl; 
        
        double int_rhs = 0.0; 
        for (int i=0;i<ddx.n;i++){
			int_rhs += ddx.h*rhs_poisson(i);
		}
		cout << "int rhs" << "\t" << int_rhs <<endl;
        /*
        static double t = 0.0;
        static int steps = 0;
        if(steps % 100 == 0) {
			std::stringstream ss;

			ss << "diagnostic-wi-wc-8-" << t << ".data";
			
			ofstream fs(ss.str().c_str());
			 
			for (int i = 0;i< ddx.n;i++){
				fs << ddx.x(i) << "\t" << mrho_old(i) << "\t" << mrho(i) << "\t" << jv_half(i) << "\t" << E_in[0](i) << "\t" << E1_bar(i) << "\t" << E1(i) << "\t" << rhs_poisson(i)
				<< "\t" << gradPhi(i) << "\t" << rhs_poisson-diff_fft(gradPhi,ddx.b-ddx.a) << endl;
			}
		}
        steps++;
        t += tau;
        */
        // update second component of E
        //E2 = E_in[1]-tau*(diff_fft(B_in,ddx.b-ddx.a)+jw_half); // B = B^{n+1/2}
        // E2 = E_in[1]-tau*(diff_fft(B_in,ddx.b-ddx.a)+jw_half)+tau*pow(ddx.h,2)*damp_fact*diff2(E_in[1]);
        
        vector<complex<double>> E2_fourier(ddx.n);
		VectorXd rhs_E2 = E_in[1]-tau*(diff_fft(B_in,ddx.b-ddx.a)+jw_half);
		fft(ddx.n, &rhs_E2(0), &E2_fourier[0]);
		
		E2 = impl_Euler_fourier(E2_fourier,tau*pow(ddx.h,2)*damp_fact, ddx.b-ddx.a);
		
        E_out[0] = E1;
        E_out[1] = E2;
        
        // update B
        // B = B_in-tau*diff_fft(E2,ddx.b-ddx.a);
        // B = B_in-tau*diff_fft(E2,ddx.b-ddx.a)+tau*pow(ddx.h,2)*damp_fact*diff2(B_in);
        
        vector<complex<double>> B_fourier(ddx.n);
		VectorXd rhs_B = B_in-tau*diff_fft(E2,ddx.b-ddx.a);
		fft(ddx.n, &rhs_B(0), &B_fourier[0]);
		
		B = impl_Euler_fourier(B_fourier,tau*pow(ddx.h,2)*damp_fact, ddx.b-ddx.a);
	        
        B_out = B; // B = B^{n+3/2}
    }    
              
    void step_K(double tau, const MatrixXd& K, MatrixXd& K_out, const MatrixXd& C1, const array<MatrixXd,2>& C2, const MatrixXd& C3, const array<VectorXd,2>& E, const VectorXd& B) {
        integrate([this,C1,C2,C3,E,B](const MatrixXd& K, MatrixXd& out) {
                    rhs_K(K, out, C1, C2, C3, E, B);
                  }, K, K_out, tau, 5);
    }

    void rhs_K(const MatrixXd& K, MatrixXd& out, const MatrixXd& C1, const array<MatrixXd,2>& C2, const MatrixXd& C3, const array<VectorXd,2>& E, const VectorXd& B) {
				
        // First term
        MatrixXd Kx = diff_fft(K, ddx.b-ddx.a);
        out = -Kx*C1.transpose();
        
        ////////////////////////////////////////////////////////////////
        // Second term
        MatrixXd temp1; 
        MatrixXd temp2; 
        
        temp1 = K*C2[0].transpose(); 
        // multiplication by the first component of the electric field
        for(int j=0;j<K.cols();j++) {
            auto t1 = temp1.col(j);
            t1 = t1.array()*E[0].array();
        }
        
        temp2 = K*C2[1].transpose();
        // multiplication by the second component of the electric field
        for(int j=0;j<K.cols();j++) {
            auto t2 = temp2.col(j);
            t2 = t2.array()*E[1].array();
        }
        
        out -= temp1;
        out -= temp2; 
        
        // Third term
        MatrixXd temp3; 
        temp3 = K*C3.transpose(); 
        for(int j=0;j<K.cols();j++) {
            auto t3 = temp3.col(j);
            t3 = t3.array()*B.array();
        }
        out -= temp3; 
        //////////////////////////////////////////////////////////////
    }

    void step_S(double tau, const MatrixXd& S, MatrixXd& S_out,
            const MatrixXd& C1, const array<MatrixXd,2>& C2, const MatrixXd& C3, 
            const array<MatrixXd,2>& D1, const MatrixXd& D2, const MatrixXd& D3) {

        integrate([this,C1,C2,C3,D1,D2,D3](const MatrixXd& S, MatrixXd& _out) {
                    rhs_S(S, _out, C1, C2, C3, D1, D2, D3);
                }, S, S_out, tau, 5);
    }
    
    void rhs_S(const MatrixXd& S, MatrixXd& out,
            const MatrixXd& C1, const array<MatrixXd,2>& C2, const MatrixXd& C3,
            const array<MatrixXd,2>& D1, const MatrixXd& D2, const MatrixXd& D3) {
        
        // First term
        out = D2*S*C1.transpose();
		
		////////////////////////////////////////////////////////////////
        // Second term
        out += D1[0]*S*C2[0].transpose();
        out += D1[1]*S*C2[1].transpose();
        
        //Third term
        out += D3*S*C3.transpose();        
        ////////////////////////////////////////////////////////////////
    }

    void step_L(double tau, const MatrixXd& L_in, MatrixXd& L_out,
            const array<MatrixXd,2>& D1, const MatrixXd& D2, const MatrixXd& D3) {

        integrate([this,D1,D2,D3](const MatrixXd& L, MatrixXd& out) {
                    rhs_L(L, out, D1, D2, D3);
                }, L_in, L_out, tau, 5);
    }

    void rhs_L(const MatrixXd& L, MatrixXd& out,
            const array<MatrixXd,2>& D1, const MatrixXd& D2, const MatrixXd& D3) {

        // First term
        out = -L*D2.transpose();
        // multiply by v and w
        for(int k=0;k<r;k++)
            for(auto i : up_to<2>(ddv.n))
                out(ddv.lin_idx(i),k) *= ddv.v(i);
        
        ////////////////////////////////////////////////////////////////
        // Second term
        MatrixXd dV = grad_V(L,ddv.b[0]-ddv.a[0]);
		MatrixXd dW = grad_W(L,ddv.b[1]-ddv.a[1]);
		
		out -= dV*D1[0].transpose();
        out -= dW*D1[1].transpose();
        
        // Third term
        MatrixXd temp(L.rows(),L.cols());	
		for(int k=0;k<L.cols();k++){
            for(auto i : up_to<2>(ddv.n)){
				temp(ddv.lin_idx(i),k) = -ddv.v(i)*dW(ddv.lin_idx(i),k)+ddv.w(i)*dV(ddv.lin_idx(i),k);
			}
		}
        out -= temp*D3.transpose();
        ////////////////////////////////////////////////////////////////
    }

    MatrixXd compute_C1(const MatrixXd& V) {
        MatrixXd C1(r,r);
        C1.setZero();
        for(auto k : up_to<2>(r,r))
            for(auto i : up_to<2>(ddv.n))
                C1(k[0],k[1]) += ddv.h[0]*ddv.h[1]*ddv.v(i)*V(ddv.lin_idx(i),k[0])*V(ddv.lin_idx(i),k[1]);

        return C1;
    }
   
	////////////////////////////////////////////////////////////////////
	array<MatrixXd,2> compute_C2(const MatrixXd& V, const MatrixXd& dV, const MatrixXd& dW) {
		// the coefficients c^2_{jl} have two components  
		MatrixXd C2_0(r,r); // first component
		C2_0.setZero(); 
		MatrixXd C2_1(r,r); // second component
		C2_1.setZero();
		for(auto k : up_to<2>(r,r)){
            for(auto i : up_to<2>(ddv.n)){
				C2_0(k[0],k[1]) += ddv.h[0]*ddv.h[1]*V(ddv.lin_idx(i),k[0])*dV(ddv.lin_idx(i),k[1]);
				C2_1(k[0],k[1]) += ddv.h[0]*ddv.h[1]*V(ddv.lin_idx(i),k[0])*dW(ddv.lin_idx(i),k[1]);
			}
		}
		return {C2_0,C2_1}; 
    }
    
    MatrixXd compute_C3(const MatrixXd& V, const MatrixXd& dV, const MatrixXd& dW) {
        MatrixXd C3(r,r);
        C3.setZero();
        for(auto k : up_to<2>(r,r)){
            for(auto i : up_to<2>(ddv.n)){
                C3(k[0],k[1]) += ddv.h[0]*ddv.h[1]*V(ddv.lin_idx(i),k[0])*(ddv.w(i)*dV(ddv.lin_idx(i),k[1])-ddv.v(i)*dW(ddv.lin_idx(i),k[1]));
			}
        }
        return C3;
    }
    ////////////////////////////////////////////////////////////////////

    array<MatrixXd,2> compute_D1(const MatrixXd& X1, const array<VectorXd,2>& E) {
        MatrixXd D1_0(r,r);
        D1_0.setZero();
        MatrixXd D1_1(r,r);
        D1_1.setZero();

        for(int i=0;i<r;i++)
            for(int j=0;j<r;j++)
                for(int k=0;k<ddx.n;k++) {
                    D1_0(i,j) += ddx.h*E[0](k)*X1(k,i)*X1(k,j);
                    D1_1(i,j) += ddx.h*E[1](k)*X1(k,i)*X1(k,j);
                }
        return {D1_0,D1_1};
    }

    MatrixXd compute_D2(const MatrixXd& X1) {
        MatrixXd D2(r,r);
        D2.setZero();

        MatrixXd nablaX1 = diff_fft(X1, ddx.b-ddx.a);
        for(int i=0;i<r;i++)
            for(int j=0;j<r;j++) {
                // compute the integral
                for(int k=0;k<ddx.n;k++)
                    D2(i,j) += ddx.h*nablaX1(k,j)*X1(k,i);
            }
		return D2;
    }
    
    ////////////////////////////////////////////////////////////////////
    MatrixXd compute_D3(const MatrixXd& X1, const VectorXd& B) {
        MatrixXd D3(r,r);
        D3.setZero();
        
        for(auto k : up_to<2>(r,r)){
			for(int j=0;j<ddx.n;j++) {
				D3(k[0],k[1]) += ddx.h*B(j)*X1(j,k[0])*X1(j,k[1]);
            }
		}
        return D3;
    }
    ////////////////////////////////////////////////////////////////////
    
   	////////////////////////////////////////////////////////////////////
    VectorXd rho(const MatrixXd& K, const MatrixXd& V) {
		VectorXd temp(K.cols()); //save here the r-values of the double integral in v-w
		temp.setZero();
		for(int k=0;k<K.cols();k++){
            for(auto i : up_to<2>(ddv.n)){
				temp(k) += ddv.h[0]*ddv.h[1]*V(ddv.lin_idx(i),k);
			}
		}
		VectorXd  charge = K*temp;
		double int_rho =0.0; 
		for(int j=0;j<ddx.n;j++) {
				int_rho += ddx.h*charge(j);
        }
		// - VectorXd::Constant(ddx.n,1.0);
		charge -= int_rho/(ddx.b-ddx.a)*VectorXd::Constant(ddx.n,1.0); // NEW
		return charge;
    }
    ////////////////////////////////////////////////////////////////////
        
    ////////////////////////////////////////////////////////////////////
    VectorXd current_v(const MatrixXd& K, const MatrixXd& V) {
		VectorXd temp(K.cols()); //save here the r-values of the double integral in v-w
		temp.setZero();
		for(int k=0;k<K.cols();k++){
            for(auto i : up_to<2>(ddv.n)){
				temp(k) += ddv.h[0]*ddv.h[1]*ddv.v(i)*V(ddv.lin_idx(i),k);
			}
		}
		VectorXd j_v = K*temp;
		return j_v;
    }
    
    VectorXd current_w(const MatrixXd& K, const MatrixXd& V) {
		VectorXd temp(K.cols()); //save here the r-values of the double integral in v-w
		temp.setZero();
		for(int k=0;k<K.cols();k++){
            for(auto i : up_to<2>(ddv.n)){
				temp(k) += ddv.h[0]*ddv.h[1]*ddv.w(i)*V(ddv.lin_idx(i),k);
			}
		}
		VectorXd j_w = K*temp;
		return j_w;
    }
    
    VectorXd poisson_correction(const VectorXd& rhs) { //returns the gradient of the correction
        vector<complex<double>> hat(ddx.n);

        fft(ddx.n, &rhs(0), &hat[0]);
        for(int k=0;k<ddx.n;k++) {
            double k_freq = double(k)*2.0*M_PI/(ddx.b-ddx.a);

            if(k==0)
                hat[k] = 0.0;
            else
                hat[k] *= -1.0/(pow(k_freq,2))/double(ddx.n);

            hat[k] *= complex<double>(0,1)*k_freq;
        }

        VectorXd gradPhi(ddx.n);
        inv_fft(ddx.n, &hat[0], &gradPhi(0));
        return gradPhi;
    }
    
    VectorXd poisson_correction_damp(const VectorXd& rhs, const VectorXd& rho, double factor, double L, double tau) { //returns the gradient of the correction
		vector<complex<double>> rhs_hat(ddx.n);
        fft(ddx.n, &rhs(0), &rhs_hat[0]);
        
        vector<complex<double>> rho_hat(ddx.n);
        fft(ddx.n, &rho(0), &rho_hat[0]);

		for(int k=0;k<ddx.n;k++) {
            double k_freq = double(k)*2.0*M_PI/(ddx.b-ddx.a);
            if(k==0)
                rhs_hat[k] = 0.0;
                
            else {
                rhs_hat[k] *= complex<double>(0,1)*k_freq/(1+factor*pow(k_freq,2));
				rhs_hat[k] -= rho_hat[k];
				rhs_hat[k] *= 1/tau;
   
                rhs_hat[k] *= -1.0/(pow(k_freq,2))/double(ddx.n); //solving Poisson eq 
			}
            rhs_hat[k] *= complex<double>(0,1)*k_freq;
        }

        VectorXd gradPhi(ddx.n);
        inv_fft(ddx.n, &rhs_hat[0], &gradPhi(0));
        return gradPhi;
    }
    ////////////////////////////////////////////////////////////////////
    
    VectorXd diff2(const VectorXd& rhs) { 
        vector<complex<double>> hat(ddx.n);

        fft(ddx.n, &rhs(0), &hat[0]);
        for(int k=0;k<ddx.n;k++) {
            double k_freq = double(k)*2.0*M_PI/(ddx.b-ddx.a);

            hat[k] *= -pow(k_freq,2)/double(ddx.n);
        }
        
        VectorXd diff2(ddx.n);
        inv_fft(ddx.n, &hat[0], &diff2(0));
        return diff2;
    }
    
    
    MatrixXd diff_fft(const MatrixXd& K, double L) {
        int n = K.rows();
        int r = K.cols();
        MatrixXd out(n,r);
        vector<complex<double>> hat(n);
        
        vector<double> tmp(n);
        fft_batch fftb(n, &tmp[0], &hat[0]); //structure
        for (int j=0;j<r;j++){
			for (int k=0;k<n;k++){
				tmp[k] = K(k,j);
				}
			fftb.forward();
			for(int k=0;k<n;k++) {
				double k_freq = double(k)*2.0*M_PI/L;
				hat[k] *= complex<double>(0,1)*k_freq/double(n);
            }
            fftb.backward();
            for (int k=0;k<n;k++){
				out(k,j) = tmp[k];
			}
		} 
                

        //for(int j=0;j<r;j++) {
            //fft(n, &K(0,j), &hat[0]);
            //for(int k=0;k<n;k++) {
                //double k_freq = double(k)*2.0*M_PI/L;
                //hat[k] *= complex<double>(0,1)*k_freq/double(n);
            //}
            //inv_fft(n, &hat[0], &out(0,j));
        //}
		return out;
	}
	
	MatrixXd impl_Euler_fourier(const vector<complex<double>>& rhs, double factor, double L) {
        int n = rhs.size();
        VectorXd out(n);
        vector<complex<double>> out_fourier(n);
        
       	for (int k=0;k<n;k++){
			double k_freq = double(k)*2.0*M_PI/L;
			out_fourier[k] = rhs[k]/(1.0+factor*pow(k_freq,2))/double(ddx.n);
		}
		inv_fft(n, &out_fourier[0],&out(0));
		return out;
	}
	
	
	MatrixXd grad_V(const MatrixXd& V,double L){
        vector< complex<double> > hat(ddv.n[0]);
        MatrixXd out(V.rows(),V.cols());
        vector<double> tmp(ddv.n[0]);
        fft_batch fftb(ddv.n[0], &tmp[0], &hat[0]);
        for(int k=0;k<r;k++){
            for(int j=0;j<ddv.n[1];j++) {
                //fft(ddv.n[0], &V(j*ddv.n[0],k), &hat[0]);
                for(int l=0;l<ddv.n[0];l++)
                    tmp[l] = V(j*ddv.n[0]+l,k);
                
                fftb.forward();
                
                for(int i=0;i<ddv.n[0];i++) {
                    double k_freq = double(i)*2.0*M_PI/L;
                    hat[i] *= complex<double>(0,1)*k_freq/double(ddv.n[0]);
                }
                
                fftb.backward();
                for(int l=0;l<ddv.n[0];l++)
                    out(j*ddv.n[0]+l,k) = tmp[l];
                //inv_fft(ddv.n[0], &hat[0], &out(j*ddv.n[0],k));
            }
        }
        return out;
	}
	
	MatrixXd grad_W(const MatrixXd& V,double L){
        vector< complex<double> > hat(ddv.n[1]);
        vector<double> tmp(ddv.n[1]);
        MatrixXd out(V.rows(),V.cols());
        
        fft_batch fftb(ddv.n[1], &tmp[0], &hat[0]);
        
        for(int j=0;j<r;j++) {
            for(int m=0;m<ddv.n[0];m++) {
                for(int l=0;l<ddv.n[1];l++)
                    tmp[l] = V(m+l*ddv.n[0],j);
                //fft(ddv.n[1], &tmp[0], &hat[0]);
                fftb.forward();
                for(int k=0;k<ddv.n[1];k++) {
                    double k_freq = double(k)*2.0*M_PI/L;
                    hat[k] *= complex<double>(0,1)*k_freq/double(ddv.n[1]);
                }
                //inv_fft(ddv.n[1], &hat[0], &tmp[0]);
                fftb.backward();
                for(int l=0;l<ddv.n[1];l++)
                    out(m+l*ddv.n[0],j) = tmp[l];
            }
        }
        return out;
	}
	////////////////////////////////////////////////////////////////////
};


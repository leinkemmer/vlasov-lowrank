#pragma once

#include <generic/common.hpp>
#include <generic/integrators.hpp>
#include <generic/qr.hpp>
#include <generic/fft.hpp>
#include <container/domain-lowrank-1x1v.hpp>
#include <fstream>
#include <Eigen/Eigenvalues> 
#include <boost/numeric/odeint.hpp>

void integrate(function<void(const lowrank2d&,lowrank2d&)> rhs, const lowrank2d& in, lowrank2d& out, double t, int substeps=1) {

    using namespace boost::numeric;
    using namespace boost::numeric::odeint;

    // this is still with a constant step size
    runge_kutta_dopri5<lowrank2d,double,lowrank2d,double,vector_space_algebra> stepper;

    out = in; // integrate_adaptive uses the same variable as in and output
    integrate_adaptive(stepper,
            [rhs](const lowrank2d& x, lowrank2d& dxdt, double t) {
            rhs(x, dxdt);
            }, out, 0.0, t, t/double(substeps));
}


struct vlasov_continuous_conservative {
    int r;
    array<double,2> a;
    array<double,2> b;
    array<double,2> h;
    array<int,2>    n;

    vector<complex<double>> hat;

    rectangular_QR qrx, qrv;

    int n_fixed_basis;
    function<double(double)> custom_efield;

    // This function is usually set to v. For Debug purposes it can make sense
    // to set it to zero, for example.
    function<double(int)> v_coeff;
    function<double(double)> f0v;

    VectorXd cmp_V1, cmp_V2, cmp_V3;


    vlasov_continuous_conservative(int _r, array<int,2> _n, array<double,2> _a,
            array<double,2> _b,
            function<double(double)> _f0v = [](double) { return 1.0; },
            int _n_fixed_basis=0)
    : r(_r), a(_a), b(_b), n(_n), qrx(_n[0],r), qrv(_n[1], r),
      n_fixed_basis(_n_fixed_basis), f0v(_f0v) {
        hat.resize(max(n[0],n[1]));
        h[0] = (b[0]-a[0])/double(n[0]);
        h[1] = (b[1]-a[1])/double(n[1]);

        v_coeff = [this](int i) { return v(i); };

        // For comparison if we can do some analytic computations
        domain_lowrank2d dl(a, b, r, n, lowrank_type::normalized_integral, f0v);
        cmp_V1 = VectorXd::Constant(n[1], 1.0);
        cmp_V1 /= sqrt(dl.inner_product(cmp_V1, cmp_V1));
        cmp_V2.resize(n[1]);
        for(Index i=0;i<Index(n[1]);i++)
            cmp_V2[i] = v(i);
        cmp_V2 /= sqrt(dl.inner_product(cmp_V2, cmp_V2));
        cmp_V3.resize(n[1]);
        for(Index i=0;i<Index(n[1]);i++)
            cmp_V3[i] = v(i)*v(i)-1.0;
        cmp_V3 /= sqrt(dl.inner_product(cmp_V3, cmp_V3));
    }

    ~vlasov_continuous_conservative() {
    }

    void reset_rank(int _r) {
        r = _r;
        qrx = rectangular_QR(n[0],r);
        qrv = rectangular_QR(n[1],r);
    }

    void init_x(function<double(double,int)> f, MatrixXd& out) {
        int n = out.rows();
        int r = out.cols();
        for(int k=0;k<r;k++)
            for(int i=0;i<n;i++)
                out(i,k) = f(x(i), k);
    }

    void init_v(function<double(double,int)> f, MatrixXd& out) {
        int n = out.rows();
        int r = out.cols();
        for(int k=0;k<r;k++)
            for(int i=0;i<n;i++)
                out(i,k) = f(v(i), k);
    }

    double x(int i) {
        return a[0] + i*h[0];
    }
    double v(int j) {
        return a[1] + j*h[1];
    }

    MatrixXd regularized_inverse(const MatrixXd& in) {
        Eigen::JacobiSVD<MatrixXd> svd(in, Eigen::ComputeThinU | Eigen::ComputeThinV);
        //cout << svd.singularValues().maxCoeff()/svd.singularValues().minCoeff() << endl;
        VectorXd svs = svd.singularValues();
        double m = svs.maxCoeff();
        for(int i=0;i<svs.size();i++)
            svs[i] = sqrt(svs[i]*svs[i] + 1e-8*m*m);
        //cout << svs.maxCoeff()/svs.minCoeff() << endl;
        MatrixXd inS = svd.matrixU()*svs.asDiagonal()*svd.matrixV().transpose();
        //Sinv = svd.matrixU()*svs.asDiagonal().inverse()*svd.matrixV();
        return inS.inverse();
    }

    void step_unconventional(double tau, lowrank2d& in, lowrank2d& out) {
        int nv = in.V.rows();
        int r  = in.V.cols();

        // compute X^{n+1}
        MatrixXd K1 = in.X*in.S + tau*rhs_K(in.X*in.S, in.V);
        qrx.compute(K1);
        out.X = qrx.Q()/sqrt(h[0]); // R is discarded

        // compute V^{n+1}
        MatrixXd Sdot = rhs_S(in.S, in.V, in.X);
        MatrixXd Skp  = in.S.block(0,n_fixed_basis,r,r-n_fixed_basis);
        MatrixXd L1   = in.V.block(0,n_fixed_basis,nv,r-n_fixed_basis)*Skp.transpose()*Skp
          + tau*rhs_L(in.V*in.S.transpose() ,in.X, in.V, in.X*in.S)*Skp
          - tau*in.V.block(0,0,nv,n_fixed_basis)*Sdot.block(0,0,r,n_fixed_basis).transpose()*Skp
          + tau*in.V.block(0,n_fixed_basis,nv,r-n_fixed_basis)*Skp.transpose()*Sdot.block(0,n_fixed_basis,r,r-n_fixed_basis);

        MatrixXd L1_all(nv,r);
        L1_all.block(0,n_fixed_basis,nv,r-n_fixed_basis) = L1;
        L1_all.block(0,0,nv,n_fixed_basis) = in.V.block(0,0,nv,n_fixed_basis); // ensure that vectors are orthogonal to U_a

        domain_lowrank2d dl(a, b, r, n, lowrank_type::normalized_integral, f0v);
        MatrixXd R; // discarded
        out.V = gram_schmidt(L1_all, R,
                [&dl](const VectorXd& v1, const VectorXd& v2) {
                    return dl.inner_product(v1, v2);
                });

        // The fixed basis functions are just copied to the output
        // representation
        for(int i=0;i<n_fixed_basis;i++)
            out.V.col(i) = in.V.col(i);

        // compute M and N
        MatrixXd M = inner_product_x(out.X, in.X);
        MatrixXd N = inner_product_v(out.V, in.V);

        // compute S^{n+1}
        out.S = M*in.S*N.transpose() + tau*rhs_S(M*in.S*N.transpose(), out.V, out.X);
    }

    void step_unconventional2(double tau, lowrank2d& in, lowrank2d& out) {
        int nv = in.V.rows();
        int r  = in.V.cols();

        // compute X^{n+1}
        MatrixXd K1 = in.X*in.S + tau*rhs_K(in.X*in.S, in.V);
        qrx.compute(K1);
        out.X = qrx.Q()/sqrt(h[0]); // R is discarded

        // Compute V^{n+1}
        MatrixXd Sdot = rhs_S(in.S, in.V, in.X);
        MatrixXd Skp    = in.S.block(0,n_fixed_basis,r,r-n_fixed_basis);
        MatrixXd lh     = Skp.transpose()*Skp;
        MatrixXd Vbl(nv,r);
        Vbl.block(0,n_fixed_basis,nv,r-n_fixed_basis)
            = in.V.block(0,n_fixed_basis,nv,r-n_fixed_basis)*lh.transpose()
            + tau*rhs_L(in.V*in.S.transpose() ,in.X, in.V, in.X*in.S)*Skp
            - tau*in.V*Sdot.transpose()*Skp;

        domain_lowrank2d dl(a, b, r, n, lowrank_type::normalized_integral, f0v);
        MatrixXd R; // discarded
        Vbl.block(0,0,nv,n_fixed_basis) = in.V.block(0,0,nv,n_fixed_basis); // ensure that vectors are orthogonal to U_a
        out.V = gram_schmidt(Vbl, R,
                [&dl](const VectorXd& v1, const VectorXd& v2) {
                    return dl.inner_product(v1, v2);
                });

        // compute M and N
        MatrixXd M = inner_product_x(out.X, in.X);
        MatrixXd N = inner_product_v(out.V, in.V);

        // compute S^{n+1}
        out.S = M*in.S*N.transpose() + tau*rhs_S(M*in.S*N.transpose(), out.V, out.X);
    }


    void step_conservative_euler(double tau, lowrank2d& in, lowrank2d& out) {
        int nv = in.V.rows();
        int r  = in.V.cols();

        // compute S^{n+1}
        MatrixXd Sdot = rhs_S(in.S, in.V, in.X);
        out.S = in.S + tau*Sdot;

        // compute X^{n+1}
        MatrixXd Soutinv = out.S.inverse();
        //MatrixXd Soutinv = regularized_inverse(out.S);
        out.X = (in.X*in.S + tau*rhs_K(in.X*in.S, in.V))*Soutinv;

        // Compute V^{n+1}
        MatrixXd Skp    = in.S.block(0,n_fixed_basis,r,r-n_fixed_basis);
        MatrixXd lh     = Skp.transpose()*Skp;

        MatrixXd lh_inv = lh.inverse();
        out.V.block(0,n_fixed_basis,nv,r-n_fixed_basis)
            = in.V.block(0,n_fixed_basis,nv,r-n_fixed_basis)
            + tau*rhs_L(in.V*in.S.transpose() ,in.X, in.V, in.X*in.S)*Skp*lh_inv.transpose()
            - tau*in.V*Sdot.transpose()*Skp*lh_inv.transpose();

        // Due to numerical errors the output for V is not necessarily
        // orthogonal. We fix this here by performing a Gram-Schmidt procedure.
        domain_lowrank2d dl(a, b, r, n, lowrank_type::normalized_integral, f0v);
        dl.lr = in;
        MatrixXd R;
        MatrixXd V_orth = gram_schmidt(out.V, R,
                [&dl](const VectorXd& v1, const VectorXd& v2) {
                    return dl.inner_product(v1, v2);
                });
        out.V = V_orth;

        // The fixed basis functions are just copied to the output
        for(int i=0;i<n_fixed_basis;i++)
            out.V.col(i) = in.V.col(i);
    }

    void step_euler(double tau, lowrank2d& in, lowrank2d& out) {
        int nv = in.V.rows();
        int r  = in.V.cols();

        MatrixXd Sinv = in.S.inverse();
        MatrixXd Sdot = rhs_S(in.S, in.V, in.X);
        out.S = in.S + tau*Sdot;
        out.X = in.X
            + tau*rhs_K(in.X*in.S, in.V)*Sinv
            - tau*in.X*Sdot*Sinv;


        MatrixXd Skp    = in.S.block(0,n_fixed_basis,r,r-n_fixed_basis);
        MatrixXd lh     = Skp.transpose()*Skp;
        MatrixXd lh_inv = lh.inverse();

        out.V.block(0,n_fixed_basis,nv,r-n_fixed_basis)
            = in.V.block(0,n_fixed_basis,nv,r-n_fixed_basis)
            + tau*rhs_L(in.V*in.S.transpose() ,in.X, in.V, in.X*in.S)*Skp*lh_inv.transpose()
            - tau*in.V*Sdot.transpose()*Skp*lh_inv.transpose();

        // The fixed basis functions are just copied to the output
        // representation
        for(int i=0;i<n_fixed_basis;i++)
            out.V.col(i) = in.V.col(i);

        /* // alternative that works just as well
        out.S = in.S + tau*rhs_S(in.S, in.V, in.X);
        MatrixXd K1 = in.X*in.S
                      + tau*rhs_K(in.X*in.S, in.V);
        MatrixXd L1 = in.V*in.S.transpose()
                      + tau*rhs_L(in.V*in.S.transpose() ,in.X, in.V);
        MatrixXd Sinv = out.S.inverse();
        out.X = K1*Sinv;
        out.V = L1*Sinv.transpose();
        */
    }

    void step_dopri(double tau, lowrank2d& in, lowrank2d& out) {

        int nv = in.V.rows();
        int r  = in.V.cols();

        integrate([this,r,nv](const lowrank2d& in,lowrank2d& out) {
                
            MatrixXd Sinv = in.S.inverse();
            MatrixXd Sdot = rhs_S(in.S, in.V, in.X);
            out.S = Sdot;
            out.X = rhs_K(in.X*in.S, in.V)*Sinv - in.X*Sdot*Sinv;

            MatrixXd Skp    = in.S.block(0,n_fixed_basis,r,r-n_fixed_basis);
            MatrixXd lh     = Skp.transpose()*Skp;
            MatrixXd lh_inv = lh.inverse();

            out.V.resize(nv,r);
            out.V.block(0,n_fixed_basis,nv,r-n_fixed_basis)
                = rhs_L(in.V*in.S.transpose() ,in.X, in.V, in.X*in.S)*Skp*lh_inv.transpose()
                - in.V*Sdot.transpose()*Skp*lh_inv.transpose();

            // The fixed basis functions are just copied to the output
            // representation
            for(int i=0;i<n_fixed_basis;i++)
                out.V.col(i) = VectorXd::Zero(nv);

        }, in, out, tau);
    }

    MatrixXd rhs_K(const MatrixXd& K, const MatrixXd& V) {
        // This computes the second term
        MatrixXd C2 = compute_C2(V);
        MatrixXd out = -K*C2.transpose();
       
        // for which we need the electric field
        VectorXd E = electric_field( rho_K(K, V) );
        // which enters in a pointwise multiplication
        for(int j=0;j<K.cols();j++) {
            auto bl = out.col(j);
            bl = bl.array()*E.array();
        }
        

        MatrixXd Ktmp = diff_fft(K, b[0]-a[0]);
        MatrixXd C1   = compute_C1(V);
        out -= Ktmp*C1.transpose();

        return out;
    }

    MatrixXd rhs_S(const MatrixXd& S, const MatrixXd& V0, const MatrixXd& X1) {

        // This is the linear term
        MatrixXd C1   = compute_C1(V0);
        MatrixXd D2   = compute_D2(X1);
        MatrixXd out = -D2*S*C1.transpose();
        
        // compute the appropriate electric field and then D1(E)
        MatrixXd K = X1*S;
        VectorXd E = electric_field( rho_K(K, V0) );
        MatrixXd D1 = compute_D1(X1, E);
        MatrixXd C2 = compute_C2(V0);
        // This is the second term
        out -= D1*S*C2.transpose();

        return out;
    }

    MatrixXd rhs_L(const MatrixXd& L, const MatrixXd& X0, const MatrixXd& V0,
            const MatrixXd& K) {

        int n = L.rows();
        int r = L.cols();

        // This computes the second term
        MatrixXd D2   = compute_D2(X0);
        MatrixXd out = -L*D2.transpose();

        // multiply with v
        for(int k=0;k<r;k++)
            for(int i=0;i<n;i++)
                out(i,k) *= v_coeff(i);

        // derivative of L
        MatrixXd L_dv  = diff_fft(L, b[1]-a[1]);
        // derivative of log(f0v)
        VectorXd dlogf0v(n);
        for(int i=0;i<n;i++)
            dlogf0v(i) = log(f0v(v(i)));
        dlogf0v = diff_fft(dlogf0v, b[1]-a[1]);
        // compute L*\partial_v log(f0v(v))
        MatrixXd L_dlogf0v = L;
        for(int k=0;k<r;k++)
            for(int i=0;i<n;i++)
                L_dlogf0v(i,k) *= dlogf0v(i); // only up to 1e-7 or so
                // if we use the exact value we can be more accurate
                //L_dlogf0v(i,k) *= -v(i);

        VectorXd E    = electric_field( rho_K(K, V0) );
        MatrixXd D1   = compute_D1(X0, E);
        out -= (L_dv + L_dlogf0v)*D1.transpose();

        return out;
    }


    VectorXd rho_K(const MatrixXd& K, const MatrixXd& V0) {
        int nx = K.rows();
        int nv = V0.rows();
        int r = K.cols();

        VectorXd rho(nx);
        rho.setZero();
        for(int k=0;k<r;k++) {
            double I_V0 = 0.0;
            for(int j=0;j<nv;j++)
                I_V0 += h[1]*V0(j,k)*f0v(v(j));

            for(int i=0;i<nx;i++)
                rho(i) += I_V0*K(i,k);
        }
        return rho - VectorXd::Constant(nx,1.0);
    }

    MatrixXd electric_field(const VectorXd& rho) {
        int n = rho.rows();
        if(custom_efield) {
            VectorXd E(n);
            for(int i=0;i<n;i++)
                E(i) = custom_efield(x(i));
            return E;
        } else {
            // solve Poisson's equation
            fft(n, &rho(0), &hat[0]);
            for(int k=0;k<n;k++) {
                double k_freq = double(k)*2.0*M_PI/(b[0]-a[0]);
                if(k != 0)
                    hat[k] *= 1.0/(complex<double>(0,1)*k_freq*double(n));
                else
                    hat[k] = 0.0;
            }
            VectorXd E(n);
            inv_fft(n, &hat[0], &E(0));
            return E;
        }
    }

    double electric_field_energy(const lowrank2d& lr, VectorXd& E) {
        E = electric_field( rho_K(lr.X*lr.S, lr.V) );

        // compute electric energy
        double s = 0.0;
        for(int i=0;i<E.rows();i++)
            s += h[0]*pow(E[i],2);
        return 0.5*s;
    }

    MatrixXd inner_product_x(const MatrixXd& X, const MatrixXd& Y) {
        int n = X.rows();
        int r = X.cols();

        MatrixXd out(r,r);

        for(int k=0;k<r;k++) {
            for(int l=0;l<r;l++) {
                double res = 0.0;
                for(int i=0;i<n;i++)
                    res += X(i,k)*Y(i,l)*h[0];
                out(k,l) = res;
            }
        }

        return out;
    }

    MatrixXd inner_product_v(const MatrixXd& V, const MatrixXd& W) {
        int n = V.rows();
        int r = V.cols();

        MatrixXd out(r,r);

        for(int k=0;k<r;k++) {
            for(int l=0;l<r;l++) {
                double res = 0.0;
                for(int i=0;i<n;i++)
                    res += f0v(v(i))*V(i,k)*W(i,l)*h[1];
                out(k,l) = res;
            }
        }

        return out;
    }

    MatrixXd diff_fft(const MatrixXd& K, double L) {
        // We assume that K and out are in Fortran-major order
        int n = K.rows();
        int r = K.cols();
        MatrixXd out(n,r);
        for(int j=0;j<r;j++) {
            fft(n, &K(0,j), &hat[0]);
            for(int k=0;k<n;k++) {
                double k_freq = double(k)*2.0*M_PI/L;
                hat[k] *= complex<double>(0,1)*k_freq/double(n);
            }
            inv_fft(n, &hat[0], &out(0,j));
        }
        return out;
    }


    VectorXd diff_fft(const VectorXd& K, double L) {
        // We assume that K and out are in Fortran-major order
        int n = K.size();
        VectorXd out(n);
        fft(n, &K(0), &hat[0]);
        for(int k=0;k<n;k++) {
            double k_freq = double(k)*2.0*M_PI/L;
            hat[k] *= complex<double>(0,1)*k_freq/double(n);
        }
        inv_fft(n, &hat[0], &out(0));
        return out;
    }
    
    MatrixXd compute_C1(const MatrixXd& V) {
        int n = V.rows();
        int r = V.cols();
        MatrixXd C1(r,r);
        C1.setZero();
        for(int i=0;i<r;i++)
            for(int j=0;j<r;j++)
                for(int k=0;k<n;k++)
                    C1(i,j) += h[1]*v_coeff(k)*V(k,i)*V(k,j)*f0v(v(k));
        return C1;
    }
    
    MatrixXd compute_C2(const MatrixXd& V) {
        int n = V.rows();
        int r = V.cols();
        MatrixXd C2(r,r);
        C2.setZero();

        MatrixXd V_f0v = V;
        for(int i=0;i<r;i++)
            for(int k=0;k<n;k++)
                V_f0v(k,i) *= f0v(v(k));
        MatrixXd nablaVf0v = diff_fft(V_f0v, b[1]-a[1]);

        // In order to get momentum conservation up to machine precision we have
        // to perform the integration here up to machine precision. Using FFT the
        // error for C_{2j} is significantly above that level. Thus, we compute the
        // integral for the fixed basis functions analytically here. The following code
        // assumes that V_1 \propto 1, V_2 \propto v, and V_3 \propto v^2/2. We first
        // check this to make sure // that the code produces correct results.
        if(n_fixed_basis >= 1 && (V.col(0)-cmp_V1).lpNorm<Eigen::Infinity>() >= 5e-7) {
            cout << "ERROR compute_C2: V1 is not equal to 1" << endl;
            cout << "difference: " << (V.col(0)-cmp_V1).lpNorm<Eigen::Infinity>() << endl;
            exit(1);
        }
        if(n_fixed_basis >= 2 && (V.col(1)-cmp_V2).lpNorm<Eigen::Infinity>() >= 5e-7) {
            cout << "ERROR compute_C2: V2 is not equal to v" << endl;
            cout << "difference: " << (V.col(1)-cmp_V2).lpNorm<Eigen::Infinity>() << endl;
            exit(1);
        }
        if(n_fixed_basis >= 3 && (V.col(2)-cmp_V3).lpNorm<Eigen::Infinity>() >= 5e-7) {
            cout << "ERROR compute_C2: V3 is not equal to v^2" << endl;
            cout << "difference: " << (V.col(2)-cmp_V3).lpNorm<Eigen::Infinity>() << endl;
            exit(1);
        }

        // the first row is always zero if n_fixed_basis >= 1, we do not need to do anything
        if(n_fixed_basis >= 2) {
            C2(1,0) = -1.0; // everything else in that row is zero
        }
        if(n_fixed_basis >= 3) {
            C2(2,1) = -sqrt(2.0);
        }
        
        // all the remaining entries of C2 are computed here
        for(int i=std::min(3,n_fixed_basis);i<r;i++) {
            for(int j=0;j<r;j++) {
                // compute the integral
                for(int k=0;k<n;k++)
                    C2(i,j) += h[1]*nablaVf0v(k,j)*V(k,i);
            }
        }

        return C2;
    }

    MatrixXd compute_D1(const MatrixXd& X1, const VectorXd& E) {
        int n = X1.rows();
        assert(n == E.size());
        int r = X1.cols();
        MatrixXd D1(r,r);
        D1.setZero();
        for(int i=0;i<r;i++)
            for(int j=0;j<r;j++)
                for(int k=0;k<n;k++)
                    D1(i,j) += h[0]*E(k)*X1(k,i)*X1(k,j);

        return D1;
    }

    MatrixXd compute_D2(const MatrixXd& X1) {
        int n = X1.rows();
        int r = X1.cols();

        MatrixXd nablaX1 = diff_fft(X1, b[0]-a[0]);

        MatrixXd D2(r,r);
        D2.setZero();
        for(int i=0;i<r;i++) {
            for(int j=0;j<r;j++)
                // compute the integral
                for(int k=0;k<n;k++)
                    D2(i,j) += h[0]*nablaX1(k,j)*X1(k,i);
        }

        return D2;
    }
};


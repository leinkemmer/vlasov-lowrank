#pragma once

#include <memory>
using std::unique_ptr;

#include <generic/common.hpp>
#include <algorithms/lowrank-continuoussplit-1x1v.hpp>


void electric_field(const VectorXd& rho, VectorXd& E, double L) {
    int n = rho.rows();
    vector<complex<double>> hat(n);
    fft(n, &rho(0), &hat[0]);
    for(int k=0;k<n;k++) {
        double k_freq = double(k)*2.0*M_PI/L;
        if(k != 0)
            hat[k] *= 1.0/(complex<double>(0,1)*k_freq*double(n));
        else
            hat[k] = 0.0;
    }
    inv_fft(n, &hat[0], &E(0));
}

typedef Eigen::Matrix<unique_ptr<domain_lowrank2d>,Eigen::Dynamic,Eigen::Dynamic> domain_lowrank2d_patches;
typedef Eigen::Matrix<unique_ptr<vlasov_continuoussplit>,Eigen::Dynamic,Eigen::Dynamic> vlasov_cs_patches;
typedef Eigen::Matrix<MatrixXd,Eigen::Dynamic,Eigen::Dynamic> MatrixXd_patches;

VectorXd patched_rho(const vlasov_cs_patches& vcs, const domain_lowrank2d_patches& in, int Nx) {
    int nx = in(0,0)->lr.X.rows();

    // compute rho
    VectorXd rho(Nx);
    rho.setZero();
    for(auto pid : up_to<2>(vcs.rows(),vcs.cols())) {
        auto& lvcs = vcs(pid[0],pid[1]);
        auto& lin = in(pid[0],pid[1]);

        int start = pid[0]*nx;
        rho.segment(start,nx) += lvcs->rho_K(lin->lr.X*lin->lr.S, lin->lr.V) + VectorXd::Constant(nx,1.0);
    }

    return rho-VectorXd::Constant(Nx,1.0);
}


double electric_field(const vlasov_cs_patches& vcs, const domain_lowrank2d_patches& in, VectorXd& E, double L) {

    // compute E
    electric_field(patched_rho(vcs, in, E.size()), E, L);

    // compute electric energy
    double ee = 0.0;
    for(int i=0;i<E.size();i++)
        ee += L/double(E.size())*pow(E[i],2);
    ee *= 0.5;
    return ee;
}

VectorXd get_local_E(VectorXd& E, int pidx, int nx) {
    return E.segment(pidx*nx,nx);
}


MatrixXd patched_compute_B(const MatrixXd& VL, const MatrixXd& VR, double h) {
    int n = VL.rows();
    int r = VL.cols();

    MatrixXd B(r,r);
    for(auto i : up_to<2>(r,r)) {
        double s = 0.0;
        for(int j=0;j<n;j++)
            s += VL(j,i[0])*VR(j,i[1])*h;
        B(i[0],i[1]) = s;
    }
    return B;
}

void patched_global_to_local(const array<int,2>& pid, const array<int,2>& patches,
        const array<double,2>& a, const array<double,2>& b, const array<int,2>& ext,
        array<double,2>& la, array<double,2>& lb, array<int,2>& lext) {

        array<double,2> L_patch = {(b[0]-a[0])/double(patches[0]), (b[1]-a[1])/double(patches[1])};
        la = {a[0] + pid[0]*L_patch[0], a[1] + pid[1]*L_patch[1]};
        lb = {a[0] + (pid[0]+1)*L_patch[0], a[1] + (pid[1]+1)*L_patch[1]};

        lext = {ext[0]/patches[0], ext[1]/patches[1]};
}


MatrixXd patched_vBC(const domain_lowrank2d& curr, const domain_lowrank2d& down,
        const domain_lowrank2d& up) {
    int r = curr.lr.X.cols();
    MatrixXd B_down = patched_compute_B(curr.lr.X, down.lr.X, curr.h[0]);
    MatrixXd B_up   = patched_compute_B(curr.lr.X, up.lr.X, curr.h[0]);

    MatrixXd bdry(2,r);
    int nv = curr.lr.V.rows();
    bdry.row(0) = ((down.lr.V*down.lr.S.transpose()).row(nv-1))*B_down.transpose();
    bdry.row(1) = ((up.lr.V*up.lr.S.transpose()).row(0))*B_up.transpose();
    return bdry;
}

MatrixXd patched_xBC(const domain_lowrank2d& curr, const domain_lowrank2d& down,
        const domain_lowrank2d& up) {
    int r = curr.lr.X.cols();
    MatrixXd B_down = patched_compute_B(curr.lr.V, down.lr.V, curr.h[1]);
    MatrixXd B_up   = patched_compute_B(curr.lr.V, up.lr.V, curr.h[1]);

    MatrixXd bdry(2,r);
    int nx = curr.lr.X.rows();
    bdry.row(0) = ((down.lr.X*down.lr.S).row(nx-1))*B_down.transpose();
    bdry.row(1) = ((up.lr.X*up.lr.S).row(0))*B_up.transpose();
    return bdry;
}

MatrixXd patched_VBC(const domain_lowrank2d& curr, const domain_lowrank2d& down,
        const domain_lowrank2d& up) {

    MatrixXd bdry = patched_vBC(curr, down, up);

    //cout << "S pseudo inverse: " << endl << curr.lr.S.completeOrthogonalDecomposition().pseudoInverse() << endl;
    MatrixXd bdryV(bdry.rows(),bdry.cols());
    bdryV.row(0) = bdry.row(0)*curr.lr.S.transpose().completeOrthogonalDecomposition().pseudoInverse();
    bdryV.row(1) = bdry.row(1)*curr.lr.S.transpose().completeOrthogonalDecomposition().pseudoInverse();
    return bdryV;
}


MatrixXd patched_XBC(const domain_lowrank2d& curr, const domain_lowrank2d& down,
        const domain_lowrank2d& up) {
    MatrixXd bdry = patched_xBC(curr, down, up);

    //cout << "S pseudo inverse: " << endl << curr.lr.S.completeOrthogonalDecomposition().pseudoInverse() << endl;
    MatrixXd bdryX(bdry.rows(),bdry.cols());
    bdryX.row(0) = bdry.row(0)*curr.lr.S.completeOrthogonalDecomposition().pseudoInverse();
    bdryX.row(1) = bdry.row(1)*curr.lr.S.completeOrthogonalDecomposition().pseudoInverse();
    return bdryX;
}


#pragma once

#include <complex>

#include <generic/common.hpp>
#include <generic/timer.hpp>
#include <generic/fft.hpp>

#include <container/domain-lowrank-fluid2d.hpp>

#include <gsl/gsl_interp2d.h>
#include <gsl/gsl_spline2d.h>

enum diff_mode {
    DM_FFT, DM_UPWIND_LXF, DM_UPWIND_STAGLXF, DM_UPWIND_NT, DM_LWVANLEER_NT
};

enum solve_mode {
    SM_EXPLICIT, SM_CVODE
};

VectorXd init_moment(domain4d_data dd, function<double(double,double)> f_rho) {
    VectorXd mom(dd.n[0]*dd.n[1]);
    for(auto i : range<2>(dd.n[0],dd.n[1])) {
        double x = dd.x(i);
        double y = dd.y(i);

        mom(dd.lin_idx_x(i)) = f_rho(x, y);
    }
    return mom;
}

struct moments {
    VectorXd rho;
    array<VectorXd,2> u;

    moments() {}

    moments(array<Index,2> n)
        : rho(n[0]*n[1]) {
            u[0].resize(n[0]*n[1]);
            u[1].resize(n[0]*n[1]);
        }

    void init(function<double(double,double)> f_rho, function<double(double,double)> f_u0,
            function<double(double,double)> f_u1, domain4d_data dd) {
        rho  = init_moment(dd, f_rho);
        u[0] = init_moment(dd, f_u0);
        u[1] = init_moment(dd, f_u1);
    }

    void write_nc(NCWriter& nc, domain4d_data dd) {
        array<VectorXd,2> rhou = {
            (rho.array()*u[0].array()).matrix(),
            (rho.array()*u[1].array()).matrix()
        };

        VectorXd coo_x = dd.coordinates_x();
        VectorXd coo_y = dd.coordinates_y();

        nc.write("x",     {dd.n[0]}, coo_x.data()          , {"x"});
        nc.write("y",     {dd.n[1]}, coo_y.data()          , {"y"});
        
        nc.write("rho",   {dd.n[1],dd.n[0]}, rho.data()    , {"y","x"});
        nc.write("u0",    {dd.n[1],dd.n[0]}, u[0].data()   , {"y","x"});
        nc.write("u1",    {dd.n[1],dd.n[0]}, u[1].data()   , {"y","x"});
        nc.write("rhou0", {dd.n[1],dd.n[0]}, rhou[0].data(), {"y","x"});
        nc.write("rhou1", {dd.n[1],dd.n[0]}, rhou[1].data(), {"y","x"});
    }
};

moments to_conservative_form(const moments& mom) {
    moments out;
    out.rho  = mom.rho;
    out.u[0] = (mom.rho.array()*mom.u[0].array()).matrix();
    out.u[1] = (mom.rho.array()*mom.u[1].array()).matrix();
    return out;
}

moments from_conservative_form(const moments& mom) {
    moments out;
    out.rho  = mom.rho;
    out.u[0] = (mom.u[0].array()/mom.rho.array()).matrix();
    out.u[1] = (mom.u[1].array()/mom.rho.array()).matrix();
    return out;
}


struct M_t {
    VectorXd M1;
    array<VectorXd, 2> M2;
    array<VectorXd, 4> M3;

    M_t(const VectorXd& _M1, const array<VectorXd, 2>& _M2, const array<VectorXd, 4>& _M3)
        : M1(_M1), M2(_M2), M3(_M3) {}
};


// returns exp(-0.5*v^2)
VectorXd get_expmvsq(domain4d_data dd) {
    VectorXd vec(dd.n[2]*dd.n[3]);
    for(auto i : range<2>(dd.n[2],dd.n[3]))
        vec(dd.lin_idx_v(i)) = exp(-0.5*pow(dd.v(i),2)-0.5*pow(dd.w(i),2));
    return vec;
}

array<MatrixXd,2> convolution_v(const MatrixXd& V, domain4d_data dd) {
    if(dd.n[2]%2 != 0 || dd.n[3]%2 != 0) {
        cout << "ERROR: convolution_v is currently only implemented for even "
             << "dd.n[2] and dd.n[3]." << endl;
        exit(1);
    }
    array<MatrixXd,2> out;
    for(size_t i=0;i<out.size();i++)
        out[i].resize(V.rows(), V.cols());

    array<Index,2> n_v = {dd.n[2], dd.n[3]};

    VectorXd vec = get_expmvsq(dd);
    fft2d f(n_v, vec.data()), f_exp(n_v, vec.data());
    f_exp.forward(vec.data());

    double normalization = pow(double(n_v[0]),2)*pow(double(n_v[1]),2)/(dd.b[2]-dd.a[2])/(dd.b[3]-dd.a[3]);
    for(Index dim=0;dim<2;dim++) {
        for(Index k=0;k<dd.r;k++) {
            for(auto i : range<2>(n_v)) {
                double coeff = (dim==0) ? dd.v(i) : dd.w(i);
                vec(dd.lin_idx_v(i)) = coeff*V(dd.lin_idx_v(i), k);
            }

            f.forward(vec.data());

            for(Index j=0;j<n_v[1];j++) {
                for(Index i=0;i<n_v[0]/2+1;i++) {
                    Index m = i + j*(n_v[0]/2+1);
                    complex<double> shift = exp(complex<double>(0.0, (i+j)*M_PI));
                    f.uhat[m] *= shift*f_exp.uhat[m]/normalization;
                }
            }

            f.backward(&out[dim](0,k));
        }
    }
    return out;
}


array<MatrixXd,4> convolution_vtensorv(const MatrixXd& V, domain4d_data dd) {
    if(dd.n[2]%2 != 0 || dd.n[3]%2 != 0) {
        cout << "ERROR: convolution_v is currently only implemented for even "
             << "dd.n[2] and dd.n[3]." << endl;
        exit(1);
    }
    array<MatrixXd,4> out;
    for(size_t i=0;i<out.size();i++)
        out[i].resize(V.rows(), V.cols());

    array<Index,2> n_v = {dd.n[2], dd.n[3]};

    VectorXd vec = get_expmvsq(dd);
    fft2d f(n_v, vec.data()), f_exp(n_v, vec.data());
    f_exp.forward(vec.data());

    double normalization = pow(double(n_v[0]),2)*pow(double(n_v[1]),2)/(dd.b[2]-dd.a[2])/(dd.b[3]-dd.a[3]);
    for(auto dim : range<2>(2,2)) {
        for(Index k=0;k<dd.r;k++) {
            for(auto i : range<2>(n_v)) {
                double coeff1 = (dim[0]==0) ? dd.v(i) : dd.w(i);
                double coeff2 = (dim[1]==0) ? dd.v(i) : dd.w(i);
                vec(dd.lin_idx_v(i)) = coeff1*coeff2*V(dd.lin_idx_v(i), k);
            }

            f.forward(vec.data());

            for(Index j=0;j<n_v[1];j++) {
                for(Index i=0;i<n_v[0]/2+1;i++) {
                    Index m = i + j*(n_v[0]/2+1);
                    complex<double> shift = exp(complex<double>(0.0, (i+j)*M_PI));
                    f.uhat[m] *= shift*f_exp.uhat[m]/normalization;
                }
            }

            f.backward(&out[dim[0]+2*dim[1]](0,k));
        }
    }
    return out;
}

MatrixXd evaluate_g(MatrixXd g, array<VectorXd,2> u, domain4d_data dd) {
    gsl_spline2d *spline = gsl_spline2d_alloc(gsl_interp2d_bicubic, dd.n[2], dd.n[3]);
    gsl_interp_accel *xacc = gsl_interp_accel_alloc();
    gsl_interp_accel *yacc = gsl_interp_accel_alloc();

    VectorXd coordinates_v = dd.coordinates_v();
    VectorXd coordinates_w = dd.coordinates_w();

    MatrixXd out(dd.n[0]*dd.n[1], dd.r);

    for(Index k=0;k<dd.r;k++) {
        gsl_spline2d_init(spline, coordinates_v.data(), coordinates_w.data(),
                          &g(0, k), dd.n[2], dd.n[3]);

        for(auto i : range<2>(dd.n[0], dd.n[1])) {
            double v = u[0](dd.lin_idx_x(i));
            double w = u[1](dd.lin_idx_x(i));
            double val = gsl_spline2d_eval(spline, v, w, xacc, yacc);

            out(dd.lin_idx_x(i),k) = val;
        }
    }


    gsl_spline2d_free(spline);
    gsl_interp_accel_free(xacc);
    gsl_interp_accel_free(yacc);
    return out;
}

double sign(double x) {
    return (x > 0.0) - (x < 0.0);
}

double minmod(double a, double b) {
    return 0.5*(sign(a)+sign(b))*std::min(fabs(a),fabs(b));
}

double no_limiter(double a, double b) {
    return a;
}


array<VectorXd,2> compute_flux_rho(const lowrank4d& in, const moments& mom, const domain4d_data& dd) {
    // Step 1
    array<MatrixXd,2> g1 = convolution_v(in.V, dd);

    // Step 2
    array<MatrixXd,2> VM1 = {evaluate_g(g1[0], mom.u, dd), evaluate_g(g1[1], mom.u, dd)};

    double fac=1.0/(2.0*M_PI);
    for(Index i=0;i<2;i++) {
        for(Index k=0;k<dd.r;k++)
            VM1[i].col(k).array() *= fac*mom.rho.array();
    }
    
    // Step 3
    MatrixXd XS = -in.X*in.S;
    array<VectorXd,2> out = {VectorXd::Zero(dd.n[0]*dd.n[1]), VectorXd::Zero(dd.n[0]*dd.n[1])};
    for(Index k=0;k<dd.r;k++) {
        for(Index i=0;i<dd.n[0]*dd.n[1];i++) {
            out[0](i) += XS(i,k)*VM1[0](i,k);
            out[1](i) += XS(i,k)*VM1[1](i,k);
        }
    }

    return out;
}


array<VectorXd,2> compute_flux_u0(const lowrank4d& in, const moments& mom_in, const domain4d_data& dd) {
    // Step 1 (compute the convolution)
    array<MatrixXd,4> g2 = convolution_vtensorv(in.V, dd);

    // Step 2 (evaluate g)
    array<MatrixXd,2> VM2 = {evaluate_g(g2[0], mom_in.u, dd), evaluate_g(g2[1], mom_in.u, dd)};
    
    double fac=1.0/(2.0*M_PI);
    for(Index i=0;i<2;i++) {
        for(Index k=0;k<dd.r;k++)
            VM2[i].col(k).array() *= fac*mom_in.rho.array();
    }

    // Step 3 (compute the flux)
    MatrixXd XS = -in.X*in.S;
    array<VectorXd,2> out = { VectorXd::Zero(dd.n[0]*dd.n[1]), VectorXd::Zero(dd.n[0]*dd.n[1]) };
    for(Index k=0;k<dd.r;k++) {
        for(Index i=0;i<dd.n[0]*dd.n[1];i++) {
            out[0](i) += XS(i,k)*VM2[0](i,k);
            out[1](i) += XS(i,k)*VM2[1](i,k);
        }
    }

    return out;
}

array<VectorXd,2> compute_flux_u1(const lowrank4d& in, const moments& mom_in, const domain4d_data& dd) {
    // Step 1 (compute the convolution)
    array<MatrixXd,4> g2 = convolution_vtensorv(in.V, dd);

    // Step 2 (evaluate g)
    array<MatrixXd,2> VM2 = {evaluate_g(g2[2], mom_in.u, dd), evaluate_g(g2[3], mom_in.u, dd)};
    
    double fac=1.0/(2.0*M_PI);
    for(Index i=0;i<2;i++) {
        for(Index k=0;k<dd.r;k++)
            VM2[i].col(k).array() *= fac*mom_in.rho.array();
    }

    // Step 3 (compute the flux)
    MatrixXd XS = -in.X*in.S;
    array<VectorXd,2> out = { VectorXd::Zero(dd.n[0]*dd.n[1]), VectorXd::Zero(dd.n[0]*dd.n[1]) };
    for(Index k=0;k<dd.r;k++) {
        for(Index i=0;i<dd.n[0]*dd.n[1];i++) {
            out[0](i) += XS(i,k)*VM2[0](i,k);
            out[1](i) += XS(i,k)*VM2[1](i,k);
        }
    }

    return out;
}

//
// compute stress tensor
//
array<MatrixXd,4> convolution_vtensorvexp(const MatrixXd& V, domain4d_data dd) {
    if(dd.n[2]%2 != 0 || dd.n[3]%2 != 0) {
        cout << "ERROR: convolution_v is currently only implemented for even "
             << "dd.n[2] and dd.n[3]." << endl;
        exit(1);
    }
    array<MatrixXd,4> out;
    for(size_t i=0;i<out.size();i++)
        out[i].resize(V.rows(), V.cols());

    array<Index,2> n_v = {dd.n[2], dd.n[3]};

    VectorXd vec(n_v[0]*n_v[1]);
    fft2d f_V(n_v, vec.data());

    // compute Fourier transform of (v \otimes v) exp(-v^2/2)
    array<fft2d,4> f_exp = {fft2d(n_v, vec.data()), fft2d(n_v, vec.data()),
                            fft2d(n_v, vec.data()), fft2d(n_v, vec.data())};
    VectorXd vecexp = get_expmvsq(dd);
    for(auto dim : range<2>(2,2)) {
        for(auto i : range<2>(n_v)) {
            double coeff1 = (dim[0]==0) ? dd.v(i) : dd.w(i);
            double coeff2 = (dim[1]==0) ? dd.v(i) : dd.w(i);
            vec(dd.lin_idx_v(i)) = coeff1*coeff2*vecexp(dd.lin_idx_v(i));
        }
        f_exp[dim[0] + 2*dim[1]].forward(vec.data());
    }

    fft2d f_inv(n_v, vec.data());
    double normalization = pow(double(n_v[0]),2)*pow(double(n_v[1]),2)/(dd.b[2]-dd.a[2])/(dd.b[3]-dd.a[3]);
    for(Index k=0;k<dd.r;k++) {
        // compute Fourier transform of V_k
        for(auto i : range<2>(n_v))
            vec(dd.lin_idx_v(i)) = V(dd.lin_idx_v(i), k);
        f_V.forward(vec.data());

        for(auto dim : range<2>(2,2)) {
            for(Index j=0;j<n_v[1];j++) {
                for(Index i=0;i<n_v[0]/2+1;i++) {
                    Index m = i + j*(n_v[0]/2+1);
                    complex<double> shift = exp(complex<double>(0.0, (i+j)*M_PI));
                    f_inv.uhat[m] = shift*f_V.uhat[m]*f_exp[dim[0] + 2*dim[1]].uhat[m]/normalization;
                }
            }

            f_inv.backward(&out[dim[0]+2*dim[1]](0,k));
        }
    }
    
    return out;
}

array<VectorXd,4> stress_tensor(const lowrank4d& in, const moments& mom_in, const domain4d_data& dd, double epsilon) {
    array<MatrixXd,4> g3 = convolution_vtensorvexp(in.V, dd);

    // evaluate g
    array<MatrixXd,4> g3_eval = {evaluate_g(g3[0], mom_in.u, dd), evaluate_g(g3[1], mom_in.u, dd),
                                 evaluate_g(g3[2], mom_in.u, dd), evaluate_g(g3[3], mom_in.u, dd)};

    // multiply with rho/(2*pi)
    double fac=1.0/(2.0*M_PI);
    for(Index i=0;i<4;i++) {
        for(Index k=0;k<dd.r;k++)
            g3_eval[i].col(k).array() *= fac*mom_in.rho.array();
    }

    // put everything together
    MatrixXd XS = -in.X*in.S;
    array<VectorXd,4> out = {mom_in.rho, VectorXd::Zero(dd.n[0]*dd.n[1]),
                             VectorXd::Zero(dd.n[0]*dd.n[1]), mom_in.rho};
    for(Index k=0;k<dd.r;k++) {
        for(Index i=0;i<dd.n[0]*dd.n[1];i++) {
            out[0](i) += XS(i,k)*g3_eval[0](i,k);
            out[1](i) += XS(i,k)*g3_eval[1](i,k);
            out[2](i) += XS(i,k)*g3_eval[2](i,k);
            out[3](i) += XS(i,k)*g3_eval[3](i,k);
        }
    }

    for(Index i=0;i<4;i++)
        out[i] /= epsilon;

    return out;
}




typedef std::function<array<VectorXd,2>(const lowrank4d&, const moments&, const domain4d_data&)>
        flux_function;


struct vlasov_bgk_moments {
    domain4d_data dd;
    double epsilon;
    double alpha; // can be used to turn of advection for testing, alpha=1.0 by default
    double cvode_tolrel, cvode_tolabs; // can be overwritten (for example in the unit tests)
    diff_mode  dm;
    solve_mode sm;

    rectangular_QR qrx, qrv;

    timer t_cvode, t_rhou;

    std::function<double(double,double)> limiter;
    bool lr_limiter;

    VectorXd C_collision;

    vlasov_bgk_moments(domain4d_data _dd, double _epsilon, diff_mode _dm = DM_FFT, solve_mode _sm = SM_CVODE)
        : dd(_dd), epsilon(_epsilon), dm(_dm),  sm(_sm), qrx(_dd.n[0]*_dd.n[1],_dd.r), qrv(_dd.n[2]*_dd.n[3], _dd.r), limiter(minmod), lr_limiter(true) {
            alpha=1.0;
            cvode_tolrel = 1e-6;
            cvode_tolabs = 1e-6;

            C_collision.resize(dd.n[0]*dd.n[1]);
            for(Index m=0;m<dd.n[0]*dd.n[1];m++)
                C_collision(m) = 1.0;
    }


    void Lie_step(double tau, lowrank4d& in, lowrank4d& out, moments& mom_in, moments& mom_out) {
        //
        // Step for the moments
        //

        // Step 4 (update the moments)
        array<VectorXd,2> flux_rho = compute_flux_rho(in, mom_in, dd);
        array<VectorXd,2> flux_u0  = compute_flux_u0(in, mom_in, dd);
        array<VectorXd,2> flux_u1  = compute_flux_u1(in, mom_in, dd);

        VectorXd I1 = diff_x_fft_vec(flux_rho[0]) + diff_y_fft_vec(flux_rho[1]);
        array<VectorXd,2> I2 = {
            diff_x_fft_vec(flux_u0[0]) + diff_y_fft_vec(flux_u0[1]),
            diff_x_fft_vec(flux_u1[0]) + diff_y_fft_vec(flux_u1[1])
        };

        if(dm == DM_FFT) {
            mom_out.rho  = mom_in.rho  + tau*I1;
            mom_out.u[0] = mom_in.u[0]
                + tau*((I2[0].array() - I1.array()*mom_in.u[0].array())/mom_in.rho.array()).matrix();
            mom_out.u[1] = mom_in.u[1]
                + tau*((I2[1].array() - I1.array()*mom_in.u[1].array())/mom_in.rho.array()).matrix();
        } else if(dm == DM_UPWIND_LXF) {
            moments cons_in = to_conservative_form(mom_in);

            moments cons_out;
            cons_out.rho  = LaxFriedrichs(tau, cons_in.rho, I1);
            cons_out.u[0] = LaxFriedrichs(tau, cons_in.u[0], I2[0]);
            cons_out.u[1] = LaxFriedrichs(tau, cons_in.u[1], I2[1]);

            mom_out = from_conservative_form(cons_out);
        } else if(dm == DM_UPWIND_STAGLXF) {
            // First half step (to the staggered grid)
            moments mom = lax_friedrichs_staggered_1(0.5*tau, in, mom_in,
                    compute_flux_rho, compute_flux_u0, compute_flux_u1); 

            // Second half step (back to the original grid)
            lowrank4d in_staggered = in;
            for(Index k=0;k<dd.r;k++) {
                in_staggered.X.col(k) = avgp0_y(avgp0_x(in.X.col(k)));
            }
            
            mom_out = lax_friedrichs_staggered_2(0.5*tau, in_staggered, mom,
                    compute_flux_rho, compute_flux_u0, compute_flux_u1);
        } else {
            // First half step (to the staggered grid)
            moments mom = nessyahu_tadmor_staggered_1(0.5*tau, in, mom_in,
                    compute_flux_rho, compute_flux_u0, compute_flux_u1); 

            // Second half step (back to the original grid)
            lowrank4d in_staggered = in;
            for(Index k=0;k<dd.r;k++) {
                in_staggered.X.col(k) = avgp0_y(avgp0_x(in.X.col(k)));
            }

            mom_out = nessyahu_tadmor_staggered_2(0.5*tau, in_staggered, mom,
                    compute_flux_rho, compute_flux_u0, compute_flux_u1); 
        }


        //
        // K step for the low-rank integrator
        //
        array<MatrixXd,2> C1 = compute_C1(in.V);
        array<MatrixXd,4> Cs = compute_Cstar(in.V);
        M_t M = compute_M(mom_in, I1, I2);
        VectorXd avg_V = average_v(in.V);
        MatrixXd K1 = step_K(tau, in.X*in.S, M, C1, Cs, avg_V, mom_in.rho);

        qrx.compute(K1);
        out.X = qrx.Q()/sqrt(dd.h[0]*dd.h[1]); // X1=out.X
        MatrixXd S1 = qrx.R()*sqrt(dd.h[0]*dd.h[1]);

        //
        // S step for the low-rank integrator
        //
        array<MatrixXd,2> D1 = compute_D1(out.X);
        MatrixXd Ds = compute_Ds(out.X, M.M1);
        array<MatrixXd,2> Dss = compute_Dss(out.X, M.M2);
        array<MatrixXd,4> Dsss = compute_Dsss(out.X, M.M3);
        MatrixXd avg_X = average_x(out.X, mom_in.rho);
        MatrixXd R = compute_R(out.X, mom_in.rho);
        MatrixXd S2 = step_S(tau, S1, C1, Cs, D1, Ds, Dss, Dsss, avg_X, avg_V, R);

        //
        // L step for the low-rank integrator
        MatrixXd L1 = step_L(tau, in.V*S2.transpose(), D1, Ds, Dss, Dsss, avg_X, R);

        qrv.compute(L1);
        out.V = qrv.Q()/sqrt(dd.h[2]*dd.h[3]);
        out.S = qrv.R().transpose()*sqrt(dd.h[2]*dd.h[3]);
    }

    moments lax_friedrichs_staggered_1(double tau, const lowrank4d& in,
            const moments& mom, flux_function comp_flux_rho, flux_function comp_flux_u0,
            flux_function comp_flux_u1) {
        array<VectorXd,2> flux_rho = comp_flux_rho(in, mom, dd);
        array<VectorXd,2> flux_u0  = comp_flux_u0(in,  mom, dd);
        array<VectorXd,2> flux_u1  = comp_flux_u1(in,  mom, dd);

        moments cons_in = to_conservative_form(mom);

        moments cons_mom;
        cons_mom.rho = 0.5*(avgp0_x(cons_in.rho) + avgp0_y(cons_in.rho))
            + tau*diff_x_right_vec(flux_rho[0]) + tau*diff_y_right_vec(flux_rho[1]);
        cons_mom.u[0] = 0.5*(avgp0_x(cons_in.u[0]) + avgp0_y(cons_in.u[0]))
            + tau*diff_x_right_vec(flux_u0[0]) + tau*diff_y_right_vec(flux_u0[1]);
        cons_mom.u[1] = 0.5*(avgp0_x(cons_in.u[1]) + avgp0_y(cons_in.u[1]))
            + tau*diff_x_right_vec(flux_u1[0]) + tau*diff_y_right_vec(flux_u1[1]);

        return from_conservative_form(cons_mom);
    }


    moments lax_friedrichs_staggered_2(double tau, const lowrank4d& in_staggered,
            const moments& mom, flux_function comp_flux_rho, flux_function comp_flux_u0,
            flux_function comp_flux_u1) {
        array<VectorXd,2> flux_rho = comp_flux_rho(in_staggered, mom, dd);
        array<VectorXd,2> flux_u0  = comp_flux_u0(in_staggered,  mom, dd);
        array<VectorXd,2> flux_u1  = comp_flux_u1(in_staggered,  mom, dd);

        moments cons_in = to_conservative_form(mom);

        moments cons_mom;
        cons_mom.rho = 0.5*(avgm0_x(cons_in.rho) + avgm0_y(cons_in.rho))
            + tau*diff_x_left_vec(flux_rho[0]) + tau*diff_y_left_vec(flux_rho[1]);
        cons_mom.u[0] = 0.5*(avgm0_x(cons_in.u[0]) + avgm0_y(cons_in.u[0]))
            + tau*diff_x_left_vec(flux_u0[0]) + tau*diff_y_left_vec(flux_u0[1]);
        cons_mom.u[1] = 0.5*(avgm0_x(cons_in.u[1]) + avgm0_y(cons_in.u[1]))
            + tau*diff_x_left_vec(flux_u1[0]) + tau*diff_y_left_vec(flux_u1[1]);

        return from_conservative_form(cons_mom);
    }


    moments nessyahu_tadmor_staggered_1(double tau, const lowrank4d& in, const moments& mom,
            flux_function comp_flux_rho, flux_function comp_flux_u0,
            flux_function comp_flux_u1) {

        moments cons_mom = to_conservative_form(mom);

        array<VectorXd,2> flux_rho = comp_flux_rho(in, mom, dd);
        array<VectorXd,2> flux_u0  = comp_flux_u0(in,  mom, dd);
        array<VectorXd,2> flux_u1  = comp_flux_u1(in,  mom, dd);

        VectorXd fprime_rho_x = compute_prime_x(flux_rho[0]);
        VectorXd fprime_rho_y = compute_prime_y(flux_rho[1]);
        
        VectorXd fprime_u0_x = compute_prime_x(flux_u0[0]);
        VectorXd fprime_u0_y = compute_prime_y(flux_u0[1]);
        
        VectorXd fprime_u1_x = compute_prime_x(flux_u1[0]);
        VectorXd fprime_u1_y = compute_prime_y(flux_u1[1]);

        moments cons_moms;
        cons_moms.rho  = cons_mom.rho  + 0.5*tau/dd.h[0]*fprime_rho_x + 0.5*tau/dd.h[1]*fprime_rho_y;
        cons_moms.u[0] = cons_mom.u[0] + 0.5*tau/dd.h[0]*fprime_u0_x  + 0.5*tau/dd.h[1]*fprime_u0_y;
        cons_moms.u[1] = cons_mom.u[1] + 0.5*tau/dd.h[0]*fprime_u1_x  + 0.5*tau/dd.h[1]*fprime_u1_y;

        moments moms = from_conservative_form(cons_moms);
        flux_rho = comp_flux_rho(in, moms, dd);
        flux_u0  = comp_flux_u0(in, moms, dd);
        flux_u1  = comp_flux_u1(in, moms, dd);

        moments cons_out;
        cons_out.rho  = nt_stencil_right(tau, cons_mom.rho,  flux_rho);
        cons_out.u[0] = nt_stencil_right(tau, cons_mom.u[0], flux_u0);
        cons_out.u[1] = nt_stencil_right(tau, cons_mom.u[1], flux_u1);
        return from_conservative_form(cons_out);
    }

    VectorXd nt_stencil_right(double tau, const VectorXd& cons_u, const array<VectorXd,2>& flux) {
        VectorXd vprime_x = compute_prime_x(cons_u);
        VectorXd vprime_y = compute_prime_y(cons_u);

        return 0.5*(avgp0_y(avgp0_x(cons_u)) + avgp0_x(avgp0_y(cons_u)))
            + 1.0/8.0*(-dd.h[0]*avgp0_y(diff_x_right_vec(vprime_x)) - dd.h[1]*avgp0_x(diff_y_right_vec(vprime_y)))
            + tau*diff_x_right_vec(flux[0]) + tau*diff_y_right_vec(flux[1]);
    }

    moments nessyahu_tadmor_staggered_2(double tau, const lowrank4d& in, const moments& mom,
            flux_function comp_flux_rho, flux_function comp_flux_u0,
            flux_function comp_flux_u1) {

        moments cons_mom = to_conservative_form(mom);

        array<VectorXd,2> flux_rho = comp_flux_rho(in, mom, dd);
        array<VectorXd,2> flux_u0  = comp_flux_u0(in, mom, dd);
        array<VectorXd,2> flux_u1  = comp_flux_u1(in, mom, dd);

        VectorXd fprime_rho_x = compute_prime_x(flux_rho[0]);
        VectorXd fprime_rho_y = compute_prime_y(flux_rho[1]);
        
        VectorXd fprime_u0_x = compute_prime_x(flux_u0[0]);
        VectorXd fprime_u0_y = compute_prime_y(flux_u0[1]);
        
        VectorXd fprime_u1_x = compute_prime_x(flux_u1[0]);
        VectorXd fprime_u1_y = compute_prime_y(flux_u1[1]);

        moments cons_moms;
        cons_moms.rho  = cons_mom.rho  + 0.5*tau/dd.h[0]*fprime_rho_x + 0.5*tau/dd.h[1]*fprime_rho_y;
        cons_moms.u[0] = cons_mom.u[0] + 0.5*tau/dd.h[0]*fprime_u0_x + 0.5*tau/dd.h[1]*fprime_u0_y;
        cons_moms.u[1] = cons_mom.u[1] + 0.5*tau/dd.h[0]*fprime_u1_x + 0.5*tau/dd.h[1]*fprime_u1_y;

        moments moms = from_conservative_form(cons_moms);
        flux_rho = comp_flux_rho(in, moms, dd);
        flux_u0  = comp_flux_u0(in, moms, dd);
        flux_u1  = comp_flux_u1(in, moms, dd);

        moments cons_out;
        cons_out.rho  = nt_stencil_left(tau, cons_mom.rho,  flux_rho);
        cons_out.u[0] = nt_stencil_left(tau, cons_mom.u[0], flux_u0);
        cons_out.u[1] = nt_stencil_left(tau, cons_mom.u[1], flux_u1);
        return from_conservative_form(cons_out);
    }
    
    VectorXd nt_stencil_left(double tau, const VectorXd& cons_u, const array<VectorXd,2>& flux) {
        VectorXd vprime_x = compute_prime_x(cons_u);
        VectorXd vprime_y = compute_prime_y(cons_u);

        return 0.5*(avgm0_y(avgm0_x(cons_u)) + avgm0_x(avgm0_y(cons_u)))
            + 1.0/8.0*(-dd.h[0]*avgm0_y(diff_x_left_vec(vprime_x)) - dd.h[1]*avgm0_x(diff_y_left_vec(vprime_y)))
            + tau*diff_x_left_vec(flux[0]) + tau*diff_y_left_vec(flux[1]);
    }

    VectorXd compute_prime_x(const VectorXd& u) {
        VectorXd d_right = dd.h[0]*diff_x_right_vec(u);
        VectorXd d_left  = dd.h[0]*diff_x_left_vec(u);
        
        VectorXd out(u.size());
        for(Index m=0;m<(Index)u.size();m++)
            out[m] = limiter(d_right[m], d_left[m]);
        return out;
    }
    
    VectorXd compute_prime_y(const VectorXd& u) {
        VectorXd d_right = dd.h[1]*diff_y_right_vec(u);
        VectorXd d_left  = dd.h[1]*diff_y_left_vec(u);
        
        VectorXd out(u.size());
        for(Index m=0;m<(Index)u.size();m++)
            out[m] = limiter(d_right[m], d_left[m]);
        return out;
    }

    VectorXd avgpm_x(const VectorXd& in) {
        VectorXd out(dd.n[0]*dd.n[1]);
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            array<Index,2> ip1 = {(i[0]+1)%dd.n[0], i[1]};
            array<Index,2> im1 = {(i[0]==0) ? dd.n[0]-1 : (i[0]-1), i[1]};
            out(dd.lin_idx_x(i)) = 0.5*(in(dd.lin_idx_x(ip1))+in(dd.lin_idx_x(im1)));
        }
        return out;
    }
    
    VectorXd avgp0_x(const VectorXd& in) {
        VectorXd out(dd.n[0]*dd.n[1]);
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            array<Index,2> ip1 = {(i[0]+1)%dd.n[0], i[1]};
            out(dd.lin_idx_x(i)) = 0.5*(in(dd.lin_idx_x(ip1))+in(dd.lin_idx_x(i)));
        }
        return out;
    }
    
    VectorXd avgm0_x(const VectorXd& in) {
        VectorXd out(dd.n[0]*dd.n[1]);
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            array<Index,2> im1 = {(i[0]==0) ? dd.n[0]-1 : (i[0]-1), i[1]};
            out(dd.lin_idx_x(i)) = 0.5*(in(dd.lin_idx_x(i))+in(dd.lin_idx_x(im1)));
        }
        return out;
    }

    VectorXd avgpm_y(const VectorXd& in) {
        VectorXd out(dd.n[0]*dd.n[1]);
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            array<Index,2> ip1 = {i[0], (i[1]+1) % dd.n[1]};
            array<Index,2> im1 = {i[0], (i[1]==0) ? dd.n[1]-1 : (i[1]-1)};
            out(dd.lin_idx_x(i)) = 0.5*(in(dd.lin_idx_x(ip1))+in(dd.lin_idx_x(im1)));
        }
        return out;
    }
    
    VectorXd avgp0_y(const VectorXd& in) {
        VectorXd out(dd.n[0]*dd.n[1]);
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            array<Index,2> ip1 = {i[0], (i[1]+1) % dd.n[1]};
            out(dd.lin_idx_x(i)) = 0.5*(in(dd.lin_idx_x(ip1))+in(dd.lin_idx_x(i)));
        }
        return out;
    }

    VectorXd avgm0_y(const VectorXd& in) {
        VectorXd out(dd.n[0]*dd.n[1]);
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            array<Index,2> im1 = {i[0], (i[1]==0) ? dd.n[1]-1 : (i[1]-1)};
            out(dd.lin_idx_x(i)) = 0.5*(in(dd.lin_idx_x(i))+in(dd.lin_idx_x(im1)));
        }
        return out;
    }


    VectorXd LaxFriedrichs(double tau, const VectorXd& in, VectorXd I) {
        // 2D Lax-Friedrichs scheme
        // (see e.g. https://itp.uni-frankfurt.de/~rezzolla/lecture_notes/2010/fd_evolution_pdes_lnotes.pdf)
        return 0.5*avgpm_x(in) + 0.5*avgpm_y(in) + tau*I;
    }

    MatrixXd mult_componentwise(const MatrixXd& K, const VectorXd& factor) {
        MatrixXd out = K;
        for(Index i=0;i<(Index)K.cols();i++) {
            auto bl  = out.col(i);
            bl = bl.array()*factor.array();
        }

        return out;
    }


    MatrixXd step_K(double tau, const MatrixXd& K, const M_t& M, const array<MatrixXd,2>& C1,
            const array<MatrixXd,4>& Cs, const VectorXd& avg_V, const VectorXd& rho) {

        MatrixXd rhs;
        if(dm == DM_FFT) {
            MatrixXd diff_x_K = diff_x_fft(K);
            MatrixXd diff_y_K = diff_y_fft(K);
            rhs = diff_x_K*C1[0].transpose() + diff_y_K*C1[1].transpose();
        } else {
            // x-direction
            {
                Eigen::EigenSolver<MatrixXd> es;

                es.compute(C1[0]);
                VectorXd lambda = es.eigenvalues().real();
                MatrixXd T = es.eigenvectors().real();

                MatrixXd in = K*T.inverse().transpose();
                MatrixXd Kx;
                if(dm == DM_LWVANLEER_NT)
                    Kx = diff_x_lwvanleer(in, lambda, tau);
                else
                    Kx = diff_x_upwind(in, lambda);

                rhs = Kx*(T*lambda.asDiagonal()).transpose();
            }

            // y-direction
            {
                Eigen::EigenSolver<MatrixXd> es;

                es.compute(C1[1]);
                VectorXd lambda = es.eigenvalues().real();
                MatrixXd T = es.eigenvectors().real();

                MatrixXd in = K*T.inverse().transpose();
                MatrixXd Ky;
                if(dm == DM_LWVANLEER_NT)
                    Ky = diff_y_lwvanleer(in, lambda, tau);
                else
                    Ky = diff_y_upwind(in, lambda);

                rhs += Ky*(T*lambda.asDiagonal()).transpose();
            }
        }

        rhs += mult_componentwise(K, M.M1)                         // M1
             + mult_componentwise(K, M.M2[0])*C1[0].transpose()    // M2
             + mult_componentwise(K, M.M2[1])*C1[1].transpose()
             + mult_componentwise(K, M.M3[0])*Cs[0].transpose()    // M3
             + mult_componentwise(K, M.M3[1])*Cs[1].transpose()
             + mult_componentwise(K, M.M3[2])*Cs[2].transpose()
             + mult_componentwise(K, M.M3[3])*Cs[3].transpose();

        VectorXd f1 = 1.0/((tau*C_collision.array()/epsilon)*rho.array()+1.0);
        VectorXd f2 = (tau*C_collision.array())*rho.array()/((tau*C_collision.array()*rho.array())+epsilon);
        return mult_componentwise(K, f1) - tau*mult_componentwise(rhs, f1)
               + mult_componentwise(MatrixXd::Ones(dd.n[0]*dd.n[1], 1)*avg_V.transpose(), f2);
    }

    MatrixXd step_S(double tau, const MatrixXd& S, const array<MatrixXd,2>& C1,
            const array<MatrixXd,4>& Cs, const array<MatrixXd,2>& D1, const MatrixXd& Ds,
            const array<MatrixXd,2>& Dss, const array<MatrixXd,4>& Dsss, const VectorXd& avg_X,
            const VectorXd& avg_V, const MatrixXd& R) {

        MatrixXd rhs = Ds*S
                       + Dss[0]*S*C1[0].transpose()
                       + Dss[1]*S*C1[1].transpose()
                       + Dsss[0]*S*Cs[0].transpose()
                       + Dsss[1]*S*Cs[1].transpose()
                       + Dsss[2]*S*Cs[2].transpose()
                       + Dsss[3]*S*Cs[3].transpose();

        MatrixXd RR    = MatrixXd::Identity(R.rows(), R.cols()) - tau/epsilon*R;
        MatrixXd R_rhs = S + tau*(D1[0]*S*C1[0].transpose() + D1[1]*S*C1[1].transpose() + rhs)
                           - tau*avg_X*avg_V.transpose()/epsilon;
        return RR.fullPivLu().solve(R_rhs);
    }

    MatrixXd step_L(double tau, const MatrixXd& L, const array<MatrixXd,2>& D1, const MatrixXd& Ds,
            const array<MatrixXd,2>& Dss, const array<MatrixXd,4>& Dsss, const VectorXd& avg_X,
            const MatrixXd& R) {

        MatrixXd L1a = L*D1[0].transpose();
        MatrixXd L1b = L*D1[1].transpose();

        MatrixXd L2 = L*Ds.transpose();

        MatrixXd L3a = L*Dss[0].transpose();
        MatrixXd L3b = L*Dss[1].transpose();

        MatrixXd L4a = L*Dsss[0].transpose();
        MatrixXd L4b = L*Dsss[1].transpose();
        MatrixXd L4c = L*Dsss[2].transpose();
        MatrixXd L4d = L*Dsss[3].transpose();

        for(Index k=0;k<dd.r;k++) {
            for(auto i : range<2>(dd.n[2],dd.n[3])) {
                L1a(dd.lin_idx_v(i),k) *= dd.v(i);
                L1b(dd.lin_idx_v(i),k) *= dd.w(i);

                L3a(dd.lin_idx_v(i),k) *= dd.v(i);
                L3b(dd.lin_idx_v(i),k) *= dd.w(i);

                L4a(dd.lin_idx_v(i),k) *= pow(dd.v(i),2);
                L4b(dd.lin_idx_v(i),k) *= dd.v(i)*dd.w(i);
                L4c(dd.lin_idx_v(i),k) *= dd.v(i)*dd.w(i);
                L4d(dd.lin_idx_v(i),k) *= pow(dd.w(i),2);
            }
        }
        
        MatrixXd RR    = MatrixXd::Identity(R.rows(), R.cols()) + tau/epsilon*R;
        MatrixXd R_rhs = L - tau*(L1a + L1b + L2 + L3a + L3b + L4a + L4b + L4c + L4d)
                           + tau*MatrixXd::Ones(dd.n[2]*dd.n[3], 1)*avg_X.transpose()/epsilon;
        return R_rhs*(MatrixXd::Identity(R.rows(), R.cols()) + tau/epsilon*R).transpose().inverse();
    }


    M_t compute_M(const moments& mom, const VectorXd& _I1, const array<VectorXd, 2>& _I2) {
        auto rho = mom.rho.array();
        auto u0  = mom.u[0].array();
        auto u1  = mom.u[1].array();
        auto I1  = _I1.array();
        auto I20 = _I2[0].array();
        auto I21 = _I2[1].array();

        VectorXd M1 = ((I1 - u0*(I20-I1*u0) - u1*(I21-I1*u1))/rho).matrix();
        array<VectorXd, 2> M2 = {
            ((diff_x_vec(mom.rho).array() + I20 - I1*u0)/rho).matrix()
                - 0.5*diff_x_vec((u0*u0 + u1*u1).matrix()),
            ((diff_y_vec(mom.rho).array() + I21 - I1*u1)/rho).matrix()
                - 0.5*diff_y_vec((u0*u0 + u1*u1).matrix()),
        };
        array<VectorXd, 4> M3 = {
            diff_x_vec(mom.u[0]), diff_x_vec(mom.u[1]),
            diff_y_vec(mom.u[0]), diff_y_vec(mom.u[1])
        };

        return M_t(M1, M2, M3);
    }

    array<VectorXd,4> compute_sigma(const moments& mom) {
        array<VectorXd, 4> nabla_u = {
            diff_x_vec(mom.u[0]), diff_x_vec(mom.u[1]),
            diff_y_vec(mom.u[0]), diff_y_vec(mom.u[1])
        };

        VectorXd div_u = diff_x_vec(mom.u[0]) + diff_y_vec(mom.u[1]);

        array<VectorXd,4> out;
        for(auto dim : range<2>(2,2)) {
            out[dim[0] + 2*dim[1]] = nabla_u[dim[0] + 2*dim[1]] + nabla_u[dim[1] + 2*dim[0]]
                                     - div_u*(dim[0]==dim[1]);
        }

        return out;
    }


    array<MatrixXd,2> compute_C1(const MatrixXd& V) {
        array<MatrixXd,2> C1;
        C1[0].resize(dd.r, dd.r);
        C1[1].resize(dd.r, dd.r);
        C1[0].setZero();
        C1[1].setZero();
        for(auto k : range<2>(dd.r,dd.r))
            for(auto i : range<2>(dd.n[2],dd.n[3])) {
                C1[0](k[0],k[1]) += dd.h[2]*dd.h[3]*dd.v(i)*V(dd.lin_idx_v(i),k[0])*V(dd.lin_idx_v(i),k[1]);
                C1[1](k[0],k[1]) += dd.h[2]*dd.h[3]*dd.w(i)*V(dd.lin_idx_v(i),k[0])*V(dd.lin_idx_v(i),k[1]);
            }

        return C1;
    }

    array<MatrixXd,4> compute_Cstar(const MatrixXd& V) {
        array<MatrixXd,4> Cs;
        for(Index i=0;i<Cs.size();i++) {
            Cs[i].resize(dd.r, dd.r);
            Cs[i].setZero();
        }

        for(auto k : range<2>(dd.r,dd.r)) {
            for(auto i : range<2>(dd.n[2],dd.n[3])) {
                for(auto m : range<2>(2,2)) {
                    double f1 = (m[0]==0 ? dd.v(i) : dd.w(i));
                    double f2 = (m[1]==0 ? dd.v(i) : dd.w(i));
                    Cs[m[0] + 2*m[1]](k[0],k[1])
                        += dd.h[2]*dd.h[3]*f1*f2*V(dd.lin_idx_v(i),k[0])*V(dd.lin_idx_v(i),k[1]);
                }
            }
        }

        return Cs;
    }

    VectorXd average_v(const MatrixXd& V) {
        VectorXd out(dd.r);
        for(Index k=0;k<dd.r;k++) {
            double avg = 0.0;
            for(auto i : range<2>(dd.n[2],dd.n[3])) {
                avg += dd.h[2]*dd.h[3]*V(dd.lin_idx_v(i), k);
            }
            out(k) = avg;
        }
        return out;
    }

    array<MatrixXd,2> compute_D1(const MatrixXd& X) {
        array<MatrixXd,2> D1;
        D1[0].resize(dd.r,dd.r);
        D1[1].resize(dd.r,dd.r);
        D1[0].setZero();
        D1[1].setZero();

        MatrixXd dX = diff_x(X);
        MatrixXd dY = diff_y(X);

        for(auto k : range<2>(dd.r,dd.r))
            for(Index m=0;m<dd.n[0]*dd.n[1];m++) {
                D1[0](k[0],k[1]) += dd.h[0]*dd.h[1]*X(m,k[0])*dX(m,k[1]);
                D1[1](k[0],k[1]) += dd.h[0]*dd.h[1]*X(m,k[0])*dY(m,k[1]);
            }

        return D1;
    }

    MatrixXd compute_R(const MatrixXd& X, const VectorXd& rho) {
        MatrixXd R(dd.r,dd.r);
        R.setZero();

        for(auto k : range<2>(dd.r,dd.r))
            for(Index m=0;m<dd.n[0]*dd.n[1];m++) {
                R(k[0],k[1]) += dd.h[0]*dd.h[1]*C_collision(m)*rho(m)*X(m,k[0])*X(m,k[1]);
            }

        return R;
    }


    MatrixXd compute_Ds(const MatrixXd& X, const VectorXd& M1) {
        MatrixXd Ds(dd.r,dd.r);
        Ds.setZero();

        for(auto k : range<2>(dd.r,dd.r))
            for(auto i : range<2>(dd.n[0],dd.n[1])) {
                Ds(k[0],k[1]) += dd.h[0]*dd.h[1]
                    *X(dd.lin_idx_x(i),k[0])*X(dd.lin_idx_x(i),k[1])*M1(dd.lin_idx_x(i));
            }

        return Ds;
    }


    array<MatrixXd,2> compute_Dss(const MatrixXd& X, const array<VectorXd, 2>& M2) {
        array<MatrixXd, 2> Dss = {
            MatrixXd::Zero(dd.r,dd.r),
            MatrixXd::Zero(dd.r,dd.r)
        };

        for(auto k : range<2>(dd.r,dd.r))
            for(auto i : range<2>(dd.n[0],dd.n[1])) {
                Dss[0](k[0],k[1]) += dd.h[0]*dd.h[1]
                    *X(dd.lin_idx_x(i),k[0])*X(dd.lin_idx_x(i),k[1])*M2[0](dd.lin_idx_x(i));

                Dss[1](k[0],k[1]) += dd.h[0]*dd.h[1]
                    *X(dd.lin_idx_x(i),k[0])*X(dd.lin_idx_x(i),k[1])*M2[1](dd.lin_idx_x(i));
            }

        return Dss;
    }


    array<MatrixXd,4> compute_Dsss(const MatrixXd& X, const array<VectorXd, 4>& M3) {
        array<MatrixXd, 4> Dsss = {
            MatrixXd::Zero(dd.r,dd.r),
            MatrixXd::Zero(dd.r,dd.r),
            MatrixXd::Zero(dd.r,dd.r),
            MatrixXd::Zero(dd.r,dd.r)
        };

        for(auto k : range<2>(dd.r,dd.r))
            for(auto i : range<2>(dd.n[0],dd.n[1])) {
                for(Index m=0;m<4;m++)
                    Dsss[m](k[0],k[1]) += dd.h[0]*dd.h[1]
                        *X(dd.lin_idx_x(i),k[0])*X(dd.lin_idx_x(i),k[1])*M3[m](dd.lin_idx_x(i));

            }

        return Dsss;
    }

    VectorXd average_x(const MatrixXd& X, const VectorXd& rho) {
        VectorXd out(dd.r);
        for(Index k=0;k<dd.r;k++) {
            double avg = 0.0;
            for(auto i : range<2>(dd.n[0],dd.n[1])) {
                avg += dd.h[0]*dd.h[1]*C_collision(dd.lin_idx_x(i))*rho(dd.lin_idx_x(i))*X(dd.lin_idx_x(i), k);
            }
            out(k) = avg;
        }
        return out;
    }

    void rho_u(const MatrixXd& X, const MatrixXd& S, const MatrixXd& V,
               MatrixXd& rho, array<MatrixXd,2>& u) {
        domain_lowrank4d_fluid dlf(dd, lowrank_type::normalized_integral);
        dlf.lr.X = X;
        dlf.lr.S = S;
        dlf.lr.V = V;
        t_rhou.start();
        dlf.rhou(rho, u);
        t_rhou.stop();
        u[0] = (u[0].array()/rho.array()).matrix();
        u[1] = (u[1].array()/rho.array()).matrix();
    }

    MatrixXd diff_x(const MatrixXd& K) {
        if(dm == DM_FFT)
            return diff_x_fft(K);
        else
            return diff_x_upwind(K, VectorXd::Ones(dd.r));
    }

    MatrixXd diff_y(const MatrixXd& K) {
        if(dm == DM_FFT)
            return diff_y_fft(K);
        else
            return diff_y_upwind(K, VectorXd::Ones(dd.r));
    }

    MatrixXd diff_x_vec(const MatrixXd& K) {
        if(dm == DM_FFT)
            return diff_x_fft_vec(K);
        else
            return diff_x_left_vec(K);
    }

    MatrixXd diff_y_vec(const MatrixXd& K) {
        if(dm == DM_FFT)
            return diff_y_fft_vec(K);
        else
            return diff_y_left_vec(K);
    }

    MatrixXd diff_x_fft(const MatrixXd& K) {
        MatrixXd out(K.rows(), K.cols());
        vector<double> tmp(dd.n[0]);
        vector< complex<double> > hat(dd.n[0]);

        fft_batch fftb(dd.n[0], &tmp[0], &hat[0]);

        // We assume that K and out are in Fortran-major order
        for(Index j=0;j<dd.r;j++) {
            for(Index m=0;m<dd.n[1];m++) {
                for(Index l=0;l<dd.n[0];l++)
                    tmp[l] = K(l+m*dd.n[0],j);
                fftb.forward();
                for(Index k=0;k<dd.n[0];k++) {
                    double k_freq = double(k)*2.0*M_PI/(dd.b[0]-dd.a[0]);
                    hat[k] *= complex<double>(0,1)*k_freq/double(dd.n[0]);
                }
                fftb.backward();
                for(Index l=0;l<dd.n[0];l++)
                    out(l+m*dd.n[0],j) = tmp[l];
            }
        }
        return out;
    }

    MatrixXd diff_y_fft(const MatrixXd& K) {
        MatrixXd out(K.rows(), K.cols());
        vector<double> tmp(dd.n[1]);
        vector< complex<double> > hat(dd.n[1]);

        fft_batch fftb(dd.n[1], &tmp[0], &hat[0]);

        // We assume that K and out are in Fortran-major order
        for(Index j=0;j<dd.r;j++) {
            for(Index m=0;m<dd.n[0];m++) {
                for(Index l=0;l<dd.n[1];l++)
                    tmp[l] = K(m+l*dd.n[0],j);
                fftb.forward();
                for(Index k=0;k<dd.n[1];k++) {
                    double k_freq = double(k)*2.0*M_PI/(dd.b[1]-dd.a[1]);
                    hat[k] *= complex<double>(0,1)*k_freq/double(dd.n[1]);
                }
                fftb.backward();
                for(Index l=0;l<dd.n[1];l++)
                    out(m+l*dd.n[0],j) = tmp[l];
            }
        }
        return out;
    }

    VectorXd diff_x_fft_vec(const VectorXd& v) {
        VectorXd out(v.size());
        vector<double> tmp(dd.n[0]);
        vector< complex<double> > hat(dd.n[0]);

        fft_batch fftb(dd.n[0], &tmp[0], &hat[0]);

        // We assume that K and out are in Fortran-major order
        for(Index m=0;m<dd.n[1];m++) {
            for(Index l=0;l<dd.n[0];l++)
                tmp[l] = v(l+m*dd.n[0]);
            fftb.forward();
            for(Index k=0;k<dd.n[0];k++) {
                double k_freq = double(k)*2.0*M_PI/(dd.b[0]-dd.a[0]);
                hat[k] *= complex<double>(0,1)*k_freq/double(dd.n[0]);
            }
            fftb.backward();
            for(Index l=0;l<dd.n[0];l++)
                out(l+m*dd.n[0]) = tmp[l];
        }
        return out;
    }


    VectorXd diff_y_fft_vec(const VectorXd& v) {
        VectorXd out(v.size());
        vector<double> tmp(dd.n[1]);
        vector< complex<double> > hat(dd.n[1]);

        fft_batch fftb(dd.n[1], &tmp[0], &hat[0]);

        // We assume that K and out are in Fortran-major order
        for(Index m=0;m<dd.n[0];m++) {
            for(Index l=0;l<dd.n[1];l++)
                tmp[l] = v(m+l*dd.n[0]);
            fftb.forward();
            for(Index k=0;k<dd.n[1];k++) {
                double k_freq = double(k)*2.0*M_PI/(dd.b[1]-dd.a[1]);
                hat[k] *= complex<double>(0,1)*k_freq/double(dd.n[1]);
            }
            fftb.backward();
            for(Index l=0;l<dd.n[1];l++)
                out(m+l*dd.n[0]) = tmp[l];
        }
        return out;
    }


    MatrixXd diff_x_upwind(const MatrixXd& K, const VectorXd& v) {
        MatrixXd out(dd.n[0]*dd.n[1],dd.r);
        for(Index j=0;j<dd.r;j++) {
            for(auto i : range<2>(dd.n[0],dd.n[1])) {
                array<Index,2> ip1 = {(i[0]+1)%dd.n[0], i[1]};
                array<Index,2> im1 = {(i[0]==0) ? dd.n[0]-1 : (i[0]-1), i[1]};
                if(v(j) >= 0.0)
                    out(dd.lin_idx_x(i),j) = (K(dd.lin_idx_x(i),j)-K(dd.lin_idx_x(im1),j))/(dd.h[0]);
                else
                    out(dd.lin_idx_x(i),j) = (K(dd.lin_idx_x(ip1),j)-K(dd.lin_idx_x(i),j))/(dd.h[0]);
            }
        }
        return out;
    }

    MatrixXd diff_y_upwind(const MatrixXd& K, const VectorXd& v) {
        MatrixXd out(dd.n[0]*dd.n[1],dd.r);
        for(Index j=0;j<dd.r;j++) {
            for(auto i : range<2>(dd.n[0],dd.n[1])) {
                array<Index,2> ip1 = {i[0], (i[1]+1) % dd.n[1]};
                array<Index,2> im1 = {i[0], (i[1]==0) ? dd.n[1]-1 : (i[1]-1)};
                if(v(j) >= 0.0)
                    out(dd.lin_idx_x(i),j) = (K(dd.lin_idx_x(i),j)-K(dd.lin_idx_x(im1),j))/(dd.h[1]);
                else
                    out(dd.lin_idx_x(i),j) = (K(dd.lin_idx_x(ip1),j)-K(dd.lin_idx_x(i),j))/(dd.h[1]);
            }
        }
        return out;
    }


    MatrixXd diff_x_lwvanleer(const MatrixXd& K, const VectorXd& v, double tau) {
        MatrixXd out(dd.n[0]*dd.n[1],dd.r);
        for(Index j=0;j<dd.r;j++) {
            for(auto i : range<2>(dd.n[0],dd.n[1])) {
                array<Index,2> ip1 = {(i[0]+1)%dd.n[0], i[1]};
                array<Index,2> im1 = {(i[0]==0) ? dd.n[0]-1 : (i[0]-1), i[1]};
                double u   = K(dd.lin_idx_x(i),j);
                double um1 = K(dd.lin_idx_x(im1),j);
                double up1 = K(dd.lin_idx_x(ip1),j);
                if(v(j) >= 0.0) {
                    double theta = (u - um1)/(up1 - u + 1e-6);
                    double phi   = (lr_limiter) ? (fabs(theta) + theta)/(1.0 + abs(theta)): 1.0;

                    double F   = u   + 0.5*(1-v(j)*tau/dd.h[0])*(up1 - u)*phi;
                    double Fm1 = um1 + 0.5*(1-v(j)*tau/dd.h[0])*(u - um1)*phi;
                    out(dd.lin_idx_x(i),j) = (F - Fm1)/dd.h[0];
                } else {
                    double theta = (up1 - u)/(u - um1 + 1e-6);
                    double phi   = (lr_limiter) ? (fabs(theta) + theta)/(1.0 + abs(theta)) : 1.0;

                    double F   = u   - 0.5*(1+v(j)*tau/dd.h[0])*(u - um1)*phi;
                    double Fp1 = up1 - 0.5*(1+v(j)*tau/dd.h[0])*(up1 - u)*phi;
                    out(dd.lin_idx_x(i),j) = (Fp1 - F)/dd.h[0];
                }
            }
        }
        return out;
    }

    MatrixXd diff_y_lwvanleer(const MatrixXd& K, const VectorXd& v, double tau) {
        MatrixXd out(dd.n[0]*dd.n[1],dd.r);
        for(Index j=0;j<dd.r;j++) {
            for(auto i : range<2>(dd.n[0],dd.n[1])) {
                array<Index,2> ip1 = {i[0], (i[1]+1)%dd.n[1]};
                array<Index,2> im1 = {i[0], (i[1]==0) ? dd.n[1]-1 : (i[1]-1)};
                double u   = K(dd.lin_idx_x(i),j);
                double um1 = K(dd.lin_idx_x(im1),j);
                double up1 = K(dd.lin_idx_x(ip1),j);
                if(v(j) >= 0.0) {
                    double theta = (u - um1)/(up1 - u + 1e-6);
                    double phi   = (lr_limiter) ? (fabs(theta) + theta)/(1.0 + abs(theta)): 1.0;

                    double F   = u   + 0.5*(1-v(j)*tau/dd.h[1])*(up1 - u)*phi;
                    double Fm1 = um1 + 0.5*(1-v(j)*tau/dd.h[1])*(u - um1)*phi;
                    out(dd.lin_idx_x(i),j) = (F - Fm1)/dd.h[1];
                } else {
                    double theta = (up1 - u)/(u - um1 + 1e-6);
                    double phi   = (lr_limiter) ? (fabs(theta) + theta)/(1.0 + abs(theta)) : 1.0;

                    double F   = u   - 0.5*(1+v(j)*tau/dd.h[1])*(u - um1)*phi;
                    double Fp1 = up1 - 0.5*(1+v(j)*tau/dd.h[1])*(up1 - u)*phi;
                    out(dd.lin_idx_x(i),j) = (Fp1 - F)/dd.h[1];
                }
            }
        }
        return out;
    }

    VectorXd diff_x_left_vec(const VectorXd& K) {
        VectorXd out(dd.n[0]*dd.n[1]);
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            array<Index,2> im1 = {(i[0]==0) ? dd.n[0]-1 : (i[0]-1), i[1]};
            out(dd.lin_idx_x(i)) = (K(dd.lin_idx_x(i))-K(dd.lin_idx_x(im1)))/dd.h[0];
        }
        return out;
    }

    VectorXd diff_y_left_vec(const VectorXd& K) {
        VectorXd out(dd.n[0]*dd.n[1]);
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            array<Index,2> im1 = {i[0], (i[1]==0) ? dd.n[1]-1 : (i[1]-1)};
            out(dd.lin_idx_x(i)) = (K(dd.lin_idx_x(i))-K(dd.lin_idx_x(im1)))/dd.h[1];
        }
        return out;
    }

    VectorXd diff_x_right_vec(const VectorXd& K) {
        VectorXd out(dd.n[0]*dd.n[1]);
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            array<Index,2> ip1 = {(i[0]+1) % dd.n[0], i[1]};
            out(dd.lin_idx_x(i)) = (K(dd.lin_idx_x(ip1))-K(dd.lin_idx_x(i)))/dd.h[0];
        }
        return out;
    }

    VectorXd diff_y_right_vec(const VectorXd& K) {
        VectorXd out(dd.n[0]*dd.n[1]);
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            array<Index,2> ip1 = {i[0], (i[1]+1) % dd.n[1]};
            out(dd.lin_idx_x(i)) = (K(dd.lin_idx_x(ip1))-K(dd.lin_idx_x(i)))/dd.h[1];
        }
        return out;
    }

    MatrixXd diff_x_cd_vec(const VectorXd& K) {
        VectorXd out(dd.n[0]*dd.n[1]);
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            array<Index,2> ip1 = {(i[0]+1)%dd.n[0], i[1]};
            array<Index,2> im1 = {(i[0]==0) ? dd.n[0]-1 : (i[0]-1), i[1]};
            out(dd.lin_idx_x(i)) = (K(dd.lin_idx_x(ip1))-K(dd.lin_idx_x(im1)))/(2.0*dd.h[0]);
        }
        return out;
    }

    MatrixXd diff_y_cd_vec(const MatrixXd& K) {
        VectorXd out(dd.n[0]*dd.n[1]);
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            array<Index,2> ip1 = {i[0], (i[1]+1) % dd.n[1]};
            array<Index,2> im1 = {i[0], (i[1]==0) ? dd.n[1]-1 : (i[1]-1)};
            out(dd.lin_idx_x(i)) = (K(dd.lin_idx_x(ip1))-K(dd.lin_idx_x(im1)))/(2.0*dd.h[1]);
        }
        return out;
    }

};


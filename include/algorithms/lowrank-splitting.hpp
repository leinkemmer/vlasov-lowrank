#include <container/domain-lowrank-1x1v.hpp>

#include <Eigen/QR>


struct lowrank_update_base {
    // The function DeltafV needs to compute \Delta f V^n.
    // The first argument is the input (X^n,S^n,V^n) the second
    // argument is the output.
    virtual void DeltafV(lowrank2d& in, MatrixXd& DfV) = 0;

    // The function UDeltaf needs to compute U \Delta f.
    // The first argument is the input (X^n,S^n,V^n), the second
    // is U1, and the third is the output.
    virtual void UDeltaf(lowrank2d& in, MatrixXd& U1, MatrixXd& U1TDf) = 0;

    virtual ~lowrank_update_base() {}
};


struct lowrank_splitting {

    rectangular_QR qrx, qrv;
    MatrixXd K1, DfV, L1, UDf, S0tild, S1hat;

    lowrank_splitting(int nx, int nv, int r) : qrx(nx,r), qrv(nv,r),
    K1(nx,r), DfV(nx,r), L1(nv,r), UDf(r,nv), S0tild(r,r), S1hat(r,r) { }

    void operator()(lowrank2d& in, lowrank2d& out, lowrank_update_base* update) {
        // step 1
        update->DeltafV(in, DfV);
        K1 = in.X*in.S + DfV;

        // step 2
        qrx.compute(K1);
        out.X = qrx.Q();
        S1hat = qrx.R();

        // step 3
        S0tild = S1hat - out.X.transpose()*DfV;

        // step 4
        update->UDeltaf(in, out.X, UDf);
        L1 = in.V*S0tild.transpose() + UDf.transpose();

        // step 5
        Eigen::HouseholderQR<MatrixXd> qr2(L1);
        qrv.compute(L1);
        out.V = qrv.Q();
        out.S = qrv.R().transpose();
    }

};


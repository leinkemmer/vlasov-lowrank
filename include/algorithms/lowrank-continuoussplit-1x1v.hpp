#pragma once

#include <generic/common.hpp>
#include <generic/integrators.hpp>
#include <generic/qr.hpp>
#include <generic/fft.hpp>
#include <container/domain-lowrank-1x1v.hpp>
#include <fstream>
#include <Eigen/Eigenvalues> 

enum class cons_mode {
    off, rank1, lc_local, lc_global, lc_both, f_local
};

bool is_lc(cons_mode cm) {
    return (cm==cons_mode::lc_local || cm==cons_mode::lc_global || cm==cons_mode::lc_both);
}

bool is_f(cons_mode cm) {
    return cm==cons_mode::f_local;
}

struct vlasov_continuoussplit {
    int r;
    array<double,2> a;
    array<double,2> b;
    array<double,2> h;
    array<int,2>    n;

    complex<double>* hat;

    rectangular_QR qrx, qrv;

    cons_mode cons_mass, cons_mom;
    double lc_both_weight;

    // This function is usually set to v. For Debug purposes it can make sense
    // to set it to zero, for example.
    function<double(int)> v_coeff;

    vlasov_continuoussplit(int _r, array<int,2> _n, array<double,2> _a,
            array<double,2> _b)
    : r(_r), a(_a), b(_b), n(_n), qrx(_n[0],r), qrv(_n[1], r) {
        hat = new complex<double>[max(n[0],n[1])];
        h[0] = (b[0]-a[0])/double(n[0]);
        h[1] = (b[1]-a[1])/double(n[1]);
        cons_mass = cons_mode::off;
        cons_mom  = cons_mode::off;
        lc_both_weight = 1.0;

        v_coeff = [this](int i) { return v(i); };
        //v_coeff = [this](int i) { return exp(-4.0*v(i)*v(i)); };
        //v_coeff = [this](int i) { return 1.0; };
        //v_coeff = [this](int i) { return (i<64) ? -1.0 : 1.0; };
        //v_coeff = [this](int i) { return 0.0; };
    }

    ~vlasov_continuoussplit() {
        delete[] hat;
    }

    void reset_rank(int _r) {
        r = _r;
        qrx = rectangular_QR(n[0],r);
        qrv = rectangular_QR(n[1],r);
    }

    void init_x(function<double(double,int)> f, MatrixXd& out) {
        int n = out.rows();
        int r = out.cols();
        for(int k=0;k<r;k++)
            for(int i=0;i<n;i++)
                out(i,k) = f(x(i), k);
    }

    void init_v(function<double(double,int)> f, MatrixXd& out) {
        int n = out.rows();
        int r = out.cols();
        for(int k=0;k<r;k++)
            for(int i=0;i<n;i++)
                out(i,k) = f(v(i), k);
    }

    double x(int i) {
        return a[0] + i*h[0];
    }
    double v(int j) {
        return a[1] + j*h[1];
    }

    void step_K(double tau, const MatrixXd& K, const MatrixXd& V, MatrixXd& K_out, const MatrixXd& X,
            MatrixXd* bdryx=nullptr, MatrixXd* bdryv=nullptr, VectorXd* E=nullptr) {
        domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
        dlr.lr.X = K;
        dlr.lr.S = MatrixXd::Identity(r,r);
        dlr.lr.V = V;
        double mass = dlr.mass();
        double p    = dlr.momentum();

        MatrixXd C1 = compute_C1(V);
        MatrixXd C2 = compute_C2(V, bdryv);
        integrate([this,V,X,C1,C2,E,bdryx](const MatrixXd& K, MatrixXd& out) {
                    rhs_K(K, V, X, out, C1, C2, bdryx, E);
                  }, K, K_out, tau);

        dlr.lr.X = K_out;
        cout << "error (K): mass: " << dlr.mass()-mass << " momentum: " << dlr.momentum()-p << endl;
    }

    void step_K_diagonalized(double tau, const MatrixXd& K, const MatrixXd& V, MatrixXd& K_out, const MatrixXd& X,
            MatrixXd* bdryx=nullptr, MatrixXd* bdryv=nullptr, VectorXd* E=nullptr) {

        domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
        dlr.lr.X = K;
        dlr.lr.S = MatrixXd::Identity(r,r);
        dlr.lr.V = V;
        double mass = dlr.mass();
        double p    = dlr.momentum();

        MatrixXd C1 = compute_C1(V);
        MatrixXd C2 = compute_C2(V, bdryv);

        Eigen::SelfAdjointEigenSolver<MatrixXd> saes(C1);
        MatrixXd lambda = saes.eigenvalues();
        MatrixXd T = saes.eigenvectors();

        MatrixXd K_in_bar = K*T.inverse().transpose();
        MatrixXd K_out_bar(K_in_bar);

        // We also need to transform the boundary
        assert(bdryx != nullptr);
        MatrixXd bdry_bar = (*bdryx)*T.inverse().transpose();

        // and C2
        MatrixXd C2_bar = T.transpose()*C2*T.inverse().transpose();

        integrate([this,V,X,C2_bar,lambda,E,&bdry_bar](const MatrixXd& K, MatrixXd& out) {
                    rhs_K_diag(K, out, C2_bar, lambda, &bdry_bar, E);
                  }, K_in_bar, K_out_bar, tau);

        K_out = K_out_bar*T.transpose();

        dlr.lr.X = K_out;
        cout << "error (K): mass: " << dlr.mass()-mass << " momentum: " << dlr.momentum()-p << endl;

    }
    
    void rhs_K_diag(const MatrixXd& K, MatrixXd& out, const MatrixXd& C2, const VectorXd& lambda,
            MatrixXd* bdry=nullptr, VectorXd* E_0=nullptr) {

        // This computes the second term
        out = -K*C2.transpose();
        // for which we need the electric field
        VectorXd E(K.rows());
        assert(E_0 != nullptr);
        E = *E_0;
        // which enters in a pointwise multiplication
        for(int j=0;j<K.cols();j++) {
            auto bl = out.col(j);
            bl = bl.array()*E.array();
        }

        // Add the first term
        MatrixXd Ktmp;
        assert(bdry != nullptr);
        Ktmp = diff_upwind(K, lambda, h[0], bdry);

        out -= Ktmp*lambda.asDiagonal();
    }


    void step_S(double tau, const MatrixXd& X, const MatrixXd& S, const MatrixXd& V, MatrixXd& S_out, MatrixXd* bdryx=nullptr, MatrixXd* bdryv=nullptr, VectorXd* E=nullptr) {
        domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
        dlr.lr.X = X;
        dlr.lr.S = S;
        dlr.lr.V = V;
        double mass = dlr.mass();
        double p    = dlr.momentum();

        MatrixXd C1 = compute_C1(V);
        MatrixXd C2 = compute_C2(V, bdryv);

        MatrixXd D2 = compute_D2(X, bdryx); // D1(E) is computed in rhs_S

        integrate([this,X,V,C1,C2,D2,E](const MatrixXd& S, MatrixXd& _out) {
                rhs_S(S,V,X,_out,C1,C2,D2,E);
                }, S, S_out, tau);

        dlr.lr.S = S_out;
        cout << "error (S): mass: " << dlr.mass()-mass << " momentum: " << dlr.momentum()-p << endl;
    }

    void step_L(double tau, const MatrixXd& X, const MatrixXd& S, const MatrixXd& V,
                const MatrixXd& L_in, MatrixXd& L_out, MatrixXd* bdryx=nullptr, MatrixXd* bdryv=nullptr, VectorXd* E_0=nullptr) {
        domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
        dlr.lr.X = X;
        dlr.lr.S = MatrixXd::Identity(r,r);
        dlr.lr.V = L_in;
        double mass = dlr.mass();
        double p    = dlr.momentum();


        MatrixXd D2 = compute_D2(X, bdryx);

        // Compute D1 which depends on the electric field
        VectorXd E(X.rows());
        if(E_0 == nullptr) {
            MatrixXd K = X*S;
            VectorXd rho = rho_K(K, V);
            electric_field(rho, E);
        } else
            E = *E_0;
        MatrixXd D1 = compute_D1(X, E);

        // make the step
        integrate([this,X,V,E,D1,D2,bdryv](const MatrixXd& L, MatrixXd& out) {
            rhs_L_linear(L, X, V, E, out, D1, D2, bdryv);
            }, L_in, L_out, tau);

        dlr.lr.V = L_out;
        cout << "error (L): mass: " << dlr.mass()-mass << " momentum: " << dlr.momentum()-p << endl;
    }


    void step_L_diagonalized(double tau, const MatrixXd& X, const MatrixXd& S, const MatrixXd& V,
                const MatrixXd& L_in, MatrixXd& L_out, MatrixXd* bdryx=nullptr, MatrixXd* bdryv=nullptr, VectorXd* E_0=nullptr) {
        domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
        dlr.lr.X = X;
        dlr.lr.S = MatrixXd::Identity(r,r);
        dlr.lr.V = L_in;
        double mass = dlr.mass();
        double p    = dlr.momentum();
        
        MatrixXd D2 = compute_D2(X, bdryx);

        // Compute D1 which depends on the electric field
        VectorXd E(X.rows());
        if(E_0 == nullptr) {
            MatrixXd K = X*S;
            VectorXd rho = rho_K(K, V);
            electric_field(rho, E);
        } else
            E = *E_0;
        MatrixXd D1 = compute_D1(X, E);

        Eigen::SelfAdjointEigenSolver<MatrixXd> saes(D1);
        MatrixXd lambda = saes.eigenvalues();
        MatrixXd T = saes.eigenvectors();

        MatrixXd L_in_bar = L_in*T.inverse().transpose();
        MatrixXd L_out_bar(L_in_bar);

        // We also need to transform the boundary
        assert(bdryv != nullptr);
        MatrixXd bdry_bar = (*bdryv)*T.inverse().transpose();
                
        // and D2
        MatrixXd D2_bar = T.transpose()*D2*T.inverse().transpose();

        integrate([this,D2_bar,lambda,&bdry_bar](const MatrixXd& L, MatrixXd& out) {
                rhs_L_linear_diag(L, out, D2_bar, lambda, &bdry_bar);
                }, L_in_bar, L_out_bar, tau);

        L_out = L_out_bar*T.transpose();

        dlr.lr.V = L_out;
        cout << "error (L): mass: " << dlr.mass()-mass << " momentum: " << dlr.momentum()-p << endl;
    }

    void rhs_L_linear_diag(const MatrixXd& L, MatrixXd& out, const MatrixXd& D2, VectorXd lambda, MatrixXd* bdry=nullptr) {
        // This computes the second term
        out = -L*D2.transpose();
        // multiply with v
        for(int k=0;k<L.cols();k++)
            for(int i=0;i<L.rows();i++)
                out(i,k) *= v_coeff(i);

        // Add the first term
        MatrixXd Ltmp;
        assert(bdry != nullptr);
        Ltmp = diff_upwind(L, lambda, h[1], bdry);

        out -= Ltmp*lambda.asDiagonal();
    }

    

    void Lie_step(double tau, lowrank2d& in, lowrank2d& out, MatrixXd* bdry=nullptr) {
        // Step I (the K step)
        MatrixXd K1(n[0],r);
        step_K(tau, in.X*in.S, in.V, K1, in.X);

        // get X1 and S1 by doing a QR
        // NOTE: it is important to normalize X such that the integral
        // is normalized (and not the sum X^TX).
        qrx.compute(K1);
        out.X = qrx.Q()/sqrt(h[0]); // X1=out.X
        MatrixXd S1 = qrx.R()*sqrt(h[0]);

        // Step II (the S step)
        MatrixXd S2(S1);
        step_S(tau, out.X, S1, in.V, S2);

        // Step III (the L step)
        MatrixXd L(in.V);
        step_L(tau, out.X, S2, in.V, in.V*S2.transpose(), L, bdry);

        // compute the output by doing a QR
        // NOTE: it is important to normalize V such that the integral
        // is normalized (and not the sum V^TV).
        qrv.compute(L);
        out.V = qrv.Q()/sqrt(h[1]);
        out.S = qrv.R().transpose()*sqrt(h[1]);
    }
    
    void Lie_step_K(double tau, lowrank2d& in, lowrank2d& out,
            MatrixXd* bdryx=nullptr, MatrixXd* bdryv=nullptr, VectorXd* E_0=nullptr) {
        // Step I (the K step)
        MatrixXd K1(n[0],r);
        step_K_diagonalized(tau, in.X*in.S, in.V, K1, in.X, bdryx, bdryv, E_0);
        //step_K(tau, in.X*in.S, in.V, K1, in.X, bdryx, bdryv, E_0);

        // get X1 and S1 by doing a QR
        // NOTE: it is important to normalize X such that the integral
        // is normalized (and not the sum X^TX).
        qrx.compute(K1);
        out.X = qrx.Q()/sqrt(h[0]); // X1=out.X
        out.S = qrx.R()*sqrt(h[0]);
        out.V = in.V;
    }
        
    void Lie_step_S(double tau, lowrank2d& in, lowrank2d& out,
            MatrixXd* bdryx=nullptr, MatrixXd* bdryv=nullptr, VectorXd* E_0=nullptr) {
        MatrixXd S1 = in.S;
        MatrixXd X1 = in.X;

        // Step II (the S step)
        MatrixXd S2(S1);
        step_S(tau, X1, S1, in.V, S2, bdryx, bdryv, E_0);
        out.X = in.X;
        out.S = S2;
        out.V = in.V;
    }

    // TODO: eventually should be renamed to Lie_step_L
    void Lie_step_L(double tau, lowrank2d& in, lowrank2d& out, MatrixXd* bdryx=nullptr, MatrixXd* bdryv=nullptr, VectorXd* E=nullptr) {
        MatrixXd S2 = in.S;
        MatrixXd X1 = in.X;

        // Step III (the L step)
        MatrixXd L(in.V);
        step_L_diagonalized(tau, X1, S2, in.V, in.V*S2.transpose(), L, bdryx, bdryv, E);
        //step_L(tau, X1, S2, in.V, in.V*S2.transpose(), L, bdryx, bdryv, E);

        // compute the output by doing a QR
        // NOTE: it is important to normalize V such that the integral
        // is normalized (and not the sum V^TV).
        qrv.compute(L);
        out.V = qrv.Q()/sqrt(h[1]);
        out.S = qrv.R().transpose()*sqrt(h[1]);
        out.X = in.X;
    }

    void Strang_step(double tau, lowrank2d& in, lowrank2d& out) {
        // Half K step
        MatrixXd K1(n[0],r);
        step_K(0.5*tau, in.X*in.S, in.V, K1, in.X);
        qrx.compute(K1);
        out.X = qrx.Q()/sqrt(h[0]); // X1=out.X
        MatrixXd S1 = qrx.R()*sqrt(h[0]);

        // Half S step
        MatrixXd S2(S1);
        step_S(0.5*tau, out.X, S1, in.V, S2);

        // Half L step (to predict f_{1/2})
        MatrixXd L12(in.V);
        step_L(0.5*tau, out.X, S2, in.V, in.V*S2.transpose(), L12);

        // Full L step
        int r = S2.rows();
        MatrixXd L(in.V);
        // Here we use out.X, I, L12 to compute the electric field only. Therefore, it
        // is permissible to use L12 in place of V (even though L12 is not necessarily
        // an orthogonal matrix).
        //
        // We start, as before, from in.V*S2.transpose() and the output is saved in L.
        step_L(tau, out.X, MatrixXd::Identity(r,r), L12, in.V*S2.transpose(), L);

        // Now we decompose the result
        qrv.compute(L);
        out.V = qrv.Q()/sqrt(h[1]);
        S1 = qrv.R().transpose()*sqrt(h[1]);

        // Half S step
        step_S(0.5*tau, out.X, S1, out.V, S2);

        // Half K step
        step_K(0.5*tau, out.X*S2, out.V, K1, out.X);
        qrx.compute(K1);
        out.X = qrx.Q()/sqrt(h[0]);
        out.S = qrx.R()*sqrt(h[0]);
    }

    VectorXd project_func_v(const MatrixXd& V, function<double(double)> f) {
        VectorXd out(V.cols());
        for(int k=0;k<V.cols();k++) {
            out(k) = 0.0;
            for(int i=0;i<V.rows();i++)
                out(k) += V(i,k)*f(v(i))*h[1];
        }
        return out;
    }

    MatrixXd project_x(const MatrixXd& X, const MatrixXd& nablaX) {
        MatrixXd out(X.cols(),nablaX.cols());
        for(int k1=0;k1<X.cols();k1++) {
            for(int k2=0;k2<nablaX.cols();k2++) {
                out(k1,k2) = 0.0;
                for(int i=0;i<X.rows();i++)
                    out(k1,k2) += X(i,k1)*nablaX(i,k2)*h[0];
            }
        }
        return out;
    }
    
    MatrixXd project_func(const MatrixXd& X, const VectorXd& E, const MatrixXd& X2) {
        MatrixXd out(X.cols(),X2.cols());
        for(int k1=0;k1<X.cols();k1++) {
            for(int k2=0;k2<X2.cols();k2++) {
                out(k1,k2) = 0.0;
                for(int i=0;i<X.rows();i++)
                    out(k1,k2) += X(i,k1)*X2(i,k2)*E(i)*h[0];
            }
        }
        return out;
    }

    int absmax_idx(const VectorXd& v) {
        int largest_idx = 0;
        double m = 0.0;
        for(int i=0;i<v.size();i++) {
            if(abs(v(i)) > m) {
                m = abs(v(i));
                largest_idx = i;
            }
        }
        return largest_idx;
    }

    void matrix_for_c(const VectorXd& alpha, const VectorXd& f1, const VectorXd& beta, double f2, MatrixXd& A, VectorXd& b, cons_mode cm) {
        Index r = alpha.size();

        A.resize(r+1,r*r);
        A.setZero();
        b.resize(r+1);
        b.setZero();

        if(cm == cons_mode::lc_local || cm == cons_mode::lc_both) {
            // first r equations
            for(Index i=0;i<r;i++)
                for(Index j=0;j<r;j++)
                    A(i,i*r+j) = lc_both_weight*alpha[j];
        
            // assemble the rhs
            b.head(r) = lc_both_weight*f1;
        }

        if(cm == cons_mode::lc_global || cm == cons_mode::lc_both) {
            for(Index i=0;i<r;i++)
                for(Index j=0;j<r;j++)
                    A(r,i*r+j) = beta(i)*alpha(j);
        
            b(r) = f2;
        }
    }

    MatrixXd solve_for_c(Index r, const MatrixXd& A, const VectorXd& b) {

        // compute pseudo-inverse (smallest solution in the l^2 norm)
        Eigen::CompleteOrthogonalDecomposition<MatrixXd> cod(A);
        VectorXd c = cod.solve(b);
        
        // There must be a way to do this in Eigen. Note that Map should not be used here because 
        // c.data() is not available after returning from this function.
        MatrixXd coeff(r,r);
        for(Index i=0;i<r;i++)
            for(Index j=0;j<r;j++)
                coeff(i,j) = c(i*r+j);

        return coeff;
    }

    void rhs_L_linear(const MatrixXd& L, const MatrixXd& X0, const MatrixXd& V0, const MatrixXd& E, MatrixXd& out, const MatrixXd& D1, const MatrixXd& D2, MatrixXd* bdry=nullptr) {
        // This computes the second term
        out = -L*D2.transpose();
        // multiply with v
        for(int k=0;k<L.cols();k++)
            for(int i=0;i<L.rows();i++)
                out(i,k) *= v_coeff(i);

        // Add the first term
        MatrixXd Ltmp;
        if(bdry == nullptr)
            Ltmp = diff_fft(L, b[1]-a[1]);
        else
            Ltmp = diff_cd2(L, h[1], bdry);
        out -= Ltmp*D1.transpose();

        MatrixXd out_orig = out;

        // Lambda corrections
        if(cons_mass == cons_mode::rank1) {
            VectorXd phi(L.rows());
            double num_normalized = 0.0;
            for(int i=0;i<L.rows();i++) {
                phi(i) = exp(-0.5*v(i)*v(i))/sqrt(2.0*M_PI);
                num_normalized += phi(i)*h[1];
            }
            phi /= num_normalized;
            domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
            VectorXd int_vL = dlr.integrate_v(L, [](double v) { return v;});
            
            // We could also exp(-v^2) as above. Here there is no issue (see
            // the comment for the momentum conservation).
            //VectorXd int_L, int_vL;
            //VectorXd phi = L*construct_phi(L, int_L, int_vL);
            // Note: construct_phi_maxwell can not be used here since it requires an orthogonal basis (e.g. V0)

            // Lambda part 1
            VectorXd lambda = VectorXd::Zero(L.cols());
            for(int k=0;k<L.cols();k++)
                for(int i=0;i<L.rows();i++)
                    lambda(k) -= out_orig(i,k)*h[1];

            // Lambda part 2
            MatrixXd nablaX0 = diff_fft(X0, b[0]-a[0]);
            MatrixXd projnablaX = project_x(X0, nablaX0);
            lambda -= projnablaX*int_vL;

            // Lambda part 3
            VectorXd int_X = dlr.integrate_x(X0, [](double) { return 1.0;});
            VectorXd cls = VectorXd::Zero(int_X.size());
            int idx = absmax_idx(int_X);
            cls(idx) = 1.0/int_X(idx);
            lambda += int_X.dot(projnablaX*int_vL)*cls;


            out += phi*lambda.transpose();
        } 
/*
        if(is_f(cons_mass) || is_f(cons_mom)) {
            MatrixXd A(2,n[1]);
            A.setZero();
            VectorXd rhs_mass(r), rhs_mom(r);
            rhs_mass.setZero();
            rhs_mom.setZero();

            domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
            if(is_f(cons_mass)) {
                VectorXd int_vL = dlr.integrate_v(L,  [](double v) { return v;  });
                VectorXd int_out = dlr.integrate_v(out_orig, [](double) { return 1.0; });

                // nabla j projected to X
                MatrixXd nablaX0 = diff_fft(X0, b[0]-a[0]);
                MatrixXd projnablaX = project_x(X0, nablaX0);
                VectorXd nablaj = projnablaX*int_vL;

                //rhs_mass = -int_out - nablaj;
                //A.row(0) = h[1]*MatrixXd::Ones(1,n[1]);
                //MatrixXd A = h[1]*MatrixXd::Ones(1,n[1]); // discretized integral operator
                //Eigen::CompleteOrthogonalDecomposition<MatrixXd> cod(A);
                //MatrixXd R(n[1],r);
                //for(int i=0;i<r;i++) {
                //    VectorXd b(1);
                //    b(0) = rhs[i];
                //    R.col(i) = cod.solve(b);
                //}

                //out += R;
            }
            if(is_f(cons_mom)) {
                // integrate out_orig
                VectorXd int_vout = dlr.integrate_v(out_orig, [](double v) { return v; });
                VectorXd int_L  = dlr.integrate_v(L,  [](double)   { return 1.0;});
                VectorXd int_vsqL = dlr.integrate_v(L, [](double v) { return v*v;});

                // nabla rho u^2 projected to X
                MatrixXd nablaX0 = diff_fft(X0, b[0]-a[0]);
                MatrixXd projnablaX = project_x(X0, nablaX0);
                VectorXd nablarhouu = projnablaX*int_vsqL;

                // E rho
                MatrixXd projEX = project_func(X0, E, X0);
                VectorXd Erho = projEX*int_L;

                //rhs_mom = -int_vout - nablarhouu + Erho;
                //for(int j=0;j<n[1];j++)
                    //A(1,j) = h[1]*v(j);
            }

            MatrixXd R(n[1],r);
            R.setZero();
            Eigen::CompleteOrthogonalDecomposition<MatrixXd> cod(A);
            for(int i=0;i<r;i++) {
                VectorXd b(2);
                b(0) = rhs_mass(i);
                b(1) = rhs_mom(i);
                R.col(i) = cod.solve(b);
            }

            out += R;
        }
        */
        
        if(is_lc(cons_mass) || is_lc(cons_mom) || is_f(cons_mass) || is_f(cons_mom)) {
            domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
            VectorXd int_L  = dlr.integrate_v(L,  [](double)   { return 1.0;});
            VectorXd int_vL = dlr.integrate_v(L,  [](double v) { return v;  });
            VectorXd int_X  = dlr.integrate_x(X0, [](double)   { return 1.0;});
            VectorXd int_V  = dlr.integrate_v(V0, [](double)   { return 1.0;});
            VectorXd int_vV = dlr.integrate_v(V0, [](double v) { return v;  });
            
            VectorXd mass_f1, mom_f1;
            double mass_f2=0.0, mom_f2=0.0;

            if(is_lc(cons_mass) || is_f(cons_mass)) {
                // integrate out_orig
                VectorXd int_out = dlr.integrate_v(out_orig, [](double) { return 1.0; });

                // nabla j projected to X
                MatrixXd nablaX0 = diff_fft(X0, b[0]-a[0]);
                MatrixXd projnablaX = project_x(X0, nablaX0);
                VectorXd nablaj = projnablaX*int_vL;

                mass_f1 = -int_out - nablaj;
                mass_f2 = -int_X.dot(int_out);
            }
            if(is_lc(cons_mom) || is_f(cons_mom)) {
                // integrate out_orig
                VectorXd int_vout = dlr.integrate_v(out_orig, [](double v) { return v; });

                // nabla rho u^2 projected to X
                MatrixXd nablaX0 = diff_fft(X0, b[0]-a[0]);
                MatrixXd projnablaX = project_x(X0, nablaX0);
                VectorXd int_vsqL = dlr.integrate_v(L, [](double v) { return v*v;});
                VectorXd nablarhouu = projnablaX*int_vsqL;

                // E rho
                MatrixXd projEX = project_func(X0, E, X0);
                VectorXd Erho = projEX*int_L;

                mom_f1 = -int_vout - nablarhouu + Erho;
                mom_f2 = -int_X.dot(int_vout);
            }

            MatrixXd A(2*(r+1),r*r), _A;
            VectorXd b(2*(r+1)), _b;
            A.setZero();
            b.setZero();
            if(is_lc(cons_mass) || is_f(cons_mass)) {
                matrix_for_c(int_V, mass_f1, int_X, mass_f2, _A, _b, cons_mass);
                A.block(0,0,r+1,r*r) = _A;
                b.segment(0,r+1) = _b;
            }
            if(is_lc(cons_mom) || is_f(cons_mom)) {
                matrix_for_c(int_vV, mom_f1, int_X, mom_f2, _A, _b, cons_mom);
                A.block(r+1,0,r+1,r*r) = _A;
                b.segment(r+1,r+1) = _b;
            }
            MatrixXd coeff = solve_for_c(r, A, b);

            out += V0*coeff.transpose();
        }

        // mu correction
        if(cons_mom == cons_mode::rank1) {
            VectorXd chi(L.rows());
            double num_normalization = 0.0;
            double num_a = 0.0;
            for(int i=0;i<chi.size();i++) {
                chi(i) = v(i)*exp(-0.5*v(i)*v(i))/sqrt(2.0*M_PI);
                num_normalization += v(i)*chi(i)*h[1];
                num_a += chi(i)*h[1];
            }
            chi /= num_normalization;
            domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
            VectorXd int_L = dlr.integrate_v(L, [](double) { return 1.0;});
            
            // We do not need to use the projected chi here, but could just
            // employ v*exp(-v^2) as in the commented out code above. The
            // downside of that approach is hat the integral over v (num_a) is
            // not exactly zero, if we employ the default boundaries from -6..6.
            // This perturbation then destroys conservation up to machine
            // precision.
            //VectorXd int_L, int_vL;
            //VectorXd chi = L*construct_chi(L, int_L, int_vL);
            // Note: using construct_chi_maxwell is not possible here (L is not orthogonal)

            // mu correction part 1
            VectorXd mu = VectorXd::Zero(L.cols());
            for(int k=0;k<L.cols();k++)
                for(int i=0;i<L.rows();i++)
                    mu(k) -= v(i)*out_orig(i,k)*h[1];


            // mu correction part 2
            MatrixXd nablaX0 = diff_fft(X0, b[0]-a[0]);
            MatrixXd projnablaX = project_x(X0, nablaX0);
            VectorXd int_vsqL = dlr.integrate_v(L, [](double v) { return v*v;});
            mu -= projnablaX*int_vsqL;
     
            // mu correction part 3
            MatrixXd projEX = project_func(X0, E, X0);
            mu += projEX*int_L;

            // mu correction part 4
            VectorXd int_X = dlr.integrate_x(X0, [](double) { return 1.0;});
            VectorXd cls = VectorXd::Zero(int_X.size());
            int idx = absmax_idx(int_X);
            cls(idx) = 1.0/int_X(idx);
            mu += int_X.dot(projnablaX*int_vsqL-projEX*int_L)*cls;

            out += chi*mu.transpose();
        }
    }

    void rhs_S(const MatrixXd& S, const MatrixXd& V0, const MatrixXd& X1,
            MatrixXd& out, const MatrixXd& C1, const MatrixXd& C2,
            const MatrixXd& D2, VectorXd* E_0=nullptr) {
        // This is the linear term
        out = D2*S*C1.transpose();
        
        // compute the appropriate electric field and then D1(E)
        MatrixXd K = X1*S;
        VectorXd E(K.rows());
        if(E_0 == nullptr) {
            VectorXd rho = rho_K(K, V0);
            electric_field(rho, E);
        } else
            E = *E_0;
        MatrixXd D1 = compute_D1(X1, E);

        // This is the second term
        out += D1*S*C2.transpose();

        MatrixXd out_orig = out;

        // Lambda correction
        if(cons_mass == cons_mode::rank1) {
            VectorXd int_V, int_vV;
            //VectorXd phi = construct_phi(V0, int_V, int_vV);
            VectorXd phi = construct_phi_maxwell(V0, int_V, int_vV);

            // Lambda part 1
            VectorXd lambda = -out_orig*int_V;

            // Lambda part 2
            MatrixXd nablaX1 = diff_fft(X1, b[0]-a[0]);
            MatrixXd projnablaX = project_x(X1, nablaX1);
            lambda += projnablaX*S*int_vV; // NOTE: here is a positive sign

            // Lambda part 3
            domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
            VectorXd int_X = dlr.integrate_x(X1, [](double) { return 1.0;});
            VectorXd cls = VectorXd::Zero(int_X.size());
            int idx = absmax_idx(int_X);
            cls(idx) = 1.0/int_X(idx);
            lambda -= int_X.dot(projnablaX*S*int_vV)*cls;


            out += lambda*phi.transpose();
        }

        // Note that for the S step the lc and f corrections are identical.
        if(is_lc(cons_mass) || is_lc(cons_mom) || is_f(cons_mass) || is_f(cons_mom)) {
            domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
            VectorXd int_V  = dlr.integrate_v(V0, [](double) { return 1.0;});
            VectorXd int_vV = dlr.integrate_v(V0, [](double v) { return v;});
            VectorXd int_X  = dlr.integrate_x(X1, [](double) { return 1.0;});
            
            VectorXd mass_f1, mom_f1;
            double mass_f2=0.0, mom_f2=0.0;

            if(is_lc(cons_mass) || is_f(cons_mass)) {
                // nabla j projected to X
                MatrixXd nablaX1 = diff_fft(X1, b[0]-a[0]);
                MatrixXd projnablaX = project_x(X1, nablaX1);
                VectorXd nablaj = projnablaX*S*int_vV;

                mass_f1 = out_orig*int_V - nablaj;
                mass_f2 = int_X.dot(out_orig*int_V);
            }

            if(is_lc(cons_mom) || is_f(cons_mom)) {
                // nabla rho u^2 projected to X
                MatrixXd nablaX1 = diff_fft(X1, b[0]-a[0]);
                MatrixXd projnablaX = project_x(X1, nablaX1);
                VectorXd int_vsqV = dlr.integrate_v(V0, [](double v) { return v*v;});
                VectorXd nablarhouu = projnablaX*S*int_vsqV;

                // E rho
                MatrixXd projEX = project_func(X1, E, X1);
                VectorXd Erho = projEX*S*int_V;

                mom_f1 = out_orig*int_vV - nablarhouu + Erho;
                mom_f2 = int_X.dot(out_orig*int_vV);
            }

            MatrixXd A(2*(r+1),r*r), _A;
            VectorXd b(2*(r+1)), _b;
            A.setZero();
            b.setZero();
            if(is_lc(cons_mass) || is_f(cons_mass)) {
                matrix_for_c(int_V, mass_f1, int_X, mass_f2, _A, _b, cons_mass);
                A.block(0,0,r+1,r*r) = _A;
                b.segment(0,r+1) = _b;
            }
            if(is_lc(cons_mom) || is_f(cons_mom)) {
                matrix_for_c(int_vV, mom_f1, int_X, mom_f2, _A, _b, cons_mom);
                A.block(r+1,0,r+1,r*r) = _A;
                b.segment(r+1,r+1) = _b;
            }
            MatrixXd coeff = solve_for_c(r, A, b);

            out -= coeff;
        }

        if(cons_mom == cons_mode::rank1) {
            // mu correction
            VectorXd int_V, int_vV;
            //VectorXd chi = construct_chi(V0, int_V, int_vV);
            VectorXd chi = construct_chi_maxwell(V0, int_V, int_vV);

            // mu correction part 1
            VectorXd mu = -out_orig*int_vV;

            // mu correction part 2
            MatrixXd nablaX1 = diff_fft(X1, b[0]-a[0]);
            MatrixXd projnablaX = project_x(X1, nablaX1);
            domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
            VectorXd int_vsqV = dlr.integrate_v(V0, [](double v) { return v*v;});
            mu += projnablaX*S*int_vsqV;

            // mu correction part 3
            MatrixXd projEX = project_func(X1, E, X1);
            mu -= projEX*S*int_V;

            // mu correction part 4
            VectorXd int_X = dlr.integrate_x(X1, [](double) { return 1.0;});
            VectorXd cls = VectorXd::Zero(int_X.size());
            int idx = absmax_idx(int_X);
            cls(idx) = 1.0/int_X(idx);
            mu -= int_X.dot(projnablaX*S*int_vsqV - projEX*S*int_V)*cls;


            out += mu*chi.transpose();
        }
    }

    VectorXd phi_maxwell(const MatrixXd& V0, const VectorXd& int_V) {
        VectorXd out = project_func_v(V0, [](double v) { return exp(-0.5*v*v)/sqrt(2.0*M_PI); });
        double total_mass = out.dot(int_V);
        return out/total_mass;
    }

    VectorXd chi_maxwell(const MatrixXd& V0, const VectorXd& int_vV) {
        VectorXd out = project_func_v(V0, [](double v) { return v*exp(-0.5*v*v)/sqrt(2.0*M_PI); });
        
        double total_momentum = out.dot(int_vV);
        return out/total_momentum;
    }


    VectorXd construct_phi_maxwell(const MatrixXd& V0, VectorXd& int_V, VectorXd& int_vV) {
        domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
        int_V = dlr.integrate_v(V0, [](double) { return 1.0;});
        int_vV = dlr.integrate_v(V0, [](double v) { return v;});

        VectorXd phi = phi_maxwell(V0, int_V);
        VectorXd chi = chi_maxwell(V0, int_vV);

        double phi_mom  = phi.dot(int_vV);
        phi -= phi_mom*chi;
        double phi_mass = phi.dot(int_V);
        return phi/phi_mass;
    }

    VectorXd construct_chi_maxwell(const MatrixXd& V0, VectorXd& int_V, VectorXd& int_vV) {
        domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
        int_V = dlr.integrate_v(V0, [](double) { return 1.0;});
        int_vV = dlr.integrate_v(V0, [](double v) { return v;});

        VectorXd phi = phi_maxwell(V0, int_V);
        VectorXd chi = chi_maxwell(V0, int_vV);

        double chi_mass = chi.dot(int_V);
        chi -= chi_mass*phi;
        double chi_mom = chi.dot(int_vV);
        return chi/chi_mom;
    }

    VectorXd construct_phi(const MatrixXd& V0, VectorXd& int_V, VectorXd& int_vV) {
        domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
        int_V = dlr.integrate_v(V0, [](double) { return 1.0;});
        int_vV = dlr.integrate_v(V0, [](double v) { return v;});

        VectorXd out = VectorXd::Zero(int_V.size());
        double total_mass = accumulate(&int_V[0], &int_V[0]+int_V.size(), 0.0);
        for(int i=0;i<out.size();i++)
            out(i) = 1.0/total_mass;

        // this ensures that the correction does not introduce any change in momentum
        double total_momentum = accumulate(&int_vV[0], &int_vV[0]+int_vV.size(), 0.0)/total_mass;
        int idx_max_momentum = absmax_idx(int_vV);
        double coeff = total_momentum/int_vV[idx_max_momentum];
        out(idx_max_momentum) -= coeff;

        out /= 1.0-coeff*int_V[idx_max_momentum];

        return out;
    }
    
    VectorXd construct_chi(const MatrixXd& V0, VectorXd& int_V, VectorXd& int_vV) {
        domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
        int_V = dlr.integrate_v(V0, [](double) { return 1.0;});
        int_vV = dlr.integrate_v(V0, [](double v) { return v;});

        // Since momentum is often small we pick here a different approach compared to phi
        VectorXd out = VectorXd::Zero(int_vV.size());
        int idx_max_momentum = absmax_idx(int_vV);
        out(idx_max_momentum) = 1.0/int_vV(idx_max_momentum);

        // this ensures that the correction does not introduce any change in mass
        double total_mass = accumulate(&int_V[0], &int_V[0]+int_V.size(), 0.0);
        for(int i=0;i<out.size();i++)
            out(i) -= int_V[idx_max_momentum]/(int_vV[idx_max_momentum]*total_mass);

        double total_momentum = 0.0;
        for(int i=0;i<out.size();i++)
            total_momentum += out(i)*int_vV(i);
        out /= total_momentum;

        return out;
    }

    void rhs_K(const MatrixXd& K, const MatrixXd& V0, const MatrixXd& X0, MatrixXd& out, const MatrixXd& C1, const MatrixXd& C2, MatrixXd* bdry=nullptr, VectorXd* E_0=nullptr) {
        // This computes the second term
        out = -K*C2.transpose(); // change C1 back to C2
        // for which we need the electric field
        VectorXd E(K.rows());
        VectorXd rho = rho_K(K, V0); // needed also for a correction
        if(E_0 == nullptr) {
            electric_field(rho, E);
        } else
            E = *E_0;
        // which enters in a pointwise multiplication
        for(int j=0;j<K.cols();j++) {
            auto bl = out.col(j);
            bl = bl.array()*E.array();
        }


        // Add the first term
        MatrixXd Ktmp;
        if(bdry == nullptr)
            Ktmp = diff_fft(K, b[0]-a[0]);
        else
            Ktmp = diff_cd2(K, h[0], bdry);
        out -= Ktmp*C1.transpose();


        // corrections
        MatrixXd out_orig = out;

        // lambda correction
        if(cons_mass == cons_mode::rank1) {
            VectorXd int_V, int_vV;
            //VectorXd phi = construct_phi(V0, int_V, int_vV);
            VectorXd phi = construct_phi_maxwell(V0, int_V, int_vV);

            // lambda correction term part 1
            VectorXd lambda = -out_orig*int_V;

            // lambda correction term part 2 ( partial_x j)
            lambda += -Ktmp*int_vV; // dx is stored in Ktmp

            out += lambda*phi.transpose();
        }

        if(is_f(cons_mass) || is_f(cons_mom)) {
            MatrixXd A(2,r);
            A.setZero();
            VectorXd rhs_mass(n[0]), rhs_mom(n[0]);
            rhs_mass.setZero();
            rhs_mom.setZero();

            if(is_f(cons_mass)) {
                domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
                VectorXd int_V  = dlr.integrate_v(V0,  [](double)   { return 1.0;});
                VectorXd int_vV = dlr.integrate_v(V0,  [](double v) { return v;  });

                MatrixXd nablaK = diff_fft(K, b[0]-a[0]);

                rhs_mass = -out_orig*int_V - nablaK*int_vV;
                cout << rhs_mass.norm() << endl;
                A.row(0) = int_V.transpose();


                VectorXd int_out = dlr.integrate_x(out_orig, [](double) { return 1.0; });
                MatrixXd projF = project_x(X0, out_orig);

                // nabla j projected to X
                //MatrixXd nablaK = diff_fft(K, b[0]-a[0]);
                MatrixXd projnablaK = project_x(X0, nablaK);
                VectorXd nablaj = projnablaK*int_vV;

                rhs_mass = X0*(-projF*int_V - nablaj);


                //VectorXd rhs = -out_orig*int_V - nablaK*int_vV;
                //MatrixXd R(n[0],r);
                //MatrixXd A = int_V.transpose();
                //Eigen::CompleteOrthogonalDecomposition<MatrixXd> cod(A);
                //for(int i=0;i<n[0];i++) {
                //    VectorXd b(1);
                //    b(0) = rhs(i);
                //    R.row(i) = cod.solve(b);
                //}

                //out += R;
                //
                //
                //
               /*
                VectorXd int_out = dlr.integrate_x(out_orig, [](double) { return 1.0; });
                MatrixXd projF = project_x(X0, out_orig);

                // nabla j projected to X
                //MatrixXd nablaK = diff_fft(K, b[0]-a[0]);
                MatrixXd projnablaK = project_x(X0, nablaK);
                //VectorXd nablaj = projnablaK*int_vV;

                //rhs_mass = -projF*int_V - nablaj;
                rhs_mass = -out_orig*int_V - X0*projnablaK*int_vV;
                */

            }
            if(is_f(cons_mom)) {
                domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
                VectorXd int_V  = dlr.integrate_v(V0,  [](double)   { return 1.0;});
                VectorXd int_vV = dlr.integrate_v(V0,  [](double v) { return v;  });
                VectorXd int_vsqV = dlr.integrate_v(V0, [](double v) { return v*v; });
                
                MatrixXd nablaK = diff_fft(K, b[0]-a[0]);

                rhs_mom = -out_orig*int_vV - nablaK*int_vsqV + (E.array()*(K*int_V).array()).matrix();
                A.row(1) = int_vV.transpose();
            }

            MatrixXd R(n[0],r);
            Eigen::CompleteOrthogonalDecomposition<MatrixXd> cod(A);
            for(int i=0;i<n[0];i++) {
                VectorXd b(2);
                b(0) = rhs_mass(i);
                b(1) = rhs_mom(i);
                R.row(i) = cod.solve(b);
            }

            out += R;
        }
        
        if(is_lc(cons_mass) || is_lc(cons_mom)) {
            domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
            VectorXd int_V  = dlr.integrate_v(V0,  [](double)   { return 1.0;});
            VectorXd int_vV = dlr.integrate_v(V0,  [](double v) { return v;  });
            VectorXd int_X  = dlr.integrate_x(X0,  [](double)   { return 1.0;});

            VectorXd mass_f1, mom_f1;
            double mass_f2=0.0, mom_f2=0.0;
            if(is_lc(cons_mass)) {
                // integrate out_orig
                VectorXd int_out = dlr.integrate_x(out_orig, [](double) { return 1.0; });
                MatrixXd projF = project_x(X0, out_orig);

                // nabla j projected to X
                MatrixXd nablaK = diff_fft(K, b[0]-a[0]);
                MatrixXd projnablaK = project_x(X0, nablaK);
                VectorXd nablaj = projnablaK*int_vV;

                mass_f1 = -projF*int_V - nablaj;
                cout << mass_f1.norm() << endl;
                mass_f2 = -int_V.dot(int_out);
            }

            if(is_lc(cons_mom)) {
                // integrate out_orig
                VectorXd int_out = dlr.integrate_x(out_orig, [](double) { return 1.0; });
                MatrixXd projF = project_x(X0, out_orig);

                // nabla j projected to X
                MatrixXd nablaK = diff_fft(K, b[0]-a[0]);
                MatrixXd projnablaK = project_x(X0, nablaK);
                VectorXd int_vsqV = dlr.integrate_v(V0, [](double v) { return v*v; });
                VectorXd nablarhouu = projnablaK*int_vsqV;

                // Eq projected to X
                MatrixXd projEX = project_func(X0, E, K);
                VectorXd Erho = projEX*int_V;

                mom_f1 = -projF*int_vV - nablarhouu + Erho;
                mom_f2 = -int_vV.dot(int_out);
            }

            Index r = X0.cols();
            MatrixXd A(2*(r+1),r*r), _A;
            VectorXd b(2*(r+1)), _b;
            A.setZero();
            b.setZero();
            if(is_lc(cons_mass)) {
                matrix_for_c(int_V, mass_f1, int_X, mass_f2, _A, _b, cons_mass);
                A.block(0,0,r+1,r*r) = _A;
                b.segment(0,r+1) = _b;
            }
            if(is_lc(cons_mom)) {
                matrix_for_c(int_vV, mom_f1, int_X, mom_f2, _A, _b, cons_mom);
                A.block(r+1,0,r+1,r*r) = _A;
                b.segment(r+1,r+1) = _b;
            }
            MatrixXd coeff = solve_for_c(r, A, b);

            out += X0*coeff;
        }

        if(cons_mom == cons_mode::rank1) {
            // mu correction
            VectorXd int_V, int_vV;
            //VectorXd chi = construct_chi(V0, int_V, int_vV);
            VectorXd chi = construct_chi_maxwell(V0, int_V, int_vV);

            // mu correction part 1
            VectorXd mu = -out_orig*int_vV;

            // lambda correction part 2
            domain_lowrank2d dlr(a, b, r, {n[0],n[1]}, lowrank_type::normalized_integral);
            VectorXd int_vsqV = dlr.integrate_v(V0, [](double v) { return v*v;});
            mu += -Ktmp*int_vsqV;

            // lambda correction part 3
            // Note that we have to use here int f dv = rho + 1
            mu += (E.array()*(rho.array()+1.0)).matrix();


            out += mu*chi.transpose();
        }
    }

    VectorXd rho_K(const MatrixXd& K, const MatrixXd& V0) {
        int nx = K.rows();
        int nv = V0.rows();
        int r = K.cols();

        VectorXd rho(nx);
        rho.setZero();
        for(int k=0;k<r;k++) {
            double I_V0 = 0.0;
            for(int j=0;j<nv;j++)
                I_V0 += h[1]*V0(j,k);

            for(int i=0;i<nx;i++)
                rho(i) += I_V0*K(i,k);
        }
        return rho - VectorXd::Constant(nx,1.0);
    }

    void electric_field(const VectorXd& rho, VectorXd& E) {
        int n = rho.rows();
        fft(n, &rho(0), hat);
        for(int k=0;k<n;k++) {
            double k_freq = double(k)*2.0*M_PI/(b[0]-a[0]);
            if(k != 0)
                hat[k] *= 1.0/(complex<double>(0,1)*k_freq*double(n));
            else
                hat[k] = 0.0;
        }
        inv_fft(n, hat, &E(0));
    }

    double electric_field_energy(const lowrank2d& lr, VectorXd& E) {
        VectorXd rho = rho_K(lr.X*lr.S, lr.V);
        electric_field(rho, E);

        // compute electric energy
        double s = 0.0;
        for(int i=0;i<E.rows();i++)
            s += h[0]*pow(E[i],2);
        return 0.5*s;
    }

    MatrixXd diff_fft(const MatrixXd& K, double L) {
        // We assume that K and out are in Fortran-major order
        int n = K.rows();
        int r = K.cols();
        MatrixXd out(n,r);
        for(int j=0;j<r;j++) {
            fft(n, &K(0,j), hat);
            for(int k=0;k<n;k++) {
                double k_freq = double(k)*2.0*M_PI/L;
                hat[k] *= complex<double>(0,1)*k_freq/double(n);
            }
            inv_fft(n, hat, &out(0,j));
        }
        return out;
    }

    MatrixXd diff_cd2(const MatrixXd& K, double h,
            MatrixXd* bdry=nullptr) {
        int n = K.rows();
        int r = K.cols();

        // setup boundary data if no external data
        // are supplied
        MatrixXd _bdry(2,r);
        if(bdry == nullptr) {
            for(int j=0;j<r;j++) {
                _bdry(0,j) = K(n-1,j);
                _bdry(1,j) = K(0,j);
            }
            bdry = &_bdry;
        }

        // compute the centered differences
        MatrixXd out(n,r);
        for(int j=0;j<r;j++) {

            out(0,j)   = 0.5*(K(1,j)-(*bdry)(0,j))/h;
            out(n-1,j) = 0.5*((*bdry)(1,j)-K(n-2,j))/h;
            for(int k=1;k<n-1;k++)
                out(k,j) = 0.5*(K(k+1,j)-K(k-1,j))/h;
        }
        
        return out;
    }

    MatrixXd diff_upwind(const MatrixXd& K, VectorXd lambda, double h,
            MatrixXd* bdry=nullptr) {
        assert(bdry != nullptr);

        int n = K.rows();
        int r = K.cols();

        MatrixXd out(n,r);
        for(int j=0;j<r;j++) {

            if(lambda[j] >= 0.0) {
                out(0,j)   = (K(0,j)-(*bdry)(0,j))/h;
                out(n-1,j) = (K(n-1,j)-K(n-2,j))/h;
                for(int k=1;k<n-1;k++)
                    out(k,j) = (K(k,j)-K(k-1,j))/h;
            } else {
                out(0,j)   = (K(1,j)-K(0,j))/h;
                out(n-1,j) = ((*bdry)(1,j)-K(n-1,j))/h;
                for(int k=1;k<n-1;k++)
                    out(k,j) = (K(k+1,j)-K(k,j))/h;
            }
        }
        
        return out;
    }


    // TODO: temp
    MatrixXd _diff_cd2(const MatrixXd& K, double h,
            MatrixXd* bdry=nullptr) {
        int n = K.rows();
        int r = K.cols();

        // compute the centered differences
        MatrixXd out(n,r);
        for(int j=0;j<r;j++) {

            //out(0,j)   = 0.5*(K(1,j)-K(n-1,j))/h;   // only for periodic
            //out(n-1,j) = 0.5*(K(0,j)-K(n-2,j))/h; // only for periodic
            out(0,j)   = (K(1,j)-K(0,j))/h;   // order 1
            out(n-1,j) = (K(n-1,j)-K(n-2,j))/h; // order 1
            //out(0,j)   =  (-3./2.*K(0,j) + 2.*K(1,j) - 1./2.*K(2,j))/h;
            //out(n-1,j) =  (3./2.*K(n-1,j) - 2.*K(n-2,j) + 1./2.*K(n-3,j))/h;
            for(int k=1;k<n-1;k++)
                out(k,j) = 0.5*(K(k+1,j)-K(k-1,j))/h;
        }
        
        return out;
    }

    MatrixXd compute_C1(const MatrixXd& V) {
        int n = V.rows();
        int r = V.cols();
        MatrixXd C1(r,r);
        C1.setZero();
        for(int i=0;i<r;i++)
            for(int j=0;j<r;j++)
                for(int k=0;k<n;k++)
                    C1(i,j) += h[1]*v_coeff(k)*V(k,i)*V(k,j);
        return C1;
    }
    
    MatrixXd compute_C2(const MatrixXd& V, MatrixXd* bdryv=nullptr) {
        int n = V.rows();
        int r = V.cols();
        MatrixXd C2(r,r);
        C2.setZero();

        MatrixXd nablaV;
        if(bdryv==nullptr)
            nablaV = diff_fft(V, b[1]-a[1]);
        else
            nablaV = _diff_cd2(V, h[1], bdryv);

        for(int i=0;i<r;i++) {
            for(int j=0;j<r;j++) {
                // compute the integral
                for(int k=0;k<n;k++)
                    C2(i,j) += h[1]*nablaV(k,j)*V(k,i);
            }
        }


        return C2;
    }

    MatrixXd compute_D1(const MatrixXd& X1, const VectorXd& E) {
        int n = X1.rows();
        assert(n == E.size());
        int r = X1.cols();
        MatrixXd D1(r,r);
        D1.setZero();
        for(int i=0;i<r;i++)
            for(int j=0;j<r;j++)
                for(int k=0;k<n;k++)
                    D1(i,j) += h[0]*E(k)*X1(k,i)*X1(k,j);

        return D1;
    }

    MatrixXd compute_D2(const MatrixXd& X1, MatrixXd* bdry=nullptr) {
        int n = X1.rows();
        int r = X1.cols();
        
        MatrixXd nablaX1;
        if(bdry == nullptr)
            nablaX1 = diff_fft(X1, b[0]-a[0]);
        else
            nablaX1 = _diff_cd2(X1, h[0], bdry);

        MatrixXd D2(r,r);
        D2.setZero();
        for(int i=0;i<r;i++) {
            for(int j=0;j<r;j++)
                // compute the integral
                for(int k=0;k<n;k++)
                    D2(i,j) += h[0]*nablaX1(k,j)*X1(k,i);
        }

        return D2;
    }
};


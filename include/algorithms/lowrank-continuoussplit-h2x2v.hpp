#pragma once

#include <generic/common.hpp>
#include <boost/numeric/odeint.hpp>
#include <generic/timer.hpp>
#include <generic/tensors.hpp>
#include <generic/integrators.hpp>
#include <generic/fft.hpp>
#include <container/domain-lowrank-h2x2v.hpp>


void integrate_tensor3(function<void(const Tensor3&,Tensor3&)> rhs, const Tensor3& in, Tensor3& out, double t, int substeps = 40) {
    using namespace boost::numeric;
    using namespace boost::numeric::odeint;

    // this is still with a constant step size
    runge_kutta_dopri5<Tensor3,double,Tensor3,double,vector_space_algebra> stepper;

    out = in; // integrate_adaptive uses the same variable as in and output
    integrate_adaptive(stepper,
            [rhs](const Tensor3& x, Tensor3& dxdt, double t) {
                rhs(x, dxdt);
            }, out, 0.0, t, t/double(substeps));
}


struct continuoussplit_2dh {
    domain2d_data dd;

    complex<double>* hat;

    continuoussplit_2dh(domain2d_data _dd) : dd(_dd) {
        hat = new complex<double>[max(dd.n[0],dd.n[1])];
    }

    ~continuoussplit_2dh() {
        delete[] hat;
    }


    void Lie_step(double tau, const lowrank2dh& in, lowrank2dh& out,
            const array<MatrixXd,2>& C1, const array<MatrixXd,2>& C2, const array<MatrixXd, 2>& E) {
        timer t_total, t_step, t_rstep, t_qr;
        t_total.start();

        // Get Q,S from Shat
        t_qr.start();
        MatrixXd Shat = in.S.reshape21({0,2,1});
        rectangular_QR qr1(Shat);
        MatrixXd Q = qr1.Q();
        MatrixXd R = qr1.R().transpose();
        t_qr.stop();

        // Ktild step
        MatrixXd Kin = in.X*R;
        MatrixXd K1(Kin);
        t_step.start();
        timer t_stepK; t_stepK.start();
        step_Ktild(tau, Kin, Q, in.Y, K1, C1, C2, E);
        t_stepK.stop();
        t_step.stop();

        // QR factors of K1
        t_qr.start();
        rectangular_QR qr2(K1);
        out.X = qr2.Q()/sqrt(dd.h[0]);
        MatrixXd R1 = qr2.R()*sqrt(dd.h[0]);
        t_qr.stop();

        // S1 step
        MatrixXd R2(R1);
        t_rstep.start();
        step_R1(tau, R1, out.X, Q, in.Y, R2, C1, C2, E);
        t_rstep.stop();

        // Reshape the Shat
        Shat = Q*R2.transpose();
        Tensor3 Shatt(dd.r);
        Shatt.from_reshape21(Shat, {0,2,1});
        Shat = Shatt.reshape21({0,1,2});
        // Get Q, S from Shat
        t_qr.start();
        rectangular_QR qr3(Shat);
        Q = qr3.Q();
        R = qr3.R().transpose();
        t_qr.stop();

        // Ltild step
        // Construct Qlin, the tensors are different between the two versions but if
        // we use the order delta gamma epsilon then the code should work as expected
        Tensor3 Qlin({dd.r[1],dd.r[0],dd.r[2]});
        Qlin.from_reshape21(Q, {1,0,2});
        // do the step
        MatrixXd N = in.Y*R;
        MatrixXd N1(N);
        t_step.start(); timer t_stepN; t_stepN.start();
        step_N(tau, N, out.X, Qlin, N1, C1, C2, E);
        t_stepN.stop(); 
        t_step.stop();

        // QR factors of N1
        t_qr.start();
        rectangular_QR qr4(N1);
        out.Y = qr4.Q()/sqrt(dd.h[1]);
        R = qr4.R()*sqrt(dd.h[1]);
        t_qr.stop();

        // S2 step
        t_rstep.start();
        step_R2(tau, R, out.X, Qlin, out.Y, R1, C1, C2, E);
        t_rstep.stop();

        // reconstruct Shat
        Shat = Q*R1.transpose();
        Shatt.from_reshape21(Shat, {0,1,2});
        
        // The step for the core tensor
        t_step.start(); timer t_stepM; t_stepM.start();
        step_M(tau, Shatt, out.X, out.Y, out.S, C1, C2, E);
        t_stepM.stop();
        t_step.stop();

        t_total.stop();
        cout << "cs2dh. Total: " << t_total.total() << " Step: " << t_step.total() << " RStep: " << t_rstep.total() << " QR: " << t_qr.total() << " Remainder: " << t_total.total()-t_step.total()-t_rstep.total()-t_qr.total() << endl;
        cout << t_stepK.total() << " " << t_stepN.total() << " " << t_stepM.total() << endl;
    }

    void step_Ktild(double tau, const MatrixXd& K, const MatrixXd& Q, const MatrixXd& Y, MatrixXd& K1,
            const array<MatrixXd,2>& C1, const array<MatrixXd,2>& C2, const array<MatrixXd, 2>& E) {

        timer t; t.start();
        Tensor3 Qt({dd.r[0],dd.r[2],dd.r[1]});
        Qt.from_reshape21(Q, {0,1,2}); // just represent as a tensor (no reshaping)
        MatrixXd a1 = compute_a1(C1[0], Qt);
        MatrixXd a2 = compute_a2(C1[1], Qt, Y);
        timer t1; t1.start();
        Tensor3  a3 = compute_a3(C2, Qt, Y, E);
        t1.stop(); cout << "compute_a3: " << t1.total() << endl;

        integrate([this,a1,a2,a3](const MatrixXd& K, MatrixXd& out) {
                rhs_Ktild(K, {a1, a2}, a3, out);
                }, K, K1, tau);

        t.stop();
        cout << "Ktild: " << t.total() << endl;
    }

    void rhs_Ktild(const MatrixXd& K, const array<MatrixXd, 3>& a, const Tensor3& a3, MatrixXd& out) {
        MatrixXd dK(K);
        cd_stencil(K, dK, dd.b[0]-dd.a[0]);

        out = -dK*a[0].transpose() - K*a[1].transpose();

        for(auto i : up_to<2>({dd.n[0],dd.r[1]}))
            for(int eta=0;eta<dd.r[1];eta++)
                out(i[0],i[1]) -= a3(i[1],eta,i[0])*K(i[0],eta);
    }

    MatrixXd compute_a1(const MatrixXd& C1x, const Tensor3& Q) {
        MatrixXd a1(dd.r[1],dd.r[1]);
        a1.setZero();
        for(int pi=0;pi<a1.cols();pi++)
            for(int eta=0;eta<a1.rows();eta++)
                for(auto i : up_to<3>({dd.r[0],dd.r[0],dd.r[2]}))
                    a1(eta,pi) += C1x(i[0],i[1])*Q(i[0],i[2],eta)*Q(i[1],i[2],pi);
        return a1;
    }

    MatrixXd compute_d1(const MatrixXd& Y) {
        // Setup a matrix with YdY_{eps,epsp} = (Y_eps, \partial_y Y_epsp)
        MatrixXd dY(Y);
        cd_stencil(Y, dY, dd.b[1]-dd.a[1]);
        MatrixXd YdY = dot(Y,dY, dd.h[1]);
        return YdY;
    }

    MatrixXd compute_a2(const MatrixXd& C1y, const Tensor3& Q, const MatrixXd& Y) {
        MatrixXd D1 = compute_d1(Y);

        MatrixXd a2(dd.r[1],dd.r[1]);
        a2.setZero();
        for(int pi=0;pi<a2.cols();pi++)
            for(int eta=0;eta<a2.rows();eta++)
                for(auto i : up_to<4>({dd.r[0],dd.r[0],dd.r[2],dd.r[2]}))
                    a2(eta,pi) += C1y(i[0],i[1])*Q(i[0],i[2],eta)*Q(i[1],i[3],pi)*D1(i[2],i[3]);

        return a2;
    }

    array<Tensor3, 2> compute_d2(const MatrixXd& Y, const array<MatrixXd, 2>& E) {
        // Setup a matrix with YdY_{eps,epsp} = (Y_eps, E Y_epsp)
        Tensor3 d2_0({dd.r[2],dd.r[2],dd.n[0]});
        Tensor3 d2_1({dd.r[2],dd.r[2],dd.n[0]});
        d2_0.setZero();
        d2_1.setZero();

        for(auto i : up_to<3>({dd.r[2],dd.r[2],dd.n[0]}))
            for(int jy=0;jy<dd.n[1];jy++) {
                d2_0(i) += dd.h[1]*Y(jy,i[0])*Y(jy,i[1])*E[0](i[2],jy);
                d2_1(i) += dd.h[1]*Y(jy,i[0])*Y(jy,i[1])*E[1](i[2],jy);
            }

        return {d2_0, d2_1};
    }

    Tensor3 compute_a3(const array<MatrixXd, 2>& C2, const Tensor3& Q, const MatrixXd& Y, const array<MatrixXd,2>& E) {
        timer t2; t2.start();
        array<Tensor3,2> d2 = compute_d2(Y, E);
        t2.stop(); cout << "d2: " << t2.total() << endl;

        Tensor3 a3({dd.r[1],dd.r[1],dd.n[0]});
        a3.setZero();


        MatrixXd A(dd.r[0],dd.r[2]), B(dd.r[0],dd.r[2]), C(dd.r[0],dd.r[2]), D(dd.r[0],dd.r[2]);
        for(int ix=0;ix<dd.n[0];ix++)
            for(int pi=0;pi<dd.r[1];pi++)
                for(int eta=0;eta<dd.r[1];eta++) {

                    A.setZero();
                    C.setZero();
                    for(auto i : up_to<4>({dd.r[0],dd.r[0],dd.r[2],1})) {
                        A(i[1],i[2]) += Q(i[0],i[2],eta)*C2[0](i[0],i[1]);
                        C(i[1],i[2]) += Q(i[0],i[2],eta)*C2[1](i[0],i[1]);
                    }

                    B.setZero();
                    D.setZero();
                    for(auto i : up_to<4>({1,dd.r[0],dd.r[2],dd.r[2]})) {
                        B(i[1],i[2]) += Q(i[1],i[3],pi)*d2[0](i[2],i[3],ix);
                        D(i[1],i[2]) += Q(i[1],i[3],pi)*d2[1](i[2],i[3],ix);
                    }

                    for(auto i : up_to<4>({1,dd.r[0],dd.r[2],1}))
                        a3(eta,pi,ix) += A(i[1],i[2])*B(i[1],i[2]) + C(i[1],i[2])*D(i[1],i[2]);

                }

/*
        for(int ix=0;ix<dd.n[0];ix++)
            for(int pi=0;pi<dd.r[1];pi++)
                for(int eta=0;eta<dd.r[1];eta++)
                    for(auto i : up_to<4>({dd.r[0],dd.r[0],dd.r[2],dd.r[2]}))
                        a3(eta,pi,ix) += (C2[0](i[0],i[1])*d2[0](i[2],i[3],ix)+C2[1](i[0],i[1])*d2[1](i[2],i[3],ix))*Q(i[0],i[2],eta)*Q(i[1],i[3],pi);
*/
        return a3;
    }

    void step_R1(double tau, const MatrixXd& R, const MatrixXd& X, const MatrixXd& Q, const MatrixXd& Y, MatrixXd& R1,
            const array<MatrixXd, 2>& C1, const array<MatrixXd, 2>& C2, const array<MatrixXd, 2>& E) {

        Tensor3 Qt({dd.r[0],dd.r[2],dd.r[1]});
        Qt.from_reshape21(Q, {0,1,2}); // just represent as a tensor (no reshaping)
        MatrixXd a1 = compute_a1(C1[0], Qt);
        MatrixXd a2 = compute_a2(C1[1], Qt, Y);

        MatrixXd b1 = compute_b1(X);

        array<Tensor3, 2> d2 = compute_d2(Y, E);
        timer t; t.start();
        Tensor4 EE  = compute_E(C2, X, Qt, d2);
        t.stop();
        cout << "compute_E: " << t.total() << endl;

        integrate([this,a1,a2,b1,EE](const MatrixXd& R, MatrixXd& out) {
                    rhs_R1(R, {a1, a2}, b1, EE, out);
                }, R, R1, tau);
    }

    void rhs_R1(const MatrixXd& R, const array<MatrixXd, 2>& a, const MatrixXd& b, const Tensor4& E,  MatrixXd& out) {
        out = b*R*a[0].transpose() + R*a[1].transpose();

        for(auto i : up_to<2>({dd.r[1],dd.r[1]}))
            for(auto j : up_to<2>({dd.r[1],dd.r[1]}))
                out(i[0],i[1]) += E(i[0],i[1],j[0],j[1])*R(j[0],j[1]);
    }

    MatrixXd compute_b1(const MatrixXd& X) {
        MatrixXd dX(X);
        cd_stencil(X, dX, dd.b[0]-dd.a[0]);
        return dot(X,dX, dd.h[0]);
    }

    array<Tensor4, 2> compute_e(const MatrixXd& X, array<Tensor3, 2> d2) {
        Tensor4 e_0({dd.r[1],dd.r[1],dd.r[2],dd.r[2]});
        Tensor4 e_1({dd.r[1],dd.r[1],dd.r[2],dd.r[2]});
        e_0.setZero();
        e_1.setZero();

        for(auto i : up_to<4>({dd.r[1],dd.r[1],dd.r[2],dd.r[2]}))
            for(int ix=0;ix<dd.n[0];ix++) {
                e_0(i) += dd.h[0]*X(ix, i[0])*X(ix, i[1])*d2[0](i[2],i[3],ix);
                e_1(i) += dd.h[0]*X(ix, i[0])*X(ix, i[1])*d2[1](i[2],i[3],ix);
            }

        return {e_0,e_1};
    }

    Tensor4 compute_E(const array<MatrixXd, 2>& C2, const MatrixXd& X, const Tensor3& Q, const array<Tensor3, 2>& d2) {
        Tensor4 E({dd.r[1],dd.r[1],dd.r[1],dd.r[1]});
        E.setZero();

        array<Tensor4, 2> e = compute_e(X, d2);
        
        // Reference implementation (not as fast)
        //for(auto i : up_to<4>({dd.r[1],dd.r[1],dd.r[1],dd.r[1]}))
        //    for(auto j : up_to<4>({dd.r[0],dd.r[0],dd.r[2],dd.r[2]}))
        //        E(i) += Q(j[0],j[2],i[1])*Q(j[1],j[3],i[3])*(C2[0](j[0],j[1])*e[0](i[0],i[2],j[2],j[3]) + C2[1](j[0],j[1])*e[1](i[0],i[2],j[2],j[3]));

        MatrixXd A({dd.r[0],dd.r[2]}), B({dd.r[0],dd.r[2]}), C({dd.r[0],dd.r[2]}), D({dd.r[0],dd.r[2]});
        for(auto i : up_to<4>({dd.r[1],dd.r[1],dd.r[1],dd.r[1]})) {
            A.setZero();
            C.setZero();
            for(auto j : up_to<4>({dd.r[0],dd.r[0],dd.r[2],1})) {
                A(j[1],j[2]) += Q(j[0],j[2],i[1])*C2[0](j[0],j[1]);
                C(j[1],j[2]) += Q(j[0],j[2],i[1])*C2[1](j[0],j[1]);
            }
            B.setZero();
            D.setZero();
            for(auto j : up_to<4>({1,dd.r[0],dd.r[2],dd.r[2]})) {
                B(j[1],j[2]) += Q(j[1],j[3],i[3])*e[0](i[0],i[2],j[2],j[3]);
                D(j[1],j[2]) += Q(j[1],j[3],i[3])*e[1](i[0],i[2],j[2],j[3]);
            }

            for(auto j : up_to<2>({dd.r[0],dd.r[2]}))
                E(i) += A(j[0],j[1])*B(j[0],j[1]) + C(j[0],j[1])*D(j[0],j[1]); 
        }

        return E;
    }

    void step_M(double tau, const Tensor3& M, const MatrixXd& X, const MatrixXd& Y, Tensor3& M1,
            const array<MatrixXd, 2>& C1, const array<MatrixXd, 2>& C2, const array<MatrixXd, 2>& E) {

        MatrixXd b1 = compute_b1(X);
        MatrixXd d1 = compute_d1(Y);

        array<Tensor3,2> d2 = compute_d2(Y, E);
        array<Tensor4, 2> e = compute_e(X, d2);

        timer t; t.start();
        integrate_tensor3([this,C1,C2,b1,d1,e](const Tensor3& M, Tensor3& out) {
                rhs_S(M, C1, C2, b1, d1, e, out); // the new RHS directly for S
                }, M, M1, tau, 1);
        t.stop(); cout << "rhs_S: " << t.total() << endl;
    }

    void rhs_S(const Tensor3& S, const array<MatrixXd, 2>& C1, const array<MatrixXd, 2>& C2, const MatrixXd& b1, const MatrixXd d1, const array<Tensor4, 2> e, Tensor3& out) {
        out.alloc(S.r);
        out.setZero();

        for(auto i : up_to<3>(dd.r)) {
            for(auto j : up_to<2>({dd.r[0],dd.r[1]}))
                out(i) -= C1[0](i[0],j[0])*b1(i[1],j[1])*S(j[0],j[1],i[2]);
            for(auto j : up_to<2>({dd.r[0],dd.r[2]}))
                out(i) -= C1[1](i[0],j[0])*d1(i[2],j[1])*S(j[0],i[1],j[1]);

            for(auto j : up_to<3>({dd.r[0],dd.r[1],dd.r[2]}))
                out(i) -= C2[0](i[0],j[0])*e[0](i[1],j[1],i[2],j[2])*S(j[0],j[1],j[2])
                        + C2[1](i[0],j[0])*e[1](i[1],j[1],i[2],j[2])*S(j[0],j[1],j[2]);
        }
    }
/*
    void rhs_M(const Tensor3& M, const array<MatrixXd, 2>& C1, const MatrixXd& b1, const MatrixXd d1, Tensor3& out) {
        out.alloc(M.r);
        out.setZero();
        for(auto i : up_to<3>({dd.r[2],dd.r[1],dd.r[0]})) {
            for(auto j : up_to<2>({dd.r[0],dd.r[2]}))
                out(i) -= C1[1](i[2],j[0])*d1(i[0],j[1])*M(j[1],i[1],j[0]);
            for(auto j : up_to<2>({dd.r[0],dd.r[1]}))
                out(i) -= C1[0](i[2],j[0])*b1(i[1],j[1])*M(i[0],j[1],j[0]);
        }
    }
*/
    MatrixXd compute_h1(const Tensor3& Q, const MatrixXd& c1y) {
        MatrixXd h1(dd.r[2],dd.r[2]);
        h1.setZero();
        for(auto i : up_to<2>({(int)h1.rows(),(int)h1.cols()}))
            for(auto j : up_to<3>({dd.r[1],dd.r[0],dd.r[0]}))
                h1(i[0],i[1]) += Q(j[0],j[1],i[0])*c1y(j[1],j[2])*Q(j[0],j[2],i[1]);
        return h1;
    }

    MatrixXd compute_h2(const Tensor3& Q, const MatrixXd& c1x, const MatrixXd& b1) {
        MatrixXd h2(dd.r[2],dd.r[2]);
        h2.setZero();
        for(auto i : up_to<2>({(int)h2.rows(),(int)h2.cols()}))
            for(auto j : up_to<4>({dd.r[1],dd.r[0],dd.r[0],dd.r[1]}))
                h2(i[0],i[1]) += Q(j[0],j[1],i[0])*c1x(j[1],j[2])*Q(j[3],j[2],i[1])*b1(j[0],j[3]);
        return h2;
    }

    array<Tensor3, 2> compute_b2(const MatrixXd& X, const array<MatrixXd, 2>& E) {
        Tensor3 b2_0({dd.r[1],dd.r[1],dd.n[1]});
        Tensor3 b2_1({dd.r[1],dd.r[1],dd.n[1]});
        b2_0.setZero();
        b2_1.setZero();

        for(auto i : up_to<3>({dd.r[1],dd.r[1],dd.n[1]}))
            for(int jx=0;jx<dd.n[0];jx++) {
                b2_0(i) += dd.h[0]*X(jx,i[0])*X(jx,i[1])*E[0](jx,i[2]);
                b2_1(i) += dd.h[0]*X(jx,i[0])*X(jx,i[1])*E[1](jx,i[2]);
            }

        return {b2_0, b2_1};
    }

    Tensor3 compute_h3(const Tensor3& Q, const array<MatrixXd, 2>& c2, const array<Tensor3, 2>& b2) {
        Tensor3 h3({dd.r[2],dd.r[2],dd.n[1]});
        h3.setZero();

        MatrixXd A(dd.r[1],dd.r[0]), C(dd.r[1],dd.r[0]), B(dd.r[1],dd.r[0]), D(dd.r[1],dd.r[0]);
        for(auto i : up_to<3>({dd.r[2],dd.r[2],dd.n[1]})) {
            A.setZero();
            C.setZero();
            for(auto j : up_to<4>({dd.r[1],dd.r[0],dd.r[0],1})) {
                A(j[0],j[2]) += Q(j[0],j[1],i[0])*c2[0](j[1],j[2]);
                C(j[0],j[2]) += Q(j[0],j[1],i[0])*c2[1](j[1],j[2]);
            }

            B.setZero();
            D.setZero();
            for(auto j : up_to<4>({dd.r[1],1,dd.r[0],dd.r[1]})) {
                B(j[0],j[2]) += Q(j[3],j[2],i[1])*b2[0](j[0],j[3],i[2]);
                D(j[0],j[2]) += Q(j[3],j[2],i[1])*b2[1](j[0],j[3],i[2]);
            }

            for(auto j : up_to<4>({dd.r[1],1,dd.r[0],1}))
                h3(i) += A(j[0],j[2])*B(j[0],j[2]) + C(j[0],j[2])*D(j[0],j[2]);

        }
/*
        for(auto i : up_to<3>({dd.r[2],dd.r[2],dd.n[1]}))
            for(auto j : up_to<4>({dd.r[1],dd.r[0],dd.r[0],dd.r[1]}))
                h3(i) += Q(j[0],j[1],i[0])*Q(j[3],j[2],i[1])*(c2[0](j[1],j[2])*b2[0](j[0],j[3],i[2])+c2[1](j[1],j[2])*b2[1](j[0],j[3],i[2]));
*/
        return h3;
    }


    Tensor4 compute_G(const Tensor3& Q, const array<MatrixXd, 2>& c2, const array<Tensor4, 2>& e) {
        Tensor4 G({dd.r[2],dd.r[2],dd.r[2],dd.r[2]});
        G.setZero();

        // Reference implementation (not as fast)
        //for(auto i : up_to<4>(G.r))
        //    for(auto j : up_to<4>({dd.r[1],dd.r[0],dd.r[0],dd.r[1]}))
        //        G(i) += Q(j[0],j[1],i[1])*Q(j[3],j[2],i[2])*(c2[0](j[1],j[2])*e[0](j[0],j[3],i[0],i[3])+c2[1](j[1],j[2])*e[1](j[0],j[3],i[0],i[3]));


        MatrixXd A(dd.r[1],dd.r[0]), B(dd.r[1],dd.r[0]), C(dd.r[1],dd.r[0]), D(dd.r[1],dd.r[0]);
        for(auto i : up_to<4>(G.r)) {
            A.setZero();
            C.setZero();
            for(auto j : up_to<4>({dd.r[1],dd.r[0],dd.r[0],1})) {
                A(j[0],j[2]) += Q(j[0],j[1],i[1])*c2[0](j[1],j[2]);
                C(j[0],j[2]) += Q(j[0],j[1],i[1])*c2[1](j[1],j[2]);
            }

            B.setZero();
            D.setZero();
            for(auto j : up_to<4>({dd.r[1],1,dd.r[0],dd.r[1]})) {
                B(j[0],j[2]) += Q(j[3],j[2],i[2])*e[0](j[0],j[3],i[0],i[3]);
                D(j[0],j[2]) += Q(j[3],j[2],i[2])*e[1](j[0],j[3],i[0],i[3]);
            }

            for(auto j : up_to<2>({dd.r[1],dd.r[0]}))
                G(i) += A(j[0],j[1])*B(j[0],j[1]) + C(j[0],j[1])*D(j[0],j[1]); 
        }

        return G;
    }

    void step_R2(double tau, const MatrixXd& R, const MatrixXd& X, const Tensor3& Q, const MatrixXd& Y, MatrixXd& R1,
            const array<MatrixXd, 2>& c1, const array<MatrixXd, 2>& c2, const array<MatrixXd, 2>& E) {
        MatrixXd h1 = compute_h1(Q, c1[1]);

        MatrixXd b1 = compute_b1(X);
        MatrixXd h2 = compute_h2(Q, c1[0], b1);

        MatrixXd d1 = compute_d1(Y);
        
        array<Tensor3, 2> d2 = compute_d2(Y, E);
        timer t1; t1.start();
        array<Tensor4, 2> e = compute_e(X, d2);
        t1.stop();
        cout << "compute_e: " << t1.total() << endl;
        timer t; t.start();
        Tensor4 G = compute_G(Q, c2, e);
        t.stop();
        cout << "compute_G: " << t.total() << endl;

        integrate([this,h1,h2,d1,G](const MatrixXd& R, MatrixXd& out) {
                    rhs_R2(R, {h1, h2}, d1, G, out);
                }, R, R1, tau);
    }

    void rhs_R2(const MatrixXd& R, const array<MatrixXd, 2>& h, const MatrixXd& d1, const Tensor4& G, MatrixXd& out) {
        //out = h[0]*R*d1.transpose() + h[1]*R;
        out = d1*R*h[0].transpose() + R*h[1].transpose();

        for(auto i : up_to<2>({dd.r[2],dd.r[2]}))
            for(auto j : up_to<2>({dd.r[2],dd.r[2]}))
                out(i[0],i[1]) += G(i[0],i[1],j[0],j[1])*R(j[1],j[0]);
    }

    void step_N(double tau, const MatrixXd& N, const MatrixXd& X, const Tensor3& Q, MatrixXd& N1,
            const array<MatrixXd, 2>& c1, const array<MatrixXd, 2>& c2, const array<MatrixXd, 2>& E) {

        MatrixXd h1 = compute_h1(Q, c1[1]);

        MatrixXd b1 = compute_b1(X);
        MatrixXd h2 = compute_h2(Q, c1[0], b1);
        
        array<Tensor3, 2> b2 = compute_b2(X, E);
        timer t; t.start();
        Tensor3 h3 = compute_h3(Q, c2, b2);
        t.stop(); cout << "h3: " << t.total() << endl;

        integrate([this,h1,h2,h3](const MatrixXd& N, MatrixXd& out) {
                    rhs_N(N, {h1, h2}, h3, out);
                }, N, N1, tau);

    }

    void rhs_N(const MatrixXd& N, const array<MatrixXd, 2>& h, const Tensor3& h3, MatrixXd& out) {
        MatrixXd dN(N);
        cd_stencil(N, dN, dd.b[1]-dd.a[1]);

        out = -dN*h[0].transpose() - N*h[1].transpose();

        for(auto i : up_to<2>({dd.n[1],dd.r[2]}))
            for(int j=0;j<dd.r[2];j++)
                out(i[0],i[1]) -= h3(i[1],j,i[0])*N(i[0],j);
    }

    // utility
    void cd_stencil(const MatrixXd& K, MatrixXd& out, double L) {
        // We assume that K and out are in Fortran-major order
        int n = K.rows();
        int r = K.cols();
        for(int j=0;j<r;j++) {
            fft(n, &K(0,j), hat);
            for(int k=0;k<n/2+1;k++) {
                double k_freq = double(k)*2.0*M_PI/L;
                hat[k] *= complex<double>(0,1)*k_freq/double(n);
            }
            inv_fft(n, hat, &out(0,j));
        }
    }

    MatrixXd dot(const MatrixXd& X1, const MatrixXd& X2, double h) {
        assert( X1.rows() == X2.rows() );
        MatrixXd D(X1.cols(), X2.cols());
        D.setZero();
        for(auto i : up_to<2>({(int)X1.cols(),(int)X2.cols()}))
            for(int j=0;j<X1.rows();j++)
                D(i[0],i[1]) += h*X1(j,i[0])*X2(j,i[1]);
        return D;
    }

};

struct continuoussplit_4dh {
    domain2d_data ddX, ddV;

    continuoussplit_2dh X, V;

    array<MatrixXd, 2> f_v;
    vector< complex<double> > hat1, hat2;

    continuoussplit_4dh(domain2d_data _ddX, domain2d_data _ddV) : ddX(_ddX), ddV(_ddV), X(_ddX), V(_ddV)  {
        // set f_v
        f_v[0].resize(ddV.n[0],ddV.n[1]);
        f_v[1].resize(ddV.n[0],ddV.n[1]);
        for(auto i : up_to<2>({ddV.n[0],ddV.n[1]})) {
            double v = ddV.a[0] + i[0]*ddV.h[0];
            double w = ddV.a[1] + i[1]*ddV.h[1];
            f_v[0](i[0],i[1]) = v;
            f_v[1](i[0],i[1]) = w;
        }

        hat1.resize(ddX.n[0]*ddX.n[1]);
        hat2.resize(ddX.n[0]*ddX.n[1]);
    }

    void Lie_step(double tau, const lowrank4dh& in, lowrank4dh& out) {
        timer t_coeff, t_E, t_step, t_qr, t_total;
        t_total.start();

        t_coeff.start();
        array<MatrixXd, 2> C1 = compute_C1D1(in.V, ddV, f_v);
        array<MatrixXd, 2> C2 = compute_C2D2(in.V, ddV);
        t_coeff.stop();
        t_E.start();
        array<MatrixXd, 2> E  = electric_field(in);
        t_E.stop();

        // Set up K
        lowrank2dh K  = in.X;
        lowrank2dh K1 = in.X;
        K.S.setZero();
        for(auto i : up_to<3>(K.S.r))
            for(int alpha=0;alpha<in.S.rows();alpha++)
                K.S(i) += in.X.S(alpha,i[1],i[2])*in.S(alpha,i[0]);
        // K step
        t_step.start();
        X.Lie_step(tau, K, K1, C1, C2, E);
        t_step.stop();
        // do the QR decomposition
        t_qr.start();
        MatrixXd S1(K1.S.r[0],K1.S.r[0]);
        QR_2dh(K1, out.X, S1);
        t_qr.stop();

        t_coeff.start();
        array<MatrixXd, 2> D1 = compute_C1D1(out.X, ddX, E);
        array<MatrixXd, 2> D2 = compute_C2D2(out.X, ddX);
        t_coeff.stop();

        // S step
        step_S(tau, S1, out.S, C1, C2, D1, D2);

        // Set up L
        lowrank2dh L = in.V;
        lowrank2dh L1 = in.V;
        L.S.setZero();
        for(auto i : up_to<3>(L.S.r))
            for(int beta=0;beta<out.S.cols();beta++)
                L.S(i) += in.V.S(beta,i[1],i[2])*out.S(i[0],beta);
        // L step
        t_step.start();
        V.Lie_step(tau, L, L1, D1, D2, f_v);
        t_step.stop();
        // do the QR decomposition
        t_qr.start();
        MatrixXd R2(L1.S.r[0],L1.S.r[0]);
        QR_2dh(L1, out.V, R2);
        out.S = R2.transpose();
        t_qr.stop();

        t_total.stop();
        cout << "cs4dh. Total: " << t_total.total() << " Step: " << t_step.total() << " Coeff: " << t_coeff.total() << " E: " << t_E.total() << " QR: " << t_qr.stop() << " Remainder: " << t_total.total()-t_step.total()-t_coeff.total()-t_E.total()-t_qr.total() << endl;
    }

    void Lie_forward(double tau, const lowrank4dh& in, lowrank4dh& out, const array<MatrixXd, 2>& E,
            const array<MatrixXd, 2>& C1, const array<MatrixXd, 2>& C2,
            const array<MatrixXd, 2>& D1, const array<MatrixXd, 2>& D2) {

        // Set up K
        lowrank2dh K  = in.X;
        lowrank2dh K1 = in.X;
        K.S.setZero();
        for(auto i : up_to<3>(K.S.r))
            for(int alpha=0;alpha<in.S.rows();alpha++)
                K.S(i) += in.X.S(alpha,i[1],i[2])*in.S(alpha,i[0]);
        // 1/2 K step
        X.Lie_step(tau, K, K1, C1, C2, E);
        // do the QR decomposition
        MatrixXd S1(K1.S.r[0],K1.S.r[0]);
        QR_2dh(K1, out.X, S1);
        
        // 1/2 S step
        step_S(tau, S1, out.S, C1, C2, D1, D2);

        // Set up L
        lowrank2dh L = in.V;
        lowrank2dh L1 = in.V;
        L.S.setZero();
        for(auto i : up_to<3>(L.S.r))
            for(int beta=0;beta<out.S.cols();beta++)
                L.S(i) += in.V.S(beta,i[1],i[2])*out.S(i[0],beta);
        // 1/2 L step
        V.Lie_step(tau, L, L1, D1, D2, f_v);
        // do the QR decomposition
        MatrixXd R2(L1.S.r[0],L1.S.r[0]);
        QR_2dh(L1, out.V, R2);
        out.S = R2.transpose();
    }

    void Lie_backward(double tau, const lowrank4dh& in, lowrank4dh& out, const array<MatrixXd, 2>& E,
            const array<MatrixXd, 2>& C1, const array<MatrixXd, 2>& C2,
            const array<MatrixXd, 2>& D1, const array<MatrixXd, 2>& D2) {

        // Set up L
        lowrank2dh L = in.V;
        lowrank2dh L1 = in.V;
        L.S.setZero();
        for(auto i : up_to<3>(L.S.r))
            for(int beta=0;beta<out.S.cols();beta++)
                L.S(i) += in.V.S(beta,i[1],i[2])*in.S(i[0],beta);
        // 1/2 L step
        V.Lie_step(tau, L, L1, D1, D2, f_v);
        // do the QR decomposition
        MatrixXd R(L1.S.r[0],L1.S.r[0]);
        QR_2dh(L1, out.V, R);

        // 1/2 S step
        step_S(tau, R.transpose(), out.S, C1, C2, D1, D2);

        // Set up K
        lowrank2dh K  = in.X;
        lowrank2dh K1 = in.X;
        K.S.setZero();
        for(auto i : up_to<3>(K.S.r))
            for(int alpha=0;alpha<in.S.rows();alpha++)
                K.S(i) += in.X.S(alpha,i[1],i[2])*out.S(alpha,i[0]);
        // 1/2 K step
        X.Lie_step(tau, K, K1, C1, C2, E);
        // do the QR decomposition
        QR_2dh(K1, out.X, out.S);
    }

    void Strang_step(double tau, const lowrank4dh& in, lowrank4dh& out) {
        // compute coefficients
        array<MatrixXd, 2> C1 = compute_C1D1(in.V, ddV, f_v);
        array<MatrixXd, 2> C2 = compute_C2D2(in.V, ddV);
        array<MatrixXd, 2> E  = electric_field(in);
        array<MatrixXd, 2> D1 = compute_C1D1(in.X, ddX, E);
        array<MatrixXd, 2> D2 = compute_C2D2(in.X, ddX);

        lowrank4dh f12 = in;
        Lie_forward(0.5*tau, in, f12, E, C1, C2, D1, D2);
        
        C1 = compute_C1D1(f12.V, ddV, f_v);
        C2 = compute_C2D2(f12.V, ddV);
        E  = electric_field(f12);
        D1 = compute_C1D1(f12.X, ddX, E);
        D2 = compute_C2D2(f12.X, ddX);
        
        Lie_forward(0.5*tau, in, f12, E, C1, C2, D1, D2);
        Lie_backward(0.5*tau, f12, out, E, C1, C2, D1, D2);
    }

    void step_S(double tau, const MatrixXd& S, MatrixXd& S_out,
            const array<MatrixXd, 2>& C1, const array<MatrixXd, 2>& C2,
            const array<MatrixXd, 2>& D1, const array<MatrixXd, 2>& D2) {

        integrate([this,C1,C2,D1,D2](const MatrixXd& S, MatrixXd& _out) {
                rhs_S(S,_out,C1,C2,D1,D2);
                }, S, S_out, tau);

    }

    void rhs_S(const MatrixXd& S, MatrixXd& out,
            const array<MatrixXd, 2>& C1, const array<MatrixXd, 2>& C2,
            const array<MatrixXd, 2>& D1, const array<MatrixXd, 2>& D2) { 
        // This is the linear term
        out = D2[0]*S*C1[0].transpose() + D2[1]*S*C1[1].transpose();

        // This is the second term
        out += D1[0]*S*C2[0].transpose() + D1[1]*S*C2[1].transpose();
    }

    array<MatrixXd, 2> compute_C1D1(const lowrank2dh& V, const domain2d_data& dd,
            array<MatrixXd,2> vE) {
        array<MatrixXd, 2> C1;
        C1[0] = MatrixXd::Zero(dd.r[0],dd.r[0]);
        C1[1] = MatrixXd::Zero(dd.r[0],dd.r[0]);

        vector<MatrixXd> dense_V = V.full();

        double hsq = dd.h[0]*dd.h[1];
        for(auto i : up_to<2>({dd.r[0],dd.r[0]}))
            for(auto j : up_to<2>({dd.n[0],dd.n[1]})) {

                double VV = dense_V[i[0]](j[0],j[1])*dense_V[i[1]](j[0],j[1]);

                C1[0](i[0],i[1]) += vE[0](j[0],j[1])*hsq*VV;
                C1[1](i[0],i[1]) += vE[1](j[0],j[1])*hsq*VV;
            }

        return C1;
    }

    array<MatrixXd, 2> compute_C2D2(const lowrank2dh& V, const domain2d_data& dd) {
        array<MatrixXd, 2> C2;
        C2[0] = MatrixXd::Zero(dd.r[0],dd.r[0]);
        C2[1] = MatrixXd::Zero(dd.r[0],dd.r[0]);

        vector<MatrixXd> dense_V = V.full();

        lowrank2dh dxV = V;
        this->V.cd_stencil(V.X, dxV.X, dd.b[0]-dd.a[0]);
        vector<MatrixXd> dx_V = dxV.full();

        lowrank2dh dyV = V;
        this->V.cd_stencil(V.Y, dyV.Y, dd.b[1]-dd.a[1]);
        vector<MatrixXd> dy_V = dyV.full();

        double hsq = dd.h[0]*dd.h[1];
        for(auto i : up_to<2>({dd.r[0],dd.r[0]}))
            for(auto j : up_to<2>({dd.n[0],dd.n[1]})) {
                double VV = dense_V[i[0]](j[0],j[1]);

                C2[0](i[0],i[1]) += hsq*VV*dx_V[i[1]](j[0],j[1]);
                C2[1](i[0],i[1]) += hsq*VV*dy_V[i[1]](j[0],j[1]);
            }

        return C2;
    }

    MatrixXd rho(const lowrank4dh& in) {
        // compute the integrall over V and W
        vector<double> int_V = integral(in.V.X, ddV.h[0]);
        vector<double> int_W = integral(in.V.Y, ddV.h[1]);

        // compute the integral over v
        vector<double> rhov(ddV.r[0]);
        fill(rhov.begin(), rhov.end(), 0.0);
        for(int i=0;i<(int)rhov.size();i++)
            for(auto j : up_to<2>({ddV.r[1],ddV.r[2]}))
                rhov[i] += in.V.S(i,j[0],j[1])*int_V[j[0]]*int_W[j[1]];

        // setup rho as a two-dimensional array
        MatrixXd rho;
        rho = -MatrixXd::Ones(ddX.n[0],ddX.n[1]); // -1.0 in rho
        vector<MatrixXd> dense_X = in.X.full();
        for(auto i : up_to<2>({ddX.n[0],ddX.n[1]}))
            for(auto j : up_to<2>({(int)in.S.rows(),(int)in.S.cols()}))
                    rho(i[0],i[1]) += dense_X[j[0]](i[0],i[1])*in.S(j[0],j[1])*rhov[j[1]];

        return rho;
    }

    array<MatrixXd, 2> electric_field(const lowrank4dh& in) {
        MatrixXd mrho = rho(in);

        // compute the potential
        int nx = ddX.n[0];
        int ny = ddX.n[1];
        fftw_plan p;
        p = fftw_plan_dft_r2c_2d(ny,nx,&mrho(0,0),(fftw_complex*)&hat1[0],FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
        fftw_execute(p);
        fftw_destroy_plan(p);

        array<MatrixXd, 2> E;
        E[0].resize(nx,ny);
        E[1].resize(nx,ny);
        for(int i=0;i<nx/2+1;i++) {
            for(int j=0;j<ny;j++) {
                double i_freq = i; // in this case there are no negative frequencies
                double j_freq = (j<ny/2+1) ? j : -(ny-j); // this expression is dangerous if ny is unsigned.
                i_freq *= 2*M_PI/(ddX.b[0]-ddX.a[0]);
                j_freq *= 2*M_PI/(ddX.b[1]-ddX.a[1]);

                complex<double>& vv = hat1[i + j*(nx/2+1)];
                if(i==0 && j==0)
                    vv = 0.0;
                else
                    vv *= -1.0/(pow(i_freq,2)+pow(j_freq,2))/double(nx*ny);

                complex<double>& vv2 = hat2[i + j*(nx/2+1)];
                vv2 = vv*complex<double>(0,1)*j_freq;
                vv *= complex<double>(0,1)*i_freq;
            }
        }

        p = fftw_plan_dft_c2r_2d(ny,nx,(fftw_complex*)&hat1[0],&E[0](0,0),FFTW_ESTIMATE);
        fftw_execute(p);
        fftw_destroy_plan(p);

        p = fftw_plan_dft_c2r_2d(ny,nx,(fftw_complex*)&hat2[0],&E[1](0,0),FFTW_ESTIMATE);
        fftw_execute(p);
        fftw_destroy_plan(p);

        return E;
    }

    double electric_field_energy(const lowrank4dh& in) {
        array<MatrixXd, 2> E = electric_field(in);

        double ee=0.0;
        double hsq = ddX.h[0]*ddX.h[1];
        for(auto i : up_to<2>({(int)E[0].rows(),(int)E[0].cols()}))
            ee += hsq*0.5*(pow(E[0](i[0],i[1]),2)+pow(E[1](i[0],i[1]),2));

        return ee;
    }

    vector<double> integral(const MatrixXd& V, double h) {
        vector<double> out(V.cols());
        fill(out.begin(), out.end(), 0.0);
        for(int i=0;i<V.cols();i++)
            for(int j=0;j<V.rows();j++)
                out[i] += h*V(j,i);
        return out;
    }

};


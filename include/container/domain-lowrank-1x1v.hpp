#pragma once

#include <generic/common.hpp>
#include <generic/netcdf.hpp>
#include <generic/qr.hpp>
#include <Eigen/SVD>
#include <boost/operators.hpp>

struct lowrank2d  :
    boost::additive1<lowrank2d ,
    boost::additive2<lowrank2d,double,
    boost::multiplicative2<lowrank2d,double>>> {

    MatrixXd X, V, S;

    lowrank2d() {}

    lowrank2d(int r, array<int,2> extension)
        : X(extension[0],r), V(extension[1],r), S(r,r) {}

    MatrixXd full() const {
        return X*S*V.transpose();
    }

    void orthogonalize(double hx, double hv) {

        rectangular_QR qrx(X);
        X = qrx.Q()/sqrt(hx);

        rectangular_QR qrv(V);
        V = qrv.Q()/sqrt(hv);

        S = sqrt(hx*hv)*qrx.R()*S*qrv.R().transpose();
    }

    lowrank2d& operator+=(const lowrank2d &p)
    {
        X += p.X;
        S += p.S;
        V += p.V;
        return *this;
    }

    lowrank2d& operator*=(const double c)
    {
        X *= c;
        S *= c;
        V *= c;
        return *this;
    }
};

void set_from_func(MatrixXd& mat, function<double(double,int)> f, double a, double h) {
    for(int idx=0;idx<mat.cols();idx++)
        for(int i=0;i<mat.rows();i++) {
            double x = a + i*h;
            mat(i,idx) = f(x,idx);
        }
}

MatrixXd gram_schmidt(const MatrixXd& _V, MatrixXd& R,
        function<double(const VectorXd&,const VectorXd&)> inner_product) {
    // do modified Gram-Schmidt (necessary for f0v!=1)
    int r = _V.cols();
    R.resize(r,r);

    MatrixXd V = _V;
    MatrixXd v = V;
    R.setZero();
    for(int i=0;i<r;i++) {
        V.col(i) = v.col(i);
        for(int j=0;j<i;j++) {
            R(j,i) = inner_product(V.col(j), v.col(i));
            V.col(i) -= R(j,i)*V.col(j);
        }
        double nrm = sqrt(inner_product(V.col(i), V.col(i)));

        if(nrm < 1e-13) {
            VectorXd nv(V.rows());
            for(Index k=0;k<Index(nv.size());k++)
                nv[i] = cos(2.0*M_PI*i*k/double(nv.size()));
            v.col(i) = nv;
            i--; // redo the current step with artifical data
        } else {
            V.col(i) = V.col(i)/nrm;
            R(i,i) = inner_product(v.col(i),V.col(i));
        }
    }

    return V;
}

void orthogonalize_mgs(MatrixXd& X, MatrixXd& S, MatrixXd& V,
        function<double(const VectorXd&,const VectorXd&)> inner_product,
        double hx, double hv) {
    int r = S.rows();

    rectangular_QR qrx(X);
    X = qrx.Q()/sqrt(hx);

    MatrixXd R(r,r);
    V = gram_schmidt(V, R, inner_product);

    S = sqrt(hx)*qrx.R()*S*R.transpose();
}

enum class lowrank_type { normalized_matrix, normalized_integral };

struct domain_lowrank2d {
    array<double,2> a;
    array<double,2> b;
    array<double,2> h;

    lowrank2d lr;
    lowrank_type type;

    function<double(double)> f0v;

    domain_lowrank2d(array<double,2> _a, array<double,2> _b,
            int r, array<int,2> _extension, lowrank_type _type,
            function<double(double)> _f0v = [](double) { return 1.0; })
        : a(_a), b(_b), lr(r, _extension), type(_type), f0v(_f0v) {
            h[0] = (b[0]-a[0])/double(_extension[0]);
            h[1] = (b[1]-a[1])/double(_extension[1]);
        }

    void init(function<double(double)> X, function<double(double)> V) {
        lr.X.setIdentity();
        lr.V.setIdentity();
        double l1 = set1(lr.X, X, a[0], h[0]);
        double l2 = set1(lr.V, V, a[1], h[1]);
        lr.S.setZero(); lr.S(0,0)=l1*l2;

        // The representation above is not yet orthogonal. This is fixed by doing two
        // QR decompositions.
        double hx = 1.0;
        double hv = 1.0;
        if(type == lowrank_type::normalized_integral) {
            hx = h[0];
            hv = h[1];
        }

        // lr.orthogonalize(hx, hv);
        orthogonalize_mgs(lr.X, lr.S, lr.V,
                [this](const VectorXd& v1, const VectorXd& v2) {
                    return inner_product(v1, v2);
                }, hx, hv);

        //BDCSVD<MatrixXd> svd(lr.full(), ComputeFullU | ComputeFullV);
        //lr.X = svd.matrixU().block(0,0,lr.X.rows(),lr.X.cols());
        //lr.V = svd.matrixV().block(0,0,lr.V.rows(),lr.V.cols());
        //lr.S = svd.singularValues().head(lr.S.rows()).asDiagonal();
    }

    void init(int rank, function<double(double,int)> X, function<double(double,int)> V) {
        lr.X.setIdentity();
        lr.V.setIdentity();
        lr.S.setZero();
        for(int k=0;k<rank;k++) {
            double l1 = set1(lr.X, [k,X](double x) { return X(x,k); }, a[0], h[0], k);
            double l2 = set1(lr.V, [k,V](double v) { return V(v,k); }, a[1], h[1], k);
            lr.S(k,k)=l1*l2;
        }

        // The representation above is not yet orthogonal. This is fixed by doing two
        // QR decompositions.
        double hx = 1.0;
        double hv = 1.0;
        if(type == lowrank_type::normalized_integral) {
            hx = h[0];
            hv = h[1];
        }

        //lr.orthogonalize(hx, hv);
        orthogonalize_mgs(lr.X, lr.S, lr.V,
                [this](const VectorXd& v1, const VectorXd& v2) {
                    return inner_product(v1, v2);
                }, hx, hv);
    }

    double inner_product(const VectorXd& v1, const VectorXd& v2) {
        int n = v1.size();
        double res = 0.0;
        for(int i=0;i<n;i++)
            res += f0v(v(i))*v1(i)*v2(i)*h[1];
        return res;
    }

    void diagonalize_S() {
        Eigen::BDCSVD<MatrixXd> svd(lr.S, Eigen::ComputeFullU | Eigen::ComputeFullV);
        MatrixXd U = svd.matrixU();
        MatrixXd V = svd.matrixV();
        MatrixXd D = svd.singularValues().asDiagonal();

        lr.X = lr.X*U;
        lr.V = lr.V*V;
        lr.S = D;
    }

    void add_to(const domain_lowrank2d& f1) {
        // Set up a low-rank representation that includes both this and f1
        int r = lr.S.rows();
        array<int,2> n = {lr.X.rows(), lr.V.rows()};
        domain_lowrank2d tmp(a, b, 2*r, n, type);
        
        tmp.lr.S.setZero();
        tmp.lr.S.block(0,0,r,r)     = this->lr.S;
        tmp.lr.S.block(r,r,r,r) = f1.lr.S;

        tmp.lr.X.setZero();
        tmp.lr.X.block(0,0,n[0],r)   = this->lr.X;
        tmp.lr.X.block(0,r,n[0],r) = f1.lr.X;
        
        tmp.lr.V.setZero();
        tmp.lr.V.block(0,0,n[1],r)   = this->lr.V;
        tmp.lr.V.block(0,r,n[1],r) = f1.lr.V;

        // The representation above is not yet orthogonal. This is fixed by doing two
        // QR decompositions.
        double hx = 1.0;
        double hv = 1.0;
        if(type == lowrank_type::normalized_integral) {
            hx = h[0];
            hv = h[1];
        }
        tmp.lr.orthogonalize(hx, hv);

        // Diagonalize S
        tmp.diagonalize_S();

        // use the first r entries (gives the best possible approximation)
        this->lr.X = tmp.lr.X.block(0,0,n[0],r);
        this->lr.V = tmp.lr.V.block(0,0,n[1],r);
        this->lr.S = tmp.lr.S.block(0,0,r,r);
    }

    void write_nc(NCWriter& nc, string name) {
        MatrixXd m_full = full();
        nc.write(name, {m_full.cols(), m_full.rows()}, m_full.data(), {"v", "x"});
    }

    double v(int j) const {
        return a[1] + j*h[1];
    }

    MatrixXd full() const {
        MatrixXd M = lr.full();
        for(int i=0;i<M.rows();i++)
            for(int j=0;j<M.cols();j++)
                M(i,j) *= f0v(v(j));
        return M;
    }

    VectorXd singular_values() {
        Eigen::JacobiSVD<MatrixXd> svd(lr.S);
        return svd.singularValues();
    }

    double mass() {
        VectorXd int_X = integrate_x(lr.X, [](double) { return 1.0;});
        VectorXd int_V = integrate_v(lr.V, [this](double v) { return f0v(v);});
        return int_X.transpose()*lr.S*int_V;
    }

    double momentum() {
        VectorXd int_X = integrate_x(lr.X, [](double x) { return 1.0;});
        VectorXd int_V = integrate_v(lr.V, [this](double v) { return v*f0v(v);});
        return int_X.transpose()*lr.S*int_V;

    }

    double kin_energy() {
        VectorXd int_X = integrate_x(lr.X, [](double x) { return 1.0;});
        VectorXd int_V = integrate_v(lr.V, [this](double v) { return 0.5*pow(v,2)*f0v(v);});
        return int_X.transpose()*lr.S*int_V;
    }

    double momentum_4d(int idx) {
        VectorXd int_X = integrate_x(lr.X, [idx](double x) {
                    if(idx==0)  return 0.5*pow(x,2);
                    else        return 1.0;
                });
        VectorXd int_V = integrate_v(lr.V, [idx](double v) { 
                    if(idx==1) return 0.5*pow(v,2);
                    else       return 1.0;
                });
        return int_X.transpose()*lr.S*int_V;
    }

    double kin_energy_4d() {
        VectorXd int_X = integrate_x(lr.X, [](double x) { return 0.5*pow(x,2);});
        VectorXd int_V = integrate_v(lr.V, [](double v) { return 1.0;});
        double ke1 = int_X.transpose()*lr.S*int_V;
        
        int_X = integrate_x(lr.X, [](double x) { return 1.0;});
        int_V = integrate_v(lr.V, [](double v) { return 0.5*pow(v,2);});
        double ke2 = int_X.transpose()*lr.S*int_V;

        return ke1+ke2;
    }

    VectorXd integrate_x(const MatrixXd& X, function<double(double)> f) {
        VectorXd out(X.cols());
        out.setZero();
        for(int k=0;k<X.cols();k++)
            for(int i=0;i<X.rows();i++) {
                double x = a[0] + i*h[0];
                out(k) += h[0]*X(i,k)*f(x);
            }
        return out;
    }

    VectorXd integrate_v(const MatrixXd& V, function<double(double)> f) {
        VectorXd out(V.cols());
        out.setZero();
        for(int k=0;k<V.cols();k++)
            for(int i=0;i<V.rows();i++) {
                double v = a[1] + i*h[1];
                out(k) += h[1]*V(i,k)*f(v);
            }
        return out;
    }

    double l1() {
        return abs(lr.full().array()).sum()*h[0]*h[1];
    }

    double l2() {
        auto full_arr = lr.full();
        return (full_arr.array()*full_arr.array()).sum()*h[0]*h[1];
    }

    double entropy() {
        auto full_arr = lr.full();
        return (full_arr.array()*log(abs(full_arr.array()))).sum()*h[0]*h[1];
    }

    private:
    // returns the l^2 norm of the vector written
    double set1(MatrixXd& mat, function<double(double)> f, double a, double h, int idx=0) {
        double l2 = 0.0;
        for(int i=0;i<mat.rows();i++) {
            double x = a + i*h;
            mat(i,idx) = f(x);
            l2 += pow(mat(i,idx),2);
        }
        l2 = sqrt(l2);
        for(int i=0;i<mat.rows();i++)
            mat(i,idx) /= l2;
        return l2;
    }

};


double error_inf(const MatrixXd& v1, const MatrixXd& v2) {
    double s = 0.0;
    for(int i=0;i<v1.rows();i++)
        for(int j=0;j<v2.cols();j++)
            s = max(s,abs(v1(i,j)-v2(i,j)));
    return s;
}

double error_inf(const domain_lowrank2d& dom, function<double(double,double)> f) {
    MatrixXd f_full = dom.full();
    double s = 0.0;
    for(int i=0;i<f_full.rows();i++)
        for(int j=0;j<f_full.cols();j++) {
            double x = dom.a[0] + i*dom.h[0];
            double v = dom.a[1] + j*dom.h[1];
            s = max(s,abs(f_full(i,j)-f(x,v)));
        }
    return s;
}


#pragma once

#include <generic/common.hpp>
#include <generic/tensors.hpp>
#include <container/domain-lowrank-1x1v.hpp>


struct lowrank2dh {
    MatrixXd X, Y;
    Tensor3 S;

    lowrank2dh(array<int,3> r, array<int,2> extension)
        : X(extension[0],r[1]), Y(extension[1],r[2]), S(r) {}

    MatrixXd full(int idx) const {
        return X*S(idx)*Y.transpose();
    }

    vector<MatrixXd> full() const {
        vector<MatrixXd> out(S.r[0]);
        for(int idx=0;idx<S.r[0];idx++)
            out[idx] = full(idx);
        return out;
    }
};

struct lowrank4dh {
    lowrank2dh X, V;
    MatrixXd S;

    lowrank4dh(array<int,3> rX, array<int,2> nX, array<int,3> rV, array<int,2> nV)
        : X(rX, nX), V(rV, nV), S(rX[0],rV[0]) {
    }

    Tensor4 full() const {
        vector<MatrixXd> XX = X.full();
        vector<MatrixXd> VV = V.full();
        Tensor4 f({(int)XX[0].rows(), (int)XX[0].cols(), (int)VV[0].rows(), (int)VV[0].cols()});
        f.setZero();
        for(auto i : up_to<4>(f.r))
            for(auto j : up_to<2>({(int)XX.size(), (int)VV.size()}))
                f(i) += S(j[0],j[1])*XX[j[0]](i[0],i[1])*VV[j[0]](i[2],i[3]);
        return f;
    }
};

struct domain2d_data {
    array<double,2> a;
    array<double,2> b;
    array<int,2>    n;
    array<int,3>    r;
    array<double,2> h;

    domain2d_data(array<double,2> _a, array<double,2> _b,
            array<int,2> _n, array<int,3> _r) : a(_a), b(_b), n(_n), r(_r) {
        h[0] = (b[0]-a[0])/double(n[0]);
        h[1] = (b[1]-a[1])/double(n[1]);
    }
};


double dot(const lowrank2dh& in1, int gamma1, const lowrank2dh& in2, int gamma2) {
    double s = 0.0;
    for(int delta=0;delta<in1.S.r[1];delta++)
        for(int epsilon=0;epsilon<in1.S.r[1];epsilon++)
            s += in1.S(gamma1,delta,epsilon)*in2.S(gamma2,delta,epsilon);
    return s;
}

void QR_2dh(const lowrank2dh& in, lowrank2dh& Q, MatrixXd& R) {
    int cols=in.S.r[0];
    Q = in; // takes care of the initialization
    R.resize(cols,cols);
    R.setZero();
    for(int k=0;k<cols;k++) {
        for(int j=0;j<k;j++) {
            R(j,k) = dot(Q,j,Q,k);

            for(auto i : up_to<2>({in.S.r[1],in.S.r[2]}))
                Q.S(k,i[0],i[1]) -= R(j,k)*Q.S(j,i[0],i[1]);
        }
        // normalize vector Q^k
        R(k,k) = sqrt(dot(Q,k,Q,k));
        for(auto i : up_to<2>({in.S.r[1],in.S.r[2]}))
            Q.S(k,i[0],i[1]) /= R(k,k);
    }
}

struct domain_lowrank4d {
    domain2d_data ddX, ddV;

    lowrank4dh lr;

    domain_lowrank4d(domain2d_data _ddX, domain2d_data _ddV) : ddX(_ddX), ddV(_ddV),
        lr(_ddX.r, _ddX.n, _ddV.r, _ddV.n) {}

    void init(int rankX, function<double(double,int)> X, function<double(double,int)> Y,
              int rankV, function<double(double,int)> V, function<double(double,int)> W) {

        // due to a limiation of domain_lowrank2d we have to have equal ranks for now
        assert(ddX.r[1] == ddX.r[2]);
        assert(ddV.r[1] == ddV.r[2]);
        domain_lowrank2d XX(ddX.a, ddX.b, ddX.r[1], ddX.n, lowrank_type::normalized_integral);
        domain_lowrank2d VV(ddV.a, ddV.b, ddV.r[1], ddV.n, lowrank_type::normalized_integral);

        XX.init(rankX, X, Y);
        VV.init(rankV, V, W);

        lr.X.X = XX.lr.X;
        lr.X.Y = XX.lr.V;
        lr.X.S.setZero();
        for(int k=0;k<lr.X.S.r[0];k++)
            for(int i=0;i<lr.X.S.r[1];i++)
                for(int j=0;j<lr.X.S.r[2];j++)
                    if(k==0)
                        lr.X.S(k,i,j) = XX.lr.S(i,j);
                    else
                        lr.X.S(k,i,j) = 1.0*(i==j && i==k);

        lr.V.X = VV.lr.X;
        lr.V.Y = VV.lr.V;
        lr.V.S.setZero();
        for(int k=0;k<lr.V.S.r[0];k++)
            for(int i=0;i<lr.V.S.r[1];i++)
                for(int j=0;j<lr.V.S.r[2];j++)
                    if(k==0)
                        lr.V.S(k,i,j) = VV.lr.S(i,j);
                    else
                        lr.V.S(k,i,j) = 1.0*(i==j && i==k);

        MatrixXd R1(lr.X.S.r[0], lr.X.S.r[0]);
        lowrank2dh lrX = lr.X;
        QR_2dh(lrX, lr.X, R1);

        MatrixXd R2(lr.V.S.r[1], lr.V.S.r[1]);
        lowrank2dh lrV = lr.V;
        QR_2dh(lrV, lr.V, R2);

        lr.S.setZero();
        lr.S(0,0) = 1.0;
        lr.S = R1*lr.S*R2.transpose();
    }

    Tensor4 full() const {
        return lr.full();
    }

    domain_lowrank2d dom2d_from_dom2dh(const lowrank2dh& in, domain2d_data dd) {
        assert(ddX.r[1] == ddX.r[2]);
        domain_lowrank2d ret(ddX.a, ddX.b, ddX.r[1], ddX.n, lowrank_type::normalized_integral);
        ret.lr.X = in.X;
        ret.lr.V = in.Y;

        return ret;
    }

    double mass() {
        domain_lowrank2d dX = dom2d_from_dom2dh(lr.X, ddX);
        VectorXd mass_X(ddX.r[0]);
        for(int i=0;i<ddX.r[0];i++) {
            dX.lr.S = lr.X.S(i);
            mass_X[i] = dX.mass();
        }
        
        domain_lowrank2d dV = dom2d_from_dom2dh(lr.V, ddV);
        VectorXd mass_V(ddV.r[0]);
        for(int i=0;i<ddV.r[0];i++) {
            dV.lr.S = lr.V.S(i);
            mass_V[i] = dV.mass();
        }

        return mass_X.transpose()*lr.S*mass_V;
    }

    double momentum(int idx) {
        domain_lowrank2d dX = dom2d_from_dom2dh(lr.X, ddX);
        VectorXd mass_X(ddX.r[0]);
        for(int i=0;i<ddX.r[0];i++) {
            dX.lr.S = lr.X.S(i);
            mass_X[i] = dX.mass();
        }
        
        domain_lowrank2d dV = dom2d_from_dom2dh(lr.V, ddV);
        VectorXd ke(ddV.r[0]);
        for(int i=0;i<ddV.r[0];i++) {
            dV.lr.S = lr.V.S(i);
            ke[i] = dV.momentum_4d(idx);
        }

        return mass_X.transpose()*lr.S*ke;
    }

    double kin_energy() {
        domain_lowrank2d dX = dom2d_from_dom2dh(lr.X, ddX);
        VectorXd mass_X(ddX.r[0]);
        for(int i=0;i<ddX.r[0];i++) {
            dX.lr.S = lr.X.S(i);
            mass_X[i] = dX.mass();
        }
        
        domain_lowrank2d dV = dom2d_from_dom2dh(lr.V, ddV);
        VectorXd ke(ddV.r[0]);
        for(int i=0;i<ddV.r[0];i++) {
            dV.lr.S = lr.V.S(i);
            ke[i] = dV.kin_energy_4d();
        }

        return mass_X.transpose()*lr.S*ke;
    }

    double l1(const Tensor4& dense_f) {
        double s = 0.0;
        double h = ddX.h[0]*ddX.h[1]*ddV.h[0]*ddV.h[1];
        for(auto i : up_to<4>(dense_f.r))
            s += abs(dense_f(i))*h;
        return s;
    }

    double l2(const Tensor4& dense_f) {
        double s = 0.0;
        double h = ddX.h[0]*ddX.h[1]*ddV.h[0]*ddV.h[1];
        for(auto i : up_to<4>(dense_f.r))
            s += dense_f(i)*dense_f(i)*h;
        return sqrt(s);
    }

    double entropy(const Tensor4& dense_f) {
        double s = 0.0;
        double h = ddX.h[0]*ddX.h[1]*ddV.h[0]*ddV.h[1];
        for(auto i : up_to<4>(dense_f.r))
            s += dense_f(i)*log(abs(dense_f(i)))*h;
        return s;
    }
};


double error_inf(const domain_lowrank4d& dom, function<double(double,double,double,double)> f) {
    Tensor4 f_full = dom.full();
    double s = 0.0;
    for(auto i : up_to<4>(f_full.r)) {
        double x = dom.ddX.a[0] + i[0]*dom.ddX.h[0];
        double y = dom.ddX.a[1] + i[1]*dom.ddX.h[1];
        double v = dom.ddV.a[0] + i[2]*dom.ddV.h[0];
        double w = dom.ddV.a[1] + i[3]*dom.ddV.h[1];

        s = max(s, abs(f_full(i)-f(x,y,v,w)));
    }

    return s;
}

struct lowrank4d {
    MatrixXd X, V, S;

    lowrank4d(int r, array<Index,4> n)
        : X(n[0]*n[1],r), V(n[2]*n[3],r), S(r,r) {}

    MatrixXd full() const {
        return X*S*V.transpose();
    }
};


struct domain4d_data {
    array<double,4> a;
    array<double,4> b;
    array<Index,4>  n;
    Index   r;
    array<double,4> h;

    domain4d_data(array<double,4> _a, array<double,4> _b,
            array<Index,4> _n, Index _r) : a(_a), b(_b), n(_n), r(_r) {
        for(Index i=0;i<4;i++)
            h[i] = (b[i]-a[i])/double(n[i]);
    }
    
    double x(const array<Index,2>& i) {
        return a[0] + i[0]*h[0];
    }
    double y(const array<Index,2>& i) {
        return a[1] + i[1]*h[1];
    }
    double v(const array<Index,2>& i) {
        return a[2] + i[0]*h[2];
    }
    double w(const array<Index,2>& i) {
        return a[3] + i[1]*h[3];
    }
    double x(const array<Index,4>& i) {
        return a[0] + i[0]*h[0];
    }
    double y(const array<Index,4>& i) {
        return a[1] + i[1]*h[1];
    }
    double v(const array<Index,4>& i) {
        return a[2] + i[2]*h[2];
    }
    double w(const array<Index,4>& i) {
        return a[3] + i[3]*h[3];
    }
    Index lin_idx_x(array<Index,2> i) {
        return i[0] + i[1]*n[0];
    }
    Index lin_idx_v(array<Index,2> i) {
        return i[0] + i[1]*n[2];
    }
    array<Index,2> mat_size_x() {
        return {n[0]*n[1],r};
    }
    array<Index,2> mat_size_v() {
        return {n[2]*n[3],r};
    }

    VectorXd coordinates_x() {
        VectorXd vec_x(n[0]);
        for(Index i=0;i<n[0];i++)
            vec_x[i] = x({i,0,0,0});
        return vec_x;
    }
    VectorXd coordinates_y() {
        VectorXd vec_y(n[1]);
        for(Index i=0;i<n[1];i++)
            vec_y[i] = y({0,i,0,0});
        return vec_y;
    }
    VectorXd coordinates_v() {
        VectorXd vec_v(n[2]);
        for(Index i=0;i<n[2];i++)
            vec_v[i] = v({0,0,i,0});
        return vec_v;
    }
    VectorXd coordinates_w() {
        VectorXd vec_w(n[3]);
        for(Index i=0;i<n[3];i++)
            vec_w[i] = w({0,0,0,i});
        return vec_w;
    }
};

// Mostly for writing tests where it must be ensured that something is initialized
// exactly as is. This routines do not provide any orthogonalization.
void lowrank4d_init_x(function<double(double,double,Index)> f, MatrixXd& out, domain4d_data dd) {
    for(Index r=0;r<dd.r;r++)
        for(auto i : range<2>(dd.n[0],dd.n[1]))
            out(dd.lin_idx_x(i),r) = f(dd.x(i), dd.y(i), r);
}

void lowrank4d_init_v(function<double(double,double,Index)> f, MatrixXd& out, domain4d_data dd) {
    for(Index r=0;r<dd.r;r++)
        for(auto i : range<2>(dd.n[2],dd.n[3]))
            out(dd.lin_idx_v(i),r) = f(dd.v(i), dd.w(i), r);
}


#pragma once

#include <generic/common.hpp>
#include <generic/netcdf.hpp>
#include <container/domain-lowrank-h2x2v.hpp>


// TODO: replace all ints by Index
template<size_t d>
array<int,d> Index_to_int(array<Index,d> idx) {
    array<int,d> ret;
    for(Index i=0;i<d;i++)
        ret[i] = idx[i];
    return ret;
}

struct domain_lowrank4d_fluid {
    domain4d_data dd;

    lowrank4d lr;
    lowrank_type type;

    domain_lowrank4d_fluid(domain4d_data _dd, lowrank_type _type)
        : dd(_dd), lr(_dd.r,_dd.n), type(_type) {
    }

    double init_x(int r, function<double(double,double,Index)> f, MatrixXd& out) {
        double l2 = 0.0;
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            out(dd.lin_idx_x(i),r) = f(dd.x(i), dd.y(i), r);
            l2 += pow(out(dd.lin_idx_x(i),r),2);
        }
        l2 = sqrt(l2);
        for(auto i : range<2>(dd.n[0],dd.n[1]))
            out(dd.lin_idx_x(i),r) /= l2;
        return l2;
    }

    double init_v(int r, function<double(double,double,Index)> f, MatrixXd& out) {
        double l2 = 0.0;
        for(auto i : range<2>(dd.n[2],dd.n[3])) {
            out(dd.lin_idx_v(i),r) = f(dd.v(i), dd.w(i), r);
            l2 += pow(out(dd.lin_idx_v(i),r),2);
        }
        l2 = sqrt(l2);
        for(auto i : range<2>(dd.n[2],dd.n[3]))
            out(dd.lin_idx_v(i),r) /= l2;
        return l2;
    }

    void init(int rank, function<double(double,double,int)> X, 
                        function<double(double,double,int)> V) {
        lr.X.setIdentity();
        lr.V.setIdentity();
        lr.S.setZero();
        for(int k=0;k<rank;k++) {
            double l1 = init_x(k, X, lr.X);
            double l2 = init_v(k, V, lr.V);
            lr.S(k,k)=l1*l2;
        }

        double hx = 1.0;
        double hv = 1.0;
        if(type == lowrank_type::normalized_integral) {
            hx = dd.h[0]*dd.h[1];
            hv = dd.h[2]*dd.h[3];
        }

        // The representation above is not yet orthogonal. This is fixed by doing two
        // QR decompositions.
        rectangular_QR qrx(lr.X);
        lr.X = qrx.Q()/sqrt(hx);

        rectangular_QR qrv(lr.V);
        lr.V = qrv.Q()/sqrt(hv);

        lr.S = sqrt(hx*hv)*qrx.R()*lr.S*qrv.R().transpose();
    }

    Tensor4 full() {
        Tensor4 out(Index_to_int(dd.n));
        MatrixXd m = lr.full();
        for(auto i : range<4>(dd.n))
            out(Index_to_int(i)) = m(dd.lin_idx_x({i[0],i[1]}), dd.lin_idx_v({i[2],i[3]}));
        return out;
    }


    void rhou(MatrixXd& rho, array<MatrixXd,2>& rhou) {
        rho.resize(dd.n[0],dd.n[1]);
        rhou[0].resize(dd.n[0],dd.n[1]);
        rhou[1].resize(dd.n[0],dd.n[1]);
        rho.setZero();
        rhou[0].setZero();
        rhou[1].setZero();

        for(Index j=0;j<dd.r;j++) {
            // compute integral over V_j
            double integral_V = 0.0;
            array<double,2> integral_Vj = {0.0, 0.0};
            for(auto l : range<2>(dd.n[2],dd.n[3])) {
                integral_V     += dd.h[2]*dd.h[3]*lr.V(dd.lin_idx_v(l),j);
                integral_Vj[0] += dd.h[2]*dd.h[3]*dd.v(l)*lr.V(dd.lin_idx_v(l),j);
                integral_Vj[1] += dd.h[2]*dd.h[3]*dd.w(l)*lr.V(dd.lin_idx_v(l),j);
            }

            // sum up the low-rank contributions
            for(auto k : range<2>(dd.n[0],dd.n[1]))
                for(Index i=0;i<dd.r;i++) {
                    rho(k[0],k[1])     += lr.X(dd.lin_idx_x(k),i)*lr.S(i,j)*integral_V;
                    rhou[0](k[0],k[1]) += lr.X(dd.lin_idx_x(k),i)*lr.S(i,j)*integral_Vj[0];
                    rhou[1](k[0],k[1]) += lr.X(dd.lin_idx_x(k),i)*lr.S(i,j)*integral_Vj[1];
                }
        }
    }
    
    // mass, momentum_x, momentum_y, energy
    array<double,4> invariants() {
        MatrixXd _rho(dd.n[0],dd.n[1]);
        array<MatrixXd,2> _rhou;
        rhou(_rho, _rhou);

        array<double,4> inv = {0.0, 0.0, 0.0, 0.0}; 
        for(auto i : range<2>(dd.n[0],dd.n[1])) {
            double rho = _rho(i[0],i[1]);
            double u0  = _rhou[0](i[0],i[1])/rho;
            double u1  = _rhou[1](i[0],i[1])/rho;
            inv[0] += dd.h[0]*dd.h[1]*rho;
            inv[1] += dd.h[0]*dd.h[1]*rho*u0;
            inv[2] += dd.h[0]*dd.h[1]*rho*u1;
            inv[3] += 0.5*dd.h[0]*dd.h[1]*(pow(u0,2)+pow(u1,2));
        }
        return inv;
    }
    
    void write_nc(NCWriter& nc) {
        MatrixXd rho(dd.n[0],dd.n[1]);
        array<MatrixXd,2> rhou;
        this->rhou(rho, rhou);

        nc.write("rho",   {dd.n[1],dd.n[0]}, rho.data(),     {"y","x"});
        nc.write("rhou0", {dd.n[1],dd.n[0]}, rhou[0].data(), {"y","x"});
        nc.write("rhou1", {dd.n[1],dd.n[0]}, rhou[1].data(), {"y","x"});
    }

    void write_lr_factors_nc(NCWriter& nc) {
        VectorXd coo_x = dd.coordinates_x();
        VectorXd coo_y = dd.coordinates_y();
        VectorXd coo_v = dd.coordinates_v();
        VectorXd coo_w = dd.coordinates_w();
        nc.write("x", {dd.n[0]}, coo_x.data(), {"x"});
        nc.write("y", {dd.n[1]}, coo_y.data(), {"y"});
        nc.write("v", {dd.n[2]}, coo_v.data(), {"v"});
        nc.write("w", {dd.n[3]}, coo_w.data(), {"w"});

        nc.write("X", {dd.r, dd.n[1], dd.n[0]}, lr.X.data(), {"r", "y", "x"});
        nc.write("S", {dd.r, dd.r},             lr.S.data(), {"r" , "r"});
        nc.write("V", {dd.r, dd.n[3], dd.n[2]}, lr.V.data(), {"r", "w", "v"});
    }

};


#pragma once

#include <generic/common.hpp>
#include <generic/netcdf.hpp>
#include <generic/qr.hpp>


struct domain1x_data {
    double a;
    double b;
    double h;
    int    n;

    domain1x_data(double _a, double _b, int _n) : a(_a), b(_b), n(_n) {
        h = (b-a)/double(n);
    }

    double x(int i) {
        return a + i*h;
    }
};

struct domain2v_data {
    array<double,2> a;
    array<double,2> b;
    array<double,2> h;
    array<int,2>    n;

    domain2v_data(array<double,2> _a, array<double,2> _b, array<int,2> _n) : a(_a), b(_b), n(_n) {
        h[0] = (b[0]-a[0])/double(n[0]);
        h[1] = (b[1]-a[1])/double(n[1]);
    }

    // computes v and w coordinate from index
    double v(const array<int,2>& i) {
        return a[0] + i[0]*h[0];
    }
    double w(const array<int,2>& i) {
        return a[1] + i[1]*h[1];
    }

    // computes linear index from two-dimensional index
    int lin_idx(array<int,2> i) {
        return i[0] + i[1]*n[0];
    }
    
};

struct lowrank1x2v {
    MatrixXd X, S, V;

    lowrank1x2v(int r, int nx, array<int,2> nv) {
        X.resize(nx, r);
        S.resize(r,r);
        V.resize(nv[0]*nv[1],r);
    }

    MatrixXd full() const {
        return X*S*V.transpose();
    }
};

void set_vec_from_func(VectorXd& vec, function<double(double)> f, double a, double h) {
    for(int i=0;i<vec.size();i++) {
        double x = a + i*h;
        vec(i) = f(x);
    }
}
 
struct domain_lowrank1x2v {
    int r;
    domain1x_data ddx;
    domain2v_data ddv;

    lowrank1x2v lr;

    domain_lowrank1x2v(int _r, domain1x_data _ddx, domain2v_data _ddv)
        : r(_r), ddx(_ddx), ddv(_ddv), lr(_r, _ddx.n, _ddv.n) {  //_ddx it is different from ddx!
        }

    void init(int rank, function<double(double,int)> X, function<double(double,double,int)> V) {
        lr.X.setIdentity();
        lr.V.setIdentity();
        lr.S.setZero();
        for(int k=0;k<rank;k++) {
            double l2_1 = set1x(lr.X, [k,X](double x) { return X(x,k); }, k);
            double l2_2 = set2v(lr.V, [k,V](double v, double w) { return V(v,w,k); }, k);
            lr.S(k,k)=l2_1*l2_2;
        }

        double hx = ddx.h;
        double hv = ddv.h[0]*ddv.h[1];

        // The representation above is not yet orthogonal. This is fixed by doing two
        // QR decompositions.
        rectangular_QR qrx(lr.X);
        lr.X = qrx.Q()/sqrt(hx);

        rectangular_QR qrv(lr.V);
        lr.V = qrv.Q()/sqrt(hv);

        lr.S = sqrt(hx*hv)*qrx.R()*lr.S*qrv.R().transpose();
    }

    void write_nc(NCWriter& nc, string name) {
        MatrixXd m_full = full();
        nc.write(name, {ddv.n[1], ddv.n[0], ddx.n}, m_full.data(), {"w", "v", "x"});
    }

    MatrixXd full() const {
        return lr.full();
    }

    double mass() {
        VectorXd int_X = integrate_x(lr.X, [](double) { return 1.0;});
        VectorXd int_V = integrate_v(lr.V, [](double,double) { return 1.0;});
        return int_X.transpose()*lr.S*int_V;
    }

    double momentum() {
        VectorXd int_X = integrate_x(lr.X, [](double x) { return 1.0;});
        VectorXd int_V = integrate_v(lr.V, [](double v, double w) { return v;});
        return int_X.transpose()*lr.S*int_V;

    }

    double kin_energy() {
        VectorXd int_X = integrate_x(lr.X, [](double x) { return 1.0;});
        VectorXd int_V = integrate_v(lr.V, [](double v, double w) { return 0.5*pow(v,2) + 0.5*pow(w,2);});
        return int_X.transpose()*lr.S*int_V;
    }

    VectorXd integrate_x(const MatrixXd& X, function<double(double)> f) {
        VectorXd out(X.cols());
        out.setZero();
        for(int k=0;k<X.cols();k++){
            for(int i=0;i<X.rows();i++) {
                double x = ddx.a + i*ddx.h;
                out(k) += ddx.h*X(i,k)*f(x);
            }}
        return out;  
    }
    
    ////////////////////////////////////////////////////////////////////
    VectorXd integrate_v(const MatrixXd& V, function<double(double,double)> f) {
        VectorXd out(V.cols());
        out.setZero();
        for(int k=0;k<V.cols();k++){  
			for(int j=0;j<ddv.n[1];j++){ //integral in w (j is the row index)
				for(int i=0;i<ddv.n[0];i++){ //integral in v (i is the column index)
					out(k) += ddv.h[0]*ddv.h[1]*V(ddv.lin_idx({i,j}),k)*f(ddv.v({i,j}),ddv.w({i,j}));
				}
			}       
		}		
        return out;  
    }
    ////////////////////////////////////////////////////////////////////
    
    double l1() {
        return abs(lr.full().array()).sum()*ddx.h*ddv.h[0]*ddv.h[1];
    }

    double l2() {
        auto full_arr = lr.full();
        return (full_arr.array()*full_arr.array()).sum()*ddx.h*ddv.h[0]*ddv.h[1];
    }

    double entropy() {
        auto full_arr = lr.full();
        return (full_arr.array()*log(abs(full_arr.array()))).sum()*ddx.h*ddv.h[0]*ddv.h[1];
    }

    private:
    // returns the l^2 norm of the vector written
    double set1x(MatrixXd& mat, function<double(double)> f, int idx=0) {
        double l2 = 0.0;
        for(int i=0;i<mat.rows();i++) {
            double x = ddx.a + i*ddx.h;
            mat(i,idx) = f(x);
            l2 += pow(mat(i,idx),2);
        }
        l2 = sqrt(l2);
        for(int i=0;i<mat.rows();i++)
            mat(i,idx) /= l2; // normalization
        return l2;
    }

    ////////////////////////////////////////////////////////////////////
    // returns the l^2 norm of the vector written
    double set2v(MatrixXd& mat, function<double(double,double)> f, int idx=0) {
        double l2 = 0.0;
        for(int j=0;j<ddv.n[1];j++) { //index on w
			for(int i=0;i<ddv.n[0];i++) { //index on v
				mat(ddv.lin_idx({i,j}),idx) = f(ddv.v({i,j}),ddv.w({i,j}));
				l2 += pow(mat(ddv.lin_idx({i,j}),idx),2);
			}
		}
		l2 = sqrt(l2);
		for(int j=0;j<mat.rows();j++) { 
			mat(j,idx) /= l2; // normalization
		}
	return l2;
	}
	////////////////////////////////////////////////////////////////////
};

////////////////////////////////////////////////////////////////////////
double electric_field_energy(array<VectorXd,2> E, double h) {
	// sum_i h*0.5*(E[0]_i^2+E[1]_i^2)
	double energy = 0.0; 
	for(int i=0;i<E[0].size();i++){
		energy += 0.5*h*(pow(E[0](i),2)+pow(E[1](i),2)); 
	}
    return energy;
}
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
double magnetic_field_energy(VectorXd B, double h) {
	// sum_i h*0.5*B_i^2
	double energy = 0.0; 
	for(int i=0;i<B.size();i++){
		energy += 0.5*h*pow(B(i),2); 
	}
    return energy;
}
////////////////////////////////////////////////////////////////////////



#pragma once

#include <generic/common.hpp>

#include <fftw3.h>

void fft(int n, const double* in, complex<double>* out) {
    fftw_plan p = fftw_plan_dft_r2c_1d(n, (double*)in, (fftw_complex*)out,
                                       FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
    fftw_execute(p);
    fftw_destroy_plan(p);
}

void inv_fft(int n, complex<double>* in, double* out) {
    fftw_plan p = fftw_plan_dft_c2r_1d(n, (fftw_complex*)in, out, FFTW_ESTIMATE);
    fftw_execute(p);
    fftw_destroy_plan(p);
}

struct fft_batch {

    fft_batch(int n, double* in, complex<double>* out) {
        p_forward = fftw_plan_dft_r2c_1d(n, (double*)in, (fftw_complex*)out,
                                         FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);

        p_backward = fftw_plan_dft_c2r_1d(n, (fftw_complex*)out, in, FFTW_ESTIMATE);
    }

    ~fft_batch() {
        fftw_destroy_plan(p_forward);
        fftw_destroy_plan(p_backward);
    }

    void forward() {
        fftw_execute(p_forward);
    }

    void backward() {
        fftw_execute(p_backward);
    }

private:
    fftw_plan p_forward, p_backward;
};


struct fft2d {
    complex<double>* uhat;

    fft2d(array<Index,2> n, double* in) {
        uhat = (complex<double>*)fftw_malloc(sizeof(complex<double>)*n[1]*(n[0]/2+1));
        p_forward = fftw_plan_dft_r2c_2d(n[1], n[0], in, (fftw_complex*)uhat,
                FFTW_ESTIMATE | FFTW_PRESERVE_INPUT | FFTW_UNALIGNED);

        p_backward = fftw_plan_dft_c2r_2d(n[1], n[0], (fftw_complex*)uhat, in,
                FFTW_ESTIMATE | FFTW_UNALIGNED);

    }

    ~fft2d() {
        fftw_free(uhat);
    }

    void forward(double* in) {
        fftw_execute_dft_r2c(p_forward, in, (fftw_complex*)uhat);
    }

    void backward(double* out) {
        fftw_execute_dft_c2r(p_backward, (fftw_complex*)uhat, out);
    }

private:
    fftw_plan p_forward, p_backward;
};



#pragma once

#include <generic/common.hpp>
#include <boost/operators.hpp>


struct Tensor4 {
    vector<double> data;
    array<int,4> r;

    Tensor4(array<int,4> _r) : r(_r) {
        data.resize(r[0]*r[1]*r[2]*r[3]);
    }

    double& operator()(const array<int,4>& i) {
        return (*this)(i[0],i[1],i[2],i[3]);
    }
    
    const double& operator()(const array<int,4>& i) const {
        return (*this)(i[0],i[1],i[2],i[3]);
    }
    
    double& operator()(int i, int j, int k, int m) {
        assert( i + r[0]*j + r[0]*r[1]*k + r[0]*r[1]*r[2]*m < (int)data.size() );
        return data[i + r[0]*j + r[0]*r[1]*k + r[0]*r[1]*r[2]*m];
    }

    const double& operator()(int i, int j, int k, int m) const {
        assert( i + r[0]*j + r[0]*r[1]*k + r[0]*r[1]*r[2]*m < (int)data.size() );
        return data[i + r[0]*j + r[0]*r[1]*k + r[0]*r[1]*r[2]*m];
    }
    
    void setZero() {
        fill(data.begin(), data.end(), 0.0);
    }
};

double error_inf(const Tensor4& t1, const Tensor4& t2) {
    assert( t1.r[0] == t2.r[0] );
    assert( t1.r[1] == t2.r[1] );
    assert( t1.r[2] == t2.r[2] );
    assert( t1.r[3] == t2.r[3] );
    double s = 0.0;
    for(auto i : up_to<4>(t1.r))
        s = max(s, abs(t1(i)-t2(i)));
    return s;
}

double norm_inf(const Tensor4& t1) {
    double s = 0.0;
    for(auto i : up_to<4>(t1.r))
        s = max(s, abs(t1(i)));
    return s;
}

struct Tensor3  :
    boost::additive1< Tensor3 ,
    boost::additive2< Tensor3 , double ,
    boost::multiplicative2< Tensor3 , double > > > {
    vector<double> data;
    array<int,3> r;

    Tensor3() { }

    void alloc(array<int,3> _r) {
        r = _r;
        data.resize(r[0]*r[1]*r[2]);
    }

    Tensor3(array<int,3> _r) {
        alloc(_r);
    }

    void setZero() {
        fill(data.begin(), data.end(), 0.0);
    }

    double& operator()(const array<int,3>& i) {
        return (*this)(i[0],i[1],i[2]);
    }

    const double& operator()(const array<int,3>& i) const {
        return (*this)(i[0],i[1],i[2]);
    }

    double& operator()(int i, int j, int k) {
        assert( i + r[0]*j + r[0]*r[1]*k < (int)data.size() );
        return data[i + r[0]*j + r[0]*r[1]*k];
    }

    const double& operator()(int i, int j, int k) const {
        assert( i + r[0]*j + r[0]*r[1]*k < (int)data.size() );
        return data[i + r[0]*j + r[0]*r[1]*k];
    }

    MatrixXd operator()(int idx) const {
        MatrixXd m(r[1],r[2]);
        for(int j=0;j<r[2];j++)
            for(int i=0;i<r[1];i++)
                m(i,j) = (*this)(idx,i,j);
        return m;
    }

    // input is a permutation. the first two indices are then grouped
    MatrixXd reshape21(array<int,3> perm) const {
        array<int,3> R = permute(r, perm);
        MatrixXd m(R[0]*R[1], R[2]);

        for(int k=0;k<r[2];k++)
            for(int j=0;j<r[1];j++)
                for(int i=0;i<r[0];i++) {
                    array<int,3> I = permute({i,j,k}, perm);
                    m(I[0] + I[1]*R[0],I[2]) = (*this)(i,j,k);
                }

        return m;
    }

    // The permutation given here has to be understood as follows:
    // The matrix indices (ab)c are arranged as abc and then the permutation
    // tells us to which these indices are mapped in the tensor.
    // For example, perm[0]=1 means that 0th index in the matrix (a in this case)
    // will be mapped to the 1st index in the tensor.
    void from_reshape21(const MatrixXd& m, array<int,3> perm) {
        array<int,3> R = permute(r, perm);
        assert(R[0]*R[1] == m.rows());
        assert(R[2] == m.cols());

        for(int k=0;k<r[2];k++)
            for(int j=0;j<r[1];j++)
                for(int i=0;i<r[0];i++) {
                    array<int,3> I = permute({i,j,k}, perm);
                    (*this)(i,j,k) = m(I[0] + I[1]*R[0], I[2]);
                }
    }

    double norm() {
        double s = 0.0;
        for(auto i : up_to<3>(r))
            s += pow((*this)(i),2);
        return sqrt(s);
    }

    Tensor3& operator+=(const Tensor3& b) {
        assert(r==b.r);
        for(auto i : up_to<3>(r))
            (*this)(i) += b(i);
        return *this;
    }

    Tensor3& operator*=(const double c) {
        for(auto i : up_to<3>(r))
            (*this)(i) *= c;
        return *this;
    }
};


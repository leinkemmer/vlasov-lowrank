#pragma once

#include <array>
using std::array;


template<int d>
struct up_to {
    array<int,d> upper_limit;
    array<int,d> current;

    up_to(array<int, d> _upper_limit) : upper_limit(_upper_limit) {
        fill(current.begin(), current.end(), 0);
    }

    up_to(std::initializer_list<int> il) {
        if(il.size() != d) {
            cout << "ERROR: initializer list in ctor of up_to must have exactly d elements" << endl;
            exit(1);
        }
        std::copy(il.begin(), il.end(), upper_limit.begin());
        fill(current.begin(), current.end(), 0);
    }
    
    template<typename... Ints> 
    up_to(Ints&&... ul) {
        static_assert(sizeof...(Ints) == d, "wrong number of arguments to range ctor.");
        upper_limit = array<int,d>({ul...});
        fill(current.begin(), current.end(), 0);
    }

    up_to& operator++() {
        for(int k=0;k<d-1;k++) {
            current[k]++;
            if(current[k] < upper_limit[k])
                return *this;
            current[k]=0;
        }
        current[d-1]++;
        return *this;
    }

    up_to begin() {
        return up_to(upper_limit);
    }

    up_to end() {
        up_to ut(upper_limit);
        fill(ut.current.begin(), ut.current.end(), 0);
        ut.current[d-1] = upper_limit[d-1];
        return ut;
    }

    array<int,d>& operator*() {
        return current;
    }

    bool operator!=(const up_to& b) {
        bool ret = true;
        for(int k=0;k<d;k++)
            ret = ret && (current[k] == b.current[k]);
        return !ret;
    }
};

array<int,3> permute(const array<int,3>& in, const array<int,3>& perm) {
    array<int,3> out;
    for(int i=0;i<3;i++)
        out[i] = in[perm[i]];
    return out;
}


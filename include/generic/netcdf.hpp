#pragma once

#include <netcdf>

struct NCWriter {
    netCDF::NcFile fs;
    vector<netCDF::NcDim> dims;

    NCWriter(string filename) : fs(filename, netCDF::NcFile::replace) {}

    void write(string name, vector<long> extension, double *data, vector<string> d_names={}) {
        vector<netCDF::NcDim> local_dims;
        for(size_t i=0;i<d_names.size();i++) {
            netCDF::NcDim dim;
            if(!dim_exists(d_names[i], extension[i], dim)) {
                dim = fs.addDim(d_names[i], extension[i]);
                dims.push_back(dim);
                local_dims.push_back(dim);
            } else
                local_dims.push_back(dim);
        }

        netCDF::NcVar data_handle = fs.addVar(name, netCDF::ncDouble, local_dims);
        data_handle.putVar(data);
    }

private:
    bool dim_exists(string name, long ext, netCDF::NcDim& dim) {
        for(size_t i=0;i<dims.size();i++) {
            if(name == dims[i].getName()) {
                if((long)dims[i].getSize() == ext) {
                    dim = dims[i];
                    return true;
                } else {
                    cout << "ERROR: dim with name " << name << " is already defined and a different size was given" << endl;
                    exit(1);
                }
            }
        }
        return false;
    }
};


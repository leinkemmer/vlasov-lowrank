#pragma once

#include <generic/common.hpp>
#include <Eigen/QR>

struct rectangular_QR {
    int n, r;
    Eigen::HouseholderQR<MatrixXd> qr;

    rectangular_QR(int _n, int _r) : n(_n), r(_r), qr(_n,_r) {}

    rectangular_QR(MatrixXd& A) : qr(A) {
        n = A.rows();
        r = A.cols();
    }

    void compute(const MatrixXd& A) {
        qr.compute(A);
    }

    MatrixXd Q() {
        // Internally Eigen stores the QR decomposition as a sequence of
        // Householder transformations. We multiply with the 'identity' to only
        // get the 'thin QR factorization'.
        // If qr.householderQ() is directly assigned to a MatrixXd object, the full matrix
        // is formed and performance suffers because of it.
        return qr.householderQ()*Eigen::MatrixXd::Identity(n, r);
    }

    MatrixXd R() {
        MatrixXd mr = qr.matrixQR().triangularView<Eigen::Upper>();
        return mr.block(0,0,r,r);
    }

};


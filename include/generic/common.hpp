#pragma once

#include <iostream>
#include <fstream>
#include <complex>
#include <cmath>
#include <array>
#include <vector>
#include <functional>
#include <algorithm>
#include <numeric>
using std::cout;
using std::endl;
using std::ofstream;
using std::complex;
using std::max;
using std::array;
using std::vector;
using std::function;
using std::fill;
using std::accumulate;
using std::abs;

#include <boost/format.hpp>

#include <Eigen/Dense>
using Eigen::MatrixXd;
using Eigen::VectorXd;

#include <generic/utility.hpp>
#include <generic/range.hpp>


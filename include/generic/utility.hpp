#pragma once

#include <iostream>
#include <array>
#include <cmath>
#include <string>
#include <sstream>
using std::cout;
using std::endl;
using std::array;
using std::string;

// This specifies the index type used for arrays and such
typedef size_t Index;


/// We overload the << operator for boost::array. This allows us to print
/// boost::array using the standard cout syntax.
template<class T, unsigned long d>
std::ostream& operator<<(std::ostream& os, const array<T,d>& a) {
    os << "("; 
    for(unsigned long i=0;i<d;i++)
        os << a[i] << (i==d-1 ? "" : ",");
    os <<  ")";
    return os;
}

/// In C++ the modulo % operator for negative integers yields a negative value.
/// Thus, the modulo operator can not be used to compute array indices. The
/// neg_modulo function handles this case separately and ensures that for
/// -n <= x <= n we obtain the correct array index (for periodic boundary
/// conditions)
int neg_modulo(int i, int n) {
    int a = i % n;
    if(a < 0)
        a += n;
    return a;
}


template<typename T>
string to_str(T v) {
    std::stringstream ss;
    ss << v;
    return ss.str();
}

/// This function facilitates the implementation of dimension independent
/// loops. The variable idx is the loop counter. To control the iteration max
/// (the upper bound of the loop indices) and start (the reset value) are
/// specified.
///
/// Example: A simple example in two dimensions which iterates over the values
/// idx=(2,2),(2,3),(3,2),(3,3).
/// array<int,2> idx = {2,2};
/// array<int,2> max = {4,4};
/// array<int,2> start = {2,2};
/// do {
///     a(idx) = ...;
/// } while(iter_next(idx,max,start);
template<Index n>
bool iter_next(array<Index,n> &idx, array<Index,n> max, array<Index,n>* start = NULL) {
    Index j;
    for(j=0;j<n;j++) {
        idx[j]++;
        if(idx[j] < max[j])
            break;
        idx[j] = start == NULL ? 0 : (*start)[j];
    }
    return j < n;
}

template<Index n>
bool iter_next_memorder(array<Index,n> &idx, array<Index,n> max, array<Index,n>* start=NULL) {
    static_assert(n % 2 == 0, "iter_next_memorder only works for indices with a size divisible by two.");

    Index j;
    for(j=0;j<n/2;j++) {
        // first do the index corresponding to o
        idx[n/2+j]++;
        if(idx[n/2+j] < max[n/2+j])
            break;
        idx[n/2+j] = start == NULL ? 0 : (*start)[n/2+j];

        // then do index corresponding to N
        idx[j]++;
        if(idx[j] < max[j])
            break;
        idx[j] = start == NULL ? 0 : (*start)[j];
    }

    return j < n/2;
}


// product of an array of numbers
template<class T, Index d>
T prod(array<T,d> a) {
    T p=1;
    for(Index i=0;i<d;i++) p*=a[i];
    return p;
}

/// This function distributes the number of cores available to the d dimensions
/// of the problem. At the moment we assume that the number of cores can be
/// written as 2^k or as k^d for some integer k.
template<Index d>
void distribute(int total_cores, array<int,d>& distribution) {
    // try to decompose as n^d
    int root=round(pow(total_cores,1./double(d)));
    for(Index i=0;i<d;i++)
        distribution[i]=root;
    if(prod(distribution)==total_cores)
        return;
    // if that fails we try to decompose as 2^n
    int remaining_cores=total_cores;
    for(Index i=0;i<d;i++) {
        distribution[i]=pow(2,int(round(log(double(remaining_cores))/log(2)))/(d-i));
        remaining_cores/=distribution[i];
    }
    if(prod(distribution)!=total_cores) {
        cout << "ERROR: please specify a number of nodes which can be decomposed as 2^n or n^d." << endl;
        exit(1);
    }
}

/// This functions returns a file name (based on name) that is different for
/// each MPI process and reflects the d-dimensional index of the slice of data
/// the process is working on.
template<Index d>
string filename(string name, array<int,d> id) {
    std::stringstream ss; ss << name << "-";
    for(Index i=0;i<d;i++) ss << id[i] << ((i!=d-1)?",":".");
    ss << "data";
    return ss.str();
}

/// This function removes the entry dim and d+dim from the input array (in) and
/// stores the remaining entries in the output array (out). This is a helper
/// function that is used to prepare boundary data in the Vlasov-Poisson
/// solver.
template<Index d>
void slice(Index dim, array<Index,2*d>& in, array<Index,2*d-2>& out) {
    int j=0;
    for(Index i=0;i<d;i++) {
        if(i!=dim) {
            out[j]=in[i];
            out[j+(d-1)]=in[i+d];
            j++;
        }
    }
}

/// This function computed the linear offset given a d-dimensional index.
template<class T, Index d>
T offset(array<T,d>& idx, array<T,d>& extension) {
    T global_offset=0; T prod=1;
    for(Index i=0;i<d;i++) {
        global_offset+=idx[i]*prod;
        prod*=extension[i];
    }
    return global_offset;
}


template<int d>
struct range {
    array<Index,d> upper_limit;
    array<Index,d> current;

    range(array<Index, d> _upper_limit) : upper_limit(_upper_limit) {
        fill(current.begin(), current.end(), 0);
    }
    
    template<typename... Ints> 
    range(Ints&&... ul) {
        static_assert(sizeof...(Ints) == d, "wrong number of arguments to range ctor.");
        upper_limit = array<Index,d>({ul...});
        fill(current.begin(), current.end(), 0);
    }
    
    range& operator++() {
        for(Index k=0;k<d-1;k++) {
            current[k]++;
            if(current[k] < upper_limit[k])
                return *this;
            current[k]=0;
        }
        current[d-1]++;
        return *this;
    }

    range begin() {
        return range(upper_limit);
    }

    range end() {
        range ut(upper_limit);
        fill(ut.current.begin(), ut.current.end(), 0);
        ut.current[d-1] = upper_limit[d-1];
        return ut;
    }

    array<Index,d>& operator*() {
        return current;
    }

    bool operator!=(const range& b) {
        bool ret = true;
        for(Index k=0;k<d;k++)
            ret = ret && (current[k] == b.current[k]);
        return !ret;
    }
};

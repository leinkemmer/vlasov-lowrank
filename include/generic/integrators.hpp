#pragma once

#include <generic/common.hpp>
#include <boost/numeric/odeint.hpp>
#include <cvode/cvode.h>
#include <nvector/nvector_serial.h>
#include <cvode/cvode_spgmr.h>

bool _integrate_single_euler_step = false;

void integrate(function<void(const MatrixXd&,MatrixXd&)> rhs, const MatrixXd& in, MatrixXd& out, double t, int substeps=40) {

    if(_integrate_single_euler_step) {
        rhs(in,out);
        out = in + t*out;
    /*
    double tau = t/double(substeps);
    MatrixXd f(out);
    out = in;
    for(int i=0;i<substeps;i++) {
        rhs(out,f);
        out += tau*f;
    }
    */
    } else {

        using namespace boost::numeric;
        using namespace boost::numeric::odeint;

        // this is still with a constant step size
        runge_kutta_dopri5<MatrixXd,double,MatrixXd,double,vector_space_algebra> stepper;
        //modified_midpoint<MatrixXd,double,MatrixXd,double,vector_space_algebra> stepper;

        out = in; // integrate_adaptive uses the same variable as in and output
        integrate_adaptive(stepper,
                [rhs](const MatrixXd& x, MatrixXd& dxdt, double t) {
                rhs(x, dxdt);
                }, out, 0.0, t, t/double(substeps));
    }
}

void ptr_to_nvec(const double* p, N_Vector u) {
    double* vec_data = NV_DATA_S(u);
    for (int i = 0; i < NV_LENGTH_S(u); i++) {
        vec_data[i] = p[i];
    }
}

void nvec_to_ptr(N_Vector u, double* p) {
    double* vec_data = NV_DATA_S(u);
    for (int i = 0; i < NV_LENGTH_S(u); i++) {
        p[i] = vec_data[i];
    }
}

struct UserData {
    Index n1, n2;
    function<void(const MatrixXd&, MatrixXd&)> rhs;

    UserData(Index _n1, Index _n2, function<void(const MatrixXd&, MatrixXd&)> _rhs)
        : n1(_n1), n2(_n2), rhs(_rhs) { }
};

int cvode_rhs(realtype t, N_Vector u, N_Vector udot, void *user_data) {
    UserData* ud = ((UserData*)user_data);
    
    MatrixXd in(ud->n1,ud->n2);
    MatrixXd out(ud->n1,ud->n2);

    nvec_to_ptr(u, &in(0,0));
    ud->rhs(in, out);
    ptr_to_nvec(&out(0,0), udot);

    return 0;
}


void check_flag(void *flagvalue, const char *funcname, int opt) {
    int *errflag;

    // Check if SUNDIALS function returned NULL pointer - no memory allocated
    if(opt == 0 && flagvalue == NULL) {
        fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed - returned NULL pointer\n\n",
                funcname);
        exit(1);
    }
    // Check if flag < 0
    else if (opt == 1) {
        errflag = (int *) flagvalue;
        if (*errflag < 0) {
            fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed with flag = %d\n\n",
                    funcname, *errflag);
            exit(1);
        }
    }
    // Check if function returned NULL pointer - no memory allocated
    else if (opt == 2 && flagvalue == NULL) {
        fprintf(stderr, "\nMEMORY_ERROR: %s() failed - returned NULL pointer\n\n",
                funcname);
        exit(1);
    }
}

void cvode_integrate(double T, const double *u0, double *u1, UserData* ud, double tolre, double tolabs) {
    void* cvode_mem;
    N_Vector u;

    u = N_VNew_Serial(ud->n1*ud->n2);
    check_flag((void*)u, "N_VNew_Serial", 0);

    cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON);
    check_flag((void*)cvode_mem, "CVodeCreate", 0);

    ptr_to_nvec(u0, u);

    int flag = CVodeInit(cvode_mem, cvode_rhs, 0.0, u);
    check_flag(&flag, "CVodeInit", 1);

    flag = CVodeSStolerances(cvode_mem, tolre, tolabs);
    check_flag(&flag, "CVodeSStolerances", 1);

    flag = CVodeSetUserData(cvode_mem, ud);
    check_flag(&flag, "CVodeSetUserData", 1);

    flag = CVodeSetMaxNumSteps(cvode_mem, 10000);
    check_flag(&flag, "CVodeSetMaxNumSteps", 1);

    flag = CVSpgmr(cvode_mem, PREC_NONE, 500);
    check_flag(&flag, "CVSpgmr", 1);

    flag = CVSpilsSetGSType(cvode_mem, MODIFIED_GS);
    check_flag(&flag, "CVSpilsSetGSType", 1);

    double t = 0.0;
    flag = CVode(cvode_mem, T, u, &t, CV_NORMAL);
    check_flag(&flag, "CVode", 1);
    long int nst;
    flag = CVodeGetNumSteps(cvode_mem, &nst);
    check_flag(&flag, "CVodeGetNumSteps", 1);

    nvec_to_ptr(u, u1);

    N_VDestroy_Serial(u);   
    CVodeFree(&cvode_mem); 
}


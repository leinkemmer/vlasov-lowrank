# - Find CVODE

if (CVODE_INCLUDES)
  # Already in cache, be silent
  set (CVODE_FIND_QUIETLY TRUE)
endif (CVODE_INCLUDES)

find_path (CVODE_INCLUDES cvode/cvode.h
    PATHS "${CVODE_DIR}/include")

find_library (CVODE_LIBRARIES NAMES sundials_cvode
    PATHS "/usr/local/include"
    PATHS "${CVODE_DIR}/lib")
find_library (CVODE_LIBRARIES_NVEC NAMES sundials_nvecserial
    PATHS "/usr/local/lib"
    PATHS "${CVODE_DIR}/lib")

# handle the QUIETLY and REQUIRED arguments and set CVODE_FOUND to TRUE if
# all listed variables are TRUE
include (FindPackageHandleStandardArgs)
find_package_handle_standard_args (CVODE DEFAULT_MSG CVODE_LIBRARIES CVODE_INCLUDES CVODE_LIBRARIES_NVEC)

mark_as_advanced (CVODE_LIBRARIES CVODE_INCLUDES CVODE_LIBRARIES_NVEC)

